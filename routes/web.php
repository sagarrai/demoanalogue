<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::domain('analogueinc.com.np')->group(function () {
    Route::get('/', 'HomeController@index' )->name('home');
});

Route::domain('it.analogueinc.com.np')->group(function () {
    Route::get('/', 'HomeController@it' )->name('it');
});

Route::domain('ecommerce.analogueinc.com.np')->group(function () {
    Route::get('/', 'HomeController@ecommerce' )->name('ecommerce');
});

Route::domain('computers.analogueinc.com.np')->group(function () {
    Route::get('/', 'HomeController@computer' )->name('computers');
});

Route::domain('bm.analogueinc.com.np')->group(function () {
    Route::get('/','HomeController@bm')->name('bm');
});

Route::domain('press.analogueinc.com.np')->group(function () {
    Route::get('/','HomeController@press')->name('press');
});

Route::domain('surveillance.analogueinc.com.np')->group(function () {
    Route::get('/','HomeController@surveillance')->name('surveillance');
}); 


Route::get('/','HomeController@index')->name('home');
Route::get('bm','HomeController@bm')->name('bm');
Route::get('it','HomeController@it')->name('it');
Route::get('ecommerce','HomeController@ecommerce')->name('ecommerce');
Route::get('computer','HomeController@computer')->name('computer');
Route::get('surveillance','HomeController@surveillance')->name('surveillance');
Route::get('press','HomeController@press')->name('press');
Route::get('press_tailor_made','HomeController@press_tailor_made')->name('press_tailor_made');
Route::get('tailorMade','HomeController@tailorMade')->name('press');
Route::get('webinar','HomeController@webinar')->name('press');


Route::get('/contact','HomeController@contact')->name('contact');
Route::get('/careers','HomeController@careers')->name('careers');
Route::get('/career_details/{title}','HomeController@career_details')->name('career_details');
Route::get('/career_apply/{slug}','HomeController@career_apply')->name('career_apply');
Route::get('/blogs','HomeController@blogs')->name('blogs');
Route::get('/blog_details/{id}','HomeController@blog_details')->name('blog_details');
Route::get('/about','HomeController@about')->name('about');
Route::get('/services','HomeController@services')->name('services');
Route::get('branding_package','HomeController@branding_package')->name('branding_package');
Route::get('social_package','HomeController@social_package')->name('social_package');
Route::get('email_package','HomeController@email_package')->name('email_package');

Route::get('tailorMade','HomeController@tailorMade');
Route::get('webinar','HomeController@webinar')->name('webinar');


//Route::get('/service_com','HomeController@service_com')->name('service_com');
//Route::get('/service_brand','HomeController@service_brand')->name('service_brand');
//Route::get('/service_it','HomeController@service_it')->name('service_it');
//Route::get('/service_surv','HomeController@service_surv')->name('service_surv');
//Route::get('/intern_details/{id}','HomeController@intern_details')->name('intern_details');
//Route::get('/apply_cv/{slug}','HomeController@apply_cv')->name('apply_cv');


Auth::routes();

//Admin route
Route::prefix('admin')->name('admin.')->group(function(){
    Route::get('dashboard','Admin\DashboardController@index')->name('dashboard');

    Route::get('post/allposts','Admin\PostController@index')->name('post.index');
    Route::get('post','Admin\PostController@create')->name('post.store');
    Route::get('post/edit/{id}','Admin\PostController@edit')->name('post.edit');

    Route::get('user/allusers','Admin\UserController@index')->name('user.index');
    Route::get('user','Admin\UserController@create')->name('user.store');

    Route::get('postcategory/allpostcategory','Admin\PostCategoryController@index')->name('postcategory.index');
    Route::get('postcategory','Admin\PostCategoryController@create')->name('postcategory.create');
    Route::get('postcategory/edit/{id}','Admin\PostCategoryController@edit')->name('postcategory.edit');

    Route::get('office/alloffice','Admin\OfficeController@index')->name('office.index');
    Route::get('office','Admin\OfficeController@create')->name('office.store');

    Route::get('contact/allcontact','Admin\ContactController@index')->name('contact.index');
    Route::get('contact','Admin\ContactController@create')->name('contact.store');
    Route::get('contact/edit/{id}','Admin\ContactController@edit')->name('contact.edit');

    Route::get('test/alltest','Admin\TestController@index')->name('test.index');
    Route::get('test','Admin\TestController@create')->name('test.store');

    Route::get("career","Admin\CareerController@index")->name("career.index");
    Route::get("career/{id}","Admin\CareerController@show")->name("career.show");

    Route::get("officeinfo/allofficeinfo","Admin\OfficeInfoController@index")->name("officeinfo.index");
    Route::get('officeinfo','Admin\OfficeInfoController@store')->name('officeinfo.store');

    Route::get("module/allmodule","Admin\ModuleController@index")->name("module.index");
    Route::get('module','Admin\ModuleController@store')->name('module.store');

    Route::get("package/allpackage","Admin\PackageController@index")->name("package.index");
    Route::get('package','Admin\PackageController@store')->name('package.store');

    Route::get("userpackage/alluserpackage","Admin\UserPackageController@index")->name("userpackage.index");
    Route::get('userpackage','Admin\UserPackageController@store')->name('userpackage.store');

    Route::get("consultancy/allconsultancy","Admin\ConsultancyController@index")->name("consultancy.index");

    Route::get("businessstationary/allbusinessstationary","Admin\BusinessStationaryController@index")->name("businessstationary.index");
    Route::get("businessstationary","Admin\BusinessStationaryController@store")->name("businessstationary.store");

    Route::get("merchandisestationary/allmerchandisestationary","Admin\MerchandiseStationaryController@index")->name("merchandisestationary.index");
    Route::get("merchandisestationary","Admin\MerchandiseStationaryController@store")->name("merchandisestationary.store");


});

Route::prefix('admin')->group(function(){
    Auth::routes();
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
