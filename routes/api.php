<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('allusers', 'API\UserController@allUsers');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
    Route::apiResource('office', 'API\OfficeController');
    Route::apiResource('userdetail', 'API\UserdetailController');
    Route::apiResource('post', 'API\PostController');
    Route::apiResource('userdetail', 'API\UserController');
    Route::apiResource('post-category', 'API\PostcategoryController');
    Route::post('roles', 'API\UserController@getRoles');
    Route::apiResource('officeinfo', 'API\OfficeInfoController');
    Route::apiResource('module', 'API\ModuleController');
    Route::post('module/create', 'API\ModuleController@store');
    Route::apiResource('package', 'API\PackageController');
    Route::post('package/create', 'API\PackageController@store');
    Route::apiResource('userpackage', 'API\UserPackageController');
    Route::apiResource('consultancy', 'API\ConsultancyController');
    Route::apiResource('contact', 'API\ContactController');
    Route::apiResource('career','API\CareerController');
    Route::apiResource('businessstationary','API\BusinessStationaryController');
    Route::apiResource('merchandisestationary','API\MerchandiseStationaryController');
});

Route::post('contact/create', 'API\ContactController@store');
Route::post('career/create','API\CareerController@store');
Route::post('userpackage/create', 'API\UserPackageController@store');
Route::post('consultancy/create', 'API\ConsultancyController@store');


