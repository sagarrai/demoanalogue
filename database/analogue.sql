-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 02, 2020 at 08:09 AM
-- Server version: 5.7.30
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `analogue_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apply_for` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expected_salary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apply_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cv_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_letter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `full_name`, `phone_number`, `email_address`, `address_line1`, `apply_for`, `expected_salary`, `apply_date`, `cv_file`, `cover_letter`, `created_at`, `updated_at`) VALUES
(1, 'MIniyan', '783273287`', 'parajuliminiyan@gmail.com', 'hjsddjshd', NULL, '32783278', '2020-05-17 07:05', 'parajuliminiyan@gmail.com1641498604.jpg', 'parajuliminiyan@gmail.com18370583.php', '2020-05-17 06:16:45', '2020-05-17 06:16:45'),
(2, 'MIniyan', '783273287`', 'parajuliminiyan@gmail.com', 'hjsddjshd', NULL, '32783278', '2020-05-17 09:05', 'parajuliminiyan@gmail.com1547331658.php', 'parajuliminiyan@gmail.com815241425.', '2020-05-17 08:40:08', '2020-05-17 08:40:08'),
(3, 'MIniyan', 'w3432423', 'parajuliminiyan@gmail.com', 'hjsddjshd', NULL, '32783278', '2020-05-17 09:05', 'parajuliminiyan@gmail.com964076715.php', 'parajuliminiyan@gmail.com849410926.php', '2020-05-17 08:43:51', '2020-05-17 08:43:51'),
(4, 'MIniyan', '783273287`', 'parajuliminiyan@gmail.com', 'hjsddjshd', NULL, '32783278', '2020-05-17 09:05', 'parajuliminiyan@gmail.com790152298.php', 'parajuliminiyan@gmail.com1897090127.php', '2020-05-17 08:46:37', '2020-05-17 08:46:37'),
(5, 'MIniyan', 'w3432423', 'parajuliminiyan@gmail.com', 'hjsddjshd', NULL, '32783278', '2020-05-17 09:05', 'parajuliminiyan@gmail.com1134869371.php', 'parajuliminiyan@gmail.com134721682.php', '2020-05-17 08:48:21', '2020-05-17 08:48:21'),
(6, 'MIniyan', '783273287`', 'parajuliminiyan@gmail.com', 'hjsddjshd', NULL, '32783278', '2020-05-17 09:05', 'parajuliminiyan@gmail.com373942283.php', 'parajuliminiyan@gmail.com1487568592.php', '2020-05-17 08:49:50', '2020-05-17 08:49:50'),
(7, 'Pratyush Nepal', '9843500983', 'pratyushnepal519@gmail.com', 'Tangal', NULL, '10000', '2020-05-17 16:05', 'pratyushnepal519@gmail.com1586044770.pdf', 'pratyushnepal519@gmail.com2008420988.pdf', '2020-05-17 15:51:45', '2020-05-17 15:51:45'),
(8, 'Ganga magar', '9849591842', 'ganga.sinjalimagar@gmail.com', 'khumaltar, lalitpur', NULL, 'above 45k', '2020-05-27 01:05', 'ganga.sinjalimagar@gmail.com967815687.pdf', 'ganga.sinjalimagar@gmail.com1653767087.pdf', '2020-05-27 00:23:36', '2020-05-27 00:23:36'),
(9, 'Dibas Pratap Basnet', '9808316032', 'dibaspratap@gmail.com', 'Baluwatar, Kathmandu, Nepal', NULL, '35,000', '2020-05-28 08:05', 'dibaspratap@gmail.com1567558673.pdf', 'dibaspratap@gmail.com1668612384.docx', '2020-05-28 07:33:38', '2020-05-28 07:33:38'),
(10, 'Suraj Timilsina', '9816117399', 'surajtimilsina61@gmail.com', 'Pokhara 14,Majheripatan', NULL, '45000/month', '2020-05-28 14:05', 'surajtimilsina61@gmail.com479035011.pdf', 'surajtimilsina61@gmail.com288252715.pdf', '2020-05-28 13:21:32', '2020-05-28 13:21:32'),
(11, 'suresh Tamang', '9849501483', 'ghisingsures@gmail.com', 'Balkot', NULL, '30000', '2020-05-30 00:05', 'ghisingsures@gmail.com640491130.pdf', 'ghisingsures@gmail.com518083558.pdf', '2020-05-29 23:35:43', '2020-05-29 23:35:43'),
(12, 'test', '123123123', 'test@gmail.com', 'test', NULL, '1231312', '2020-06-01 12:06', 'test@gmail.com628969045.JPG', 'test@gmail.com2042847416.JPG', '2020-06-01 11:08:31', '2020-06-01 11:08:31'),
(13, 'Tshering Lama', '9843588734', 'contact@lamatshering.com.np', 'Hattidanda-9, Kageshwori Manohara', NULL, '80000', '2020-06-02 06:06', 'contact@lamatshering.com.np1965322174.pdf', 'contact@lamatshering.com.np1092912524.pdf', '2020-06-02 05:31:13', '2020-06-02 05:31:13');

-- --------------------------------------------------------

--
-- Table structure for table `consultancies`
--

CREATE TABLE `consultancies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `objective` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` bigint(20) DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `full_name`, `phone`, `email`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 'bibek chaudhary', 9845485841, 'bibek@bibek.com', 'demo', 'test', '2020-05-17 06:10:26', '2020-05-17 06:10:26'),
(2, 'bibek chaudhary', 9845485841, 'bibek@bibek.com', 'demo', 'test', '2020-05-17 06:10:32', '2020-05-17 06:10:32'),
(3, 'bibek', 9845485841, 'bibek@htd.com', 'Demo', 'test', '2020-05-17 10:04:27', '2020-05-17 10:04:27'),
(4, 'bibek', 9845485841, 'bibek@htd.com', 'Demo', 'test', '2020-05-17 10:04:30', '2020-05-17 10:04:30'),
(5, 'MIniyan', 9845700611, 'parajuliminiyan@gmail.com', 'sfsddsd', 'sdfsdfsdsd', '2020-05-17 10:07:40', '2020-05-17 10:07:40'),
(6, 'MIniyan', 9845700611, 'parajuliminiyan@gmail.com', 'sfsddsd', 'hjhjhhghhhhj', '2020-05-17 10:09:44', '2020-05-17 10:09:44'),
(7, 'demo', 9845485841, 'demo@demo.com', 'demo', 'demo', '2020-05-17 10:10:30', '2020-05-17 10:10:30'),
(8, 'demo', 9876543210, 'demo@demo.com', 'demo', 'demo', '2020-05-17 10:12:28', '2020-05-17 10:12:28'),
(9, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test', 'test', '2020-05-28 09:15:59', '2020-05-28 09:15:59'),
(10, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'testing', 'This is a test message for email', '2020-05-29 08:34:20', '2020-05-29 08:34:20'),
(11, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test 1test', 'This is a test message', '2020-05-29 08:57:50', '2020-05-29 08:57:50'),
(12, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test 1', 'test 1', '2020-05-31 05:17:14', '2020-05-31 05:17:14'),
(13, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test 1', 'test 1', '2020-05-31 05:17:31', '2020-05-31 05:17:31'),
(14, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test', 'test', '2020-05-31 05:21:02', '2020-05-31 05:21:02'),
(15, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'testeststseta', 'astasdfasdfasdfasdf', '2020-05-31 05:30:19', '2020-05-31 05:30:19'),
(16, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'tests', 'asfasdfasdf', '2020-05-31 05:32:46', '2020-05-31 05:32:46'),
(17, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'asdfasdfaaas', 'dfasdfadsf', '2020-05-31 05:33:51', '2020-05-31 05:33:51'),
(18, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test', 'test', '2020-05-31 05:38:13', '2020-05-31 05:38:13'),
(19, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'testeststseta', 'asdfasdf', '2020-05-31 05:50:46', '2020-05-31 05:50:46'),
(20, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'testing', 'test', '2020-05-31 05:52:15', '2020-05-31 05:52:15'),
(21, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test 1test', 'testasdf', '2020-05-31 05:53:44', '2020-05-31 05:53:44'),
(22, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'test', 'atsadsf', '2020-05-31 06:06:25', '2020-05-31 06:06:25'),
(23, 'Sandesh Lohani', 9823421098, 'sandeshninbox@gmail.com', 'sdflasjdlk', 'kldasjfl', '2020-05-31 06:07:15', '2020-05-31 06:07:15'),
(24, 'Sandesh Lohani', 9823421098, 'sandeshninbox@gmail.com', 'sdflasjdlk', 'kldasjfl', '2020-05-31 06:08:53', '2020-05-31 06:08:53'),
(25, 'Sandesh Lohani', 9823421098, 'sandeshninbox@gmail.com', 'sdflasjdlk', 'kldasjfl', '2020-05-31 06:10:18', '2020-05-31 06:10:18'),
(26, 'djkalsjd', 78976887, 'sandeshninbox@gmail.com', 'dalkfj', 'alkdfjkl', '2020-05-31 06:11:12', '2020-05-31 06:11:12'),
(27, 'lkajsdlka', 324234234, 'sandeshninbox@gmail.com', 'lkajfla', 'lkjdsflkajl', '2020-05-31 06:15:11', '2020-05-31 06:15:11'),
(28, 'lkajsdlka', 324234234, 'sandeshninbox@gmail.com', 'lkajfla', 'lkjdsflkajl', '2020-05-31 06:15:20', '2020-05-31 06:15:20'),
(29, 'Sandesh Lohani', 9809809908, 'sandeshninbox@gmail.com', 'Hello', 'hello', '2020-05-31 06:19:26', '2020-05-31 06:19:26'),
(30, 'Sandesh Lohani', 9809809908, 'sandeshninbox@gmail.com', 'Hello', 'hello', '2020-05-31 06:19:49', '2020-05-31 06:19:49'),
(31, 'testing', 990809342, 'sandeshninbox@gmail.com', 'testing', 'testing', '2020-05-31 06:20:56', '2020-05-31 06:20:56'),
(32, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'trest', '5wss', '2020-06-01 04:33:01', '2020-06-01 04:33:01'),
(33, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'testeststseta', 'hgjhvkj', '2020-06-01 04:34:32', '2020-06-01 04:34:32'),
(34, 'Sagar Rai', 9816308014, 'sagar.raiii31@gmail.com', 'testing', 'fxzxcvbncv', '2020-06-01 04:35:36', '2020-06-01 04:35:36'),
(35, 'Sandesh Lohani', 890080980, 'sandesh.analogue51@gmail.com', 'testing', 'testing', '2020-06-02 05:15:09', '2020-06-02 05:15:09'),
(36, 'Sandesh Lohani', 234124234, 'sandeshninbox@gmail.com', 'testing', 'testing', '2020-06-02 05:16:30', '2020-06-02 05:16:30'),
(37, 'Sandesh Lohani', 2342342, 'sandeshninbox@gmail.com', 'test', 'test', '2020-06-02 05:42:02', '2020-06-02 05:42:02'),
(38, 'Sandesh Lohani', 98080989, 'sandeshninbox@gmail.com', 'test', 'test', '2020-06-02 05:53:24', '2020-06-02 05:53:24'),
(39, 'Sandesh Lohani', 9843526424, 'sandesh.analogue51@gmail.com', 'Testing', 'Hi this is testing message.', '2020-06-02 05:58:39', '2020-06-02 05:58:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2020_02_03_045246_create_offices_table', 1),
(9, '2020_02_06_060142_create_userdetails_table', 1),
(10, '2020_02_06_081815_create_posts_table', 1),
(11, '2020_02_13_110233_create_postcategories', 1),
(12, '2020_02_14_063502_create_tests_table', 1),
(13, '2020_03_02_043405_create_contacts_table', 1),
(14, '2020_03_27_082346_create_careers_table', 1),
(15, '2020_04_02_065417_update_table_posts_add_post_type', 1),
(16, '2020_04_09_072503_create_office_infos_table', 1),
(17, '2020_04_16_084012_create_permission_tables', 1),
(18, '2020_04_28_071043_create_modules_table', 1),
(19, '2020_04_29_104358_create_packages_table', 1),
(20, '2020_04_30_050435_create_user_packages_table', 1),
(21, '2020_05_04_113826_create_consultancies_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('011fa1cb789c2f9644a032805e9c30bdd152780ace4fc8d0fd901e1e1805a8c92a1663a4d1f12483', 1, 3, 'MyApp', '[]', 0, '2020-06-01 10:00:01', '2020-06-01 10:00:01', '2021-06-01 11:00:01'),
('020fd896423f0819871fe4d36e1207f4a40e55d96c9bc1a861dcbfb64dccd200f97f1af5112e79a2', 1, 3, 'MyApp', '[]', 0, '2020-05-22 07:49:55', '2020-05-22 07:49:55', '2021-05-22 08:49:55'),
('0968b47d52bfcdb0bd0da2fa90316df9449554085b5d1bd4a24e01aa904fdb85af79525f98499591', 1, 3, 'MyApp', '[]', 0, '2020-06-02 04:37:35', '2020-06-02 04:37:35', '2021-06-02 05:37:35'),
('0c6191c2f8a6d51d3b43dc0ab648745176783f150b1ecbd1ec72e54411dfa4a721327c61df2a6fb5', 1, 3, 'MyApp', '[]', 0, '2020-05-17 05:52:38', '2020-05-17 05:52:38', '2021-05-17 06:52:38'),
('0d21f84702100096b090b98a4b7749b77580a0a0d0fdbe3953a17b43565d28fa96132c46bef3b8d5', 1, 3, 'MyApp', '[]', 0, '2020-05-24 03:37:18', '2020-05-24 03:37:18', '2021-05-24 04:37:18'),
('16e1d2071524431ee865b96704889a9ceb40bb5ff3de0ce5baaa818d5f903bcddaea5d775a9ec152', 1, 3, 'MyApp', '[]', 0, '2020-05-24 06:47:22', '2020-05-24 06:47:22', '2021-05-24 07:47:22'),
('1bb3c9d832e9f0c8b20dcc7a4e0ee4db82cf68e67c72114fb165c48d7eb38c52b814f4119e0ade10', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:20:07', '2020-06-01 09:20:07', '2021-06-01 10:20:07'),
('2939950531442f2b42a758ddeb3c18e85dc543e24edeb52b85848c6e5af70c32ead65331a354e7a0', 1, 3, 'MyApp', '[]', 0, '2020-05-24 12:41:18', '2020-05-24 12:41:18', '2021-05-24 13:41:18'),
('331eb9390c415baf7133374c69388bbd6a5ff5ef3db1303ee62638c8bbb9f826b97bdcc0cf60288b', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:18:14', '2020-06-01 09:18:14', '2021-06-01 10:18:14'),
('3b51cce77a4e8c5c1d1035054ee382e5d120db5ada7b24b77dc7b1005d8416829a96021844740d47', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:44:53', '2020-06-01 09:44:53', '2021-06-01 10:44:53'),
('5564622c17ee39a68f7288925043512a8fafa9183240ef8441a47939066d7cc5e9cc96550de1e5b7', 1, 3, 'MyApp', '[]', 0, '2020-06-01 10:09:04', '2020-06-01 10:09:04', '2021-06-01 11:09:04'),
('5741ff3ef5f25baa764c7066e109592dfea93a5c7ddd1e78e3a355d824edb295b8b4fb6f851e7ddd', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:51:38', '2020-06-01 09:51:38', '2021-06-01 10:51:38'),
('5bafde852f29015a774419cc50a087c176f65ca33083bef03bd12b2ee473d4d575267796330456e1', 1, 3, 'MyApp', '[]', 0, '2020-06-01 10:14:02', '2020-06-01 10:14:02', '2021-06-01 11:14:02'),
('6199165865771b728affe9c0e881a225b6b9921c25f250dc56f7c8357ada6c55b99a9ec591365fe2', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:48:04', '2020-06-01 09:48:04', '2021-06-01 10:48:04'),
('6d6896b2df0d3edf225097824a822c8b5b4065411ffed3aa8741c808d6db7aed60b133fc9e868973', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:15:50', '2020-06-01 09:15:50', '2021-06-01 10:15:50'),
('6f3aa284bd07b1a468f1601007df2e9a2989a5c1384888f5308a4dc4e747a66dab0cb99ec658ceb5', 1, 3, 'MyApp', '[]', 0, '2020-06-01 10:20:54', '2020-06-01 10:20:54', '2021-06-01 11:20:54'),
('6f977f44ebd8ed644529867590f10841eb5d8b5c6435e130dbc1828e977e13b8fc846435cae79d77', 1, 3, 'MyApp', '[]', 0, '2020-05-26 07:14:08', '2020-05-26 07:14:08', '2021-05-26 08:14:08'),
('75ae5673d87e92b8adcb6e932310a59dac6d0ff5bba32668aa426b8eab7bff51495bc5ea2b4bd967', 1, 3, 'MyApp', '[]', 0, '2020-05-29 04:39:38', '2020-05-29 04:39:38', '2021-05-29 05:39:38'),
('7860c08f996f07162ad6bd22b90d4ef35e834055b09778b00bf2b2f992c0f2d7e35e82f50bbc844f', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:54:31', '2020-06-01 09:54:31', '2021-06-01 10:54:31'),
('7ade59751f8d8b03982d150836dbecfe5572e76282f3715cd3ff68ba577ce8ee91ab8efa8779a681', 1, 3, 'MyApp', '[]', 0, '2020-05-17 09:42:52', '2020-05-17 09:42:52', '2021-05-17 10:42:52'),
('81db75c798fea8c3a810020bd0c879d4bd4fb624dcb2169e6f57451f1cac4d2f72eefac97757c034', 1, 3, 'MyApp', '[]', 0, '2020-06-01 10:36:56', '2020-06-01 10:36:56', '2021-06-01 11:36:56'),
('8209e688329af3a170437da4ce17b8c1ae2c05355ba6eb60122482210575b378282a468736b9dc96', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:06:06', '2020-06-01 09:06:06', '2021-06-01 10:06:06'),
('843a83f2e7349f661012fcc1d8baac90f686ccdd1b739df20dec6cf73fc6ad6e9b7f7343de535e6b', 1, 3, 'MyApp', '[]', 0, '2020-06-01 10:21:31', '2020-06-01 10:21:31', '2021-06-01 11:21:31'),
('8f443c7a204984dc585edd25597b9012606cbded899ea4cf0dad495a96589f93b144cfecd0bf4d0d', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:03:44', '2020-06-01 09:03:44', '2021-06-01 10:03:44'),
('8f9ed5106b6622688b3ba213cce75d518c8aea8b934afd73a1935586b152b0ac87627f4b480efb12', 1, 3, 'MyApp', '[]', 0, '2020-05-24 05:43:11', '2020-05-24 05:43:11', '2021-05-24 06:43:11'),
('905a02e5cfbeec7ec9f55055900d0f004cc35b89e6ffbe358ea43cc440241377683a43b4faba458e', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:45:50', '2020-06-01 09:45:50', '2021-06-01 10:45:50'),
('98faf41a84903537b239a75359dae5ee603fa8712bb3688239cd6a29a5aba5e6f31c5b27ccc5d1f2', 1, 3, 'MyApp', '[]', 0, '2020-05-16 15:40:32', '2020-05-16 15:40:32', '2021-05-16 16:40:32'),
('9d4c0e010a38d51fa9bfc920cc584f83fab2c21c2294179f0a811c3e23aeeb48fb25c175de15d8cf', 1, 3, 'MyApp', '[]', 0, '2020-05-19 15:48:51', '2020-05-19 15:48:51', '2021-05-19 16:48:51'),
('9ec2987a066dcaefcdca249ed6a691d0f8cf1da09915eceaa026864a9f73551c06f4f2760c3a02bd', 1, 3, 'MyApp', '[]', 0, '2020-05-24 13:01:20', '2020-05-24 13:01:20', '2021-05-24 14:01:20'),
('a0dc0cc6922f00784b24f9f08e326f7a6102f159afd05e9bd7957a105a1d57da7b74bf182c4a5074', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:09:00', '2020-06-01 09:09:00', '2021-06-01 10:09:00'),
('a4c97d59195ad4287e7af98b27dfae7a77748987e9f101f491d67477ef93fb4821546ce37946f296', 1, 3, 'MyApp', '[]', 0, '2020-05-24 06:49:00', '2020-05-24 06:49:00', '2021-05-24 07:49:00'),
('a81397cb82d249070579416ff590f52a3e4edae1243e7446da01e282ae8ce78715594adbcb6f7556', 1, 3, 'MyApp', '[]', 0, '2020-05-17 05:51:01', '2020-05-17 05:51:01', '2021-05-17 06:51:01'),
('b062cff48f13113eebda6bb1ee9ec374664318a07a9e073524e975533347c3b04dd5ced391eda07b', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:15:37', '2020-06-01 09:15:37', '2021-06-01 10:15:37'),
('b1353f470d2bb3608d15683feb82b9f60755d5cd7ddd5fa4fb6bec0111b643913915969ea88315c8', 1, 3, 'MyApp', '[]', 0, '2020-05-16 03:26:28', '2020-05-16 03:26:28', '2021-05-16 09:11:28'),
('b1da10a6bf79c4f8c4c1dd7070c98ad642efcf107afc33fe704f65688facc350099ff9345067f66d', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:14:38', '2020-06-01 09:14:38', '2021-06-01 10:14:38'),
('b4213f2383c3f26b085b5dc4b096cbc9e8ba02f03ed902abbee286e77dc295866bc83c0a402b6adc', 1, 3, 'MyApp', '[]', 0, '2020-05-24 04:32:07', '2020-05-24 04:32:07', '2021-05-24 05:32:07'),
('b6127bb3b2bda6ac72f4831fe8511594cde614e6241496cbcef3abf93d2d2675ec6aaa9e43384ae4', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:17:33', '2020-06-01 09:17:33', '2021-06-01 10:17:33'),
('ba751ac494a2b7f1e5544b358e0148a477c432b5b71af89c9cc547fd51101f4cabb6c86c037abb72', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:16:24', '2020-06-01 09:16:24', '2021-06-01 10:16:24'),
('bcc0c67d04c7773582f6e00ef934e98767da157eb684ddb2e2686448f35f3d2bc0106864dc31440a', 1, 3, 'MyApp', '[]', 0, '2020-05-17 01:36:57', '2020-05-17 01:36:57', '2021-05-17 02:36:57'),
('bff5d5b0315cc96356ce0a46a6efc88ce1ebc4c1b626300a1485499c5ac169aea2592e551f3792db', 1, 3, 'MyApp', '[]', 0, '2020-06-01 08:58:47', '2020-06-01 08:58:47', '2021-06-01 09:58:47'),
('c039458f335e116bef66d54903009cfe32ce1c70a37891438b4dcdd9b1384d79fbd657eb382c1a7c', 1, 3, 'MyApp', '[]', 0, '2020-05-16 08:47:16', '2020-05-16 08:47:16', '2021-05-16 14:32:16'),
('c22cb7f70d386f270f97de13ce545aa4e106cdfa67a6977bcd0b05c56d5ca202bc718e5093f70d49', 1, 3, 'MyApp', '[]', 0, '2020-05-17 05:54:57', '2020-05-17 05:54:57', '2021-05-17 06:54:57'),
('c61fbd2663b5ef24f52f421edd902c678e22555a76271c56093a3024a56525bcaf93f3aea90142c1', 1, 3, 'MyApp', '[]', 0, '2020-05-17 05:57:23', '2020-05-17 05:57:23', '2021-05-17 06:57:23'),
('cb1c774457df7caa0e8bf0896fde386bb6cd3128c130c3d96863866004766b5cab49952c18f36ef5', 1, 3, 'MyApp', '[]', 0, '2020-05-16 17:33:51', '2020-05-16 17:33:51', '2021-05-16 18:33:51'),
('cfac8a39aa0fff5382c00ac67f0debfbff8b2aaf10533d9c3e04e43ebe0c6f61bc09159ceb44678d', 1, 3, 'MyApp', '[]', 0, '2020-05-17 10:13:16', '2020-05-17 10:13:16', '2021-05-17 11:13:16'),
('dd3a4dc0c2e2da91371f8857dc19143e2b941bfc50c7d0fb6b72662b5b1b6a323607821fe85ecf23', 1, 3, 'MyApp', '[]', 0, '2020-05-16 10:04:25', '2020-05-16 10:04:25', '2021-05-16 15:49:25'),
('ddf56ff034714deac2ee224045f1e53ababd1c64bbaa1f1a03e55e2f62c612de23a3951c3653dfc2', 1, 3, 'MyApp', '[]', 0, '2020-05-29 04:39:01', '2020-05-29 04:39:01', '2021-05-29 05:39:01'),
('e13b486ec966c10a684680045eb0b2eddcb8f8ebd2538b85ad9b0772ee001335659869685dc5e44d', 1, 3, 'MyApp', '[]', 0, '2020-06-01 08:50:05', '2020-06-01 08:50:05', '2021-06-01 09:50:05'),
('e2281f7d97725236289c2d1091bbcc05cd1c77ae79bcb8329f1add882f820f3578bf8b0a45be5ecb', 1, 3, 'MyApp', '[]', 0, '2020-05-16 16:27:11', '2020-05-16 16:27:11', '2021-05-16 17:27:11'),
('e44ad781963739e7007bd4c434d9abc7598752621def379fca13919a270f1eab1c71a1898d2a4049', 1, 3, 'MyApp', '[]', 0, '2020-05-16 04:52:56', '2020-05-16 04:52:56', '2021-05-16 10:37:56'),
('e76815df8e269b61d242c004ce206997cbf5d33f8a02ed6cbefb6316afdff65e68dc9551560f0d94', 1, 3, 'MyApp', '[]', 0, '2020-05-26 06:42:31', '2020-05-26 06:42:31', '2021-05-26 07:42:31'),
('e7e6ea3f63fc35d12c428c8118df8dc416548194258c34ffa6be2836854cea99cd6c232e9d29287d', 1, 3, 'MyApp', '[]', 0, '2020-05-26 06:42:05', '2020-05-26 06:42:05', '2021-05-26 07:42:05'),
('ec5b5fb00d44922a2856fabcd9e0d60e7c62ecdd0d4684a7409da7a6a59c1563831246eba7f378fc', 1, 3, 'MyApp', '[]', 0, '2020-05-17 10:12:43', '2020-05-17 10:12:43', '2021-05-17 11:12:43'),
('f6d4603cfad17d99a0fc3fc60260066eb8f2387a490202af9c834ca2feaac600a6b429573df76952', 1, 3, 'MyApp', '[]', 0, '2020-05-17 02:05:59', '2020-05-17 02:05:59', '2021-05-17 03:05:59'),
('f805fa1b65a2a5b236b9bb01747f55e0d1807d84ad2dee0b1d3d9aef4ec2571d9e8bb0d0831d46d7', 1, 3, 'MyApp', '[]', 0, '2020-05-16 16:50:20', '2020-05-16 16:50:20', '2021-05-16 17:50:20'),
('fa16aab19f14124921231d06d1647bf58bbc841666cf294aaf5b25e2163639face017ac560e0b349', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:14:30', '2020-06-01 09:14:30', '2021-06-01 10:14:30'),
('fc8117e0bd4c3664c2092785547fec636e82a5ba8135ec7732c6ef94acfebe9743682a1ea5b5d7fa', 1, 3, 'MyApp', '[]', 0, '2020-06-01 09:08:05', '2020-06-01 09:08:05', '2021-06-01 10:08:05'),
('fe166b801cf91fbe41317ee1ea255d0f9cc36bd8e28e29dd4c7da84a31e25a302dcec1d80f5a1cfa', 1, 3, 'MyApp', '[]', 0, '2020-05-24 13:01:20', '2020-05-24 13:01:20', '2021-05-24 14:01:20');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'ouJSa7YzDRUD1XT2qzRlJwYWMPNv5Cs1eKAgicQX', 'http://localhost', 1, 0, 0, '2020-05-16 03:25:35', '2020-05-16 03:25:35'),
(2, NULL, 'Laravel Password Grant Client', 'i00oRDZ4Dyhm9bCsD6fXgWduDboZkY4D6G3IowmE', 'http://localhost', 0, 1, 0, '2020-05-16 03:25:35', '2020-05-16 03:25:35'),
(3, NULL, 'Laravel Personal Access Client', 'O0nhvKlnPkO4Cj6popxkf1SGCUeAqulTxxm7Bnvf', 'http://localhost', 1, 0, 0, '2020-05-16 03:25:40', '2020-05-16 03:25:40'),
(4, NULL, 'Laravel Password Grant Client', 'AtrBbytf199gRCiv2wahKhHM2BPwVe91Z6YE02ID', 'http://localhost', 0, 1, 0, '2020-05-16 03:25:40', '2020-05-16 03:25:40');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-16 03:25:35', '2020-05-16 03:25:35'),
(2, 3, '2020-05-16 03:25:40', '2020-05-16 03:25:40');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `office_code` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nick_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `currency_code` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_box` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_1` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_2` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `fax` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_office_id` int(11) DEFAULT NULL,
  `registration_number` int(11) DEFAULT NULL,
  `pan_number` int(11) DEFAULT NULL,
  `allow_transaction_posting` tinyint(1) NOT NULL DEFAULT '0',
  `audit_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `audit_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `office_infos`
--

CREATE TABLE `office_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `website_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_description` text COLLATE utf8mb4_unicode_ci,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyright_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `postcategories`
--

CREATE TABLE `postcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `weight` int(11) DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_id` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `postcategories`
--

INSERT INTO `postcategories` (`id`, `title`, `description`, `slug`, `active`, `weight`, `user_id`, `office_id`, `created_at`, `updated_at`) VALUES
(1, 'Blog', NULL, 'blog', 1, NULL, '1', 1, '2020-05-16 10:14:20', '2020-05-16 10:14:20'),
(2, 'Job', 'Job', 'jobs', 1, NULL, '1', 1, '2020-05-16 05:34:53', '2020-05-16 05:34:53'),
(3, 'Intern', NULL, 'intern', 1, NULL, '1', 1, '2020-05-16 16:45:12', '2020-05-16 16:45:12');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `office_id` bigint(20) NOT NULL,
  `deadline` date DEFAULT NULL,
  `other_description` text COLLATE utf8mb4_unicode_ci,
  `no_of_opening` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `post_type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'post'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `short_text`, `description`, `category`, `user_id`, `meta_description`, `feature_image`, `publish`, `office_id`, `deadline`, `other_description`, `no_of_opening`, `created_at`, `updated_at`, `post_type`) VALUES
(10, 'Shopify Expert (Mid-level)', NULL, '<header style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, sans-serif;\"><a style=\"transition: all 0.5s ease 0s;\"><p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px;\">Do you have a passion for E-Commerce and Drop Shipping ? Are you passionate about finding trending products? Do you want to work directly with Entrepreneurs on a daily basis? We are a group of successful entrepreneurs seeking a Drop Shipping Ninja.<br>&nbsp;&nbsp;&nbsp;&nbsp;If you share a passion for working in a fast paced, rapid growth company committed to professional excellence, then we\'re interested in talking to you! We only want the best of the best. If you are not an “Team” player, this role is not for you.<br>&nbsp;&nbsp;&nbsp;&nbsp;We are looking for someone to do product research for a new Shopify store, by find trending products but also products that will provide sales for us long term!<br>We are looking for an extremely professional who will be upfront with their research tactics and their reasons behind each product they recommend.</p></a></header><div class=\"row\"><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"width: 540px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; font-family: Montserrat, sans-serif; padding: 0px;\">Job description</h4><p class=\"description\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul><li>Developing eCommerce sites on Shopify platform</li><li>Developing custom Shopify themes</li><li>Migrating websites from other eCommerce platforms to Shopify</li><li>Extending Shopify’s functionalities to next level using storefront APIs, liquid programming, meta fields, etc</li><li>Integrating third-party and platform supported apps in the sites</li><li>Setting up payment integration, shipping methods and taxes in Shopify platform</li><li>Page speed optimization of Shopify sites</li><li>Implementing SEO/CRO best practices in Shopify sites</li><li>Transforming complex layout PSDs into pixel-perfect presentation-layer HTML5/CSS3 templates</li><li>Creating responsive website designs</li><li>Working with version control systems such as GIT / SVN</li><li>Working under tight deadlines</li><li>Handling multiple projects at the same time</li><li>Producing high quality of work with a strong focus on detail</li><li>Thoroughly QA work prior to submission</li><li>Development of Shopify-based web applications</li><li>Work closely with the Ecommerce team for research and development</li><li>Communicate to the Project Manager/Team Leader with efficiency, accuracy, progress and/or delays in the project tasks</li><li>Share knowledge and ideas among other developers on the projector tools used, or on any innovation made during the project</li><li>Prepare project documents for each project, including project development report and project completion report</li></ul><p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px;\"></p></div><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"width: 540px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; font-family: Montserrat, sans-serif; padding: 0px;\">Other Specification</h4><p class=\"description\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul><li>A strong knowledge of Shopify platform</li><li>An expertise in Shopify theme development and customization</li><li>Expert-level knowledge of Shopify Liquid templating language</li><li>A proficiency in working with different Shopify JS APIs (storefront, AJAX Cart, Sections etc)</li><li>An expertise in HTML5, CSS3 and JavaScript/ECMA Script 5/6</li><li>A knowledge of SCSS (preferred)</li><li>An expertise in JavaScript libraries (eg: jQuery).</li><li>A positive and upbeat attitude with the ability to learn quickly</li><li>A minimum of 2 years of experience in Shopify Websites Development</li><li>Experience with WordPress, Magento, and BigCommerce (preferred)</li><li>Experience with Ruby (preferred)</li><li>Proficiency in PHP/MYSQL and AJAX (preferred)</li><li>Shopify Certification (Preferred),Highly creative with experience in identifying target</li>audiences and devising digital campaigns that engage, inform and motivate<li>Experience on third party apps integrations with external systems, via MS/CRM/ERP/Middleware/shopify apps etc</li><li>Knowledge on version control systems such as GIT</li></ul><p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px;\"></p></div></div><p><a style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); transition: all 0.5s ease 0s; outline: none; font-family: &quot;Open Sans&quot;, sans-serif;\"></a></p><div style=\"box-sizing: border-box;\"><div class=\"row\" style=\"box-sizing: border-box; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;\"><div class=\"col-12\" style=\"box-sizing: border-box; position: relative; width: 540px; padding-right: 15px; padding-left: 15px; flex: 0 0 100%; max-width: 100%;\"><p style=\"box-sizing: border-box; margin: 0px 0px 30px; padding: 0px;\"><b style=\"box-sizing: border-box; font-weight: bolder;\">Pirority will be given to the candidate who can join immediately .</b></p></div></div></div>', '2', 1, NULL, '', 0, 1, '2020-05-31', NULL, 4, '2020-05-17 02:17:13', '2020-05-17 02:17:13', 'post'),
(11, 'Digital Marketing Specialist', NULL, '<header style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, sans-serif;\"><a style=\"transition: all 0.5s ease 0s;\"><p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px;\">Analogue 51 is an IT firm which is a company under PG and Sons, designed for future excellence where we provide paramount services pertaining to the field. Analogue 51 is the only company that will be fully based on pure IT and computer applications.</p></a></header><p><a style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); transition: all 0.5s ease 0s; outline: none; font-family: &quot;Open Sans&quot;, sans-serif;\"></a></p><div class=\"row\" style=\"box-sizing: border-box; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;\"><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"box-sizing: border-box; position: relative; width: 540px; padding-right: 15px; padding-left: 15px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"box-sizing: border-box; margin: 0px 0px 20px; font-family: Montserrat, sans-serif; font-weight: 400; line-height: 1.2; color: inherit; font-size: 1.5rem; padding: 0px;\">Job description</h4><p class=\"description\" style=\"box-sizing: border-box; margin: 0px 0px 0px 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\"><li style=\"box-sizing: border-box;\">Plan and execute all digital marketing, including SEO/SEM, marketing database, email, social media and display advertising campaigns</li><li style=\"box-sizing: border-box;\">Design, build and maintain our and clients social media presence</li><li style=\"box-sizing: border-box;\">Create content for promotional materials pertaining to marketing and Search Engine Optimization (SEO) related activities</li><li style=\"box-sizing: border-box;\">Identify trends and insights, and optimize spend and performance based on the insights</li><li style=\"box-sizing: border-box;\">Brainstorm new and creative growth strategies</li><li style=\"box-sizing: border-box;\">Collaborate with internal teams to create landing pages and optimize the user experience</li><li style=\"box-sizing: border-box;\">Utilize the strong analytical ability to evaluate end-to-end customer experience across multiple channels and customer touch points</li><li style=\"box-sizing: border-box;\">Manage, create and publish original, high-quality content</li><li style=\"box-sizing: border-box;\">Create online banner adverts and oversee pay per click (PPC) ad management</li><li style=\"box-sizing: border-box;\">Working knowledge of ad serving tools (e.g., DART, Atlas)</li><li style=\"box-sizing: border-box;\">Design and develop banners and brochures for online and offline campaigns.</li><li style=\"box-sizing: border-box;\">Evaluate emerging technologies. Provide thought leadership and perspective for adoption where appropriate</li><li style=\"box-sizing: border-box;\">Deliver recommendations to improve client’s site rankings and traffic and/or other measurable results, based on agreed KPIs/ business objectives for the project</li><li style=\"box-sizing: border-box;\">Responsible for R&amp;D and reaching potential clients with liaison with Business Development team.</li><li style=\"box-sizing: border-box;\">Continuously recommend ideas/tactics and formulate campaign optimization plans</li></ul><p style=\"box-sizing: border-box; margin: 0px 0px 30px; padding: 0px;\"></p></div><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"box-sizing: border-box; position: relative; width: 540px; padding-right: 15px; padding-left: 15px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"box-sizing: border-box; margin: 0px 0px 20px; font-family: Montserrat, sans-serif; font-weight: 400; line-height: 1.2; color: inherit; font-size: 1.5rem; padding: 0px;\">Other Specification</h4><p class=\"description\" style=\"box-sizing: border-box; margin: 0px 0px 0px 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\"><li style=\"box-sizing: border-box;\">Masters degree in either marketing, IT or digital</li><li style=\"box-sizing: border-box;\">Proven working 2 plus years’ experience in digital marketing</li><li style=\"box-sizing: border-box;\">Demonstrable experience leading and managing SEO/SEM, email marketing, social media marketing.</li><li style=\"box-sizing: border-box;\">Having the skill set of content writing will be preferred</li><li style=\"box-sizing: border-box;\">Excellent client-facing skills</li><li style=\"box-sizing: border-box;\">Highly creative with experience in identifying target audiences and devising digital campaigns that engage, inform and motivate</li><li style=\"box-sizing: border-box;\">Experience in optimizing landing pages and user funnels</li><li style=\"box-sizing: border-box;\">Experience with A/B and multivariate experiments</li><li style=\"box-sizing: border-box;\">Solid knowledge of website analytics tools (e.g., Google Analytics)</li><li style=\"box-sizing: border-box;\">Working knowledge of ad serving tools (e.g., DART, Atlas)</li><li style=\"box-sizing: border-box;\">Experience in setting up and optimizing Google Adwords campaigns</li><li style=\"box-sizing: border-box;\">Strong analytical skills and data-driven thinking</li><li style=\"box-sizing: border-box;\">Up-to-date with the latest trends and best practices in online marketing and measurement</li></ul></div></div>', '2', 1, NULL, '', 0, 1, '2020-05-31', NULL, 2, '2020-05-17 02:18:19', '2020-05-17 02:18:19', 'post'),
(12, 'Marketing and Sales Executive', NULL, '<header><a style=\"transition: all 0.5s ease 0s;\"><p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px;\">Analogue 51 is an IT firm which is a company under PG and Sons, designed for future excellence where we provide paramount services pertaining to the field. Analogue 51 is the only company that will be fully based on pure IT and computer applications.</p></a></header><p><a style=\"transition: all 0.5s ease 0s; outline: none;\"><span style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, sans-serif;\"></span><div style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, sans-serif;\"><div class=\"row\"><div class=\"col-12\" style=\"width: 540px;\"></div></div></div></a></p><div class=\"row\" style=\"box-sizing: border-box; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"box-sizing: border-box; position: relative; width: 540px; padding-right: 15px; padding-left: 15px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"box-sizing: border-box; margin: 0px 0px 20px; font-family: Montserrat, sans-serif; font-weight: 400; line-height: 1.2; color: inherit; font-size: 1.5rem; padding: 0px;\">Job description</h4><p class=\"description\" style=\"box-sizing: border-box; margin: 0px 0px 0px 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\"><li style=\"box-sizing: border-box;\">Conduct market research to identify selling possibilities and evaluate customer needs</li><li style=\"box-sizing: border-box;\">Actively seek out new sales opportunities through cold calling, networking and social media</li><li style=\"box-sizing: border-box;\">Set up meetings with potential clients and listen to their wishes and concerns</li><li style=\"box-sizing: border-box;\">Prepare and deliver appropriate presentations on products/ services</li><li style=\"box-sizing: border-box;\">Create frequent reviews and reports with sales and financial data</li><li style=\"box-sizing: border-box;\">Ensure the availability of stock for sales and demonstrations</li><li style=\"box-sizing: border-box;\">Participate on behalf of the company in exhibitions or conferences</li><li style=\"box-sizing: border-box;\">Negotiate/close deals and handle complaints or objections</li><li style=\"box-sizing: border-box;\">Collaborate with team to achieve better results</li></ul><p style=\"box-sizing: border-box; margin: 0px 0px 30px; padding: 0px;\"></p></div><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"box-sizing: border-box; position: relative; width: 540px; padding-right: 15px; padding-left: 15px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"box-sizing: border-box; margin: 0px 0px 20px; font-family: Montserrat, sans-serif; font-weight: 400; line-height: 1.2; color: inherit; font-size: 1.5rem; padding: 0px;\">Other Specification</h4><p class=\"description\" style=\"box-sizing: border-box; margin: 0px 0px 0px 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\"><li style=\"box-sizing: border-box;\">Must have completed Bachelors degree or Master\'s running</li><li style=\"box-sizing: border-box;\">Proven experience as a marketing executive or relevant role</li><li style=\"box-sizing: border-box;\">Proficiency in English</li><li style=\"box-sizing: border-box;\">Excellent knowledge of MS office applications</li><li style=\"box-sizing: border-box;\">Thorough understanding of marketing and negotiating techniques</li><li style=\"box-sizing: border-box;\">Fast learner and passion for sales</li><li style=\"box-sizing: border-box;\">Self-motivated with a results-driven approach</li><li style=\"box-sizing: border-box;\">Aptitude in delivering attractive presentations</li><li style=\"box-sizing: border-box;\">Should have smart and pleasant personality</li></ul><p style=\"box-sizing: border-box; margin: 0px 0px 30px; padding: 0px;\"></p></div></div>', '2', 1, NULL, '', 0, 1, '2020-05-31', NULL, 5, '2020-05-17 02:19:44', '2020-05-17 02:19:44', 'post'),
(15, 'Digital Marketing Intern', NULL, '<header style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, sans-serif;\"><a style=\"transition: all 0.5s ease 0s;\"><p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; padding: 0px;\">Analogue 51 is an IT firm which is a company under PG and Sons, designed for future excellence where we provide paramount services pertaining to the field. Analogue 51 is the only company that will be fully based on pure IT and computer applications.</p></a></header><p><a style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); transition: all 0.5s ease 0s; outline: none; font-family: &quot;Open Sans&quot;, sans-serif;\"></a></p><div class=\"row\" style=\"box-sizing: border-box; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;\"><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"box-sizing: border-box; position: relative; width: 497px; padding-right: 15px; padding-left: 15px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"box-sizing: border-box; margin: 0px 0px 20px; font-family: Montserrat, sans-serif; font-weight: 400; line-height: 1.2; color: inherit; font-size: 1.5rem; padding: 0px;\">Job description</h4><p class=\"description\" style=\"box-sizing: border-box; margin: 0px 0px 0px 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\"><li style=\"box-sizing: border-box;\">Coordination with marketing team for requirement understanding.</li><li style=\"box-sizing: border-box;\">Managing marketing materials such as inquiry forms, brochures, banners</li><li style=\"box-sizing: border-box;\">Support the marketing manager in overseeing the department’s operations</li><li style=\"box-sizing: border-box;\">Organize and attend marketing activities or events to raise brand awareness</li><li style=\"box-sizing: border-box;\">Plan advertising and promotional campaigns for products or services on a variety of media (social, print etc.)</li><li style=\"box-sizing: border-box;\">Assist in company’s branding and media communication activities such as press releases, advertisements, marketing collateral, and web site</li><li style=\"box-sizing: border-box;\">Other duties as deemed necessary and assigned</li></ul><p style=\"box-sizing: border-box;\"><a style=\"color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); transition: all 0.5s ease 0s; outline: none; font-family: &quot;Open Sans&quot;, sans-serif;\"></a></p><div class=\"row\" style=\"box-sizing: border-box; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;\"><div class=\"col-lg-6 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\" style=\"box-sizing: border-box; position: relative; width: 497px; padding-right: 15px; padding-left: 15px; animation-name: bounceInUp; margin-bottom: 30px;\"><h4 style=\"box-sizing: border-box; margin: 0px 0px 20px; font-family: Montserrat, sans-serif; font-weight: 400; line-height: 1.2; color: inherit; font-size: 1.5rem; padding: 0px;\">Other Specification</h4><p class=\"description\" style=\"box-sizing: border-box; margin: 0px 0px 0px 60px; padding: 0px; font-size: 14px; line-height: 24px;\"></p><ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\"><li style=\"box-sizing: border-box;\">Monitor Current Trends, Research &amp; Plan Contents</li><li style=\"box-sizing: border-box;\">Basic knowledge of google analytics, tags and html</li><li style=\"box-sizing: border-box;\">Able to act as a social media handler</li><li style=\"box-sizing: border-box;\">Passionate about writing and strong command over English writing and vocabulary is a must</li><li style=\"box-sizing: border-box;\">Excellent writing, editing, and communication skills. If you love to write, you’re going todo really well here</li><li style=\"box-sizing: border-box;\">Deep interest and knowledge in specific entertainment genre and niche</li><li style=\"box-sizing: border-box;\">Photo and Video editing skills such Adobe Premiere, After Effects and Photoshop is a huge plus</li><li style=\"box-sizing: border-box;\">Knows, Excel, PowerPoint, and Word at an advance level</li><li style=\"box-sizing: border-box;\">Knowledge of search engine optimization, including basic keyword research is helpful</li></ul></div></div></div></div>', '3', 1, NULL, '', 0, 1, NULL, NULL, NULL, '2020-05-17 02:35:39', '2020-05-17 02:35:39', 'post'),
(20, 'UI-UX Frontend Developer', 'Mid-Level', '<p class=\"MsoNormal\" style=\"mso-margin-bottom-alt:auto;line-height:normal\"><b>Job Description<o:p></o:p></b></p><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:\r\nnormal\">If\r\nyou’re looking for a career that transforms, inspires, challenges and rewards\r\nyou, then come and join us.<o:p></o:p></p><p class=\"MsoListParagraphCxSpFirst\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Create visual aspects of marketing\r\nmaterials, websites and other media, including posters, infographics,\r\npromotional videos, and short videos by studying information and materials\r\navailable<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Create graphical posts for social media\r\naccounts of our clients<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Invent new ideas for branding,\r\nadvertising campaigns, and marketing messages<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Design materials using paper sketches\r\nand digital illustration software<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Analyze trends, look at new data and\r\nkeep with the times<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Overseeing design changes and\r\nimprovements throughout the development process<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Integrating technical and visual\r\nsolutions into a design concept<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Possess an understanding of advertising\r\ncampaigns<o:p></o:p></p><p class=\"MsoListParagraphCxSpMiddle\" style=\"mso-margin-bottom-alt:auto;\r\nmso-add-space:auto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;\r\ntab-stops:list .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;\r\nmso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;\r\nmso-bidi-font-family:Symbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Working with clients, briefing and\r\nadvising them with regard to design style, format, print production, and\r\ntimescales<o:p></o:p></p><p class=\"MsoListParagraphCxSpLast\" style=\"mso-margin-bottom-alt:auto;mso-add-space:\r\nauto;text-indent:-.25in;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:\r\nlist .5in\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;mso-bidi-language:HI;mso-bidi-font-weight:bold\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]-->Students well updated to computer\r\ngraphics, design software and opting to build a professional portfolio also may\r\napply<b><o:p></o:p></b></p><p class=\"MsoNormal\" style=\"mso-margin-bottom-alt:auto;line-height:normal\"><b>Qualifications and Skills<o:p></o:p></b></p><ul type=\"disc\">\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Bachelor’s in a computer or relevant field<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Minimum 3+years development experience in all areas\r\n     of application design and programming using Microsoft technologies<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">A portfolio that demonstrates both JavaScript\r\n     proficiency and understanding of SOLID design principles (plugins, node\r\n     packages, etc.).<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Deep Knowledge of JavaScript framework (like:&nbsp;\r\n     Angular, Vue JS, React) preferable&nbsp;<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Experience doing test-driven development, functional\r\n     testing and continuous integration is desirable.<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Web API and REST Web Services<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Knowledge of Version Controlling (Git, TFS or SVN)<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Strong skill set in Adobe Photoshop, Illustrator,\r\n     After Effects and Premiere Pro<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Highly creative, detail-oriented and deadline-driven<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in\">Should have the knowledge of wireframe, prototype,\r\n     mock-up<o:p></o:p></li>\r\n</ul><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\nline-height:normal\"><b>Priority will be given to the candidate who can join\r\nimmediately.<o:p></o:p></b></p>', '2', 1, NULL, '', 0, 1, '2020-06-20', NULL, 4, '2020-05-24 06:56:30', '2020-05-24 06:56:30', 'post'),
(21, 'PHP Developer', 'Senior Level', '<p class=\"MsoNormal\" style=\"mso-margin-bottom-alt:auto;line-height:normal\"><b>Job Description<o:p></o:p></b></p><p class=\"MsoNormal\"><span style=\"mso-bidi-font-size:12.0pt;line-height:115%;\r\nmso-bidi-font-family:&quot;Times New Roman&quot;\">We are looking for a PHP Developer\r\nresponsible for managing back-end services and the interchange of data between\r\nthe server and the users. Your primary focus will be the development of all\r\nserver-side logic, definition and maintenance of the central database, and\r\nensuring high performance and responsiveness to requests from the front-end.\r\nYou will also be responsible for integrating the front-end elements built by\r\nyour co-workers into the application. Therefore, a basic understanding of\r\nfront-end technologies is necessary as well.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"mso-margin-bottom-alt:auto;line-height:normal\"><b>Qualifications and Skills<o:p></o:p></b></p><ul type=\"disc\">\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">A minimum of 3-year experience with strong knowledge\r\n     of PHP web frameworks like Laravel, CI, Yii, etc.<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Understanding\r\n     the fully synchronous behavior of PHP<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Understanding\r\n     of MVC design patterns<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Basic\r\n     understanding of front-end technologies, such as JavaScript, HTML5, and\r\n     CSS3<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Knowledge\r\n     of object oriented PHP programming<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Understanding accessibility and security compliance<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Strong knowledge of the common PHP or web server\r\n     exploits and their solutions<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Understanding fundamental design principles behind a\r\n     scalable application<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">User authentication and authorization between\r\n     multiple systems, servers, and environments<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Integration of multiple data sources and databases\r\n     into one system<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Familiarity with limitations of PHP as a platform\r\n     and its workarounds<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Creating database schemas that represent and support\r\n     business processes<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Familiarity with SQL/NoSQL databases and their\r\n     declarative query languages<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Proficient understanding of code versioning tools,\r\n     such as Git<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Strong desire to evolve, progress, learn, and\r\n     collaborate<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Self-motivated, able to work independently and can\r\n     prioritize among many competing needs<o:p></o:p></li>\r\n <li class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\n     line-height:normal;mso-list:l0 level1 lfo1;tab-stops:list .5in\">Excellent verbal, written and interpersonal\r\n     communication skills<o:p></o:p></li>\r\n</ul><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\nline-height:normal\"><b>Priority will be given to the candidate who can join\r\nimmediately.<o:p></o:p></b></p>', '2', 1, NULL, '', 0, 1, '2020-06-20', NULL, 4, '2020-05-24 07:02:07', '2020-05-24 07:02:07', 'post');
INSERT INTO `posts` (`id`, `title`, `short_text`, `description`, `category`, `user_id`, `meta_description`, `feature_image`, `publish`, `office_id`, `deadline`, `other_description`, `no_of_opening`, `created_at`, `updated_at`, `post_type`) VALUES
(22, 'Content Writer', NULL, '<ul style=\"margin-top:0in\" type=\"disc\">\r\n <li class=\"MsoNormal\" style=\"color:#404040;margin-bottom:0in;margin-bottom:.0001pt;\r\n     line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;vertical-align:\r\n     baseline\"><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\n     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial\">Produce\r\n     well-researched content for publication online and in print<o:p></o:p></span></li>\r\n <li class=\"MsoNormal\" style=\"color:#404040;margin-bottom:0in;margin-bottom:.0001pt;\r\n     line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;vertical-align:\r\n     baseline\"><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\n     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial\">Organize\r\n     writing schedules to complete drafts of content or finished projects\r\n     within deadlines<o:p></o:p></span></li>\r\n <li class=\"MsoNormal\" style=\"color:#404040;margin-bottom:0in;margin-bottom:.0001pt;\r\n     line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;vertical-align:\r\n     baseline\"><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\n     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial\">Utilize\r\n     industry best practices and familiarity with the organization\'s mission to\r\n     inspire ideas and content<o:p></o:p></span></li>\r\n <li class=\"MsoNormal\" style=\"color:#404040;margin-bottom:0in;margin-bottom:.0001pt;\r\n     line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;vertical-align:\r\n     baseline\"><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\n     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial\">Communicate\r\n     and cooperate with a writing team, including a content manager, editors,\r\n     and web publishers<o:p></o:p></span></li>\r\n <li class=\"MsoNormal\" style=\"color:#404040;margin-bottom:0in;margin-bottom:.0001pt;\r\n     line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;vertical-align:\r\n     baseline\"><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\n     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial\">Follow\r\n     an editorial calendar, collaborating with other members of the content\r\n     production team to ensure timely delivery of materials<o:p></o:p></span></li>\r\n <li class=\"MsoNormal\" style=\"color:#404040;margin-bottom:0in;margin-bottom:.0001pt;\r\n     line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;vertical-align:\r\n     baseline\"><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\n     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial\">Develop\r\n     related content for multiple platforms, such as websites, email marketing,\r\n     product descriptions, videos, and blogs<o:p></o:p></span></li>\r\n</ul><p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;line-height:\r\nnormal;vertical-align:baseline\"><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" style=\"mso-margin-bottom-alt:auto;line-height:normal\"><b>Qualifications and Skills<o:p></o:p></b></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Proven record of excellent writing demonstrated in a\r\nprofessional portfolio<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Impeccable grasp of the English language, including idioms and\r\ncurrent trends in slang and expressions<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Ability to work independently with little or no daily\r\nsupervision<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Strong interpersonal skills and willingness to communicate with\r\nclients, colleagues, and management<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Ability to work on multiple projects with different objectives\r\nsimultaneously<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Strict adherence to the style guides of each company and their\r\npolicies for publication<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Good time management skills, including prioritizing, scheduling,\r\nand adapting as necessary<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Proficiency with computers, especially writing programs, such as\r\nGoogle Docs and Microsoft Word, Excel, Outlook, and PowerPoint<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0in 0in 0.0001pt 40.5pt; text-indent: -0.25in; line-height: 16.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><!--[if !supportLists]--><span style=\"font-size:10.0pt;mso-bidi-font-size:\r\n11.5pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol;color:#404040\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-size:11.5pt;font-family:&quot;inherit&quot;,serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:Arial;\r\ncolor:#404040\">Bachelor in Marketing, English, Journalism or related field\r\n(preferred)<o:p></o:p></span></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;\r\nline-height:normal\"><b>Priority will be given to the candidate who can join\r\nimmediately.<o:p></o:p></b></p>', '2', 1, NULL, '', 0, 1, '2020-06-20', NULL, 2, '2020-05-24 12:59:23', '2020-05-24 12:59:23', 'post'),
(24, 'Educationist | Curriculum Development Manager', NULL, '<p class=\"text-muted\" style=\"line-height: 1.6; font-family: Nunito, sans-serif; background-color: rgb(248, 249, 252); color: rgb(132, 146, 166) !important;\">Scholarkeys is looking for an experienced Educationist with curriculum writing experience, and a passion for creating equitable courses that encourage practical approach. Join our team to help us develop and revise curriculum that supports the education system to pull off “study from home” effectively and make the most out of technology. We can all agree to the fact that with technology taking over, virtual learning is key to changing how we teach and learn. Now ScholarKeys brings you an e-learning platform where we bring education and technology hand in hand. While each team member takes ownership of large parts of projects, we share work and tasks across projects to make sure all of our projects are aligned under the same curriculum values and contribute to a post pandemic, we can never be sure that this won’t happen again? Our ultimate vision with ScholarKeys is to provide our nation with all in one platform where learning and teaching can both be productive and effective. In the nation full of school management software, we want to provide the education system a platform for higher learning and innovative collaboration.</p><ul class=\"list-unstyled feature-list text-muted pb-3 mb-5\" style=\"font-family: Nunito, sans-serif; background-color: rgb(248, 249, 252); color: rgb(132, 146, 166) !important;\"><li style=\"margin-bottom: 12px; position: relative; padding-left: 28px;\"><svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-check-circle fea icon-sm text-success mr-2\"><path d=\"M22 11.08V12a10 10 0 1 1-5.93-9.14\"></path><polyline points=\"22 4 12 14.01 9 11.01\"></polyline></svg>Collaborate with the management and the curriculum team to design, develop, write, and improve Scholarkeys curriculum.</li><li style=\"margin-bottom: 12px; position: relative; padding-left: 28px;\"><svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-check-circle fea icon-sm text-success mr-2\"><path d=\"M22 11.08V12a10 10 0 1 1-5.93-9.14\"></path><polyline points=\"22 4 12 14.01 9 11.01\"></polyline></svg>Incorporate research-based best practices to create an engaging and approachable course for all students and their teachers.</li><li style=\"margin-bottom: 12px; position: relative; padding-left: 28px;\"><svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-check-circle fea icon-sm text-success mr-2\"><path d=\"M22 11.08V12a10 10 0 1 1-5.93-9.14\"></path><polyline points=\"22 4 12 14.01 9 11.01\"></polyline></svg>Actively seek and incorporate feedback from the team, teachers, and students through ongoing testing and evaluation of curriculum.</li><li style=\"margin-bottom: 12px; position: relative; padding-left: 28px;\"><svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-check-circle fea icon-sm text-success mr-2\"><path d=\"M22 11.08V12a10 10 0 1 1-5.93-9.14\"></path><polyline points=\"22 4 12 14.01 9 11.01\"></polyline></svg>Coordinate with the Product and Engineering teams on the development/refinement of tools that support the learning, teaching, and curriculum development process.</li><li style=\"margin-bottom: 12px; position: relative; padding-left: 28px;\"><svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-check-circle fea icon-sm text-success mr-2\"><path d=\"M22 11.08V12a10 10 0 1 1-5.93-9.14\"></path><polyline points=\"22 4 12 14.01 9 11.01\"></polyline></svg>Coordinate with the Professional Learning team on the content of curriculum-focused learning experiences for teachers; helping design and run professional learning experiences (depending on experience and interest)</li><li style=\"margin-bottom: 12px; position: relative; padding-left: 28px;\"><svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-check-circle fea icon-sm text-success mr-2\"><path d=\"M22 11.08V12a10 10 0 1 1-5.93-9.14\"></path><polyline points=\"22 4 12 14.01 9 11.01\"></polyline></svg>Manage relationships with external partners</li><li></li></ul>', '2', 1, NULL, '', 0, 1, '2020-06-30', 'Required\r\nComprehensive knowledge of Alternative assessment, including the concepts covered in the elementary through high school grade bands of the Nepal Curriculum Board standards\r\nDeep understanding of and commitment to equity, best practices to support equitable classroom environments, and gender- and culturally-responsive teaching practices\r\nClassroom teaching experience (preferably experience across multiple grade bands); Authentic teaching experience preferred\r\nThe creativity and expertise to develop engaging, fun activities that can be used successfully with a wide range of learners, and be taught by a wide range of teachers\r\nPrior collaborative curriculum writing/activity design experience\r\nFluency with and deep understanding of Alternative and research-based best practices\r\nPreferred\r\nExperience teaching in a Virtual classroom environment with students from diverse backgrounds (e.g. students of all walk of life, students from low-income communities, rural students, immigrant students) (strongly preferred)\r\nExperience developing or delivering online learning experiences\r\nBachelor’s degree or higher in Education.\r\nExperience/familiarity with Nepal Education Board curriculum\r\nExperience/familiarity with School level courses.\r\nExperience supporting and collaborating with teachers on Virtual lessons', 1, '2020-06-01 09:16:51', '2020-06-01 09:16:51', 'post');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `address_line_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` int(11) NOT NULL,
  `latitude` bigint(20) NOT NULL,
  `longitude` bigint(20) NOT NULL,
  `phone_number` bigint(20) NOT NULL,
  `office_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `status`, `email`, `email_verified_at`, `password`, `remember_token`, `office_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 'info@analogueinc.com.np', NULL, '$2y$10$CTTSKMsFnGZHo/jpG91nOuGtYiAbj4K/viPLh4.TpWJ/3mGEj95.i', NULL, NULL, '2020-05-16 03:24:45', '2020-05-16 03:24:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_packages`
--

CREATE TABLE `user_packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `package_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_details` text COLLATE utf8mb4_unicode_ci,
  `package_total_price` bigint(20) DEFAULT NULL,
  `other_message` text COLLATE utf8mb4_unicode_ci,
  `contact` int(11) NOT NULL DEFAULT '0',
  `template` int(11) NOT NULL DEFAULT '0',
  `newsletter` int(11) NOT NULL DEFAULT '0',
  `social_post` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_packages`
--

INSERT INTO `user_packages` (`id`, `full_name`, `email`, `phone`, `package_name`, `package_type`, `package_details`, `package_total_price`, `other_message`, `contact`, `template`, `newsletter`, `social_post`, `created_at`, `updated_at`) VALUES
(1, 'MIniyan', 'hello@hello.com', 98567890867, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 03:29:22', '2020-05-16 03:29:22'),
(2, 'MIniyan', 'hello@hello.com', 98567890867, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 03:29:23', '2020-05-16 03:29:23'),
(3, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 03:30:18', '2020-05-16 03:30:18'),
(4, 'MIniyan', 'miniyanp@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Logo\n													\n													\n													I don\'t need a logo.\n													I\'d like to upgrade my logo.\n												\n													Social Media Assets\n													\n													\n													What are Social Media Assets? \n													I need Social Media Assets.\n												\n													Branding Templates\n													\n													\n													I already have a branding template.\n													Yes, I need my own branding template.\n												\n													Email Signature\n													\n													\n													I already have an email signature.\n													Sure, let\'s get this one too.\n												\n													Signage\n													\n													\n													I have my own Signage.\n													Yes, I\'d like a Signage.\n												\n													Merchandising\n													\n													\n													It\'s not necessary.\n													Help my brand speak for itself.\n												\n													Icon Pack\n													\n													\n													I don\'t need this.\n													Yes, I need an Icon Pack.\n												\n													Business Card\n													\n													\n												\n													Letterhead\n													\n													\n												\n													Cover Letter\n													\n													\n												\n													Leaflet\n													\n													\n												\n													Envelope\n													\n													\n												\n													Brochure', 70000, NULL, 0, 0, 0, 0, '2020-05-16 03:33:59', '2020-05-16 03:33:59'),
(5, 'dsfdsfds', NULL, NULL, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 04:52:34', '2020-05-16 04:52:34'),
(6, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 04:56:07', '2020-05-16 04:56:07'),
(7, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:02:05', '2020-05-16 05:02:05'),
(8, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:14:21', '2020-05-16 05:14:21'),
(9, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:16:27', '2020-05-16 05:16:27'),
(10, 'MIniyan', 'miniyanp@gmail.com', 9845700611, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:17:03', '2020-05-16 05:17:03'),
(11, 'asfs', 'miniyanp@gmail.com', 9845700611, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:17:10', '2020-05-16 05:17:10'),
(12, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:17:28', '2020-05-16 05:17:28'),
(13, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:19:17', '2020-05-16 05:19:17'),
(14, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Premium Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:20:07', '2020-05-16 05:20:07'),
(15, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Premium Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:20:24', '2020-05-16 05:20:24'),
(16, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Premium Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:20:45', '2020-05-16 05:20:45'),
(17, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Premium Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:21:09', '2020-05-16 05:21:09'),
(18, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory', 3500, NULL, 0, 0, 0, 0, '2020-05-16 05:23:17', '2020-05-16 05:23:17'),
(19, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory', 3500, NULL, 0, 0, 0, 0, '2020-05-16 05:24:35', '2020-05-16 05:24:35'),
(20, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory', 3500, NULL, 0, 0, 0, 0, '2020-05-16 05:25:10', '2020-05-16 05:25:10'),
(21, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Social Media Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:26:48', '2020-05-16 05:26:48'),
(22, 'dsfdsfds', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:28:36', '2020-05-16 05:28:36'),
(23, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Premium Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:28:43', '2020-05-16 05:28:43'),
(24, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Premium Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:29:29', '2020-05-16 05:29:29'),
(25, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Premium Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:29:54', '2020-05-16 05:29:54'),
(26, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Social Media Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:30:23', '2020-05-16 05:30:23'),
(27, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Professional Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 05:31:16', '2020-05-16 05:31:16'),
(28, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 08:12:01', '2020-05-16 08:12:01'),
(29, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 08:12:41', '2020-05-16 08:12:41'),
(30, 'MIniyan', 'miniyanp@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Email Signature\n													\n													\n													I already have an email signature.\n													Sure, let\'s get this one too.\n												\n													Signage\n													\n													\n													I have my own Signage.\n													Yes, I\'d like a Signage.\n												\n													Merchandising\n													\n													\n													It\'s not necessary.\n													Help my brand speak for itself.\n												\n													Business Card\n													\n													\n												\n													Leaflet\n													\n													\n												\n													Envelope', 35500, 'sdfsdfsd', 0, 0, 0, 0, '2020-05-16 15:41:11', '2020-05-16 15:41:11'),
(31, 'bibek', 'bibek.analogue51@gmail.com', 9845485841, 'Branding and Marketing', 'Professional Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 15:42:38', '2020-05-16 15:42:38'),
(32, 'asfs', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-16 15:46:52', '2020-05-16 15:46:52'),
(33, 'MIniyan', 'miniyanp@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Signage\n													\n													\n													I have my own Signage.\n													Yes, I\'d like a Signage.\n												\n													Business Card\n													\n													\n												\n													Leaflet', 14500, 'jksdjkdsjkjkds', 0, 0, 0, 0, '2020-05-16 15:47:59', '2020-05-16 15:47:59'),
(34, 'bibek chaudhary', 'bibek@outlook.com', 9845485841, 'Branding and Marketing', 'Standard Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-17 06:04:46', '2020-05-17 06:04:46'),
(35, 'bibek chaudhary', 'bibek@bibek.com', 9845485841, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Email Signature\n													\n													\n													I already have an email signature.\n													Sure, let\'s get this one too.\n												\n													Letterhead', 5500, 'Demo May 17', 0, 0, 0, 0, '2020-05-17 06:06:12', '2020-05-17 06:06:12'),
(36, 'sanam', 'sanam@gmail.com', 89998, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Logo\n													\n													\n													I don\'t need a logo.\n													I\'d like to upgrade my logo.\n												\n													Social Media Assets\n													\n													\n													What are Social Media Assets? \n													I need Social Media Assets.\n												\n													Merchandising\n													\n													\n													It\'s not necessary.\n													Help my brand speak for itself.\n												\n													Leaflet\n													\n													\n												\n													Envelope', 32000, 'from jasndfjanjkda', 0, 0, 0, 0, '2020-05-17 09:57:51', '2020-05-17 09:57:51'),
(37, 'nishan prasai', 'sitoula.analogue51@gmail.com', 9860793050, 'Branding and Marketing', 'Basic Type', NULL, NULL, NULL, 0, 0, 0, 0, '2020-05-17 09:58:44', '2020-05-17 09:58:44'),
(38, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Logo\n													\n													\n													I don\'t need a logo.\n													I\'d like to upgrade my logo.\n												\n													Social Media Assets\n													\n													\n													What are Social Media Assets? \n													I need Social Media Assets.\n												\n													Signage\n													\n													\n													I have my own Signage.\n													Yes, I\'d like a Signage.\n												\n													Merchandising\n													\n													\n													It\'s not necessary.\n													Help my brand speak for itself.\n												\n													Business Card', 28000, 'dfsfds', 0, 0, 0, 0, '2020-05-18 04:37:16', '2020-05-18 04:37:16'),
(39, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Business Card\n													\n													\n												\n													Cover Letter\n													\n													\n												\n													Leaflet\n													\n													\n												\n													Envelope', 16500, 'jkjkkjkjjk', 0, 0, 0, 0, '2020-05-18 04:39:39', '2020-05-18 04:39:39'),
(40, 'MIniyan', 'parajuliminiyan@gmail.com', 9845700611, 'Branding and Marketing Package', 'Tailor Type', ', \n													Style Guideline\n													\n													\n													This is mandatory\n												\n													Typography\n													\n													\n													This is mandatory\n												\n													Icon Pack\n													\n													\n													I don\'t need this.\n													Yes, I need an Icon Pack.\n												\n													Business Card\n													\n													\n												\n													Leaflet\n													\n													\n												\n													Envelope', 27500, 'hjhjhhjjjh', 0, 0, 0, 0, '2020-05-18 04:42:53', '2020-05-18 04:42:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consultancies`
--
ALTER TABLE `consultancies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `offices_email_unique` (`email`),
  ADD KEY `offices_user_id_foreign` (`user_id`);

--
-- Indexes for table `office_infos`
--
ALTER TABLE `office_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postcategories`
--
ALTER TABLE `postcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userdetails_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_packages`
--
ALTER TABLE `user_packages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `consultancies`
--
ALTER TABLE `consultancies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `office_infos`
--
ALTER TABLE `office_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `postcategories`
--
ALTER TABLE `postcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_packages`
--
ALTER TABLE `user_packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offices`
--
ALTER TABLE `offices`
  ADD CONSTRAINT `offices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD CONSTRAINT `userdetails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
