<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('userdetails');

        Schema::create('offices', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('office_code',12)->nullable();
            $table->string('office_name',150);
            $table->string('nick_name',50)->nullable();
            $table->date('registration_date')->nullable();
            $table->string('currency_code',12)->nullable();
            $table->string('po_box',128)->nullable();
            $table->string('address_line_1',128)->nullable();
            $table->string('address_line_2',128)->nullable();
            $table->string('street',50)->nullable();
            $table->string('city',50)->nullable();
            $table->string('state',50)->nullable();
            $table->string('zip_code',24)->nullable();
            $table->string('country',50)->nullable();
            $table->bigInteger('phone')->nullable();
            $table->string('fax',24)->nullable();
            $table->string('email',128)->unique();
            $table->string('url',50)->nullable();
            $table->string('logo')->nullable();
            $table->integer('parent_office_id')->nullable();
            $table->integer('registration_number')->length(100)->nullable();
            $table->integer('pan_number')->length(100)->nullable();
            $table->boolean('allow_transaction_posting')->default(0);
            $table->string('audit_user_id')->nullable();
            $table->timestamp('audit_ts')->useCurrent();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
