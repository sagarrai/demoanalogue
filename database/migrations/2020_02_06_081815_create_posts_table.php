<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('posts');
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            /* $table->string('slug')->nullable(); */
            $table->string('short_text')->nullable();
            $table->text('description')->nullable();
            $table->string('category')->nullable();
            $table->bigInteger('user_id')->unsigned();
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('meta_description')->nullable();
            //$table->string('keywords')->nullable();
            $table->string('feature_image')->nullable();
            $table->boolean('publish')->default(0);
            $table->bigInteger("office_id");
            $table->date('deadline')->nullable();
            $table->text('other_description')->nullable();
            $table->integer('no_of_opening')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
