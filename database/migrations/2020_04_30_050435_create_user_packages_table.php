<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_packages', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('email')->nullable();
            $table->bigInteger('phone')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_address')->nullable();
            $table->string('package_name')->nullable();
            $table->string('package_type')->nullable();
            $table->text('package_details')->nullable();
            $table->bigInteger('package_total_price')->nullable();
            $table->text('other_message')->nullable();
            $table->integer('contact')->default(0);
            $table->integer('template')->default(0);
            $table->integer('newsletter')->default(0);
            $table->integer('social_post')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_packages');
    }
}
