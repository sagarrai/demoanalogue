<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('userdetails');
        Schema::create('userdetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();  
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->integer('postal_code');
            $table->bigInteger('latitude');
            $table->bigInteger('longitude');
            $table->bigInteger('phone_number');
            $table->bigInteger("office_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userdetails');
    }
}
