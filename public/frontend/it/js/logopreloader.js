(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#FFFFFF").beginStroke().moveTo(-70,36.8).lineTo(-70,-36.8).lineTo(70,-36.8).lineTo(70,36.8).closePath();

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(-70,-36.8,140,73.6), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(45.3,50.2).curveTo(44.9,50,44.4,49.2).curveTo(36.2,34.3,24,12.8).lineTo(1.6,-26.7).lineTo(-20.4,8.9).lineTo(-44.9,48.9).curveTo(-45.6,50,-46.1,50.1).curveTo(-46.7,50.3,-47.7,49.6).curveTo(-50.6,47.4,-54.2,45.4).curveTo(-55.1,44.8,-55.3,44.3).lineTo(-55.3,44.1).curveTo(-55.3,43.6,-54.8,42.8).curveTo(-49.3,34,-33.6,8).lineTo(1.4,-49.5).curveTo(1.7,-50.1,2,-50.2).curveTo(2.3,-50.2,2.6,-49.5).lineTo(3.3,-48.3).curveTo(37.5,12.5,54.7,42.8).curveTo(55.4,44,55.2,44.6).curveTo(55,45.2,53.9,45.7).curveTo(50.5,47.4,46.6,49.8).curveTo(46,50.2,45.6,50.2).lineTo(45.3,50.2).closePath();
	this.shape.setTransform(-0.012,0.0175,0.3323,0.3323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-18.3,-16.6,36.7,33.3), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#00ABE9").beginStroke().moveTo(13.5,18.5).curveTo(12.1,17.5,10.7,14.7).curveTo(8.9,11.2,1.6,-1.6).curveTo(0.9,-2.8,0.6,-2.8).curveTo(0.3,-2.8,-0.4,-1.6).lineTo(-11.4,16.3).curveTo(-13.1,19.3,-16.4,19.1).lineTo(-21.5,19).curveTo(-22.8,19,-22.2,18).lineTo(-0.1,-18).curveTo(0.7,-19.3,1,-19.3).curveTo(1.2,-19.3,2,-17.9).lineTo(21.7,17.1).curveTo(22.5,18.6,22.4,18.8).curveTo(22.2,19,20.5,19).lineTo(18.1,19).curveTo(17.1,19.3,16.2,19.3).curveTo(14.7,19.3,13.5,18.5).closePath();
	this.shape.setTransform(0.0445,0.0294,0.3323,0.3323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-7.4,-6.4,14.9,12.9), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(-6.3,12).curveTo(-8.7,11.7,-10.2,9.8).curveTo(-11.8,8,-11.8,5.5).curveTo(-11.8,5.2,-11.6,4.9).lineTo(-11.2,4.2).lineTo(-10.3,5.2).lineTo(-10.1,5.6).lineTo(-10.1,6.2).curveTo(-9.4,9.8,-6.8,11.2).curveTo(-4.2,12.5,-0.8,11).curveTo(1.5,10,2.7,7.2).curveTo(4,4.3,3.4,1.4).curveTo(2.8,-1.4,0.9,-3).curveTo(-0,-3.8,-1.6,-4.3).curveTo(-3.3,-4.9,-4.8,-4.7).curveTo(-5.9,-4.6,-7.2,-3.9).curveTo(-8.6,-3.1,-9.3,-2).lineTo(-8.7,-3).lineTo(-8.5,-3.5).lineTo(-8.6,-3.4).curveTo(-9.6,-2.3,-9.5,-1.7).lineTo(-10.2,-2.4).curveTo(-9.7,-8,-9.3,-10.8).curveTo(-9.2,-11.1,-8.7,-11.4).curveTo(-8.2,-11.8,-7.8,-11.8).curveTo(-4.8,-12,1.4,-12.1).curveTo(1.5,-12.1,1.5,-12.1).curveTo(1.5,-12.1,1.6,-12.1).curveTo(1.6,-12,1.7,-12).curveTo(1.7,-11.9,1.8,-11.8).lineTo(2,-11.3).lineTo(2,-10.3).curveTo(1.9,-9.9,1.2,-9.9).lineTo(-7.7,-9.9).curveTo(-9.1,-10,-8.9,-8.8).lineTo(-8.5,-6.5).curveTo(-8.2,-5.2,-8.3,-4.3).lineTo(-8.5,-3.5).curveTo(-7.2,-4.8,-5.1,-5.2).curveTo(-1.9,-5.8,1.4,-3.9).curveTo(4.7,-1.9,5.5,1.2).curveTo(6.3,4.5,4.6,7.5).curveTo(2.9,10.6,-0.5,11.6).curveTo(-2.3,12.1,-4.4,12.1).lineTo(-6.3,12).closePath().moveTo(8.5,11.2).lineTo(8.9,10.3).curveTo(9.2,9.8,9.2,9.4).lineTo(9.3,-8.4).curveTo(9.3,-9.5,9,-9.9).curveTo(8.6,-10.3,7.6,-10.2).curveTo(7.4,-10.1,6.9,-10.4).lineTo(7.6,-10.8).lineTo(9,-11).lineTo(11.2,-11.6).lineTo(11.4,-9.1).lineTo(11.4,-0.8).lineTo(11.4,-0.8).lineTo(11.4,9.5).lineTo(11.8,11.5).lineTo(8.5,11.5).closePath();
	this.shape.setTransform(0.0152,0.014,0.3323,0.3323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(-3.9,-4,7.9,8.1), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(16.1,25.3).lineTo(16.2,8.9).lineTo(16.2,6.4).lineTo(16.2,-15.6).curveTo(16.2,-16.9,16.8,-17.6).curveTo(17.3,-18.3,17.8,-18.3).curveTo(18.4,-18.3,19,-17.7).curveTo(19.6,-16.9,19.6,-15.7).lineTo(19.5,25.3).closePath().moveTo(6.9,25.3).lineTo(7,16.2).lineTo(7,12.5).lineTo(7,-15.4).curveTo(7,-16.2,7.2,-16.8).curveTo(7.4,-17.5,8,-18).curveTo(8.3,-18.3,8.7,-18.4).curveTo(9.1,-18.4,9.4,-18).curveTo(10.4,-17.3,10.4,-16).lineTo(10.3,-4).lineTo(10.3,-2.3).lineTo(10.3,25.3).closePath().moveTo(-1.6,25.3).lineTo(-1.7,-15.5).curveTo(-1.7,-17.1,-0.7,-18).curveTo(-0.3,-18.3,0.1,-18.3).curveTo(0.4,-18.3,0.8,-18).curveTo(1.8,-17.3,1.8,-15.8).lineTo(1.7,-2.9).lineTo(1.7,0.9).lineTo(1.7,25.3).closePath().moveTo(-10.9,25.3).lineTo(-10.9,-8.2).lineTo(-11,-15.9).curveTo(-11,-17.3,-9.9,-18.1).curveTo(-9.2,-18.6,-8.5,-18).curveTo(-7.6,-17.3,-7.6,-15.8).lineTo(-7.6,0.2).lineTo(-7.6,0.2).lineTo(-7.6,16.2).lineTo(-7.6,16.5).lineTo(-7.6,25.3).closePath().moveTo(-19.2,25.3).lineTo(-19.2,-2.7).lineTo(-19.2,-3.9).lineTo(-19.3,-15.8).curveTo(-19.3,-17.2,-18.5,-17.9).curveTo(-17.6,-18.7,-16.7,-17.8).curveTo(-15.8,-17,-15.8,-15.5).lineTo(-15.8,-9.6).lineTo(-15.8,-9.6).lineTo(-15.8,25.3).closePath().moveTo(-20.2,-21.1).curveTo(-20.7,-21.1,-20.8,-21.6).lineTo(-20.8,-24.8).curveTo(-20.6,-25.3,-20,-25.3).lineTo(20.3,-25.3).curveTo(20.8,-25.2,20.8,-24.7).lineTo(20.8,-21.8).curveTo(20.8,-21.4,20.5,-21.2).curveTo(20.3,-21.1,19.9,-21.1).closePath();
	this.shape.setTransform(0,0.0229);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-20.8,-25.2,41.6,50.5), null);


(lib.gradient = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginLinearGradientFill(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-14,0,14,0).beginStroke().moveTo(-14,36.8).lineTo(-14,-36.8).lineTo(14,-36.8).lineTo(14,36.8).closePath();

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.gradient, new cjs.Rectangle(-14,-36.8,28,73.6), null);


(lib.future = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(22.4,5).curveTo(20.9,4.4,20.5,3.3).curveTo(19.8,1.5,20.6,-0.2).curveTo(21.1,-1.3,22,-1.7).curveTo(23,-2.2,24,-1.9).curveTo(25.3,-1.6,25.7,-0.4).curveTo(25.9,0,26.2,1.3).curveTo(26.3,2,25.5,2).lineTo(22.1,2).curveTo(21.6,2,21.5,2.2).curveTo(21.4,2.4,21.5,2.8).curveTo(21.8,3.5,22.4,3.9).curveTo(23,4.3,23.8,4.2).lineTo(25.1,4.1).curveTo(25.2,4.1,25.2,4.1).curveTo(25.3,4.1,25.3,4.1).curveTo(25.4,4.1,25.4,4.1).curveTo(25.4,4.1,25.4,4.2).curveTo(25.4,4.2,25.4,4.2).curveTo(25.5,4.3,25.5,4.3).curveTo(25.4,4.4,25.4,4.4).curveTo(25.4,4.5,25.4,4.5).lineTo(25,5).curveTo(24.5,5.2,23.8,5.2).curveTo(23.2,5.2,22.4,5).closePath().moveTo(22.3,-0.7).curveTo(21.7,-0.4,21.6,0.1).curveTo(21.4,0.5,21.6,0.7).curveTo(21.8,0.9,22.1,1).lineTo(24.5,1).curveTo(24.6,1,24.7,1).curveTo(24.7,0.9,24.8,0.9).curveTo(24.8,0.9,24.9,0.9).curveTo(24.9,0.8,24.9,0.8).lineTo(25,0.5).curveTo(24.9,-0.2,24.5,-0.6).curveTo(24.1,-1,23.4,-1).lineTo(23.3,-1).curveTo(22.7,-1,22.3,-0.7).closePath().moveTo(-4.8,5).curveTo(-5.4,4.6,-5.5,3.9).lineTo(-5.6,2.7).lineTo(-5.6,-0.3).curveTo(-5.5,-1,-6.3,-1).curveTo(-6.7,-1,-6.8,-1.3).curveTo(-6.9,-1.3,-6.9,-1.4).curveTo(-6.9,-1.4,-6.9,-1.5).curveTo(-6.9,-1.5,-6.9,-1.6).curveTo(-6.9,-1.6,-6.8,-1.7).curveTo(-6.7,-1.9,-6.4,-1.9).curveTo(-5.8,-2,-5.7,-2.1).curveTo(-5.6,-2.2,-5.6,-2.8).curveTo(-5.5,-3.4,-5.4,-3.5).curveTo(-5.2,-3.8,-4.8,-3.8).lineTo(-4.6,-3.6).lineTo(-4.5,-3.2).lineTo(-4.5,-2.5).curveTo(-4.5,-1.8,-3.8,-1.9).lineTo(-3.6,-1.9).lineTo(-3,-1.4).lineTo(-3.3,-1.2).lineTo(-3.6,-1).curveTo(-4.3,-0.9,-4.4,-0.7).curveTo(-4.5,-0.6,-4.5,0.1).lineTo(-4.5,3.1).curveTo(-4.5,3.7,-4.3,3.9).curveTo(-4.2,4.1,-3.6,4.3).lineTo(-3.3,4.4).lineTo(-2.9,4.6).lineTo(-3.3,4.9).lineTo(-3.6,5.2).lineTo(-4,5.2).curveTo(-4.5,5.2,-4.8,5).closePath().moveTo(3.2,4.9).curveTo(2,4.3,2,2.8).lineTo(2,-0.6).lineTo(2,-1).lineTo(2.5,-1.8).lineTo(2.8,-1.4).lineTo(2.9,-1).lineTo(3,1.8).lineTo(3.1,3.1).curveTo(3.4,4.2,4.4,4.3).curveTo(5,4.3,5.5,4).curveTo(6,3.7,6.1,3.2).lineTo(6.3,1.6).lineTo(6.5,-0.1).lineTo(6.5,-1).curveTo(6.5,-1.7,7,-1.7).curveTo(7,-1.7,7,-1.7).curveTo(7.1,-1.7,7.1,-1.7).curveTo(7.2,-1.6,7.2,-1.6).curveTo(7.2,-1.5,7.3,-1.5).lineTo(7.5,-1).lineTo(7.5,1.7).lineTo(7.5,1.7).lineTo(7.5,4.1).curveTo(7.5,4.6,7.4,4.7).curveTo(7,5.3,6.7,4.8).curveTo(6.6,4.7,6.6,4.6).curveTo(6.5,4.6,6.5,4.5).curveTo(6.4,4.5,6.4,4.5).curveTo(6.4,4.5,6.3,4.5).curveTo(6.1,4.4,5.8,4.7).curveTo(5.2,5.2,4.4,5.2).curveTo(3.8,5.2,3.2,4.9).closePath().moveTo(-16,4.8).curveTo(-16.9,4.2,-16.9,2.8).lineTo(-16.9,-1).lineTo(-16.9,-1).lineTo(-16.7,-1.5).curveTo(-16.7,-1.6,-16.6,-1.7).curveTo(-16.6,-1.7,-16.6,-1.8).curveTo(-16.6,-1.8,-16.5,-1.8).curveTo(-16.5,-1.8,-16.5,-1.8).curveTo(-16.2,-1.8,-16,-1.6).curveTo(-15.9,-1.3,-15.9,-1).lineTo(-15.9,2.1).lineTo(-15.8,2.9).curveTo(-15.7,4.1,-14.6,4.2).curveTo(-14.1,4.3,-13.6,4.1).curveTo(-13.1,3.8,-12.9,3.3).curveTo(-12.6,2.7,-12.6,2.1).lineTo(-12.5,-1.1).lineTo(-12.4,-1.7).curveTo(-12.4,-1.7,-12.3,-1.8).curveTo(-12.3,-1.8,-12.2,-1.8).curveTo(-12.2,-1.9,-12.1,-1.9).curveTo(-12.1,-1.9,-12,-1.9).curveTo(-11.4,-1.9,-11.4,-1.1).lineTo(-11.4,1.5).lineTo(-11.4,1.5).lineTo(-11.4,4.5).curveTo(-11.4,4.9,-11.5,5).curveTo(-11.7,5.1,-12,4.9).curveTo(-12.4,4.6,-12.9,4.7).lineTo(-13.7,4.9).curveTo(-14.3,5.1,-14.8,5.1).curveTo(-15.5,5.1,-16,4.8).closePath().moveTo(13.4,5.1).curveTo(12.8,5.1,12.8,4.6).lineTo(12.8,-1.3).curveTo(12.8,-1.9,13.4,-1.8).lineTo(14.8,-1.8).lineTo(15.1,-1.9).lineTo(15.4,-1.9).lineTo(16.2,-1.5).lineTo(15.9,-1.2).lineTo(15.7,-1).lineTo(15.5,-0.9).lineTo(15.2,-0.8).curveTo(14,-0.5,14,0.6).lineTo(13.9,4.4).lineTo(13.9,4.9).curveTo(13.8,4.9,13.8,5).curveTo(13.8,5,13.7,5).curveTo(13.7,5,13.6,5.1).curveTo(13.6,5.1,13.5,5.1).lineTo(13.4,5.1).closePath().moveTo(-24.3,5).curveTo(-24.4,5,-24.5,5).curveTo(-24.6,5,-24.6,5).curveTo(-24.7,4.9,-24.7,4.9).curveTo(-24.8,4.9,-24.8,4.8).lineTo(-24.9,4.3).lineTo(-24.9,-0).lineTo(-25,-0.7).curveTo(-25.1,-1,-25.6,-1).curveTo(-25.8,-1,-26.2,-1.4).lineTo(-25.9,-1.7).lineTo(-25.6,-1.9).curveTo(-25.2,-1.9,-25,-2.2).lineTo(-24.8,-2.9).curveTo(-24.8,-3.7,-24.7,-4.1).curveTo(-24.5,-4.7,-24,-5).curveTo(-23.4,-5.3,-22.8,-5.2).lineTo(-22.5,-5).lineTo(-22.2,-4.8).lineTo(-22.7,-4.3).lineTo(-23,-4.1).curveTo(-23.5,-3.9,-23.8,-3.3).curveTo(-24,-2.7,-23.7,-2.2).curveTo(-23.7,-2.2,-23.7,-2.1).curveTo(-23.6,-2.1,-23.6,-2.1).curveTo(-23.6,-2,-23.5,-2).curveTo(-23.4,-2,-23.4,-1.9).lineTo(-23.2,-1.9).lineTo(-23,-1.9).lineTo(-22.6,-1.7).lineTo(-22.3,-1.4).curveTo(-22.3,-0.9,-23.1,-1).curveTo(-23.8,-1.1,-23.8,-0.2).lineTo(-23.8,2.1).lineTo(-23.8,4.3).lineTo(-23.9,4.8).curveTo(-23.9,4.9,-23.9,4.9).curveTo(-24,4.9,-24,5).curveTo(-24.1,5,-24.1,5).curveTo(-24.2,5,-24.2,5).lineTo(-24.3,5).closePath();
	this.shape.setTransform(0.0363,0.0375);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.future, new cjs.Rectangle(-26.1,-5.2,52.3,10.5), null);


(lib._for = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(-0.7,5).curveTo(-2.6,4.5,-2.9,2.1).lineTo(-2.9,1.6).curveTo(-2.9,-0.1,-1.9,-1.1).curveTo(-1,-2.1,0.5,-2.1).curveTo(2,-2,2.9,-0.7).curveTo(3.6,0.4,3.6,1.7).curveTo(3.5,3,2.7,4.1).curveTo(2.1,4.8,1.2,5.1).lineTo(0.3,5.2).curveTo(-0.2,5.2,-0.7,5).closePath().moveTo(-1.2,-0.3).lineTo(-1.6,0.6).lineTo(-2,1.5).lineTo(-1.7,2.5).lineTo(-1.3,3.3).curveTo(-0.7,4.3,0.3,4.3).curveTo(1.4,4.2,2,3.3).curveTo(2.9,1.7,2,-0.1).curveTo(1.5,-1,0.5,-1.1).lineTo(0.4,-1.1).curveTo(-0.6,-1.1,-1.2,-0.3).closePath().moveTo(8.8,5.1).curveTo(8.3,5.1,8.3,4.5).lineTo(8.3,1.8).lineTo(8.3,-0.8).curveTo(8.3,-1.2,8.5,-1.6).lineTo(8.7,-1.7).lineTo(8.9,-1.7).curveTo(9.2,-1.5,9.6,-1.5).lineTo(10.2,-1.7).lineTo(11,-2).lineTo(11.3,-1.9).lineTo(11.7,-1.7).lineTo(11.5,-1.4).lineTo(11.4,-1.1).lineTo(10.7,-0.9).curveTo(9.6,-0.5,9.6,0.5).lineTo(9.5,3.9).lineTo(9.4,4.6).curveTo(9.3,5.1,8.9,5.1).lineTo(8.8,5.1).closePath().moveTo(-10.2,4.7).lineTo(-10.4,4.3).lineTo(-10.4,2.6).lineTo(-10.4,-0.3).curveTo(-10.4,-0.9,-11,-1).lineTo(-11.4,-1.2).lineTo(-11.7,-1.5).lineTo(-11.4,-1.7).lineTo(-11.1,-1.9).curveTo(-10.5,-2,-10.4,-2.7).lineTo(-10.3,-4).curveTo(-10.1,-4.7,-9.4,-5).curveTo(-8.8,-5.4,-8.2,-5.2).curveTo(-7.9,-5.1,-7.7,-4.7).lineTo(-8,-4.4).lineTo(-8.2,-4.2).lineTo(-8.7,-4.1).curveTo(-8.8,-4,-8.9,-4).curveTo(-8.9,-4,-9,-3.9).curveTo(-9,-3.9,-9,-3.9).curveTo(-9.1,-3.8,-9.1,-3.8).curveTo(-9.2,-3.5,-9.3,-2.4).lineTo(-9.2,-2.2).lineTo(-9,-2).lineTo(-8,-1.7).lineTo(-7.7,-1.5).lineTo(-7.9,-1.3).lineTo(-8,-1.2).curveTo(-8.4,-1,-8.6,-1).curveTo(-9.1,-1,-9.2,-0.8).curveTo(-9.4,-0.6,-9.4,-0.2).lineTo(-9.4,3.8).lineTo(-9.4,4.4).lineTo(-9.7,4.7).lineTo(-10,5.1).lineTo(-10.2,4.7).closePath();
	this.shape.setTransform(0.025,0.0079);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib._for, new cjs.Rectangle(-11.6,-5.2,23.299999999999997,10.5), null);


(lib.excellence = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(39.5,4.6).curveTo(38.6,3.8,38.2,2.9).curveTo(37.9,1.7,38.2,0.5).curveTo(38.5,-0.7,39.4,-1.4).curveTo(40.7,-2.4,42.1,-1.9).curveTo(43.5,-1.5,43.9,0.1).lineTo(44,1).curveTo(44.1,1.5,44,1.7).curveTo(43.8,2,43.2,2).lineTo(40,2).curveTo(39.6,2,39.4,2.2).curveTo(39.3,2.4,39.4,2.8).curveTo(39.6,3.4,40.2,3.9).curveTo(40.8,4.3,41.5,4.2).lineTo(42.7,4).lineTo(43,4.1).lineTo(43.4,4.2).lineTo(43.2,4.7).lineTo(42.9,4.9).curveTo(42.2,5.2,41.5,5.2).curveTo(40.4,5.2,39.5,4.6).closePath().moveTo(40.1,-0.7).curveTo(39.6,-0.4,39.4,0.2).curveTo(39.2,0.9,39.9,0.9).lineTo(42.4,1).curveTo(42.9,0.9,42.9,0.4).curveTo(42.8,-0.3,42.3,-0.6).curveTo(41.9,-1.1,41.2,-1.1).curveTo(40.6,-1.1,40.1,-0.7).closePath().moveTo(30.6,4.7).curveTo(29.2,3.9,28.9,2.2).curveTo(28.5,0.3,29.9,-1.1).curveTo(31.3,-2.4,33.1,-2).lineTo(33.7,-1.6).curveTo(33.7,-1.6,33.8,-1.6).curveTo(33.8,-1.5,33.9,-1.5).curveTo(33.9,-1.4,33.9,-1.4).curveTo(34,-1.3,34,-1.3).curveTo(34,-1.2,33.9,-1.2).curveTo(33.9,-1.1,33.9,-1.1).curveTo(33.8,-1,33.8,-1).curveTo(33.7,-1,33.6,-1).lineTo(33.1,-1).lineTo(32.8,-1).lineTo(32.5,-1).curveTo(31.7,-1,31.3,-0.8).curveTo(30.7,-0.7,30.4,-0.1).curveTo(29.5,1.7,30.5,3.2).curveTo(31.2,4.4,32.7,4.1).lineTo(33.5,4.1).lineTo(33.7,4.2).lineTo(34,4.5).lineTo(33.8,4.8).lineTo(33.6,5).curveTo(33,5.2,32.4,5.2).curveTo(31.4,5.2,30.6,4.7).closePath().moveTo(-23.4,3.8).curveTo(-24.7,1.9,-23.7,-0.2).curveTo(-22.8,-2,-20.5,-2.1).lineTo(-20.1,-2.1).lineTo(-19,-1.4).lineTo(-19.6,-1.1).curveTo(-20,-0.9,-20.2,-1).curveTo(-21.5,-1,-22.2,-0.4).curveTo(-22.9,0.3,-23,1.5).curveTo(-23,2.7,-22.2,3.5).curveTo(-21.4,4.2,-20.2,4.2).lineTo(-19.6,4.2).lineTo(-18.9,4.4).lineTo(-19.4,5).curveTo(-19.6,5.2,-19.9,5.1).lineTo(-20.7,5.2).curveTo(-22.4,5.2,-23.4,3.8).closePath().moveTo(-13,4.8).curveTo(-14.7,4,-14.7,1.9).lineTo(-14.7,1.6).lineTo(-14.8,1.6).lineTo(-14.6,0.3).curveTo(-14.3,-1,-13.2,-1.7).curveTo(-12,-2.3,-10.7,-2).curveTo(-9.7,-1.5,-9.3,-0.5).lineTo(-8.9,1.1).curveTo(-8.9,1.6,-9,1.8).curveTo(-9.2,2,-9.7,2).lineTo(-12.8,2).curveTo(-13.4,2,-13.5,2.2).curveTo(-13.6,2.3,-13.4,2.9).curveTo(-13.1,3.6,-12.3,4).curveTo(-11.5,4.4,-10.6,4.2).lineTo(-10.2,4).lineTo(-9.4,4.2).lineTo(-9.6,4.6).lineTo(-9.9,4.9).curveTo(-10.7,5.2,-11.4,5.2).curveTo(-12.2,5.2,-13,4.8).closePath().moveTo(-12,-1).curveTo(-12.6,-0.9,-13,-0.5).curveTo(-13.5,-0,-13.5,0.5).curveTo(-13.6,0.9,-13.2,1).lineTo(-11.9,1).lineTo(-11.9,0.9).lineTo(-10.5,0.9).curveTo(-9.9,0.9,-10.1,0.2).curveTo(-10.2,-0.4,-10.8,-0.7).curveTo(-11.2,-1,-11.7,-1).lineTo(-12,-1).closePath().moveTo(-42.2,4.9).curveTo(-43.8,4.2,-44,2.6).curveTo(-44.1,1.3,-43.8,-0.1).curveTo(-43.6,-0.9,-42.8,-1.5).curveTo(-42,-2.1,-41,-2.1).curveTo(-39,-2,-38.5,0.1).lineTo(-38.3,1.3).curveTo(-38.2,2,-38.9,2).lineTo(-42.5,2).curveTo(-43.1,2,-42.9,2.6).curveTo(-42.7,3.5,-41.9,4).curveTo(-41.1,4.4,-40.1,4.1).lineTo(-39.4,4).lineTo(-38.8,4.2).lineTo(-39,4.6).lineTo(-39.2,4.9).curveTo(-40,5.2,-40.7,5.2).curveTo(-41.5,5.2,-42.2,4.9).closePath().moveTo(-42.2,-0.7).curveTo(-42.7,-0.4,-42.9,0.1).curveTo(-43.2,0.9,-42.4,1).lineTo(-40.1,1).curveTo(-39.2,0.8,-39.5,-0.1).curveTo(-39.9,-1,-41.1,-1.1).curveTo(-41.7,-1.1,-42.2,-0.7).closePath().moveTo(9.8,4.9).curveTo(8.3,4.3,8,2.6).curveTo(7.8,1.5,8.1,0.2).curveTo(8.4,-1.1,9.6,-1.7).curveTo(10.7,-2.3,12,-1.9).curveTo(13,-1.5,13.3,-0.6).lineTo(13.8,1.3).curveTo(13.9,1.9,13.1,2).lineTo(11.2,2).lineTo(9.5,2).curveTo(9.4,2,9.4,2).curveTo(9.3,2,9.3,2).curveTo(9.2,2,9.2,2).curveTo(9.2,2.1,9.2,2.1).curveTo(9.1,2.1,9.1,2.2).curveTo(9.1,2.2,9.1,2.2).curveTo(9.1,2.3,9.1,2.4).curveTo(9.1,2.4,9.1,2.5).curveTo(9.2,3.3,9.9,3.9).curveTo(10.7,4.3,11.6,4.2).lineTo(12.7,4).lineTo(12.9,4.1).lineTo(13.2,4.2).lineTo(13,4.7).lineTo(12.8,4.9).curveTo(12,5.2,11.2,5.2).curveTo(10.5,5.2,9.8,4.9).closePath().moveTo(10.7,-1).curveTo(10,-0.9,9.5,-0.5).curveTo(9,0,9.1,0.6).lineTo(9.2,0.8).lineTo(9.4,1).lineTo(10.7,1).lineTo(11.9,1).curveTo(12.4,0.9,12.5,0.7).curveTo(12.6,0.6,12.5,0.2).curveTo(12.4,-0.4,11.9,-0.7).curveTo(11.5,-1,11,-1).lineTo(10.7,-1).closePath().moveTo(2.3,5).curveTo(2.1,4.8,2,4.3).lineTo(2.1,1.6).lineTo(2.1,-4.2).lineTo(2.1,-4.9).curveTo(2.2,-5.2,2.6,-5.2).curveTo(2.7,-5.2,2.7,-5.2).curveTo(2.8,-5.2,2.9,-5.2).curveTo(2.9,-5.2,3,-5.1).curveTo(3,-5.1,3.1,-5).curveTo(3.2,-4.8,3.2,-4.2).lineTo(3.2,-0.1).lineTo(3.2,4.3).lineTo(3.1,4.7).curveTo(3,4.8,3,4.8).curveTo(3,4.9,2.9,4.9).curveTo(2.9,4.9,2.9,5).curveTo(2.8,5,2.8,5).lineTo(2.5,5.1).lineTo(2.3,5).closePath().moveTo(18.5,4.4).lineTo(18.5,-1.2).curveTo(18.5,-1.7,18.7,-1.8).lineTo(19,-1.7).lineTo(19.3,-1.6).curveTo(19.5,-1.4,19.7,-1.4).lineTo(20.2,-1.5).curveTo(21.6,-2.4,22.8,-1.8).curveTo(23.9,-1.2,24,0.4).lineTo(24.1,4.2).curveTo(24.1,5.1,23.6,5).curveTo(23,5,23,4.2).lineTo(23,0.5).curveTo(22.9,-0.1,22.7,-0.5).curveTo(22.5,-0.9,21.9,-1).curveTo(21.2,-1.1,20.7,-0.9).curveTo(20.1,-0.6,19.9,0.1).curveTo(19.7,0.7,19.6,2.1).lineTo(19.6,3.1).lineTo(19.6,4).lineTo(19.6,4.5).curveTo(19.5,5,19,5).curveTo(18.5,5.1,18.5,4.4).closePath().moveTo(-4.2,4.6).lineTo(-4.3,-4.1).lineTo(-4.2,-4.9).curveTo(-4.2,-5.2,-3.7,-5.2).curveTo(-3.3,-5.2,-3.2,-5).curveTo(-3.1,-4.8,-3.1,-4.1).lineTo(-3.1,4.6).curveTo(-3.1,5,-3.7,5).curveTo(-4.2,5,-4.2,4.6).closePath().moveTo(-33.9,4.9).lineTo(-34,4.7).lineTo(-33.9,4.4).lineTo(-32.5,2.3).curveTo(-32.1,1.6,-32.1,1.4).curveTo(-32.1,1,-32.6,0.4).curveTo(-33.3,-0.5,-33.6,-1.3).curveTo(-33.7,-1.3,-33.7,-1.4).curveTo(-33.8,-1.5,-33.8,-1.5).curveTo(-33.8,-1.6,-33.8,-1.6).curveTo(-33.8,-1.7,-33.7,-1.7).curveTo(-33.6,-1.9,-33.1,-1.9).lineTo(-32.8,-1.7).lineTo(-32.5,-1.4).lineTo(-31.5,0.1).lineTo(-31.2,0.4).curveTo(-31.2,0.4,-31.2,0.3).curveTo(-31.2,0.3,-31.1,0.3).curveTo(-31.1,0.3,-31.1,0.2).curveTo(-31.1,0.2,-31,0.1).lineTo(-30.1,-1.4).curveTo(-29.8,-1.8,-29.4,-1.9).lineTo(-29.1,-1.9).lineTo(-28.8,-1.7).lineTo(-29,-1.3).lineTo(-30,0.4).curveTo(-30.5,1.1,-30.5,1.3).curveTo(-30.5,1.6,-30,2.2).lineTo(-28.6,4.3).lineTo(-28.5,4.6).lineTo(-28.5,4.9).lineTo(-28.9,4.9).lineTo(-29.2,4.9).curveTo(-29.7,4.6,-29.9,4.3).lineTo(-30.9,2.7).lineTo(-31.2,2.3).curveTo(-31.3,2.3,-31.3,2.4).curveTo(-31.3,2.4,-31.4,2.4).curveTo(-31.4,2.5,-31.5,2.5).curveTo(-31.5,2.6,-31.5,2.7).lineTo(-32.6,4.3).curveTo(-32.9,4.7,-33.3,4.9).lineTo(-33.6,5).lineTo(-33.9,4.9).closePath();
	this.shape.setTransform(-0.005,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.excellence, new cjs.Rectangle(-44,-5.2,88.1,10.5), null);


(lib.column = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(-4.5,70.1).curveTo(-5,66.7,-5,65).lineTo(-5.1,-40.8).curveTo(-5,-45.6,-2.1,-48).curveTo(-0.9,-49.1,0.1,-49.1).curveTo(1.2,-49.2,2.5,-48.2).curveTo(5.4,-45.9,5.4,-41.6).lineTo(5.2,-2.8).lineTo(5,58.8).curveTo(5,61.8,4.1,63.6).curveTo(3,65.7,0.6,66.9).curveTo(-0.5,67.4,-1.8,68.4).lineTo(-4.1,70.1).closePath().moveTo(-24.4,57.3).lineTo(-25.1,56.7).curveTo(-30.1,47.9,-31.4,45.4).curveTo(-32.5,43.1,-32.5,40.1).lineTo(-33,-41.7).curveTo(-33.1,-46,-29.8,-48.4).curveTo(-27.7,-49.9,-25.6,-48.3).curveTo(-22.8,-46.1,-22.8,-41.6).lineTo(-22.8,6.7).lineTo(-22.7,6.7).lineTo(-22.7,54.6).lineTo(-22.8,56).lineTo(-23.6,57.9).lineTo(-24.4,57.3).closePath().moveTo(21.3,52.2).lineTo(21.2,-40.3).curveTo(21.1,-42.9,21.7,-44.6).curveTo(22.4,-46.7,24.1,-48.2).curveTo(25.2,-49.2,26.2,-49.2).curveTo(27.3,-49.2,28.5,-48.3).curveTo(31.3,-46.1,31.3,-42).lineTo(31.2,-6.2).lineTo(30.9,44.3).curveTo(30.9,46.6,30.3,48.1).curveTo(29.5,49.7,27.6,50.7).curveTo(26.8,51.1,25.5,52).lineTo(23.5,53.5).curveTo(22.8,53.9,22.3,53.9).curveTo(21.3,53.9,21.3,52.2).closePath().moveTo(49,35.7).lineTo(48.8,31.5).lineTo(48.8,-3.5).lineTo(48.7,-3.5).lineTo(48.7,-40.9).curveTo(48.7,-44.8,50.5,-47.1).curveTo(51.9,-49,53.8,-49.1).curveTo(55.5,-49.1,57.1,-47.2).curveTo(59,-44.7,59,-41.3).lineTo(58.5,-1.7).lineTo(58.2,27.4).curveTo(58.2,31.4,55,33.3).lineTo(50.4,36.4).curveTo(49.9,36.7,49.6,36.7).curveTo(49.1,36.7,49,35.7).closePath().moveTo(-50.5,16.7).curveTo(-56.2,9.1,-57.4,0.7).curveTo(-57.8,-2.9,-57.8,-5.8).lineTo(-58,-41.6).curveTo(-58,-45.8,-55.6,-47.8).curveTo(-52.8,-50.2,-50.2,-47.6).curveTo(-47.6,-45.1,-47.6,-40.8).lineTo(-47.7,15.7).curveTo(-47.7,16.4,-47.8,17.6).lineTo(-48.3,19.5).closePath().moveTo(-60.7,-57.5).curveTo(-62.4,-57.4,-62.5,-59.1).lineTo(-62.6,-68.6).curveTo(-62.1,-70.2,-60.1,-70.1).lineTo(60.9,-70.1).curveTo(62.6,-70,62.6,-68.3).lineTo(62.6,-59.6).curveTo(62.5,-58.4,61.9,-57.9).curveTo(61.2,-57.4,59.7,-57.4).closePath();
	this.shape.setTransform(0.0004,-0.1205,0.3323,0.3323);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.beginFill("#0F0F0F").beginStroke().moveTo(-1.8,-14.2).lineTo(1.5,-14.2).lineTo(1.5,13.5).lineTo(-1.8,15.6).closePath().moveTo(-11.1,6.8).lineTo(-11.1,-14.2).lineTo(-7.8,-13.8).lineTo(-7.7,12.1).closePath().moveTo(6.9,-14.1).lineTo(10.1,-14.2).lineTo(10.1,8.1).lineTo(6.8,10.2).closePath().moveTo(16,-14).lineTo(19.3,-14.2).lineTo(19.3,2.3).lineTo(16,4.5).closePath().moveTo(-19.3,-6.4).lineTo(-19.3,-14.2).lineTo(-16,-15.6).lineTo(-16,-1).closePath();
	this.shape_1.setTransform(0.175,7.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.column, new cjs.Rectangle(-20.8,-23.4,41.6,46.9), null);


(lib.analogueinc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.beginFill("#0F0F0F").beginStroke().moveTo(-13.7,61.6).curveTo(-20.5,59.7,-24.7,53.7).curveTo(-28.2,48.8,-27.1,43.2).curveTo(-26,37.6,-21,34.3).curveTo(-20.2,33.7,-20.1,33.4).curveTo(-19.9,33,-20.8,32.3).curveTo(-26.7,28.1,-25.2,20.5).curveTo(-23.8,13.2,-17.7,10.8).curveTo(-16.2,10.3,-16.1,9.9).curveTo(-16.1,9.5,-17.2,8.3).curveTo(-22.9,2.6,-23.7,-4.8).curveTo(-24.4,-11,-21.6,-19.1).curveTo(-19.8,-24.5,-12.7,-28).curveTo(1.2,-35,15.7,-29.8).lineTo(18.5,-28.9).lineTo(18.4,-31.5).curveTo(18.4,-33.1,18.6,-34.3).curveTo(19.6,-38.7,24.1,-41.7).curveTo(28.6,-44.7,32.9,-43.9).curveTo(34.3,-43.6,35.4,-42.7).curveTo(37.2,-41.3,36.4,-38.6).curveTo(35.8,-36.8,33.4,-36.7).lineTo(26.1,-36.7).curveTo(22.5,-36.6,20.8,-33.5).curveTo(19.9,-31.9,20.1,-30.1).curveTo(20.4,-28.4,21.6,-27).lineTo(22.3,-26.3).curveTo(28.4,-21.7,30,-15).curveTo(31.5,-9.1,29.6,-1.4).curveTo(26.4,11.3,11.7,14.8).curveTo(-0.3,17.8,-10.6,12.5).curveTo(-12.8,11.4,-14.5,11.6).curveTo(-16.2,11.8,-17.9,13.4).curveTo(-22.5,17.6,-20.8,22.6).curveTo(-19.1,27.3,-14.5,27.5).lineTo(3.5,28.4).lineTo(3.5,28.1).lineTo(17.4,28).curveTo(25.3,27.8,30.1,34.8).curveTo(34.9,41.7,31.9,49.1).curveTo(27.8,59.2,16.9,61.9).curveTo(9.8,63.7,2.5,63.7).curveTo(-5.5,63.7,-13.7,61.6).closePath().moveTo(-19.6,37.6).curveTo(-22.5,42.4,-21.4,48.3).curveTo(-20.4,54.2,-15.9,57.4).curveTo(-12,60.2,-6.5,61.4).curveTo(-2.7,62.2,3.7,62.5).curveTo(5.8,62.6,8.9,62).lineTo(14.1,61.1).curveTo(21.1,60.1,25.5,55.7).curveTo(30.3,50.8,29.1,44.5).curveTo(28.3,40.4,25.1,37.9).curveTo(22,35.5,17.5,35.4).lineTo(1.9,35.4).lineTo(-13.7,34.8).lineTo(-14.1,34.8).curveTo(-17.9,34.8,-19.6,37.6).closePath().moveTo(0.3,-30).curveTo(-9.9,-29.7,-13.9,-19.9).curveTo(-15.8,-15.3,-16.1,-8.4).lineTo(-15.4,-2).curveTo(-14.1,5.7,-10.4,9.5).curveTo(-6.6,13.4,0.5,14.2).curveTo(8.6,15.2,14.1,11.7).curveTo(19.9,8.1,22,0.1).curveTo(23.1,-3.9,22.9,-8.5).curveTo(22.7,-12.2,21.7,-17.1).curveTo(19.5,-26.7,10.1,-29.1).curveTo(7,-30,1.5,-30).lineTo(0.3,-30).closePath().moveTo(46.8,26.9).curveTo(38.3,22.8,38.2,12.9).lineTo(37.8,-26.1).curveTo(37.7,-28.3,35.4,-28.8).lineTo(32.7,-29.3).lineTo(32.1,-30).lineTo(32.8,-30.4).lineTo(37,-30.7).lineTo(47.5,-30.4).lineTo(48.5,-30.1).curveTo(47.8,-29.2,47.7,-29.2).curveTo(45.9,-29,45.4,-27.7).curveTo(45.2,-27.1,45.2,-25).lineTo(45.3,10.7).curveTo(45.4,14.9,46,18.6).curveTo(47.3,26.2,55.1,26.8).curveTo(64.3,27.5,69.6,20.3).curveTo(80.7,5.3,79.9,-10.8).curveTo(79.7,-14.1,79.8,-18.8).lineTo(79.9,-26.8).curveTo(79.9,-29.3,80.6,-29.9).curveTo(81.2,-30.5,83.7,-30.5).curveTo(85.9,-30.5,86.6,-29.7).curveTo(87.3,-29,87.3,-26.6).lineTo(87.3,20.9).curveTo(87.3,22.9,87.7,24).curveTo(88.3,25.7,89.7,26.1).curveTo(91,26.5,92.4,25.3).lineTo(94.3,23.3).lineTo(95.1,22.8).lineTo(95.4,23.7).curveTo(95.4,25.9,93.3,27.3).curveTo(89.4,30,85,28.1).curveTo(80.6,26.1,80.1,21.3).curveTo(79.8,18.9,79.8,15).lineTo(79.8,8.7).lineTo(79.3,4.6).lineTo(78,8.2).curveTo(75,16.5,69.1,22.8).curveTo(64.6,27.5,58.7,28.6).curveTo(56.9,29,55.2,29).curveTo(51,29,46.8,26.9).closePath().moveTo(-170.3,27.5).curveTo(-177.7,24.8,-178.2,16.1).curveTo(-178.7,7,-171.7,4.1).curveTo(-167.6,2.4,-161.7,0.3).lineTo(-151.5,-3.4).curveTo(-148.1,-4.4,-146.4,-5).curveTo(-141,-7.2,-138.8,-12.9).curveTo(-136.7,-18.5,-139.2,-23.9).curveTo(-141.9,-29.5,-149.4,-30.3).curveTo(-154.6,-30.9,-159.4,-28.4).curveTo(-163.2,-26.4,-164.7,-22.6).curveTo(-166.2,-18.9,-164.9,-14.8).curveTo(-164.1,-12.3,-164.8,-10.6).curveTo(-165.5,-8.9,-167.7,-8.3).curveTo(-170.3,-7.5,-171.5,-9.8).curveTo(-174,-14,-172.5,-18.5).curveTo(-170,-26.3,-162.6,-29.5).curveTo(-151,-34.3,-139.3,-29.6).curveTo(-131.2,-26.3,-131.1,-17.2).lineTo(-131,20.5).curveTo(-131,21.6,-130.8,22.4).curveTo(-130.5,23.8,-129.8,24.7).curveTo(-128.9,25.8,-127.7,25.8).curveTo(-127,25.9,-126,25.3).lineTo(-124.2,24.1).lineTo(-122.9,22.9).lineTo(-123.3,25).curveTo(-124.9,28.5,-129.1,28.8).curveTo(-134.7,29.1,-137,24.9).curveTo(-138.1,22.7,-138.2,20).lineTo(-138.3,14.1).lineTo(-138.3,8.1).lineTo(-138.7,6.7).curveTo(-139.3,7.7,-139.4,7.9).curveTo(-141.8,15.5,-146.9,21.5).curveTo(-151.2,26.6,-157.6,28.2).curveTo(-160.3,28.9,-162.9,28.9).curveTo(-166.6,28.9,-170.3,27.5).closePath().moveTo(-139.2,-7.8).curveTo(-141.1,-6.1,-144.1,-4.8).curveTo(-145.8,-4,-149.4,-2.6).curveTo(-157,0.6,-164.7,4.1).curveTo(-168.3,5.7,-169.6,10).curveTo(-171.2,15.1,-169.7,20.1).curveTo(-168.6,23.6,-166.6,25).curveTo(-164.4,26.5,-160.4,26.4).curveTo(-153.2,26.4,-148,20.1).curveTo(-139.1,9,-138.3,-7.3).lineTo(-138.4,-8.1).lineTo(-139.2,-7.8).closePath().moveTo(-110.4,27.1).curveTo(-114,24.6,-114.6,20.2).curveTo(-114.8,18.4,-114.8,15.9).lineTo(-114.8,-59.4).curveTo(-114.8,-61.6,-115.3,-62.1).curveTo(-115.7,-62.6,-118.1,-62.7).lineTo(-120.7,-62.9).curveTo(-120.9,-62.9,-121.3,-63.3).curveTo(-120.9,-63.6,-120.7,-63.6).lineTo(-106.9,-63.7).curveTo(-106.3,-63.7,-106.2,-63.6).curveTo(-106.1,-63.4,-106.4,-62.9).curveTo(-107.3,-61.5,-107.3,-60).lineTo(-107.3,18.9).lineTo(-107,22.4).curveTo(-106.8,25.2,-104.2,26).curveTo(-101.6,26.7,-99.6,24.4).lineTo(-98.3,23).lineTo(-98.1,24.3).curveTo(-98.1,25,-98.2,25.5).curveTo(-99,27.8,-102.5,28.6).curveTo(-103.7,28.9,-105,28.9).curveTo(-107.8,28.9,-110.4,27.1).closePath().moveTo(118.8,28).curveTo(104.2,24.6,100.8,7.9).curveTo(100.4,6,100.1,3.5).lineTo(99.7,0.1).curveTo(99.5,-13.2,107.7,-22.4).curveTo(115.9,-31.6,128.1,-32.2).curveTo(135.8,-32.6,142.5,-29.1).curveTo(148.9,-25.8,150,-18.5).curveTo(151.2,-10.7,144.2,-5.7).curveTo(140.3,-2.9,134.2,-1).lineTo(116.5,4.6).curveTo(112.4,5.8,109.8,9.3).curveTo(107.1,12.7,109,16.7).curveTo(112.8,25.1,122,27.1).curveTo(131.2,29.1,138.6,23.1).curveTo(144.8,18.1,143.5,10.9).curveTo(143.3,9.9,143.4,8.4).curveTo(143.5,6.3,145.6,5.6).curveTo(147.3,5.1,148.7,6).curveTo(150.1,7,150.1,8.8).curveTo(150.2,12.7,149.2,15).curveTo(143.8,27.2,129.6,28.6).curveTo(127.6,28.8,125.7,28.8).curveTo(122,28.8,118.8,28).closePath().moveTo(114.9,-25.3).curveTo(109.3,-19.4,108.4,-11.8).curveTo(108,-7.9,107.7,-2.6).lineTo(107.3,6.6).curveTo(107.3,6.9,107.4,7.3).lineTo(107.6,8.1).curveTo(108.6,7.7,108.9,7.4).curveTo(114.7,2.3,122.9,0.3).curveTo(126.7,-0.6,134.1,-3).curveTo(141.5,-5.5,142.2,-13.6).lineTo(142.3,-16.9).lineTo(142.2,-16.9).lineTo(142.2,-20.7).curveTo(141.9,-24.2,140.1,-26.4).curveTo(138.2,-28.7,134.9,-29.4).curveTo(131.1,-30.2,127.6,-30.2).lineTo(127.3,-30.2).curveTo(119.7,-30.2,114.9,-25.3).closePath().moveTo(-191.9,28.1).curveTo(-196.7,26.5,-197,21.8).curveTo(-197.3,15,-197.4,5.3).curveTo(-197.5,-9.6,-197.6,-17).curveTo(-197.6,-19.8,-198.3,-21.9).curveTo(-199.8,-26.5,-203.7,-28.7).curveTo(-207.7,-30.9,-212.6,-29.9).curveTo(-223.4,-27.5,-229,-16.1).curveTo(-229.6,-14.9,-229.7,-12.1).lineTo(-229.8,23.8).curveTo(-229.8,25.6,-229.4,25.9).curveTo(-229.1,26.2,-227.2,26.4).lineTo(-224.9,26.8).lineTo(-226,27.2).lineTo(-227.1,27.4).curveTo(-243.7,28.7,-261.3,27.4).curveTo(-261.8,27.4,-263.8,26.8).lineTo(-261.3,26.3).lineTo(-257.7,26).lineTo(-258.9,22.5).lineTo(-269.6,-5.8).curveTo(-270.3,-7.6,-271,-8.1).curveTo(-271.7,-8.6,-273.7,-8.6).lineTo(-303.8,-8.6).curveTo(-306,-8.6,-306.8,-8).curveTo(-307.5,-7.5,-308.3,-5.4).lineTo(-319,24.1).curveTo(-319.6,25.7,-319.4,26).curveTo(-319.2,26.3,-317.5,26.3).lineTo(-314.1,26.3).lineTo(-313.4,26.5).lineTo(-313.7,26.8).lineTo(-314,27.1).lineTo(-318.2,27.6).curveTo(-320.7,28,-322.4,28).curveTo(-323.5,28.1,-324.9,27.8).lineTo(-327.5,27.3).curveTo(-327.7,27.3,-328.1,26.9).lineTo(-327.4,26.6).lineTo(-324.9,26.3).curveTo(-322.7,26.3,-322,25.8).curveTo(-321.3,25.3,-320.6,23.3).lineTo(-291.9,-55.3).lineTo(-291.8,-55.5).curveTo(-291,-57.8,-291.2,-58.2).curveTo(-291.5,-58.6,-293.9,-58.9).lineTo(-294.7,-59).lineTo(-296,-60.1).lineTo(-295.3,-60.5).lineTo(-294.6,-60.7).curveTo(-286.3,-60.8,-283.5,-60.7).curveTo(-282.5,-60.6,-281.8,-59.7).curveTo(-281.4,-59.2,-280.9,-57.8).lineTo(-250.6,23.1).curveTo(-249.8,25.3,-249.1,25.8).curveTo(-248.5,26.2,-246.2,26.3).lineTo(-239.7,26.3).curveTo(-238,26.3,-237.6,25.9).curveTo(-237.2,25.5,-237.2,23.8).lineTo(-237.2,-0.6).lineTo(-237.2,-0.6).lineTo(-237.3,-25.5).curveTo(-237.5,-27.5,-238.1,-28.1).curveTo(-238.6,-28.7,-240.5,-29).curveTo(-241.4,-29.1,-243.1,-29.7).lineTo(-241.7,-30.1).curveTo(-240.9,-30.4,-240.4,-30.4).lineTo(-230.1,-30.5).curveTo(-229.3,-30.5,-229.1,-30.2).curveTo(-228.9,-29.9,-229.3,-29).curveTo(-230.1,-27.2,-230,-24.5).lineTo(-229.8,-19.8).curveTo(-229.1,-19.6,-229,-19.7).lineTo(-226.3,-23.1).curveTo(-218.7,-32.6,-207.1,-31.9).curveTo(-200.5,-31.5,-195.4,-27.7).curveTo(-189.9,-23.6,-189.9,-15.3).lineTo(-189.8,19.9).curveTo(-189.8,22.4,-189.3,23.8).curveTo(-188.6,26,-187,26.2).curveTo(-185.4,26.5,-183.9,24.8).lineTo(-183.3,24.1).lineTo(-182.2,23.1).lineTo(-181.9,23.9).lineTo(-181.9,24.7).curveTo(-182.2,25.7,-183,26.5).curveTo(-185.3,28.7,-188.5,28.7).curveTo(-190.1,28.7,-191.9,28.1).closePath().moveTo(-290,-55.1).lineTo(-305.7,-12).curveTo(-306,-11,-305.8,-10.7).curveTo(-305.5,-10.4,-304.5,-10.4).lineTo(-273,-10.4).curveTo(-271.9,-10.4,-271.7,-10.7).curveTo(-271.5,-11,-271.9,-12).lineTo(-288.1,-55.1).curveTo(-288.2,-55.5,-289.1,-56.5).lineTo(-290,-55.1).closePath().moveTo(-64.3,28.7).curveTo(-70.2,28.6,-74.9,27.2).curveTo(-88.4,23.2,-92.2,7.4).curveTo(-94.3,-0.9,-92.7,-7.3).curveTo(-92.5,-8.5,-92.3,-9).curveTo(-91.7,-10.4,-89.4,-15.2).curveTo(-87.6,-19.1,-86.9,-21.7).curveTo(-86.5,-23.3,-84.4,-24.9).curveTo(-75.9,-32.1,-66.4,-31.7).lineTo(-65.2,-31.6).curveTo(-54.5,-32.1,-46.4,-25.5).curveTo(-38.8,-19.4,-37.4,-9.1).curveTo(-35.9,1.5,-38.4,9.5).curveTo(-41.2,18.4,-48.1,23.6).curveTo(-54.9,28.7,-64,28.7).lineTo(-64.3,28.7).closePath().moveTo(-85.9,-10.5).curveTo(-86.1,-10.4,-86.1,-9.8).lineTo(-85.3,0.1).curveTo(-84.9,5.9,-84.3,9.9).curveTo(-82.9,18.6,-77.7,22.9).curveTo(-70.4,28.9,-60.7,26.8).curveTo(-50.5,24.6,-47.1,14.5).curveTo(-44.3,6,-44.5,-4.1).lineTo(-44.8,-7.1).lineTo(-45.3,-10.7).curveTo(-45.9,-14.2,-46.7,-16.9).curveTo(-49.6,-25.6,-57.9,-29).curveTo(-66.2,-32.5,-74.1,-28.3).curveTo(-80.1,-25.1,-80.7,-19).curveTo(-80.8,-17.5,-80.3,-14.5).curveTo(-80.2,-12.2,-81.5,-11.1).curveTo(-82.9,-9.9,-85.1,-10.5).lineTo(-85.6,-10.6).lineTo(-85.9,-10.5).closePath().moveTo(259.8,27.8).curveTo(255.6,26.3,254.5,21.8).curveTo(254,19.9,254,17.1).lineTo(253.9,-14.6).curveTo(253.9,-18.6,253.5,-20.5).curveTo(252.5,-26.1,248,-28.9).curveTo(243.5,-31.7,237.8,-30.2).curveTo(227.9,-27.6,222.5,-17.1).curveTo(221.7,-15.6,221.6,-12.6).lineTo(221.5,23).curveTo(221.5,24.9,222,25.4).curveTo(222.5,25.8,224.4,25.9).lineTo(225.7,26.1).lineTo(226.9,26.5).lineTo(225.7,26.9).curveTo(225,27.2,224.5,27.2).curveTo(220.2,27.2,214.8,27.1).curveTo(212.7,27,213.7,25.1).curveTo(214.3,23.9,214.3,22).lineTo(214.2,-25.5).curveTo(213.8,-28.2,213.3,-28.7).curveTo(212.8,-29.2,210,-29.6).lineTo(208.3,-30.1).curveTo(208.8,-30.6,209,-30.7).curveTo(212.4,-31.2,213.5,-31.3).curveTo(216.2,-31.3,221.5,-31.1).curveTo(223,-31.1,222.3,-29.9).curveTo(221.2,-28,221.3,-25.2).lineTo(221.5,-20.3).curveTo(222.3,-20.1,222.4,-20.2).lineTo(225.4,-23.9).curveTo(230.2,-29.9,236.8,-31.7).curveTo(246.6,-34.4,255.4,-28.7).curveTo(261.2,-24.9,261.3,-17.9).lineTo(261.3,1).lineTo(261.4,1).lineTo(261.4,19.8).curveTo(261.4,22.7,262.9,24.3).curveTo(264.4,26.1,266.2,24.9).lineTo(268.4,23.2).lineTo(269.3,22.3).lineTo(269.5,23.1).curveTo(269.6,23.5,269.5,23.7).lineTo(268.8,25.3).curveTo(268.3,26.2,267.7,26.6).curveTo(265.4,28.3,262.8,28.3).curveTo(261.4,28.3,259.8,27.8).closePath().moveTo(182.4,28.1).curveTo(181.8,28.1,181,27.8).lineTo(179.5,27.2).lineTo(182.3,26.3).lineTo(183.1,26.3).curveTo(186.4,26.2,187.1,25.5).curveTo(187.7,24.8,187.7,21.4).lineTo(187.7,-51.1).curveTo(187.7,-54.1,187.2,-54.5).curveTo(186.7,-54.9,183.7,-54.3).lineTo(182.1,-53.9).curveTo(181.1,-53.7,180.4,-53.7).curveTo(179,-53.7,178,-54.5).curveTo(177.7,-54.8,177.5,-55.4).curveTo(177.3,-56,177.4,-56.4).curveTo(177.5,-56.7,178.1,-57).curveTo(178.6,-57.2,179.2,-57.3).lineTo(183,-57.4).curveTo(185.5,-57.5,186.9,-57.6).curveTo(190.7,-58,193.5,-61.2).curveTo(193.7,-61.5,194.7,-62.1).curveTo(195.1,-61.2,195.1,-60.7).lineTo(195.3,-57.7).lineTo(195.3,-17.5).lineTo(195.2,-17.5).lineTo(195.3,24.1).curveTo(195.1,26.4,197.5,26.3).lineTo(201.3,26.6).lineTo(202.2,27.1).lineTo(201.8,27.5).lineTo(201.3,27.8).curveTo(200.4,28.1,199.4,28.1).lineTo(190.9,28.2).lineTo(182.4,28.1).closePath().moveTo(296.9,27.4).curveTo(286.7,25.9,280,19.1).curveTo(272.2,11.1,273,-0.7).curveTo(273.9,-12.6,283,-19.8).curveTo(290.4,-25.6,298.8,-26.6).curveTo(307,-27.6,315.8,-23.9).curveTo(325.7,-19.7,327.8,-9.9).lineTo(328.1,-9.8).lineTo(328.1,-3).lineTo(327.2,-1.7).curveTo(325.5,0.1,324,-0.8).curveTo(323.5,-1.2,323.1,-1.9).curveTo(322.7,-2.6,322.6,-3.3).curveTo(322.5,-4.3,322.5,-6.5).curveTo(322.6,-8.6,322.4,-9.6).curveTo(321.7,-16,317.7,-19.9).curveTo(313.7,-23.8,307,-24.8).curveTo(296,-26.5,288.6,-20.3).curveTo(282.1,-14.7,280.2,-4.7).curveTo(278.9,2.5,280.6,9.2).curveTo(282.7,17.5,288.6,22).curveTo(294.6,26.6,303.1,26.5).curveTo(310.3,26.4,315.9,23).curveTo(317.1,22.2,317.7,21.5).curveTo(318.4,20.5,318.4,19.2).curveTo(318.4,18.1,319.2,17.5).curveTo(319.7,17,321,16.5).curveTo(321.6,16.3,322.2,17).curveTo(322.9,17.7,322.5,18.4).curveTo(321.9,19.7,320.9,21.4).lineTo(319,24.3).curveTo(319,24.4,319,24.4).curveTo(318.9,24.4,318.9,24.4).curveTo(318.8,24.4,318.8,24.4).curveTo(318.7,24.4,318.7,24.4).lineTo(318.3,24.3).curveTo(317.7,23.6,316.9,23.8).lineTo(315.5,24.5).curveTo(309.3,27.8,302,27.8).curveTo(299.5,27.8,296.9,27.4).closePath();
	this.shape.setTransform(0.0044,0.0206,0.3323,0.3323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.analogueinc, new cjs.Rectangle(-109,-21.1,218.1,42.3), null);


(lib.hider = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.gradient();
	this.instance.setTransform(70.05,0);

	this.instance_1 = new lib.Symbol6();
	this.instance_1.setTransform(-14,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.hider, new cjs.Rectangle(-84,-36.8,168.1,73.6), null);


(lib.all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Symbol3();
	this.instance.setTransform(-0.25,-9.75);

	this.instance_1 = new lib.Symbol4();
	this.instance_1.setTransform(0.65,-21.4);

	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(0.65,-25.4);

	this.instance_3 = new lib.column();
	this.instance_3.setTransform(0,18.65);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.all, new cjs.Rectangle(-20.8,-42,41.6,84.1), null);


// stage content:
(lib.logopreloader = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// number_idn
	this.instance = new lib.Symbol3();
	this.instance.setTransform(138.95,137.35);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(23).to({_off:false},0).wait(1).to({y:135.8575,alpha:0.2132},0).wait(1).to({y:134.6449,alpha:0.3864},0).wait(1).to({y:133.6646,alpha:0.5265},0).wait(1).to({y:132.8754,alpha:0.6392},0).wait(1).to({y:132.2425,alpha:0.7296},0).wait(1).to({y:131.7372,alpha:0.8018},0).wait(1).to({y:131.3363,alpha:0.8591},0).wait(1).to({y:131.0211,alpha:0.9041},0).wait(1).to({y:130.7761,alpha:0.9391},0).wait(1).to({y:130.589,alpha:0.9659},0).wait(1).to({y:130.4497,alpha:0.9858},0).wait(1).to({y:130.35,alpha:1},0).to({_off:true},14).wait(101));

	// blue_triangle
	this.instance_1 = new lib.Symbol4();
	this.instance_1.setTransform(139.85,129.3);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(29).to({_off:false},0).wait(1).to({y:127.0399,alpha:0.2132},0).wait(1).to({y:125.2036,alpha:0.3864},0).wait(1).to({y:123.7193,alpha:0.5265},0).wait(1).to({y:122.5242,alpha:0.6392},0).wait(1).to({y:121.5657,alpha:0.7296},0).wait(1).to({y:120.8006,alpha:0.8018},0).wait(1).to({y:120.1936,alpha:0.8591},0).wait(1).to({y:119.7162,alpha:0.9041},0).wait(1).to({y:119.3452,alpha:0.9391},0).wait(1).to({y:119.0619,alpha:0.9659},0).wait(1).to({y:118.851,alpha:0.9858},0).wait(1).to({y:118.7,alpha:1},0).to({_off:true},8).wait(101));

	// big_triangle
	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(139.85,122.9);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(35).to({_off:false},0).wait(1).to({y:121.1516,alpha:0.2132},0).wait(1).to({y:119.7311,alpha:0.3864},0).wait(1).to({y:118.5829,alpha:0.5265},0).wait(1).to({y:117.6583,alpha:0.6392},0).wait(1).to({y:116.9169,alpha:0.7296},0).wait(1).to({y:116.325,alpha:0.8018},0).wait(1).to({y:115.8554,alpha:0.8591},0).wait(1).to({y:115.4861,alpha:0.9041},0).wait(1).to({y:115.1991,alpha:0.9391},0).wait(1).to({y:114.98,alpha:0.9659},0).wait(1).to({y:114.8168,alpha:0.9858},0).wait(1).to({y:114.7,alpha:1},0).to({_off:true},2).wait(101));

	// column
	this.instance_3 = new lib.all();
	this.instance_3.setTransform(139.2,140.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(49).to({_off:false},0).to({regX:0.1,regY:0.1,scaleX:1.0876,scaleY:1.0876,x:139.3,y:140.2},6,cjs.Ease.quintInOut).wait(2).to({x:22.7,y:140},29,cjs.Ease.quartInOut).wait(2).to({regX:0,regY:0,scaleX:1,scaleY:1,x:22.6,y:140.15},6,cjs.Ease.quintInOut).wait(56));

	// square_hider (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_3 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_4 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_5 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_6 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_7 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_8 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_9 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_10 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_11 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_12 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_13 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.3,91.2).closePath();
	var mask_graphics_14 = new cjs.Graphics().moveTo(17.3,21.2).lineTo(87.3,21.2).lineTo(87.3,91.2).lineTo(17.4,91.2).closePath();
	var mask_graphics_15 = new cjs.Graphics().moveTo(17.1,21.3).lineTo(87.1,20.9).lineTo(87.4,90.9).lineTo(17.4,91.3).closePath();
	var mask_graphics_16 = new cjs.Graphics().moveTo(16.6,21.3).lineTo(86.6,20.3).lineTo(87.7,90.2).lineTo(17.7,91.3).closePath();
	var mask_graphics_17 = new cjs.Graphics().moveTo(15.6,21.5).lineTo(85.5,18.9).lineTo(88.2,88.8).lineTo(18.2,91.5).closePath();
	var mask_graphics_18 = new cjs.Graphics().moveTo(14,21.9).lineTo(83.8,16.7).lineTo(89,86.5).lineTo(19.2,91.7).closePath();
	var mask_graphics_19 = new cjs.Graphics().moveTo(11.8,22.5).lineTo(81.3,13.7).lineTo(90.1,83.1).lineTo(20.6,91.9).closePath();
	var mask_graphics_20 = new cjs.Graphics().moveTo(9.4,23.4).lineTo(78.2,10.3).lineTo(91.3,79.1).lineTo(22.6,92.2).closePath();
	var mask_graphics_21 = new cjs.Graphics().moveTo(7,24.8).lineTo(74.6,6.7).lineTo(92.7,74.3).lineTo(25,92.4).closePath();
	var mask_graphics_22 = new cjs.Graphics().moveTo(4.7,26.4).lineTo(70.7,3.1).lineTo(94,69.1).lineTo(28,92.4).closePath();
	var mask_graphics_23 = new cjs.Graphics().moveTo(3,28.2).lineTo(67.2,0.3).lineTo(95.1,64.5).lineTo(30.9,92.4).closePath();
	var mask_graphics_24 = new cjs.Graphics().moveTo(1.8,29.8).lineTo(64.3,-1.8).lineTo(95.8,60.7).lineTo(33.4,92.3).closePath();
	var mask_graphics_25 = new cjs.Graphics().moveTo(1.1,31).lineTo(62.2,-3.2).lineTo(96.4,57.9).lineTo(35.3,92.1).closePath();
	var mask_graphics_26 = new cjs.Graphics().moveTo(0.8,31.8).lineTo(60.9,-3.9).lineTo(96.7,56.3).lineTo(36.5,92).closePath();
	var mask_graphics_27 = new cjs.Graphics().moveTo(0.6,32.3).lineTo(60.3,-4.3).lineTo(96.9,55.3).lineTo(37.2,91.9).closePath();
	var mask_graphics_28 = new cjs.Graphics().moveTo(0.5,32.5).lineTo(59.9,-4.5).lineTo(96.9,54.9).lineTo(37.5,91.9).closePath();
	var mask_graphics_29 = new cjs.Graphics().moveTo(0.6,32.6).lineTo(59.9,-4.5).lineTo(97.1,54.8).lineTo(37.7,91.9).closePath();

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(3).to({graphics:mask_graphics_3,x:87.3342,y:91.2225}).wait(1).to({graphics:mask_graphics_4,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_5,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_6,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_7,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_8,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_9,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_10,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_11,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_12,x:87.3341,y:91.2224}).wait(1).to({graphics:mask_graphics_13,x:87.3342,y:91.2225}).wait(1).to({graphics:mask_graphics_14,x:87.3422,y:91.2248}).wait(1).to({graphics:mask_graphics_15,x:87.428,y:91.2504}).wait(1).to({graphics:mask_graphics_16,x:87.6763,y:91.3236}).wait(1).to({graphics:mask_graphics_17,x:88.1674,y:91.4638}).wait(1).to({graphics:mask_graphics_18,x:88.9803,y:91.6816}).wait(1).to({graphics:mask_graphics_19,x:90.0604,y:91.9397}).wait(1).to({graphics:mask_graphics_20,x:91.3012,y:92.1835}).wait(1).to({graphics:mask_graphics_21,x:92.6578,y:92.3685}).wait(1).to({graphics:mask_graphics_22,x:93.973,y:92.4397}).wait(1).to({graphics:mask_graphics_23,x:95.0546,y:92.3876}).wait(1).to({graphics:mask_graphics_24,x:95.8482,y:92.261}).wait(1).to({graphics:mask_graphics_25,x:96.3935,y:92.115}).wait(1).to({graphics:mask_graphics_26,x:96.6953,y:92.008}).wait(1).to({graphics:mask_graphics_27,x:96.8565,y:91.9417}).wait(1).to({graphics:mask_graphics_28,x:96.9314,y:91.9085}).wait(1).to({graphics:mask_graphics_29,x:97.05,y:91.925}).wait(121));

	// column
	this.instance_4 = new lib.Symbol2();
	this.instance_4.setTransform(139.2,160.6);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off:false},0).to({alpha:1},10).to({_off:true},36).wait(101));

	// excellence
	this.instance_5 = new lib.excellence();
	this.instance_5.setTransform(197.6,163.3);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(101).to({_off:false},0).to({x:213.6,alpha:1},17,cjs.Ease.cubicOut).wait(32));

	// future
	this.instance_6 = new lib.future();
	this.instance_6.setTransform(115.1,163.25);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(96).to({_off:false},0).to({x:131.1,alpha:1},17,cjs.Ease.cubicOut).wait(37));

	// for_idn
	this.instance_7 = new lib._for();
	this.instance_7.setTransform(64.6,163.25);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(91).to({_off:false},0).to({x:80.6,alpha:1},17,cjs.Ease.cubicOut).wait(42));

	// hider
	this.instance_8 = new lib.hider();
	this.instance_8.setTransform(84,127.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(59).to({scaleX:0.3597,x:29.85},30,cjs.Ease.quartInOut).wait(61));

	// analogue_inc
	this.instance_9 = new lib.analogueinc();
	this.instance_9.setTransform(31,127.45);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(59).to({_off:false},0).to({x:170.55},30,cjs.Ease.quartInOut).wait(61));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(62,230.4,217.60000000000002,-44.400000000000006);
// library properties:
lib.properties = {
	id: '20D5A530B99A1E4EBB539E03C6BAAC68',
	width: 280,
	height: 280,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.StageGL();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['20D5A530B99A1E4EBB539E03C6BAAC68'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;