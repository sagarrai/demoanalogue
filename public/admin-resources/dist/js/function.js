$(document).ready(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    $("#login-btn").click(function () {
        var email = $("#email").val();
        var password = $("#password").val();
        var token = $("#token").val();
       
        if(email == ""){
            $("#emailMsg").html("Email is required").css("display","block");
            $("#email").addClass("is-invalid");
        }else{
            $("#emailMsg").css("display","none");
            $("#email").removeClass("is-invalid");
            $("#email").addClass("is-valid");
        }

        if(password == ""){
            $("#passwordMsg").html("Password is required").css("display","block");
            $("#password").addClass("is-invalid");
        }else{
            $("#passwordMsg").css("display","none");
            $("#password").removeClass("is-invalid");
            $("#password").addClass("is-valid");
        }

       

        if(email !="" && password !=""){

            // var url = "http://analoguecms.local/api/login";
            var base_url = get_baseurl();

            $.ajax({
                type: 'post',
                url: base_url + '/api/login',

                data: {
                    "email": email,
                    "password": password,
                    "token": token,
                },
                beforeSend: function(){ 
                    $("#spinner_login").show();
                    $("#logintext").hide();
                },

                complete: function(){ 
                    $("#spinner_login").hide();
                    $("#logintext").show();
                },
                success: function (response) {
                    setCookie("token", response.data.token, "1");
                    // console.log("Login form response",response);
                    if (response.code == 200) {
                        
                        $("#infoMesaage").show();
                        $("#login-form").hide();

                        toastr.success('Login Successfully !');
                        window.setTimeout(function () {
                            window.location.href = base_url + '/admin/dashboard';
                        }, 1000);
                    }else{

                    }
                },

                error: function (response) {
                    if (response.code != 200) {
                        toastr.error('Something Went Wrong, Please try again !');
                    }
                }
            });
        }


    });

    // var token = getCookie("token")
    // console.log(token);
    
});


