$(document).ready(function () {
    $("#register-btn").click(function () {

        var name = $("#name").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var c_password = $("#c_password").val();
        var agreeTerms = $("#agreeTerms").val();
        var base_url = get_baseurl();

        ///alert(agreeTerms);
        var err = 0;

        if(name == ""){
            $("#nameMsg").html("Fullname is required").css("display","block");
            $("#name").addClass("is-invalid");
            err = 1;
        }else{
            $("#nameMsg").css("display","none");
            $("#name").removeClass("is-invalid");
            $("#name").addClass("is-valid");
            
        }

        if(email == ""){
            $("#emailMsg").html("Email is required").css("display","block");
            $("#email").addClass("is-invalid");
            err = 1;
        }else{
            $("#emailMsg").css("display","none");
            $("#email").removeClass("is-invalid");
            $("#email").addClass("is-valid");
        }

        if(password == ""){
            $("#passwordMsg").html("Password is required").css("display","block");
            $("#password").addClass("is-invalid");
            err = 1;
        }else{
            $("#passwordMsg").css("display","none");
            $("#password").removeClass("is-invalid");
            $("#password").addClass("is-valid");
        }

        if(c_password == ""){
            $("#c_passwordMsg").html("Confirm  Password is required").css("display","block");
            $("#c_password").addClass("is-invalid");
            err = 1;
        }else if(password != c_password){
            $("#c_passwordMsg").html("Confirm  Password not match with password ").css("display","block");
            $("#c_password").addClass("is-invalid");
            err = 1;
        }else{
            $("#c_passwordMsg").css("display","none");
            $("#c_password").removeClass("is-invalid");
            $("#c_password").addClass("is-valid");
        }



        if(err == 0){
            $.ajax({
                type: 'post',
                url: base_url + '/api/register',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    "name": name,
                    "email": email,
                    "password": password,
                    "c_password": c_password,
                },
                beforeSend: function(){ 
                    $("#spinnerSignupBth").show();
                    $("#registerText").hide();
                },

                complete: function(){ 
                    $("#spinnerSignupBth").hide();
                    $("#registerText").show();
                },
                success: function (response) {
                    if (response.code == "200") {
                        $("#register-form").hide();
                        $('#registered-msg').removeAttr('style');
                        window.setTimeout(function () {
                            window.location.href = base_url + '/admin/login';
                        }, 2000);
                    }
                },

                error: function (response) {
                    if (response.code != 200) {
                        $('#not-register-msg').removeAttr('style');
                        //setTimeout(location.reload.bind(location), 3000);

                    }
                }
            });
        }

    });

});