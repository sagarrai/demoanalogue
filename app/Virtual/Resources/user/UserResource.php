<?php 
/**
 * @OA\Schema(
 *     title="UserResource",
 *     description="User resource",
 *     @OA\Xml(
 *         name="UserResource"
 *     )
 * )
 */
class UserResource
{
    /**
     * @OA\Property(
     *     title="Status",
     *     description="Status of response",
     *     format="bool",
     *     example="true or false",
     * )
     *
     * @var bool
     */

    private  $status;

    /**
     * @OA\Property(
     *     title="Cosde",
     *     description="Status code of response",
     *     format="integer",
     *     example="200",
     * )
     *
     * @var int
     */

    private $code;

    /**
     * @OA\Property(
     *     title="Message",
     *     description="Message of current action",
     *     format="string",
     *     example="User login successful.",
     * )
     *
     * @var string
     */
    private $message;
    
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\UserModel[]
     */
    private $data;
}