<?php 

/**
 * @OA\Schema(
 *      title="BusinessStationary",
 *      description="Create new BusinessStationary ",
 *      type="object",
 *      required={"brand_name","brand_description","brand_image"}
 * )
 */

class StoreBusinessStationaryRequest
{
   
      /**
     * @OA\Property(
     *     title="brand_name",
     *     description="Name of the Brand",
     *     example="Stickers"
     * )
     *
     * @var string
     */
    private $brand_name;

      /**
     * @OA\Property(
     *     title="brand_description",
     *     description="Description of the brand",
     *     example="This is a description of the brand"
     * )
     *
     * @var string
     */
    private $brand_description;

      /**
     * @OA\Property(
     *     title="brand_image",
     *     description="Image of the brand",
     *     example="abc.jpg"
     * )
     *
     * @var string
     */
    private $brand_image;

}