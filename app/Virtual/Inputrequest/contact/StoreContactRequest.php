<?php 

/**
 * @OA\Schema(
 *      title="Contact ",
 *      description="Create new contact ",
 *      type="object",
 *      required={"full_name","phone","email","subject","message"}
 * )
 */

class StoreContactRequest
{
   
      /**
     * @OA\Property(
     *     title="full_name",
     *     description="full_name of user",
     *     example="Sagar Rai"
     * )
     *
     * @var string
     */
    private $full_name;

      /**
     * @OA\Property(
     *     title="phone",
     *     description="Phone number of user",
     *     example="9816308014"
     * )
     *
     * @var string
     */
    private $phone;

      /**
     * @OA\Property(
     *     title="email",
     *     description="Email account of user",
     *     example="sagar.raiii31@gmail.com"
     * )
     *
     * @var string
     */
    private $email;

      /**
     * @OA\Property(
     *     title="subject",
     *     description="Subject of user",
     *     example="Greeting."
     * )
     *
     * @var string
     */
    private $subject;

     /**
     * @OA\Property(
     *     title="message",
     *     description="Message of user.",
     *     example="This is a dummy message."
     * )
     *
     * @var string
     */
    private $message;

    /**
     * @OA\Property(
     *     title="module",
     *     description="module of entered by user.",
     *     example="Press Module"
     * )
     *
     * @var string
     */
    private $module;

    /**
     * @OA\Property(
     *     title="user_seeking",
     *     description="This is a thing that user is looking for.",
     *     example="Merchandising"
     * )
     *
     * @var string
     */
    private $user_seeking;

}