<?php 

/**
 * @OA\Schema(
 *      title="Update contact ",
 *      description="Update existing contact ",
 *      type="object",
 *      required={"full_name","phone","email","subject","message"}
 * )
 */

class UpdateContactRequest
{
   
      /**
     * @OA\Property(
     *     title="full_name",
     *     description="full_name of the user",
     *     example="Sagar Rai"
     * )
     *
     * @var string
     */
    private $full_name;


    /**
     * @OA\Property(
     *     title="phone",
     *     description="Phone number of the user",
     *     format="integer",
     *     example="9817308014 ",
     * )
     *
     * @var integer
     */
    private $phone;

    /**
     * @OA\Property(
     *     title="email",
     *     description="Email of user",
     *     format="string",
     *     example="sagar.raiii31@gmail.com",
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *     title="subject",
     *     description="Subject of the user",
     *     format="string",
     *     example="Greeting",
     * )
     *
     * @var string
     */
    private $subject;

    /**
     * @OA\Property(
     *     title="message",
     *     description="Message of the user",
     *     format="string",
     *     example="This is a dummy text message of the user.",
     * )
     *
     * @var string
     */
    private $message;

    /**
     * @OA\Property(
     *     title="module",
     *     description="module of entered by user.",
     *     format="string",
     *     example="This is a dummy module."
     * )
     *
     * @var string
     */
    private $module;

    /**
     * @OA\Property(
     *     title="user_seeking",
     *     description="This is a thing that user is looking for.",
     *     format="string",
     *     example="Merchandising"
     * )
     *
     * @var string
     */
    private $user_seeking;
}