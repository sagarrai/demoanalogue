<?php 

/**
 * @OA\Schema(
 *      title="Store office request",
 *      description="Store office request body data",
 *      type="object",
 *      required={"user_id","email","office_name","pan_number"}
 * )
 */

class StoreOfficeRequest
{
    /**
     * @OA\Property(
     *      title="user_id",
     *      description="User ID of the new office",
     *      example=1
     * )
     *
     * @var string
     */
    public $user_id;

    /**
     * @OA\Property(
     *      title="office_code",
     *      description="Office code for the new office",
     *      example=8192
     * )
     *
     * @var integer
     */
    public $office_code;

    /**
     * @OA\Property(
     *      title="office_name",
     *      description="This is a name for the office",
     *      example="Analogue Incorporate"
     * )
     * @var string
     */
    public $office_name;

    /**
     * @OA\Property(
     *      title="nick_name",
     *      description="This is a nick name for the office",
     *      example="AnalogueInc"
     * )
     * @var string
     */
    public $nick_name;

    /**
     * @OA\Property(
     *      title="registration_date",
     *      description="This is a registration date for the office",
     *      example="2020-02-13"
     * )
     * @var string
     */
    public $registration_date;

    /**
     * @OA\Property(
     *      title="currency_code",
     *      description="This is a currency code for the office",
     *      example="Rs."
     * )
     * @var string
     */
    public $currency_code;

    /**
     * @OA\Property(
     *      title="po_box",
     *      description="This is a P.O. Box for the office",
     *      example=123545
     * )
     * @var integer
     */
    public $po_box;

    /**
     * @OA\Property(
     *      title="address_line_1",
     *      description="This is a address line 1 for the office",
     *      example="Budhha Chowk"
     * )
     * @var string
     */
    public $address_line_1;

    /**
     * @OA\Property(
     *      title="address_line_2",
     *      description="This is a address line 2 for the office",
     *      example="Nakhipot"
     * )
     * @var string
     */
    public $address_line_2;

    /**
     * @OA\Property(
     *      title="street",
     *      description="This is a street for the office",
     *      example="Bishal Chowk"
     * )
     * @var string
     */
    public $street;

    /**
     * @OA\Property(
     *      title="city",
     *      description="This is a city for the office",
     *      example="Lalitpur"
     * )
     * @var string
     */
    public $city;

    /**
     * @OA\Property(
     *      title="state",
     *      description="This is a state for the office",
     *      example="Province number 3"
     * )
     * @var string
     */
    public $state;

    /**
     * @OA\Property(
     *      title="zip_code",
     *      description="This is a zip code for the office",
     *      example=1235
     * )
     * @var integer
     */
    public $zip_code;

    /**
     * @OA\Property(
     *      title="country",
     *      description="This is a country for the office",
     *      example="Nepal"
     * )
     * @var string
     */
    public $country;

    /**
     * @OA\Property(
     *      title="phone",
     *      description="This is a phone for the office",
     *      example=981234567
     * )
     * @var integer
     */
    public $phone;

    /**
     * @OA\Property(
     *      title="fax",
     *      description="This is a fax for the office",
     *      example="fax no. 1983"
     * )
     * @var string
     */
    public $fax;

    /**
     * @OA\Property(
     *      title="email",
     *      description="This is an	email for the office",
     *      example="iamsagar@gmail.com"
     * )
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="url",
     *      description="This is an	url for the office",
     *      example="https://www.youtube.com/"
     * )
     * @var string
     */
    public $url;

    /**
     * @OA\Property(
     *      title="logo",
     *      description="This is an	logo for the office",
     *      example="logo.jpg"
     * )
     * @var string
     */
    public $logo;

    /**
     * @OA\Property(
     *      title="parent_office_id",
     *      description="This is a	parent office id for the office",
     *      example=123
     * )
     * @var integer
     */
    public $parent_office_id;

    /**
     * @OA\Property(
     *      title="registration_number",
     *      description="This is a registration number for the office",
     *      example=12122
     * )
     * @var integer
     */
    public $registration_number;

    /**
     * @OA\Property(
     *      title="pan_number",
     *      description="Pan number for the office",
     *      example=120120
     * )
     * @var string
     */
    public $pan_number;

    /**
     * @OA\Property(
     *      title="allow_transaction_posting",
     *      description="This is an allow transaction posting for the office",
     *      example=1
     * )
     * @var string
     */
    public $allow_transaction_posting;

    /**
     * @OA\Property(
     *      title="audit_user_id",
     *      description="Audit user id account for the office",
     *      example=1
     * )
     * @var boolean
     */
    public $audit_user_id;

    /**
     * @OA\Property(
     *      title="audit_ts",
     *      description="Audit ts account for the office",
     *      example="2020-02-13 10:47:42"
     * )
     * @var string
     */
    public $audit_ts;

    /**
     * @OA\Property(
     *      title="deleted",
     *      description="Deleted office status",
     *      example=1
     * )
     * @var boolean
     */
    public $deleted;
    
}