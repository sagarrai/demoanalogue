<?php 

/**
 * @OA\Schema(
 *      title="Login user request",
 *      description="Login user request body data",
 *      type="object",
 *      required={"email","password"}
 * )
 */

class LoginUserRequest
{
   
    /**
     * @OA\Property(
     *      title="email",
     *      description="Email address of the new user",
     *      example="sagar@gmail.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="password",
     *      description="New password of the user (this must be alpha numeric)",
     *      example="123"
     * )
     * @var string
     */
    public $password;
}