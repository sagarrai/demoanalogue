<?php

/**
 * @OA\Schema(
 *     description="Some simple request createa as example",
 *     type="object",
 *     title="Example storing request",
 * )
 */
class StoreTestRequest
{
    /**
     * @OA\Property(
     *     title="Title",
     *     description="Some text field",
     *     format="string",
     *     example="test"
     * )
     *
     * @var string
     */
    public $title;

    /**
     * @OA\Property(
     *     title="User ID",
     *     description="Some integer",
     *     format="int64",
     *     example="123"
     * )
     *
     * @var int
     */
    public $user_id;

    /**
     * @OA\Property(
     *     title="file",
     *     description="This is a image name",
     *     format="file",
     *     example="test.jpg"
     * )
     *
     * @var binary
     */
    public $file;

    /**
     * @OA\Property(
     *     title="file_url",
     *     description="This is a url of image",
     *     format="string",
     *     example="http://www.test.com/test.jpg"
     * )
     *
     * @var string
     */
    public $file_url;
}
