<?php 

/**
 * @OA\Schema(
 *      title="Update OfficeInfo ",
 *      description="Updating exisitng Office Information ",
 *      type="object",
 * )
 */

class UpdateOfficeInfoRequest{

    public $website_logo;

      /**
     * @OA\Property(
     *     title="footer_title",
     *     description="This is a footer title",
     *     example="Analogue"
     * )
     *
     * @var string
     */
    public $footer_title;

       /**
     * @OA\Property(
     *     title="footer_description",
     *     description="This is a footer description",
     *     example="In need of business promotional plans and marketing needs, resort to Analogue Inc. for edgy business game plan. Optimize your brand’s social presence with the Analogue’s professionalism."
     * )
     *
     * @var string
     */
    public $footer_description;

     /**
     * @OA\Property(
     *     title="address",
     *     description="This is a address of the Company",
     *     example="Gaushala, Panglasthan, Kathmandu"
     * )
     *
     * @var string
     */
    
    public $address;

    /**
     * @OA\Property(
     *     title="phone",
     *     description="This is a phone number of the Company",
     *     example="+977 980-2320803"
     * )
     *
     * @var integer
     */
    
    public $phone;

     /**
     * @OA\Property(
     *     title="email",
     *     description="This is a email address of the company",
     *     example="info@analogueinc.com.np"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     title="website",
     *     description="This is a website of the company",
     *     example="www.analogueinc.com.np"
     * )
     *
     * @var string
     */

    public $website;
    
    /**
     * @OA\Property(
     *     title="facebook_link",
     *     description="This is a link for facebook of the company",
     *     example="https://www.facebook.com/Analogue-Inc-111451123769739"
     * )
     *
     * @var integer
     */
    public $facebook_link;

    /**
     * @OA\Property(
     *     title="twitter_link",
     *     description="This is a link for twitter of the company",
     *     example="https://twitter.com/AnalogueInc"
     * )
     *
     * @var integer
     */
    public $twitter_link;

    /**
     * @OA\Property(
     *     title="linkedin_link",
     *     description="This is a link for linkedIn of the company",
     *     example="https://www.linkedin.com/analoguein123/"
     * )
     *
     * @var integer
     */
    public $linkedin_link;

    /**
     * @OA\Property(
     *     title="instagram_link",
     *     description="This is a link for instagram of the company",
     *     example="https://www.instagram.com/analoguein123/"
     * )
     *
     * @var integer
     */
    public $instagram_link;

     /**
     * @OA\Property(
     *     title="copyright_description",
     *     description="This is a copyright description",
     *     example="Copyright © 2020 ALL RIGHTS RESERVED. POWERED BY ANALOGUE INC., A PGNSONS COMPANY."
     * )
     *
     * @var string
     */
    public $copyright_description;


}