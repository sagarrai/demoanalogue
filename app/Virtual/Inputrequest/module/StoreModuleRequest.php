<?php 

/**
 * @OA\Schema(
 *      title="Module",
 *      description="Create new module ",
 *      type="object",
 *      required={"name"}
 * )
 */

class StoreModuleRequest{

     /**
     * @OA\Property(
     *     title="name",
     *     description="description for module",
     *     example="Branding and Marketing"
     * )
     *
     * @var string
     */
    public $name;

      /**
     * @OA\Property(
     *     title="meta_description",
     *     description="Meta description for module",
     *     example="This is for meta description"
     * )
     *
     * @var string
     */
    public $meta_description;

      /**
     * @OA\Property(
     *     title="status",
     *     description="Status of module",
     *     example="1"
     * )
     *
     * @var integer
     */
    public $status;
    
    public $banner_image;


}