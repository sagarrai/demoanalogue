<?php 

/**
 * @OA\Schema(
 *      title="Consultancy",
 *      description="Create new consultancy ",
 *      type="object",
 *      required={"full_name"}
 * )
 */

class StoreConsultancyRequest {

     /**
     * @OA\Property(
     *     title="full_name",
     *     description="Full name of consultanct",
     *     example="Sheldon Cooper"
     * )
     *
     * @var string
     */
    public $full_name;

     /**
     * @OA\Property(
     *     title="email",
     *     description="Email of consultancy",
     *     example="sheldon@gmail.com"
     * )
     *
     * @var string
     */
    public $email;

     /**
     * @OA\Property(
     *     title="phone",
     *     description="Consultancy phone number",
     *     example="981236740"
     * )
     *
     * @var integer
     */
    
    public $phone;

     /**
     * @OA\Property(
     *     title="objective",
     *     description="This is his/her main motive for consultant",
     *     example="Make a website for us."
     * )
     *
     * @var string
     */
    public $objective;

    /**
     * @OA\Property(
     *     title="budget",
     *     description="budget of your consultant",
     *     example="1000000"
     * )
     *
     * @var integer
     */
    public $budget;

    /**
     * @OA\Property(
     *     title="deadline",
     *     description="Name of the package",
     *     example="2020-01-27 17:50:45"
     * )
     *
     * @var \DateTime
     */
    public $deadline;

    /**
     * @OA\Property(
     *     title="message",
     *     description="Message of the free consultancy",
     *     example="This is a short message for a free consultancy"
     * )
     *
     * @var string
     */
    public $message;

}