<?php 

/**
 * @OA\Schema(
 *      title="Store post ",
 *      description="Create new post ",
 *      type="object",
 *      required={"title"}
 * )
 */

class StorePostRequest
{
   
      /**
     * @OA\Property(
     *     title="title",
     *     description="Title of the post",
     *     example="Blog"
     * )
     *
     * @var string
     */
    private $title;

      /**
     * @OA\Property(
     *     title="slug",
     *     description="Slug of the post",
     *     example="slug"
     * )
     *
     * @var string
     */
    private $slug;

      /**
     * @OA\Property(
     *     title="short_text",
     *     description="Short Text of the post",
     *     example="This is a short text message."
     * )
     *
     * @var string
     */
    private $short_text;

      /**
     * @OA\Property(
     *     title="description",
     *     description="This is descriptiom",
     *     example="This is a description message."
     * )
     *
     * @var string
     */
    private $description;

     /**
     * @OA\Property(
     *     title="category",
     *     description="This is category",
     *     example="This is a category message."
     * )
     *
     * @var string
     */
    private $category;

    /**
     * @OA\Property(
     *     title="user_id",
     *     description="Current user id",
     *     format="integer",
     *     example=1,
     * )
     *
     * @var integer
     */
    private $user_id;

    /**
     * @OA\Property(
     *     title="meta_description",
     *     description="This is meta_description",
     *     format="string",
     *     example="this is meta description",
     * )
     *
     * @var string
     */
    private $meta_description;

    
    private $feature_image;

    /**
     * @OA\Property(
     *     title="publish",
     *     description="This is publish",
     *     format="integer",
     *     example=1,
     * )
     *
     * @var integer
     */
    private $publish;

    /**
     * @OA\Property(
     *     title="Office Id",
     *     description="Current office id",
     *     format="integer",
     *     example=1,
     * )
     *
     * @var string
     */
    private $office_id;

    /**
     * @OA\Property(
     *     title="deadline",
     *     description="Deadline for the job",
     *     format="datetime",
     *     example="2020-01-27 17:50:45",
     * )
     *
     * @var \DateTime
     */
    private $deadline;

    /**
     * @OA\Property(
     *     title="other_description",
     *     description="Other description",
     *     format="string",
     *     example="Lorem ipsum",
     * )
     *
     * @var string
     */
    private $other_description;

    /**
     * @OA\Property(
     *     title="no_of_opening",
     *     description="Number of opening",
     *     format="integer",
     *     example=2,
     * )
     *
     * @var integer
     */
    private $no_of_opening;

}