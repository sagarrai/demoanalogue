<?php 

/**
 * @OA\Schema(
 *      title="UserPackage ",
 *      description="Create new user package ",
 *      type="object",
 *      required={"full_name"}
 * )
 */

class StoreUserPackageRequest{

     /**
     * @OA\Property(
     *     title="full_name",
     *     description="Full name of package",
     *     example="Harry Porter"
     * )
     *
     * @var string
     */
    public $full_name;

      /**
     * @OA\Property(
     *     title="email",
     *     description="User Package email",
     *     example="harry@gmail.com"
     * )
     *
     * @var string
     */
    public $email;

     /**
     * @OA\Property(
     *     title="phone",
     *     description="User Package phone number",
     *     example="981236740"
     * )
     *
     * @var integer
     */
    
    public $phone;

    /**
     * @OA\Property(
     *     title="company_name",
     *     description="Name of the company",
     *     example="ABC company"
     * )
     *
     * @var string
     */
    public $company_name;

    /**
     * @OA\Property(
     *     title="company_address",
     *     description="Address of the company",
     *     example="Koteswor, Kathmandu"
     * )
     *
     * @var string
     */
    public $company_address;

      /**
     * @OA\Property(
     *     title="package_name",
     *     description="Name of the package",
     *     example="Social and Marketing"
     * )
     *
     * @var string
     */
    public $package_name;

    /**
     * @OA\Property(
     *     title="package_type",
     *     description="Type of the package",
     *     example="Basic Type"
     * )
     *
     * @var string
     */
    public $package_type;

    /**
     * @OA\Property(
     *     title="package_details",
     *     description="All list of small packages chose by the user",
     *     example="Style guidelines, Typography, Email"
     * )
     *
     * @var string
     */
    public $package_details;

    /**
     * @OA\Property(
     *     title="package_total_price",
     *     description="Total price of the package",
     *     example="50000"
     * )
     *
     * @var integer
     */
    public $package_total_price;

    /**
     * @OA\Property(
     *     title="other_message",
     *     description="Other message that user have enter",
     *     example="This is a other message"
     * )
     *
     * @var integer
     */
    public $other_message;

    /**
     * @OA\Property(
     *     title="contact",
     *     description="Total number of contact user have",
     *     example="981111111"
     * )
     *
     * @var string
     */
    public $contact;

    /**
     * @OA\Property(
     *     title="template",
     *     description="otal number of template user have",
     *     example="2"
     * )
     *
     * @var integer
     */
    public $template;

    /**
     * @OA\Property(
     *     title="newsletter",
     *     description="otal number of newsletter user have",
     *     example="3"
     * )
     *
     * @var integer
     */
    public $newsletter;

    /**
     * @OA\Property(
     *     title="social_post",
     *     description="otal number of social post user have",
     *     example="4"
     * )
     *
     * @var integer
     */
    public $social_post;

}