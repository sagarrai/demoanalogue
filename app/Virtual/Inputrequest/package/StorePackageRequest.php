<?php 

/**
 * @OA\Schema(
 *      title="Package",
 *      description="Create new package ",
 *      type="object",
 *      required={"name"}
 * )
 */

class StorePackageRequest {

     /**
     * @OA\Property(
     *     title="name",
     *     description="Name of package",
     *     example="Social Marketing"
     * )
     *
     * @var string
     */
    public $name;

      /**
     * @OA\Property(
     *     title="code",
     *     description="Package code",
     *     example="1200"
     * )
     *
     * @var integer
     */
    public $code;

     /**
     * @OA\Property(
     *     title="description",
     *     description="Description of package",
     *     example="This is a description"
     * )
     *
     * @var string
     */
    public $description;

     /**
     * @OA\Property(
     *     title="module",
     *     description="Module of the package",
     *     example="Branding and Marketing"
     * )
     *
     * @var string
     */
    public $module;

    /**
     * @OA\Property(
     *     title="type",
     *     description="Different types of type",
     *     example="Basic/Standard/Premium"
     * )
     *
     * @var string
     */
    public $type;

}