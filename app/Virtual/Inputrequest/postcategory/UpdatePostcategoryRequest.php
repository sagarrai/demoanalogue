<?php 

/**
 * @OA\Schema(
 *      title="Update post category",
 *      description="Update existing post category",
 *      type="object",
 *      required={"title","slug","description","user_id"}
 * )
 */

class UpdatePostcategoryRequest
{
   
      /**
     * @OA\Property(
     *     title="title",
     *     description="Title of the postcategory",
     *     example="Blog"
     * )
     *
     * @var string
     */
    private $title;


    /**
     * @OA\Property(
     *     title="description",
     *     description="Description of the postcategory",
     *     format="string",
     *     example="Blog category",
     * )
     *
     * @var string
     */
    private $description;

    /**
     * @OA\Property(
     *     title="slug",
     *     description="Url of the postcategory",
     *     format="string",
     *     example="blog-category",
     * )
     *
     * @var string
     */
    private $slug;

    /**
     * @OA\Property(
     *     title="User Id",
     *     description="Current user id",
     *     format="integer",
     *     example="1",
     * )
     *
     * @var string
     */
    private $user_id;
}