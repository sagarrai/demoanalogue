<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="BusinessStationary",
 *     description="BusinessStationary Model",
 * )
 */
class BusinessStationaryModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

     /**
     * @OA\Property(
     *     title="brand_name",
     *     description="Name of the Brand",
     *     format="string",
     *     example="Stickers"
     * )
     *
     * @var string
     */
    private $brand_name;

      /**
     * @OA\Property(
     *     title="brand_description",
     *     description="Description of the brand",
     *     format="string",
     *     example="This is a description of the brand"
     * )
     *
     * @var string
     */
    private $brand_description;

      /**
     * @OA\Property(
     *     title="brand_image",
     *     description="Image of the brand",
     *     format="string",
     *     example="abc.jpg"
     * )
     *
     * @var string
     */
    private $brand_image;

    /**
     * @OA\Property(
     *     title="category",
     *     description="Category of the brand",
     *     format="integer",
     *     example="1"
     * )
     *
     * @var integer
     */
    private $category;

}