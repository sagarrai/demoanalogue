<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Office",
 *     description="Office model",
 * 
 * )
 */
class OfficeModel
{
    /**
     * @OA\Property(
     *     title="id",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="user_id",
     *     description="user_id",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $user_id;

    /**
     * @OA\Property(
     *     title="office_code",
     *     description="office_code",
     *     format="string",
     *     example="analog-01"
     * )
     *
     * @var string
     */
    private $office_code;

    /**
     * @OA\Property(
     *     title="office_name",
     *     description="office_name",
     *     format="string",
     *     example="Analogue Inc"
     * )
     *
     * @var string
     */
    private $office_name;

    /**
     * @OA\Property(
     *     title="nick_name",
     *     description="This is a nick name of office module",
     *     format="string",
     *     example="Analogue Inc"
     * )
     *
     * @var string
     */
    private $nick_name;

    /**
     * @OA\Property(
     *     title="registration_date",
     *     description="This is a registration date of Office Module",
     *     format="date-time",
     *     example="2020-01-27 17:50:45"
     * )
     *
     * @var DateTime
     */
    private $registration_date;

    /**
     * @OA\Property(
     *     title="currency_code",
     *     description="This is a currency code for office Module",
     *     format="string",
     *     example="Rs."
     * )
     *
     * @var string
     */
    private $currency_code;

    /**
     * @OA\Property(
     *     title="po_box",
     *     description="This is a P.O box for office Module",
     *     format="string",
     *     example="Dharan, 57200, D.P.O."
     * )
     *
     * @var string
     */
    private $po_box;

    /**
     * @OA\Property(
     *     title="address_line_1",
     *     description="This is a address line 1 for office Module",
     *     format="string",
     *     example="Dharan-17, Railway Line"
     * )
     *
     * @var string
     */
    private $address_line_1;

    /**
     * @OA\Property(
     *     title="address_line_2",
     *     description="This is a address line 2 for office Module",
     *     format="string",
     *     example="Nakhipot, Lalitpur"
     * )
     *
     * @var string
     */
    private $address_line_2;

    /**
     * @OA\Property(
     *     title="street",
     *     description="This is a street for office Module",
     *     format="string",
     *     example="Bishal Chowk"
     * )
     *
     * @var string
     */
    private $street;

    /**
     * @OA\Property(
     *     title="city",
     *     description="This is a city for office Module",
     *     format="string",
     *     example="Lalitpur City"
     * )
     *
     * @var string
     */
    private $city;

    /**
     * @OA\Property(
     *     title="state",
     *     description="This is a state for office Module",
     *     format="string",
     *     example="Province no. 3"
     * )
     *
     * @var string
     */
    private $state;

    /**
     * @OA\Property(
     *     title="zip_code",
     *     description="This is a zip code for office Module",
     *     format="string",
     *     example="Mr. Sagar Rai, 3256 Epiphenomenal Avenue, Minneapolis, MN 55416"
     * )
     *
     * @var string
     */
    private $zip_code;

    /**
     * @OA\Property(
     *     title="country",
     *     description="This is a country for office Module",
     *     format="string",
     *     example="Nepal"
     * )
     *
     * @var string
     */
    private $country;

    /**
     * @OA\Property(
     *     title="phone",
     *     description="This is a phone number for office Module",
     *     format="integer",
     *     example="981234567"
     * )
     *
     * @var integer
     */
    private $phone;

    /**
     * @OA\Property(
     *     title="Fax number",
     *     description="This is a Fax number for office Module",
     *     format="string",
     *     example="fax number +1 323 555"
     * )
     *
     * @var integer
     */
    private $fax;

    /**
     * @OA\Property(
     *     title="Email",
     *     description="This is a email for office module",
     *     format="email",
     *     example="my_name@gmail.com",
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *     title="url",
     *     description="This is a url for office module",
     *     format="string",
     *     example="http://analogueinc.com.np/",
     * )
     *
     * @var string
     */
    private $url;

    /**
     * @OA\Property(
     *     title="logo",
     *     description="This is a logo for office module",
     *     format="string",
     *     example="http://analogueinc.com.np/image/logo.jpg",
     * )
     *
     * @var string
     */
    private $logo;

    /**
     * @OA\Property(
     *     title="parent_office_id",
     *     description="This is a parent_office_id for office module",
     *     format="string",
     *     example="Office 1204",
     * )
     *
     * @var string
     */
    private $parent_office_id;

    /**
     * @OA\Property(
     *     title="registration_number",
     *     description="This is a registration number for office module",
     *     format="integer",
     *     example="120040",
     * )
     *
     * @var integer
     */
    private $registration_number;

    /**
     * @OA\Property(
     *     title="pan_number",
     *     description="This is a pan number for office module",
     *     format="integer",
     *     example="97540",
     * )
     *
     * @var integer
     */
    private $pan_number;

    /**
     * @OA\Property(
     *     title="allow_transaction_posting",
     *     description="This is a allow transaction posting for office module",
     *     format="boolean",
     *     example="1",
     * )
     *
     * @var boolean
     */
    private $allow_transaction_posting;

    /**
     * @OA\Property(
     *     title="audit_user_id",
     *     description="This is a allow transaction posting for office module",
     *     format="integer",
     *     example="user-id 101",
     * )
     *
     * @var integer
     */
    private $audit_user_id;

    /**
     * @OA\Property(
     *     title="audit_ts",
     *     description="This is a audit ts for office module",
     *     format="date",
     *     example="2020-01-27 17:50:45",
     * )
     *
     * @var Datetime
     */
    private $audit_ts;

    /**
     * @OA\Property(
     *     title="deleted",
     *     description="This is a deleted status for office module",
     *     format="boolean",
     *     example="0",
     * )
     *
     * @var boolean
     */
    private $deleted;

    /**
     * @OA\Property(
     *     title="Created at",
     *     description="Created at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $created_at;

    /**
     * @OA\Property(
     *     title="Updated at",
     *     description="Updated at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $updated_at;

}