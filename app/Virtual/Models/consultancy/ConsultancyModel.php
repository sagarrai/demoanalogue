<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Consultancy",
 *     description="Consultancy Model",
 * )
 */
class ConsultancyModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="full_name",
     *     description="This is a for consultancy",
     *     format="string",
     *     example="Harry Porter"
     * )
     *
     * @var string
     */
    private $full_name;

      /**
     * @OA\Property(
     *     title="email",
     *     description="This is a consultancy email",
     *     format="string",
     *     example="harry@gmail.com"
     * )
     *
     * @var string
     */
    private $email;

      /**
     * @OA\Property(
     *     title="phone",
     *     description="This is a consultancy phone number",
     *     format="integer",
     *     example="981129384"
     * )
     *
     * @var integer
     */
    private $phone;

      /**
     * @OA\Property(
     *     title="objective",
     *     description="This is main objective for consultancy",
     *     format="string",
     *     example="Make a website for us"
     * )
     *
     * @var string
     */
    private $objective;

    /**
     * @OA\Property(
     *     title="budget",
     *     description="This is budget for consultancy",
     *     format="integer",
     *     example="1000000"
     * )
     *
     * @var integer
     */
    private $budget;

    /**
     * @OA\Property(
     *     title="deadline",
     *     description="This is deadline for consultancy",
     *     format="datetime",
     *     example="2020-01-27 17:50:45"
     * )
     *
     * @var \Datetime
     */
    private $deadline;

    /**
     * @OA\Property(
     *     title="message",
     *     description="This is main message for consultancy",
     *     format="string",
     *     example="This is a message seciton for consultancy"
     * )
     *
     * @var string
     */
    private $message;

}