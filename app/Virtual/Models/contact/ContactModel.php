<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Contact",
 *     description="Contact Model",
 * )
 */
class ContactModel
{
    /**
     * @OA\Property(
     *     title="id",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="full_name",
     *     description="Full name of user",
     *     format="string",
     *     example="Sagar Rai"
     * )
     *
     * @var string
     */
    private $full_name;

    /**
     * @OA\Property(
     *     title="phone",
     *     description="This is a phone",
     *     format="string",
     *     example="9816308014"
     * )
     *
     * @var string
     */
    private $phone;

    /**
     * @OA\Property(
     *     title="email",
     *     description="This is a email",
     *     format="string",
     *     example="sagar.analogue51@gmail.com"
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *     title="subject",
     *     description="This is a subject message.",
     *     format="string",
     *     example="Greeting"
     * )
     *
     * @var string
     */
    private $subject;

     /**
     * @OA\Property(
     *     title="message",
     *     description="This is a message",
     *     format="string",
     *     example="This is a dummy message for description"
     * )
     *
     * @var string
     */
    private $message;

    /**
     * @OA\Property(
     *     title="module",
     *     description="module of entered by user.",
     *     format="string",
     *     example="This is a dummy module."
     * )
     *
     * @var string
     */
    private $module;

    /**
     * @OA\Property(
     *     title="user_seeking",
     *     description="This is a thing that user is looking for.",
     *     format="string",
     *     example="Merchandising"
     * )
     *
     * @var string
     */
    private $user_seeking;

}