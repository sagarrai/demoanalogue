<?php

namespace App\Virtual\Models\user;

/**
 * @OA\Schema(
 *     title="User Login Model",
 *     description="User login model",
 * )
 */
class UserLoginModel
{
    /**
     * @OA\Property(
     *     title="Status",
     *     description="Status of response",
     *     format="bool",
     *     example="true or false",
     * )
     *
     * @var bool
     */

    private  $status;

    /**
     * @OA\Property(
     *     title="Cosde",
     *     description="Status code of response",
     *     format="integer",
     *     example="200",
     * )
     *
     * @var int
     */

    private $code;

    /**
     * @OA\Property(
     *     title="Message",
     *     description="Message of current action",
     *     format="string",
     *     example="User login successful.",
     * )
     *
     * @var string
     */
    private $message;
    /**
     * @OA\Property(
     *     title="Email",
     *     description="Email",
     *     format="email",
     *     example="utshab.analogue51@gmail.com",
     * )
     *
     * @var string
     */
    private $email;


}