<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="OfficeInfo",
 *     description="Office info Model",
 * )
 */
class OfficeInfoModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="website_logo",
     *     description="This is a website logo",
     *     format="string",
     *     example="analogue.png"
     * )
     *
     * @var string
     */
    private $website_logo;

      /**
     * @OA\Property(
     *     title="footer_title",
     *     description="This is a footer title",
     *     format="string",
     *     example="test"
     * )
     *
     * @var string
     */
    private $footer_title;

      /**
     * @OA\Property(
     *     title="footer_description",
     *     description="This is a footer description",
     *     format="string",
     *     example="This is a description"
     * )
     *
     * @var string
     */
    private $footer_description;

      /**
     * @OA\Property(
     *     title="address",
     *     description="This is a address for the company",
     *     format="string",
     *     example="Bishal Chowk"
     * )
     *
     * @var string
     */
    private $address;

     /**
     * @OA\Property(
     *     title="phone",
     *     description="This is a phone number for the company",
     *     format="integer",
     *     example="9802320803"
     * )
     *
     * @var integer
     */
    private $phone;

    /**
     * @OA\Property(
     *     title="email",
     *     description="This is a email address of the company",
     *     format="string",
     *     example="info@analogueinc.com.np",
     * )
     *
     * @var string
     */
    private $email;

    /**
     * @OA\Property(
     *     title="website",
     *     description="This is a website for the company",
     *     format="string",
     *     example="www.analogueinc.com.np",
     * )
     *
     * @var string
     */
    private $website;

    /**
     * @OA\Property(
     *     title="facebook_link",
     *     description="This is a facebook link",
     *     format="string",
     *     example="https://www.facebook.com/Analogue-Inc-111451123769739",
     * )
     *
     * @var string
     */
    private $facebook_link;

    /**
     * @OA\Property(
     *     title="twitter_link",
     *     description="This is a twitter link",
     *     format="string",
     *     example="https://twitter.com/AnalogueInc",
     * )
     *
     * @var string
     */
    private $twitter_link;

    /**
     * @OA\Property(
     *     title="linkedin_link",
     *     description="This is a facebook link",
     *     format="string",
     *     example="https://www.linkedin.com/analoguein123/",
     * )
     *
     * @var string
     */
    private $linkedin_link;

    /**
     * @OA\Property(
     *     title="instagram_link",
     *     description="This is a facebook link",
     *     format="string",
     *     example="https://www.instagram.com/analoguein123/",
     * )
     *
     * @var string
     */
    private $instagram_link;

        /**
     * @OA\Property(
     *     title="copyright_description",
     *     description="This is a copyright description",
     *     format="string",
     *     example="Copyright © 2020 ALL RIGHTS RESERVED. POWERED BY ANALOGUE INC., A PGNSONS COMPANY.",
     * )
     *
     * @var string
     */
    private $copyright_description;

}