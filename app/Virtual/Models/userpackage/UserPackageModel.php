<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Package",
 *     description="Package Model",
 * )
 */
class UserPackageModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="full_name",
     *     description="This is a for user package",
     *     format="string",
     *     example="Harry Porter"
     * )
     *
     * @var string
     */
    private $full_name;

      /**
     * @OA\Property(
     *     title="email",
     *     description="This is a user packge email",
     *     format="string",
     *     example="harry@gmail.com"
     * )
     *
     * @var string
     */
    private $email;

      /**
     * @OA\Property(
     *     title="phone",
     *     description="This is a user packge phone number",
     *     format="integer",
     *     example="981129384"
     * )
     *
     * @var integer
     */
    private $phone;

    /**
     * @OA\Property(
     *     title="company_name",
     *     description="Name of the company",
     *     format="string",
     *     example="ABC company"
     * )
     *
     * @var string
     */
    public $company_name;

    /**
     * @OA\Property(
     *     title="company_address",
     *     description="Address of the company",
     *     format="string",
     *     example="Koteswor, Kathmandu"
     * )
     *
     * @var string
     */
    public $company_address;

      /**
     * @OA\Property(
     *     title="package_name",
     *     description="This is for package name entered by the user",
     *     format="string",
     *     example="Social Media Marketing"
     * )
     *
     * @var string
     */
    private $package_name;

    /**
     * @OA\Property(
     *     title="package_type",
     *     description="Type of the package",
     *     format="string",
     *     example="Basic Type"
     * )
     *
     * @var string
     */
    public $package_type;

    /**
     * @OA\Property(
     *     title="package_details",
     *     description="All list of small packages chose by the user",
     *     format="string",
     *     example="Style guidelines, Typography, Email"
     * )
     *
     * @var string
     */
    private $package_details;

    /**
     * @OA\Property(
     *     title="package_total_price",
     *     description="This is Total price of the package",
     *     format="integer",
     *     example="50000"
     * )
     *
     * @var integer
     */
    private $package_total_price;

    /**
     * @OA\Property(
     *     title="other_message",
     *     description="Other message that user have enter",
     *     format="string",
     *     example="I want little cheaper price of your package"
     * )
     *
     * @var string
     */
    public $other_message;

    /**
     * @OA\Property(
     *     title="contact",
     *     description="Total number of contact user have",
     *     format="integer",
     *     example="1"
     * )
     *
     * @var integer
     */
    public $contact;

    /**
     * @OA\Property(
     *     title="template",
     *     description="otal number of template user have",
     *     format="integer",
     *     example="2"
     * )
     *
     * @var integer
     */
    public $template;

    /**
     * @OA\Property(
     *     title="newsletter",
     *     description="otal number of newsletter user have",
     *     format="integer",
     *     example="3"
     * )
     *
     * @var integer
     */
    public $newsletter;

    /**
     * @OA\Property(
     *     title="social_post",
     *     description="otal number of social post user have",
     *     format="integer",
     *     example="4"
     * )
     *
     * @var integer
     */
    public $social_post;

}