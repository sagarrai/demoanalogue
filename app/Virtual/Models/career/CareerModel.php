<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Career",
 *     description="Career Model",
 * )
 */
class CareerModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="full_name",
     *     description="This is a full_name",
     *     format="string",
     *     example="test"
     * )
     *
     * @var string
     */
    private $full_name;

      /**
     * @OA\Property(
     *     title="phone_number",
     *     description="This is a phone number",
     *     format="integer",
     *     example="9816308014"
     * )
     *
     * @var integer
     */
    private $phone_number;

      /**
     * @OA\Property(
     *     title="email_address",
     *     description="This is a email address",
     *     format="string",
     *     example="sagar@gmail.com"
     * )
     *
     * @var string
     */
    private $email_address;

      /**
     * @OA\Property(
     *     title="address_line1",
     *     description="This is a address line 1 text",
     *     format="string",
     *     example="Bishal Chowk"
     * )
     *
     * @var string
     */
    private $address_line1;

     /**
     * @OA\Property(
     *     title="apply_for",
     *     description="This is a apply for section",
     *     format="string",
     *     example="Manager"
     * )
     *
     * @var string
     */
    private $apply_for;

    /**
     * @OA\Property(
     *     title="expected_salary",
     *     description="Expected salary of the user",
     *     format="integer",
     *     example="40000",
     * )
     *
     * @var integer
     */
    private $expected_salary;

    /**
     * @OA\Property(
     *     title="apply_date",
     *     description="This is a meta description",
     *     format="datetime",
     *     example="2020-01-27 17:50:45",
     * )
     *
     * @var \DateTime
     */
    private $apply_date;

    /**
     * @OA\Property(
     *     title="cv_file",
     *     description="This is a cv file",
     *     format="string",
     *     example="cv.docx",
     * )
     *
     * @var string
     */
    private $cv_file;

    /**
     * @OA\Property(
     *     title="cover_letter",
     *     description="This is for cover letter",
     *     format="string",
     *     example="cv.docx",
     * )
     *
     * @var string
     */
    private $cover_letter;

}