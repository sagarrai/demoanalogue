<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Test",
 *     description="Test Model",
 * )
 */
class TestModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="user_id",
     *     description="user ID",
     *     format="integer",
     *     example=1,
     * )
     *
     * @var integer
     */
    private $user_id;

    /**
     * @OA\Property(
     *     title="file",
     *     description="This is a image name",
     *     format="string",
     *     example="img.jpg",
     * )
     *
     * @var string
     */
    private $file;

    /**
     * @OA\Property(
     *     title="file_url",
     *     description="This is a url",
     *     format="string",
     *     example="http://www.example.com/test.jpg",
     * )
     *
     *
     * @var string
     */
    private $file_url;

}