<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Package",
 *     description="Package Model",
 * )
 */
class PackageModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="name",
     *     description="This is a for package",
     *     format="string",
     *     example="test"
     * )
     *
     * @var string
     */
    private $name;

      /**
     * @OA\Property(
     *     title="code",
     *     description="This is a packge code",
     *     format="integer",
     *     example="1200"
     * )
     *
     * @var integer
     */
    private $code;

      /**
     * @OA\Property(
     *     title="description",
     *     description="This is a description for package",
     *     format="string",
     *     example="sagar@gmail.com"
     * )
     *
     * @var string
     */
    private $description;

      /**
     * @OA\Property(
     *     title="module",
     *     description="This is for package module",
     *     format="string",
     *     example="Branding and Marketing"
     * )
     *
     * @var string
     */
    private $module;

     /**
     * @OA\Property(
     *     title="type",
     *     description="Different type of package like basic, premium, standard or taylor",
     *     format="string",
     *     example="basic, premium"
     * )
     *
     * @var string
     */
    private $type;

}