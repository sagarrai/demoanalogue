<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Postcategory",
 *     description="Postcategory model",
 * )
 */
class PostcategoryModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="title",
     *     description="Title of the postcategory",
     *     example="Blog"
     * )
     *
     * @var string
     */
    private $title;


    /**
     * @OA\Property(
     *     title="description",
     *     description="Description of the postcategory",
     *     format="string",
     *     example="Blog category",
     * )
     *
     * @var string
     */
    private $description;

    /**
     * @OA\Property(
     *     title="slug",
     *     description="Url of the postcategory",
     *     format="string",
     *     example="blog-category",
     * )
     *
     * @var string
     */
    private $slug;

    /**
     * @OA\Property(
     *     title="active",
     *     description="Url of the postcategory",
     *     format="boolean",
     *     example="true",
     * )
     *
     * @var boolean
     */
    private $active;

     /**
     * @OA\Property(
     *     title="weight",
     *     description="Weight of the postcategory it use in post ordering asc or desc",
     *     format="integer",
     *     example="5",
     * )
     *
     * @var boolean
     */

    private $weight;

    /**
     * @OA\Property(
     *     title="user_id",
     *     description="Associate user id of the postcategory",
     *     format="integer",
     *     example="1",
     * )
     *
     * @var integer
     */
    private $user_id;

    /**
     * @OA\Property(
     *     title="Created at",
     *     description="Created at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $created_at;

    /**
     * @OA\Property(
     *     title="Updated at",
     *     description="Updated at",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $updated_at;
}
