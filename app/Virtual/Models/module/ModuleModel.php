<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Module",
 *     description="Module Model",
 * )
 */
class ModuleModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="name",
     *     description="This is a name",
     *     format="string",
     *     example="Branding and Marketing"
     * )
     *
     * @var string
     */
    private $name;

      /**
     * @OA\Property(
     *     title="meta_description",
     *     description="This is a meta description",
     *     format="string",
     *     example="This is a meta description"
     * )
     *
     * @var string
     */
    private $meta_description;

      /**
     * @OA\Property(
     *     title="status",
     *     description="Status for module",
     *     format="integer",
     *     example="1"
     * )
     *
     * @var integer
     */
    private $status;

    private $banner_image;

}