<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Post",
 *     description="Post Model",
 * )
 */
class PostModel
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="integer",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="title",
     *     description="This is a title",
     *     format="string",
     *     example="test"
     * )
     *
     * @var string
     */
    private $title;

      /**
     * @OA\Property(
     *     title="slug",
     *     description="This is a slug",
     *     format="string",
     *     example="slug"
     * )
     *
     * @var string
     */
    private $slug;

      /**
     * @OA\Property(
     *     title="slug",
     *     description="This is a short text",
     *     format="string",
     *     example="This is a short text message"
     * )
     *
     * @var string
     */
    private $short_text;

      /**
     * @OA\Property(
     *     title="description",
     *     description="This is a description text",
     *     format="string",
     *     example="This is a description text message"
     * )
     *
     * @var string
     */
    private $description;

     /**
     * @OA\Property(
     *     title="category",
     *     description="This is a category",
     *     format="string",
     *     example="This is a category"
     * )
     *
     * @var string
     */
    private $category;

    /**
     * @OA\Property(
     *     title="user_id",
     *     description="Associate user id of the post",
     *     format="integer",
     *     example=1,
     * )
     *
     * @var integer
     */
    private $user_id;

    /**
     * @OA\Property(
     *     title="meta_description",
     *     description="This is a meta description",
     *     format="string",
     *     example="test",
     * )
     *
     * @var string
     */
    private $meta_description;

    /**
     * @OA\Property(
     *     title="keywords",
     *     description="This is a keywords",
     *     format="string",
     *     example="keywords",
     * )
     *
     * @var string
     */
    private $keywords;

    /**
     * @OA\Property(
     *     title="feature_image",
     *     description="This is a feature image",
     *     format="string",
     *     example="img.jpg",
     * )
     *
     * @var string
     */
    private $feature_image;

    /**
     * @OA\Property(
     *     title="publish",
     *     description="This is a publish",
     *     format="integer",
     *     example=0,
     * )
     *
     *
     * @var integer
     */
    private $publish;

    /**
     * @OA\Property(
     *     title="office_id",
     *     description="Associate office id of the post",
     *     format="integer",
     *     example=1,
     * )
     *
     * @var integer
     */
    private $office_id;

    /**
     * @OA\Property(
     *     title="deadline",
     *     description="Deadline for the job",
     *     format="datetime",
     *     example="2020-01-27 17:50:45",
     * )
     *
     * @var \DateTime
     */
    private $deadline;

    /**
     * @OA\Property(
     *     title="other_description",
     *     description="Other description",
     *     format="string",
     *     example="Lorem ipsum",
     * )
     *
     * @var string
     */
    private $other_description;

    /**
     * @OA\Property(
     *     title="no_of_opening",
     *     description="Number of opening",
     *     format="integer",
     *     example=2,
     * )
     *
     * @var integer
     */
    private $no_of_opening;

}