<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OfficeRepository::class, EloquentOffice::class);
        $this->app->singleton(PostInterface::class, RepositoryPost::class);
        $this->app->singleton(UserdetailInterface::class, RepositoryUserdetail::class);
        $this->app->singleton(ContactInterface::class, RepositoryContact::class);
        $this->app->singleton(CareerInterface::class, CareerRepository::class);
        $this->app->singleton(OfficeInfoInterface::class, OfficeInfoRepository::class);
        $this->app->singleton(ModuleInterface::class, ModuleRepository::class);
        $this->app->singleton(PackageInterface::class, PackageRepository::class);
        $this->app->singleton(UserPackageInterface::class, UserPackageRepository::class);
        $this->app->singleton(ConsultancyInterface::class, ConsultancyRepository::class);
        $this->app->singleton(BusinessStationaryInterface::class, BusinessStationaryRepository::class);
        $this->app->singleton(MerchandiseStationaryInterface::class, MerchandiseStationaryRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
