<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Career\CareerRepository;
use Illuminate\Http\Request;
use Validator;

class CareerController extends Controller
{
    public function __construct(CareerRepository $career, BaseController $response){
        $this->career = $career;
        $this->response = $response;
    }

     /**
     * @OA\get(
     *      path="/career",
     *      operationId="getcareerList",
     *      tags={"Career"},
     *      summary="Get all career",
     *      description="Retrive the all contact by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function index()
    {
        try {
            $data = $this->career->getAll();
            return $this->response->sendSuccess($data, 'career list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'career list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/career/create",
     *      operationId="StoreCareer",
     *      tags={"Career"},
     *      summary="Create new career",
     *      description="Create new career",
     *
     *      @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *
     *             @OA\Schema(
     *                 allOf={
     *
     *                     @OA\JsonContent(ref="#/components/schemas/StoreCareerRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="cv_file",
     *                             property="cv_file",
     *                             type="string",
     *                             format="binary"
     *                         )
     *                     ),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="cover_letter",
     *                             property="cover_letter",
     *                             type="string",
     *                             format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/CareerModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *
     * )
     */
    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'full_name' => 'required',
                'phone_number' => 'required',
                'email_address' => 'required',
                'apply_date' => 'required',
                ]);

            if ($validator->fails()) {
                return $this->response->sendError('Validation Error.',['error'=>$validator->errors()],400);
            }

            //This is for cv upload
            $file = $request->file('cv_file');
            $new_name = $request->email_address.rand() . '.' . $file->getClientOriginalExtension();
            $link1 = env('APP_URL').'/'.'public/cvupload/' . $new_name;
            $file->move(public_path('/cvupload/'), $new_name);


            //This is for cover letter
            $file = $request->file('cover_letter');
            $cover_new_name = $request->email_address.rand() . '.' . $file->getClientOriginalExtension();
            $link2 = env('APP_URL').'/'.'public/coverletter/' . $cover_new_name;
            $file->move(public_path('/coverletter/'), $cover_new_name);


            $data = $this->career->create(array_merge($request->all(),['cv_file' => $new_name, 'cover_letter' => $cover_new_name ]));

            if($data) {
                $details = [
                    'full_name' => $request->full_name,
                    'phone_number' => $request->phone_number,
                    'email_address' => $request->email_address,
                    'address_line1' => $request->address_line1,
                    'apply_for' => $request->apply_for,
                    'expected_salary' => $request->expected_salary,
                    'cv_file' => $link1,
                    'cover_letter' => $link2,
                ];
                $to = ['hr@analogueinc.com.np',$details['email_address']];
                $this->career->sendmail($details, $to);
            }

            return $this->response->sendSuccess($data, 'career created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'contact cannot created.'],400);
        }
    }


     /**
     * @OA\get(
     *      path="/career/{id}",
     *      operationId="getcareerDetails",
     *      tags={"Career"},
     *      summary="Get detail of career",
     *      description="Retrive the detail career by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="career id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreCareerRequest")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->career->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'career retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'career id not found .'],200);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'career cannot retrived .'],500);
        }
    }

    /**
     *
     * @OA\delete(
     *      path="/career/{id}",
     *      operationId="deletecareerDetails",
     *      tags={"Career"},
     *      summary="Delete career",
     *      description="Delete career",
     *
     *       @OA\Parameter(
     *          name="id",
     *          description="career id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/StoreCareerRequest")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function destroy($id)
    {
        try{
            $data = $this->career->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'career id not found .'],200);
            }

            $imagePath = $this->career->show($id);
            if($imagePath){
                if(file_exists(public_path('/imageupload/cvfile/').$imagePath->cvFile)){
                    unlink(public_path('/imageupload/cvfile/').$imagePath->cvFile);
                }

            }
            $data = $this->career->delete($id);

            return $this->response->sendSuccess($data, 'career delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }


}
