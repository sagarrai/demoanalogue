<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Model\Office;
use App\Repositories\Office\EloquentOffice;
use App\Http\Controllers\Controller; 
use Validator; 
use App\Http\Controllers\API\BaseController;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $eloquentOffice;
    private $successStatus = 200;

    public function __construct(EloquentOffice $eloquentOffice, BaseController $response) 
    {
        $this->eloquentOffice = $eloquentOffice;
        $this->response = $response;
    }

    /**
     * @OA\Get(
     *      path="/alloffices",
     *      operationId="getOffice",
     *      tags={"Office"},
     *      summary="Get all Offices",
     *      description="This function retrieves all office lists ",
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */

    public function index() 
    {
        try {
            $data = $this->eloquentOffice->getAll();
            return $this->response->sendSuccess($data, 'Office get all successful.');
        } catch(Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Cannot get office list'],401);
        }
    }

    /**
     * @OA\Post(
     *      path="/office",
     *      operationId="postOffice",
     *      tags={"Office"},
     *      summary="Store Office Detail",
     *      description="This function Stores an office detail",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreOfficeRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */

    public function store(Request $request) 
    {
        
        try {
            $validator = Validator::make($request->all(), [ 
                'user_id' => 'required', 
                'office_code' => 'required', 
                'office_name' => 'required', 
                'email' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            $data = $this->eloquentOffice->create($request->all());
            return $this->response->sendSuccess($data, 'Office created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'Office is not created.'],400);        
        }
    }

    /**
     * @OA\Get(
     *      path="/office/{id}",
     *      operationId="showOffice",
     *      tags={"Office"},
     *      summary="Show Office data Detail",
     *      description="This function show an office detail",
     * @OA\Parameter(
     *          name="id",
     *          description="Office id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */
    
    public function show($id) 
    {
        try {
            $officeShow = $this->eloquentOffice->show($id);
            $success['eloquentOffice'] = $officeShow;
            return response()->json(['success'=>$success], $this->successStatus);
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],101);        
        }
    }

    /**
     * @OA\Put(
     *      path="/office/{id}",
     *      operationId="putOffice",
     *      tags={"Office"},
     *      summary="Update Office data Detail",
     *      description="This function update an office detail",
     * @OA\Parameter(
     *          name="id",
     *          description="Office id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateOfficeRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */

    public function update(Request $request, $id) 
    {
        try {
            $officeUpdate = Office::find($id);
            $officeUpdate->update($request->all());
            $success['eloquentOffice'] = $officeUpdate;
            return response()->json(['success'=>$success], $this->successStatus);
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],101);        
        }
    }

    /**
     * @OA\Delete(
     *      path="/office/{id}",
     *      operationId="deleteOffice",
     *      tags={"Office"},
     *      summary="Get all Offices",
     *      description="This function delete all office lists ",
     * @OA\Parameter(
     *          name="id",
     *          description="Office id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */
    
    public function destroy($id) 
    {
        try {
            $officeDelete = $this->eloquentOffice->delete($id);
            
            $success['eloquentOffice'] = $officeDelete;
            return response()->json(['success'=>$success], $this->successStatus);
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],101);        
        }
    }   
}
