<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Userpackage\UserPackageRepository;
use Illuminate\Http\Request;
use Validator;
use App\Model\UserPackage;

class UserPackageController extends Controller
{
	public function __construct(UserPackageRepository $userpackage, BaseController $response){
        $this->userpackage = $userpackage;
        $this->response = $response;
    } 

    /**
     * @OA\get(
     *      path="/userpackage",
     *      operationId="getuserpackageList",
     *      tags={"UserPackage"},
     *      summary="Get all package",
     *      description="Retrive the all user details package by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function index()
    {
        try {
            $data = $this->userpackage->getAll();
            return $this->response->sendSuccess($data, 'userpackage list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'userpackage list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/userpackage/create",
     *      operationId="StoreUserPackage",
     *      tags={"UserPackage"},
     *      summary="Create new user package",
     *      description="Create new user package",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreUserPackageRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserPackageModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * 
     * )
     */

    public function store(Request $request)
    {
        try {
            $data = $this->userpackage->create($request->all());

            return $this->response->sendSuccess($data, 'user package created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'user package cannot created.'],400);        
        }
    }

    /** 
     * 
     * @OA\delete(
     *      path="/userpackage/{id}",
     *      operationId="deleteuserpackageDetails",
     *      tags={"UserPackage"},
     *      summary="Delete user package",
     *      description="Delete user package",
     * 
     *       @OA\Parameter(
     *          name="id",
     *          description="user package id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserPackageModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 
    public function destroy($id)
    {
        try{
            $data = $this->userpackage->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'User Package id not found .'],201);
            }

            $data = $this->userpackage->delete($id);
            return $this->response->sendSuccess($data, 'User package delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }
}
