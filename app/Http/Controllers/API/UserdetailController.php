<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Model\Userdetail;
use App\Repositories\Userdetail\RepositoryUserdetail;
use App\Http\Controllers\Controller; 
use Validator; 

class UserdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $repositoryUserdetail;
    private $successStatus = 200;

    public function __construct(RepositoryUserdetail $repositoryUserdetail) 
    {
        $this->repositoryUserdetail = $repositoryUserdetail;
    }

    public function index()
    {
        try {
            $userdetailAll = $this->repositoryUserdetail->getAll();
            $success['repositoryUserdetail'] = $userdetailAll;
            return response()->json(['success'=>$success], $this->successStatus); 
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],101);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [ 
                'user_id' => 'required', 
                'phone_number' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Unauthorised.',['error'=>$validator->errors()],401);            
            }

            $userdetailStore = $this->repositoryUserdetail->create($request->all());
            $success['repositoryUserdetail'] = $userdetailStore;
            return response()->json(['success'=>$success], $this->successStatus); 
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],401);        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Userdetail  $userdetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userdetailShow = $this->repositoryUserdetail->show($id);
            $success['repositoryUserdetail'] = $userdetailShow;
            return response()->json(['success'=>$success], $this->successStatus);
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],101);        
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Userdetail  $userdetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $userdetailUpdate = Userdetail::find($id);
            $userdetailUpdate->update($request->all());
            $success['repositoryUserdetail'] = $userdetailUpdate;
            return response()->json(['success'=>$success], $this->successStatus);
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],101);        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Userdetail  $userdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $userdetailDelete = $this->repositoryUserdetail->delete($id);
            
            $success['repositoryUserdetail'] = $userdetailDelete;
            return response()->json(['success'=>$success], $this->successStatus);
        }
        catch (Exception $e) {
            return $this->response->sendError('Unauthorised.',['error'=>'Unauthorised'],101);        
        }
    }
}
