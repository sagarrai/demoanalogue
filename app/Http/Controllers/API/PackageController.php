<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Package\PackageRepository;
use Illuminate\Http\Request;
use Validator;

class PackageController extends Controller
{
    public function __construct(PackageRepository $package, BaseController $response){
        $this->package = $package;
        $this->response = $response;
    } 

    /**
     * @OA\get(
     *      path="/package",
     *      operationId="getpackageList",
     *      tags={"Package"},
     *      summary="Get all package",
     *      description="Retrive the all package by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function index()
    {
        try {
            $data = $this->package->getAll();
            return $this->response->sendSuccess($data, 'package list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'package list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/package/create",
     *      operationId="StorePackage",
     *      tags={"Package"},
     *      summary="Create new package",
     *      description="Create new package",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StorePackageRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PackageModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     * )
     */

    public function store(Request $request)
    {
        try {
            $data = $this->package->create($request->all());

            return $this->response->sendSuccess($data, 'package created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'package cannot created.'],400);        
        }
    }
}
