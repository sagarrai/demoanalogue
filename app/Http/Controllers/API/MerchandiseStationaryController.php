<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Model\MerchandiseStationary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Repositories\MerchandiseStationary\MerchandiseStationaryRepository;
use App\Http\Controllers\API\BaseController;

class MerchandiseStationaryController extends Controller
{
    public function __construct(MerchandiseStationaryRepository $merchandisestationary, BaseController $response){
        $this->merchandisestationary = $merchandisestationary;
        $this->response = $response;
    }

   /**
     * @OA\get(
     *      path="/merchandisestationary",
     *      operationId="getMerchandiseStationary",
     *      tags={"MerchandiseStationary"},
     *      summary="Get all merchandisestationary",
     *      description="Retrive the all merchandisestationary by user",
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MerchandiseStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */

    public function index()
    {
        try {
            $data = $this->merchandisestationary->getAll();
            return $this->response->sendSuccess($data, 'Merchandise Stationary all list successfully.');
        } 
        catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'Merchandise Stationary all cannot list.'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/merchandisestationary",
     *      operationId="StoreMerchandiseStationary",
     *      tags={"MerchandiseStationary"},
     *      summary="Create new",
     *      description="Create a new Merchandisestationary ",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     * 
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/StoreMerchandiseStationaryRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="brand_image",
     *                             property="brand_image",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MerchandiseStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * 
     *      security={ {"bearer": {}} },
     * )
     */

    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [ 
                'brand_name' => 'required', 
                'brand_description' => 'required', 
                'brand_image' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            if($request->file('brand_image') != "")
            {
                $image = $request->file('brand_image');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $link = env('APP_URL').'public/imageupload/merchandisestationary/' . $new_name;

                $image->move(public_path('/imageupload/merchandisestationary/'), $new_name);

                $data = $this->merchandisestationary->create(array_merge($request->all(),['brand_image' => $new_name ]));
            }
            else
            {
                $data = $this->merchandisestationary->create(array_merge($request->all(),['brand_image' => "" ])); 
            }
               
            return $this->response->sendSuccess($data, 'Merchandise Stationary Created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'merchandisestationary cannot created.'],400);        
        }
    }

    /**
     * @OA\get(
     *      path="/merchandisestationary/{id}",
     *      operationId="getmerchandisestationaryDetails",
     *      tags={"MerchandiseStationary"},
     *      summary="Get detail of merchandisestationary",
     *      description="Retrive the detail merchandisestationary  by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="merchandisestationary id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MerchandiseStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */
    
    public function show($id)
    {
        try {
            $data = $this->merchandisestationary->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'merchandisestationary retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'merchandisestationary id not found .'],200);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'merchandisestationary cannot retrived .'],500);
        }
    }

    /** 
     * 
     * @OA\put(
     *      path="/merchandisestationary/{id}",
     *      operationId="UpdatemerchandisestationaryDetails",
     *      tags={"MerchandiseStationary"},
     *      summary="Update MerchandiseStationary",
     *      description="Update the MerchandiseStationary by id",
     * 
    *       @OA\Parameter(
    *          name="id",
    *          description="MerchandiseStationary id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     *                     
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/UpdateMerchandiseStationaryRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="brand_image",
     *                             property="brand_image",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MerchandiseStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 

    public function update(Request $request, $id)
    {
       
        try {
            $view  = $this->merchandisestationary->show($id);
            
            $validator = Validator::make($request->all(), [ 
                'brand_name' => 'required', 
                'brand_description' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            if($request->file('brand_image') !=""){
                $image = $request->file('brand_image');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $image->move(public_path('/imageupload/merchandisestationary/'), $new_name);

                $data = $this->merchandisestationary->edit(array_merge($request->all(),['brand_image' => $new_name ]),$id);
            }else{
                $data = $this->merchandisestationary->edit(array_merge($request->all(),['brand_image' => $view->brand_image ]),$id); 
            }
    
            return $this->response->sendSuccess($data, 'merchandisestationary updated.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Internal Server Error .',['error'=>'Server Error'],500);        
        }
    }

    /** 
     * 
     * @OA\delete(
     *      path="/merchandisestationary/{id}",
     *      operationId="deletemerchandisestationaryDetails",
     *      tags={"MerchandiseStationary"},
     *      summary="Delete merchandisestationary",
     *      description="Delete merchandisestationary",
     * 
     *       @OA\Parameter(
     *          name="id",
     *          description="post id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/MerchandiseStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 

    public function destroy($id)
    {
        try{
            $data = $this->merchandisestationary->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'merchandisestationary id not found .'],200);
            }

            if($data){
                if(file_exists(public_path('/imageupload/merchandisestationary/').$data->brand_image)){
                    unlink(public_path('/imageupload/merchandisestationary/').$data->brand_image);
                }
            }  
            $data = $this->merchandisestationary->delete($id);
            
            return $this->response->sendSuccess($data, 'merchandisestationary delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

}
