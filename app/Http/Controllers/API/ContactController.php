<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Model\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\Repositories\Contact\RepositoryContact;

class ContactController extends Controller
{
    public function __construct(RepositoryContact $contact, BaseController $response){
        $this->contact = $contact;
        $this->response = $response;
    }

     /**
     * @OA\get(
     *      path="/contact",
     *      operationId="getContactList",
     *      tags={"Contact"},
     *      summary="Get all contact",
     *      description="Retrive the all contact by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ContactModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function index()
    {
        try {
            $data = $this->contact->getAll();
            return $this->response->sendSuccess($data, 'contact list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'contact cannot list.'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/contact/create",
     *      operationId="StoreContact",
     *      tags={"Contact"},
     *      summary="Create new contact",
     *      description="Create new contact",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreContactRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ContactModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *
     * )
     */
    public function store(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'full_name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'subject' => 'required',
                'message' => 'required',
                ]);

            if ($validator->fails()) {
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);
            }

            $data = $this->contact->create($request->all());
            if($data) {
                $details = [
                    'title' => $request->subject,
                    'body' => $request->message,
                    'email' => $request->email,
                ];
                $to = "sitoula.analogue51@gmail.com";
                $this->contact->sendmail($details, $to);  //Create then send email.
            }
            return $this->response->sendSuccess($data, 'contact created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'contact cannot created.'],400);
        }
    }

    /**
     * @OA\get(
     *      path="/contact/{id}",
     *      operationId="getcontactDetails",
     *      tags={"Contact"},
     *      summary="Get detail of contact",
     *      description="Retrive the detail contact by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="contact id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ContactModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->contact->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'contact retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'contact id not found .'],201);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'contact cannot retrived .'],500);
        }
    }

    /**
     *
     * @OA\put(
     *      path="/contact/{id}",
     *      operationId="UpdatecontactDetails",
     *      tags={"Contact"},
     *      summary="Update contact",
     *      description="Update the contact by id",
     *
    *       @OA\Parameter(
    *          name="id",
    *          description="contact id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    *
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateContactRequest")
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ContactModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function update(Request $request, $id)
    {
        try{
            $validator = Validator::make($request->all(), [
                'full_name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'subject' => 'required',
                'message' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);
            }

            $data = $this->contact->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'contact id not found .'],201);
            }
            $data = $this->contact->update($request->all(), $id);
            $data = $this->contact->show($id);

            return $this->response->sendSuccess($data, 'contact update success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

     /**
     *
     * @OA\delete(
     *      path="/contact/{id}",
     *      operationId="deletecontactDetails",
     *      tags={"Contact"},
     *      summary="Delete contact",
     *      description="Delete contact",
     *
     *       @OA\Parameter(
     *          name="id",
     *          description="contact id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ContactModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */
    public function destroy($id)
    {
        try{
            $data = $this->contact->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'contact id not found .'],201);
            }

            $data = $this->contact->delete($id);
            return $this->response->sendSuccess($data, 'contact delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }
}
