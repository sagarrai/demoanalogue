<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Consultancy\ConsultancyRepository;
use Illuminate\Http\Request;
use Validator;

class ConsultancyController extends Controller
{
    public function __construct(ConsultancyRepository $consultancy, BaseController $response){
        $this->consultancy = $consultancy;
        $this->response = $response;
    }

    /**
     * @OA\get(
     *      path="/consultancy",
     *      operationId="getconsultancyList",
     *      tags={"Consultancy"},
     *      summary="Get all package",
     *      description="Retrive the all user details package by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *
     *     )
     */

    public function index()
    {
        try {
            $data = $this->consultancy->getAll();
            return $this->response->sendSuccess($data, 'consultancy list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'consultancy list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/consultancy/create",
     *      operationId="StoreConsultancyRequest",
     *      tags={"Consultancy"},
     *      summary="Create new consultancy",
     *      description="Create new consultancy",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreConsultancyRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ConsultancyModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *
     * )
     */

    public function store(Request $request)
    {
        try {
            $data = $this->consultancy->create($request->all());

            if($data) {
                $details = [
                    'full_name' => $request->full_name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'budget' => $request->budget,
                    'title' => $request->objective,
                    'body' => $request->message,
                    'deadline' => $request->deadline,
                ];

                $to = $details['email'];
                $this->consultancy->sendmail($details, $to);
            }

            return $this->response->sendSuccess($data, 'consultancy created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'consultancy cannot created.'],400);
        }
    }

    /** 
     * 
     * @OA\delete(
     *      path="/consultancy/{id}",
     *      operationId="deleteconsultancyDetails",
     *      tags={"Consultancy"},
     *      summary="Delete consultancy",
     *      description="Delete consultancy",
     * 
     *       @OA\Parameter(
     *          name="id",
     *          description="consultancy id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ConsultancyModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 
    public function destroy($id)
    {
        try{
            $data = $this->consultancy->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'Consultancy id is not found .'],201);
            }

            $data = $this->consultancy->delete($id);
            return $this->response->sendSuccess($data, 'Consultancy deleted successfully.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

}
