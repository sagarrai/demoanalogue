<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Model\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Repositories\Post\RepositoryPost;
use App\Http\Controllers\API\BaseController;

class PostController extends Controller
{
    public function __construct(RepositoryPost $post, BaseController $response){
        $this->post = $post;
        $this->response = $response;
    }

   /**
     * @OA\get(
     *      path="/post",
     *      operationId="getPostList",
     *      tags={"Post"},
     *      summary="Get all post",
     *      description="Retrive the all post by user",
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */

    public function index()
    {
        try {
            $data = $this->post->getAll();
            return $this->response->sendSuccess($data, 'Post all list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'Post all cannot list.'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/post",
     *      operationId="StorePost",
     *      tags={"Post"},
     *      summary="Create new",
     *      description="Create new post ",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     * 
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/StorePostRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="feature_image",
     *                             property="feature_image",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * 
     * 
     *      security={ {"bearer": {}} },
     * )
     */

    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [ 
                'title' => 'required', 
                'user_id' => 'required', 
                'office_id' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            if($request->file('feature_image') !=""){
                $image = $request->file('feature_image');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $link = env('APP_URL').'public/imageupload/blog/' . $new_name;

                $image->move(public_path('/imageupload/blog/'), $new_name);

                $data = $this->post->create(array_merge($request->all(),['feature_image' => $new_name ]));
            }else{
                $data = $this->post->create(array_merge($request->all(),['feature_image' => "" ])); 
            }

               
            return $this->response->sendSuccess($data, 'Post created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'Post cannot created.'],400);        
        }
    }

    /**
     * @OA\get(
     *      path="/post/{id}",
     *      operationId="getpostDetails",
     *      tags={"Post"},
     *      summary="Get detail of post",
     *      description="Retrive the detail post  by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="Post id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->post->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'post retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'post id not found .'],200);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'post cannot retrived .'],500);
        }
    }

    /** 
     * 
     * @OA\put(
     *      path="/post/{id}",
     *      operationId="UpdatePostDetails",
     *      tags={"Post"},
     *      summary="Update Post",
     *      description="Update the Post by id",
     * 
    *       @OA\Parameter(
    *          name="id",
    *          description="Post id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     *                     
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/StorePostRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="feature_image",
     *                             property="feature_image",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 

    public function update(Request $request, $id)
    {
       
        try {
            $view  = $this->post->show($id);
            
            $validator = Validator::make($request->all(), [ 
                'title' => 'required', 
                'user_id' => 'required', 
                'office_id' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            if($request->file('feature_image') !=""){
                $image = $request->file('feature_image');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $image->move(public_path('/imageupload/blog/'), $new_name);

                $data = $this->post->edit(array_merge($request->all(),['feature_image' => $new_name ]),$id);
            }else{
                $data = $this->post->edit(array_merge($request->all(),['feature_image' => $view->feature_image ]),$id); 
            }
    
            return $this->response->sendSuccess($data, 'Post updated.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Internal Server Error .',['error'=>'Server Error'],500);        
        }
    }

    /** 
     * 
     * @OA\delete(
     *      path="/post/{id}",
     *      operationId="deletepostDetails",
     *      tags={"Post"},
     *      summary="Delete post",
     *      description="Delete post",
     * 
     *       @OA\Parameter(
     *          name="id",
     *          description="post id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 

    public function destroy($id)
    {
        try{
            $data = $this->post->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'post id not found .'],200);
            }

            if($data){
                if(file_exists(public_path('/imageupload/cvfile/').$data->feature_image)){
                    unlink(public_path('/imageupload/cvfile/').$data->feature_image);
                }
            }  
            $data = $this->post->delete($id);
            
            return $this->response->sendSuccess($data, 'post delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }
}
