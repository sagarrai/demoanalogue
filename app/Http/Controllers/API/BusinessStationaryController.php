<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Model\BusinessStationary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Repositories\BusinessStationary\BusinessStationaryRepository;
use App\Http\Controllers\API\BaseController;

class BusinessStationaryController extends Controller
{
    public function __construct(BusinessStationaryRepository $businessstationary, BaseController $response){
        $this->businessstationary = $businessstationary;
        $this->response = $response;
    }

   /**
     * @OA\get(
     *      path="/businessstationary",
     *      operationId="getBusinessStationary",
     *      tags={"BusinessStationary"},
     *      summary="Get all businessstationary",
     *      description="Retrive the all businessstationary by user",
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BusinessStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *     )
     */

    public function index()
    {
        try {
            $data = $this->businessstationary->getAll();
            return $this->response->sendSuccess($data, 'Business Stationary all list successfully.');
        } 
        catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'Business Stationary all cannot list.'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/businessstationary",
     *      operationId="StoreBusinessStationary",
     *      tags={"BusinessStationary"},
     *      summary="Create new",
     *      description="Create a new businessstationary ",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     * 
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/StoreBusinessStationaryRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="brand_image",
     *                             property="brand_image",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BusinessStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * 
     *      security={ {"bearer": {}} },
     * )
     */

    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [ 
                'brand_name' => 'required', 
                'brand_description' => 'required', 
                'brand_image' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            if($request->file('brand_image') != "")
            {
                $image = $request->file('brand_image');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $link = env('APP_URL').'public/imageupload/businessstationary/' . $new_name;

                $image->move(public_path('/imageupload/businessstationary/'), $new_name);

                $data = $this->businessstationary->create(array_merge($request->all(),['brand_image' => $new_name ]));
            }
            else
            {
                $data = $this->businessstationary->create(array_merge($request->all(),['brand_image' => "" ])); 
            }
               
            return $this->response->sendSuccess($data, 'Business Stationary created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'Businessstationary cannot created.'],400);        
        }
    }

    /**
     * @OA\get(
     *      path="/businessstationary/{id}",
     *      operationId="getbusinessstationaryDetails",
     *      tags={"BusinessStationary"},
     *      summary="Get detail of businessstationary",
     *      description="Retrive the detail businessstationary  by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="businessstationary id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BusinessStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */
    
    public function show($id)
    {
        try {
            $data = $this->businessstationary->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'businessstationary retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'businessstationary id not found .'],200);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'businessstationary cannot retrived .'],500);
        }
    }

    /** 
     * 
     * @OA\put(
     *      path="/businessstationary/{id}",
     *      operationId="UpdatebusinessstationaryDetails",
     *      tags={"BusinessStationary"},
     *      summary="Update BusinessStationary",
     *      description="Update the BusinessStationary by id",
     * 
    *       @OA\Parameter(
    *          name="id",
    *          description="BusinessStationary id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     *                     
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/StoreBusinessStationaryRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="brand_image",
     *                             property="brand_image",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BusinessStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 

    public function update(Request $request, $id)
    {
       
        try {
            $view = $this->businessstationary->show($id);
            
            $validator = Validator::make($request->all(), [ 
                'brand_name' => 'required', 
                'brand_description' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            if($request->file('brand_image') !=""){
                $image = $request->file('brand_image');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();

                $image->move(public_path('/imageupload/businessstationary/'), $new_name);

                $data = $this->businessstationary->edit(array_merge($request->all(),['brand_image' => $new_name ]),$id);
            }else{
                $data = $this->businessstationary->edit(array_merge($request->all(),['brand_image' => $view->brand_image ]),$id); 
            }
    
            return $this->response->sendSuccess($data, 'businessstationary updated.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Internal Server Error .',['error'=>'Server Error'],500);        
        }
    }

    /** 
     * 
     * @OA\delete(
     *      path="/businessstationary/{id}",
     *      operationId="deletebusinessstationaryDetails",
     *      tags={"BusinessStationary"},
     *      summary="Delete businessstationary",
     *      description="Delete businessstationary",
     * 
     *       @OA\Parameter(
     *          name="id",
     *          description="post id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/BusinessStationaryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 

    public function destroy($id)
    {
        try{
            $data = $this->businessstationary->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'businessstationary id not found .'],200);
            }

            if($data){
                if(file_exists(public_path('/imageupload/businessstationary/').$data->brand_image)){
                    unlink(public_path('/imageupload/businessstationary/').$data->brand_image);
                }
            }  
            $data = $this->businessstationary->delete($id);
            
            return $this->response->sendSuccess($data, 'businessstationary delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

}
