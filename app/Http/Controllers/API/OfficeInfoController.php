<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\OfficeInfo;
use App\Http\Controllers\API\BaseController;
use App\Repositories\OfficeInfo\OfficeInfoRepository; 

class OfficeInfoController extends Controller
{
    function __construct(OfficeInfoRepository $officeinfo, BaseController $response){
        $this->officeinfo = $officeinfo;
        $this->response = $response;
    }

    /**
     * @OA\get(
     *      path="/officeinfo",
     *      operationId="getofficeinfoList",
     *      tags={"OfficeInfo"},
     *      summary="Get all officeinfo",
     *      description="Retrive the all information about office",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function index()
    {
        try {
            $data = $this->officeinfo->getAll();
            return $this->response->sendSuccess($data, 'Office Info list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'officeinfo cannot list.'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/officeinfo",
     *      operationId="StoreOfficeInfo",
     *      tags={"OfficeInfo"},
     *      summary="Create new office info",
     *      description="Create new office info",
     *     
     *      @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/StoreOfficeInfoRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="website_logo",
     *                             property="website_logo",
     *                             type="string", 
     *                             format="binary",
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeInfoModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     *      )
     * )
     */
    public function store(Request $request)
    {
        try {
            $file = $request->file('website_logo');
            $new_name = rand() . '.' . $file->getClientOriginalExtension();
            $link = env('APP_URL').'public/officeinfo/' . $new_name;

            $file->move(public_path('/officeinfo/'), $new_name);

            $data = $this->officeinfo->create(array_merge($request->all(),['website_logo' => $new_name ]));
            
            return $this->response->sendSuccess($data, 'Office Info created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'officeinfo cannot created.'],400);        
        }
    }

    /**
     * @OA\get(
     *      path="/officeinfo/{id}",
     *      operationId="getofficeinfoDetails",
     *      tags={"OfficeInfo"},
     *      summary="Get detail of officeinfo",
     *      description="Retrive the detail office information",
     *      @OA\Parameter(
     *          name="id",
     *          description="officeinfo id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeInfoModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->officeinfo->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'officeinfo retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'officeinfo id not found .'],201);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'officeinfo cannot retrived .'],500);
        }
    }

    /** 
     * 
     * @OA\put(
     *      path="/officeinfo/{id}",
     *      operationId="UpdateofficeinfoDetails",
     *      tags={"OfficeInfo"},
     *      summary="Update officeinfo",
     *      description="Update the officeinfo by id",
     * 
    *       @OA\Parameter(
    *          name="id",
    *          description="officeinfo id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    * 
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateOfficeInfoRequest")
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeInfoModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 
    public function update(Request $request, $id)
    {
        try{
            $data = $this->officeinfo->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'officeinfo id not found .'],201);
            }
            $data = $this->officeinfo->update($request->all(), $id);
            $data = $this->officeinfo->show($id);
                
            return $this->response->sendSuccess($data, 'officeinfo update success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

    /** 
     * 
     * @OA\delete(
     *      path="/officeinfo/{id}",
     *      operationId="deleteOfficeInfo",
     *      tags={"OfficeInfo"},
     *      summary="Delete officeinfo",
     *      description="Delete officeinfo",
     * 
     *       @OA\Parameter(
     *          name="id",
     *          description="officeinfo id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/OfficeInfoModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 
    public function destroy($id)
    {
        try{
            $data = $this->officeinfo->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'Office Info id not found .'],201);
            }

            $data = $this->officeinfo->delete($id);
            return $this->response->sendSuccess($data, 'officeinfo delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }
}
