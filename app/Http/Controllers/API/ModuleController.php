<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Repositories\Module\ModuleRepository;
use Illuminate\Http\Request;
use Validator;

class ModuleController extends Controller
{
    public function __construct(ModuleRepository $module, BaseController $response){
        $this->module = $module;
        $this->response = $response;
    } 

    /**
     * @OA\get(
     *      path="/module",
     *      operationId="getmoduleList",
     *      tags={"Module"},
     *      summary="Get all module",
     *      description="Retrive the all contact by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *         
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function index()
    {
        try {
            $data = $this->module->getAll();
            return $this->response->sendSuccess($data, 'module list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'module list not found .'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/module/create",
     *      operationId="StoreModule",
     *      tags={"Module"},
     *      summary="Create new module",
     *      description="Create new Module ",
     *
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     * 
     *             @OA\Schema(
     *                 allOf={
     *                      
     *                     @OA\JsonContent(ref="#/components/schemas/StoreModuleRequest"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="banner_image",
     *                             property="banner_image",
     *                             type="string", 
     *                             format="binary"
     *                         )
     *                     ),
     *                 }
     *             )
     *         )
     *     ),
     * 
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ModuleModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * 
     *      security={ {"bearer": {}} },
     * )
     */

    public function store(Request $request)
    {
        try {
            if($request->file('banner_image') !=""){
                $image = $request->file('banner_image');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $link = env('APP_URL').'public/moduleimage/' . $new_name;

                $image->move(public_path('/moduleimage/'), $new_name);

                $data = $this->module->create(array_merge($request->all(),['banner_image' => $new_name ]));
            }else{
                $data = $this->module->create(array_merge($request->all(),['banner_image' => "" ])); 
            }

            return $this->response->sendSuccess($data, 'Module created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'Module cannot created.'],400);        
        }
    }

}
