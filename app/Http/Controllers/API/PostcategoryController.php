<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Model\Postcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\Http\Controllers\API\BaseController;
use App\Repositories\Postcategory\PostcategoryRepository;

class PostcategoryController extends Controller
{
    public function __construct(PostcategoryRepository $postcategory, BaseController $response){
        $this->postcategory = $postcategory;
        $this->response = $response;
    }

    /**
     * @OA\get(
     *      path="/post-category",
     *      operationId="getPostList",
     *      tags={"Postcategory"},
     *      summary="Get all post category",
     *      description="Retrive the all post category by user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostcategoryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */

    public function index()
    {
        try {
            $data = $this->postcategory->getAll();
            return $this->response->sendSuccess($data, 'Post category list successfully.');
        } catch(Exception $e) {
            return $this->response->sendError('No Content.',['error'=>'Post category cannot list.'],201);
        }
    }

    /**
     * @OA\Post(
     *      path="/post-category",
     *      operationId="StorePostcategory",
     *      tags={"Postcategory"},
     *      summary="Create new",
     *      description="Create new post category",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StorePostcategoryRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostcategoryModel")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * 
     *      security={ {"bearer": {}} },
     * )
     */
    public function store(Request $request)
    {
        
        try {
            $validator = Validator::make($request->all(), [ 
                'title' => 'required', 
                'slug' => 'required|unique:postcategories', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            $data = $this->postcategory->create($request->all());
            return $this->response->sendSuccess($data, 'Post category created.');
        }
        catch (Exception $e) {
            return $this->response->sendError('Bad Request.',['error'=>'Post category cannot created.'],400);        
        }
    }

    /**
     * @OA\get(
     *      path="/post-category/{id}",
     *      operationId="getpostcategoryDetails",
     *      tags={"Postcategory"},
     *      summary="Get detail of post category",
     *      description="Retrive the detail post category by user",
     *      @OA\Parameter(
     *          name="id",
     *          description="Postcategory id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     *     
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostcategoryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */
    public function show($id)
    {
        try {
            $data = $this->postcategory->show($id);

            if(!is_null($data)){
                return $this->response->sendSuccess($data, 'Post category detail  retrived.');
            }
            return $this->response->sendError('No Content.',['error'=>'Post category id not found .'],201);
        } catch(Exception $e) {
            return $this->response->sendError('Server Error.',['error'=>'Post category detail cannot retrived .'],500);
        }
    }

    /** 
     * 
     * @OA\put(
     *      path="/post-category/{id}",
     *      operationId="updatePostcategoryDetails",
     *      tags={"Postcategory"},
     *      summary="Update Postcategory",
     *      description="Update the postcategory by id",
     * 
    *       @OA\Parameter(
    *          name="id",
    *          description="Postcategory id",
    *          required=true,
    *          in="path",
    *          @OA\Schema(
    *              type="integer"
    *          )
    *       ),
    * 
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdatePostcategoryRequest")
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostcategoryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 
    public function update(Request $request, $id)
    {
        try{
            $validator = Validator::make($request->all(), [ 
                'title' => 'required', 
                'slug' => 'required', 
            ]);

            if ($validator->fails()) { 
                return $this->response->sendError('Bad Request.',['error'=>$validator->errors()],400);            
            }

            $data = $this->postcategory->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'Post category id not found .'],201);
            }
            $data = $this->postcategory->update($request->all(), $id);
            $data = $this->postcategory->show($id);
                
            return $this->response->sendSuccess($data, 'Post category update success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }

   /** 
     * 
     * @OA\delete(
     *      path="/post-category/{id}",
     *      operationId="deletePostcategoryDetails",
     *      tags={"Postcategory"},
     *      summary="Delete Postcategory",
     *      description="Delete Postcategory",
     * 
     *       @OA\Parameter(
     *          name="id",
     *          description="Postcategory id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *       ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/PostcategoryModel")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      security={ {"bearer": {}} },
     * 
     *     )
     */ 
    public function destroy($id)
    {
        try{
            $data = $this->postcategory->show($id);
            if(is_null($data)){
                return $this->response->sendError('No Content.',['error'=>'Post category id not found .'],201);
            }

            $data = $this->postcategory->delete($id);
            return $this->response->sendSuccess($data, 'Post category delete success.');
        }catch(Exception $e){
            return $this->response->sendError('Server Error.',[],500);
        }
    }
}
