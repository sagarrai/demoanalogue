<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Analogue CMS api documentation",
     *      description="L5 Swagger OpenApi for analogue cms",
     *      @OA\Contact(
     *          email="sagar@analogueinc.com.np"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Testing API Server"
     * )

     *
     * @OA\Tag(
     *     name="User",
     *     description="API Endpoints of Users"
     * )
     * 
     *  @OA\Tag(
     *     name="Postcategory",
     *     description="API Endpoints of Postcategory"
     * )
     * 
     *  @OA\Tag(
     *     name="Office",
     *     description="API Endpoints of Office"
     * )
     * 
     * @OA\Tag(
     *     name="Post",
     *     description="API Endpoints of Post"
     * )
     * 
     * @OA\Tag(
     *     name="Contact",
     *     description="API Endpoints of Contact"
     * )
     *
     * @OA\Tag(
     *     name="OfficeInfo",
     *     description="API Endpoints of Office Info"
     * )
     *
     * @OA\Tag(
     *     name="Module",
     *     description="API Endpoints of Module"
     * )
     *
     * @OA\Tag(
     *     name="Package",
     *     description="API Endpoints of Package"
     * )
     *
     * @OA\Tag(
     *     name="UserPackage",
     *     description="API Endpoints of User Package"
     * )
     *
     * @OA\Tag(
     *     name="Consultancy",
     *     description="API Endpoints of Consultancy"
     * )
     *
     * @OA\Tag(
     *     name="BusinessStationary",
     *     description="API Endpoints of BusinessStationary"
     * )
     *
     * @OA\Tag(
     *     name="MerchandiseStationary",
     *     description="API Endpoints of MerchandiseStationary"
     * )
     * 
     */
    
}

