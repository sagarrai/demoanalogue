<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\Cors;
use App\Mail\SendMailable;
use App\Model\Post;
use App\Model\BusinessStationary;
use App\Model\MerchandiseStationary;
use App\Model\Postcategory;
use \Illuminate\Support\Str;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        return view('frontend.index');
    }

    public function about()
    {
        return view('frontend.about');
    }

    public function services()
    {
        return view('frontend.services');
    }

    public function careers()
    {
        $jobs = Post::orderBy('created_at', 'desc')->get();
        return view('frontend.careers',compact("jobs"));
    }

    public function career_details($title)
    {
        $jobs = Post::where('title',$title)->get();
        return view('frontend.career_details',compact("jobs"));
    }

    public function career_apply($title)
    {
        $jobs = Post::where('title',$title)->first();
        return view('frontend.career_apply',compact('jobs'));
    }

    public function blogs()
    {
        $posts = Post::all();
        $postcategories = Postcategory::all();
        return view('frontend.blogs', compact('posts','postcategories'));
    }

    public function blog_details($id)
    {
        $blogs = Post::where('id',$id)->get();
        return view('frontend.blog_details',compact("blogs"));
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function bm()
    {
        return view('frontend.bm.bm');
    }
    public function it()
    {
        return view('frontend.it.it');
    }
    public function ecommerce()
    {
        return view('frontend.ecommerce.ecommerce');
    }

    public function webinar()
    {
        return view('frontend.webinar');
    }
    
    public function computer()
    {
        return view('frontend.computer.computer');
    }

    public function surveillance()
    {
        return view('frontend.surveillance.surveillance');
    }

    public function branding_package()
    {
        return view('frontend.branding_package');
    }

    public function social_package()
    {
        return view('frontend.social_package');
    }

    public function email_package()
    {
        return view('frontend.email_package');
    }

    public function press()
    {
        $brands = BusinessStationary::all();
        $mbrands = MerchandiseStationary::all();
        return view('frontend.press.press',compact("brands","mbrands"));
    }

    public function press_tailor_made()
    {
        return view('frontend.press.tailor_made');
    }


}
