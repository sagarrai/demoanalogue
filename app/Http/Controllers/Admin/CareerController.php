<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Model\Career;

class CareerController extends Controller
{
    public function index()
    {
        return view('backend.career.index');
    }

    public function show($id)
    {
    	$career = Career::findOrFail($id);
        return view('backend.career.show',compact('career'));
    }

}
