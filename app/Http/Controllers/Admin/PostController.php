<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Post;

class PostController extends Controller
{
    public function index()
    {
        return view('backend.post.index');
    }

    public function create()
    {
        return view('backend.post.store');
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('backend.post.edit',compact('post'));
    }
    
}
