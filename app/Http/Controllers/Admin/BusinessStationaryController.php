<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\BusinessStationary;

class BusinessStationaryController extends Controller
{
    public function index()
    {
        return view('backend.businessstationary.index');
    }

    public function create()
    {
        return view('backend.businessstationary.store');
    }
    
    public function edit($id)
    {
        $brand = BusinessStationary::findOrFail($id);
        return view('backend.businessstationary.edit',compact('brand'));
    }
}
