<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Module;

class ModuleController extends Controller
{
    public function index()
    {
        return view('backend.module.index');
    }

    public function store()
    {
        return view('backend.module.store');
    }
}
