<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\OfficeInfo;

class OfficeInfoController extends Controller
{
    public function index()
    {
        return view('backend.officeinfo.index');
    }

    public function store()
    {
        return view('backend.officeinfo.store');
    }

    public function edit($id)
    {
        $officeinfo = OfficeInfo::findOrFail($id);
        return view('backend.officeinfo.edit',compact('officeinfo'));
    }

}
