<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\UserPackage;

class UserPackageController extends Controller
{
    public function index()
    {
        return view('backend.userpackage.index');
    }

    public function store()
    {
        return view('backend.userpackage.store');
    }
}
