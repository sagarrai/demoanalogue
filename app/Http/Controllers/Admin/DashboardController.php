<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Middleware\Cors;
use App\User;
use App\Model\Post;
use App\Model\Contact;
use App\Model\Office;
use App\Model\UserPackage;
use App\Model\Module;
use App\Model\BusinessStationary;

class DashboardController extends Controller
{
    public function index()
    {
        $counts = [
            'userCount' => User::all()->count(),
            'postCount' => Post::all()->count(),
            'contactCount' => Contact::all()->count(),
            'officeCount' => Office::all()->count(),
            'userpackageCount' => UserPackage::all()->count(),
            'jobCount' => Post::all()->where('category','2')->count(),
            'moduleCount' => Module::all()->count(),
            'businessstationaryCount' => BusinessStationary::all()->count(),
        ];

        return view('backend.index', compact('counts'));
    }

}
