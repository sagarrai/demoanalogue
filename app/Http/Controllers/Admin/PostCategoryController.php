<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostCategoryController extends Controller
{
    
    public function index()
    {
        return view('backend.postcategory.index');
    }

    public function create()
    {
        return view('backend.postcategory.store');
    }

    public function edit()
    {
        return view('backend.postcategory.edit');
    }

}
