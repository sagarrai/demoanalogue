<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Package;

class PackageController extends Controller
{
    public function index()
    {
        return view('backend.package.index');
    }

    public function store()
    {
        return view('backend.package.store');
    }
}
