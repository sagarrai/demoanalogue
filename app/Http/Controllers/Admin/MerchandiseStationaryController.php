<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MerchandiseStationary;

class MerchandiseStationaryController extends Controller
{
    public function index()
    {
        return view('backend.merchandisestationary.index');
    }

    public function create()
    {
        return view('backend.merchandisestationary.store');
    }
    
    public function edit($id)
    {
        $mbrand = MerchandiseStationary::findOrFail($id);
        return view('backend.merchandisestationary.edit',compact('mbrand'));
    }
}
