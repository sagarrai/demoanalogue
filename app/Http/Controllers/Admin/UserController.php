<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    public function index()
    {
        return view('backend.user.index');
    }

    public function create()
    {
        return view('backend.user.store');
    }

}
