<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Consultancy extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'full_name', 'email', 'phone' , 'objective', 'budget', 'deadline', 'message'
    ];
}
