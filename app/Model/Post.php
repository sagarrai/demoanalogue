<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'title', 'slug', 'short_text', 'description', 'category', 'user_id', 'meta_description', 'keywords', 'feature_image', 'publish', 'office_id', 'deadline', 'other_description', 'no_of_opening'
    ];

    public function post()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
