<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MerchandiseStationary extends Model
{
    protected $fillable = [
        'brand_name', 'brand_description', 'brand_image'
    ];
}
