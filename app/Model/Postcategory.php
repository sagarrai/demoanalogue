<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Postcategory extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'title', 'description',	'slug', 'active', 'weight', 'user_id', 'office_id'
    ];

    public function setUser_idAttribute($value)
    {
        $this->attributes['user_id'] = auth()->user()->id;
    }
}
