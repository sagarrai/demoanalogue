<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'full_name', 'phone', 'email', 'subject', 'message', 'module', 'user_seeking',
    ];

    protected $table = 'contacts';
}
