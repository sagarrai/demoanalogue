<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'full_name', 'email', 'phone', 'company_name', 'company_address', 'package_name', 'package_type', 'package_details', 'package_total_price', 'other_message', 'contact', 'template', 'newsletter', 'social_post',
    ];
}
