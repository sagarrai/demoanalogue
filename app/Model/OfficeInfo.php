<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OfficeInfo extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'website_logo', 'footer_title', 'footer_description', 'address', 'phone' , 'email', 'website', 'facebook_link', 'twitter_link', 'linkedin_link', 'instagram_link', 'copyright_description'
    ];
}
