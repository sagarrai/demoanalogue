<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'user_id', 'office_code', 'office_name', 'nice_name', 'registration_date', 'currency_code',
        'po_box', 'address_line_1', 'address_line_2', 'street', 'city', 'state', 'zip_code', 'country',
        'phone' ,'fax', 'email', 'url', 'logo', 'parent_office_id', 'registration_number', 'pan_number',
        'allow_transaction_posting', 'audit_user_id', 'audit_ts', 'deleted',
    ];

}
