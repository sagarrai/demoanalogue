<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'full_name', 'phone_number' , 'email_address', 'address_line1', 'apply_for',
        'expected_salary', 'apply_date', 'cv_file', 'cover_letter',
    ];
}
