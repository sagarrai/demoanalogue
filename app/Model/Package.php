<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
    	'name', 'code', 'description','module', 'type'
    ];
}
