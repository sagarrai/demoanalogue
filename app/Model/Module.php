<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
    	'name', 'banner_image', 'meta_description','status',
    ];

}
