<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Userdetail extends Model
{
//    use Laravel\Passport\HasApiTokens;
    protected $fillable = [
        'user_id','address_line_1','address_line_2','postal_code','latitude','longitude','phone_number'
    ];

    public function userdetail()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
