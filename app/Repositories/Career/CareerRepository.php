<?php

namespace App\Repositories\Career;
use App\Model\Career;
use \App\Mail\ToAdmin;
use App\Repositories\Career\CareerInterface;
use Illuminate\Support\Facades\Mail;

class CareerRepository implements CareerInterface {

	protected $career;

	public function __construct(Career $career)
	{
		$this->career = $career;
	}

	public function getAll()
	{
		return $this->career->all();
	}

	public function show($id)
	{
		return $this->career->find($id);
	}

	public function create(array $attributes)
	{
		return $this->career->create($attributes);
	}

	public function update(array $attributes, $id)
	{
		$career = $this->career->find($id);
		return $career->update($attributes);
	}

	public function delete($id)
	{
		return  $this->career->destroy($id);
	}

	public function sendmail($details, $to)
	{
		Mail::to($to)->send(new ToAdmin($details));
	}

}
