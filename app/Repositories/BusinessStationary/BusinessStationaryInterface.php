<?php

namespace App\Repositories\BusinessStationary;

interface BusinessStationaryInterface 
{
    public function getAll();

	public function create(array $attributes);

	public function show($id);

	public function edit(array $attributes, $id);

	public function delete($id);

}