<?php 

namespace App\Repositories\BusinessStationary;
use App\Model\BusinessStationary;
use App\Repositories\BusinessStationary\BusinessStationaryInterface;

class BusinessStationaryRepository implements BusinessStationaryInterface {

	protected $businessstationary;

	public function __construct(BusinessStationary $businessstationary) 
	{
		$this->businessstationary = $businessstationary;
	}

	public function getAll() 
	{
		return $this->businessstationary->all();
	}

	public function show($id) 
	{
		return $this->businessstationary->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->businessstationary->create($attributes);
	}

	public function edit(array $attributes, $id) 
	{
		$businessstationary = $this->businessstationary->find($id);
		return $businessstationary->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->businessstationary->destroy($id);
	}

} 