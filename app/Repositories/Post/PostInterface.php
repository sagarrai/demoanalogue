<?php 

namespace App\Repositories\Post;

interface PostInterface
{
	public function getAll();

	public function create(array $attributes);

	public function show($id);

	public function edit(array $attributes, $id);

	public function delete($id);

	public function paginate($perPage = 10, $columns = array('*'));

}