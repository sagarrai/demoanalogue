<?php 

namespace App\Repositories\Post;
use App\Model\Post;

class RepositoryPost implements PostInterface {

	protected $post;

	public function __construct(Post $post) 
	{
		$this->post = $post;
	}

	public function getAll() 
	{
		$val = $this->post->all();
		return $val;
	}

	public function show($id) 
	{
		return $this->post->findOrFail($id);
	}

	public function create(array $attributes) 
	{
		return $this->post->create($attributes);
	}

	public function edit(array $attributes, $id) 
	{
		$postUpdate = $this->post->find($id);

		$postUpdate->update($attributes);
		return $postUpdate;
	}

	public function delete($id) 
	{
		$postDelete = $this->post->destroy($id);
		return $postDelete;
	}

	public function paginate($perPage = 0, $columns = array('*'))
	{
		return $this->post->paginate($perPage, $columns);
	}

}