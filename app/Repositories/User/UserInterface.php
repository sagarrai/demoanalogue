<?php 

namespace App\Repositories\User;

interface UserInterface
{
	public function getAll();

	public function create(array $attributes);

	public function show($id);

	public function update(array $attributes, $id);

	public function delete($id);
}