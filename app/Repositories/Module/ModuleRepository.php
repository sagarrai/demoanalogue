<?php 

namespace App\Repositories\Module;
use App\Model\Module;
use App\Repositories\Module\ModuleInterface;

class ModuleRepository implements ModuleInterface {

	protected $module;

	public function __construct(Module $module) 
	{
		$this->module = $module;
	}

	public function getAll() 
	{
		return $this->module->all();
	}

	public function show($id) 
	{
		return $this->module->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->module->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$module = $this->module->find($id);
		return $module->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->module->destroy($id);
	}

} 