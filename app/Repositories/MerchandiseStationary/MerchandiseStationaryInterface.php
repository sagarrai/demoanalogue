<?php

namespace App\Repositories\MerchandiseStationary;

interface MerchandiseStationaryInterface 
{
    public function getAll();

	public function create(array $attributes);

	public function show($id);

	public function edit(array $attributes, $id);

	public function delete($id);

}