<?php 

namespace App\Repositories\MerchandiseStationary;
use App\Model\MerchandiseStationary;
use App\Repositories\MerchandiseStationary\MerchandiseStationaryInterface;

class MerchandiseStationaryRepository implements MerchandiseStationaryInterface {

	protected $merchandisestationary;

	public function __construct(MerchandiseStationary $merchandisestationary) 
	{
		$this->merchandisestationary = $merchandisestationary;
	}

	public function getAll() 
	{
		return $this->merchandisestationary->all();
	}

	public function show($id) 
	{
		return $this->merchandisestationary->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->merchandisestationary->create($attributes);
	}

	public function edit(array $attributes, $id) 
	{
		$merchandisestationary = $this->merchandisestationary->find($id);
		return $merchandisestationary->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->merchandisestationary->destroy($id);
	}

} 