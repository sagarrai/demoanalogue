<?php 

namespace App\Repositories\Userdetail;
use App\Model\Userdetail;

class RepositoryUserdetail implements UserdetailInterface {

	protected $userdetail;

	public function __construct(Userdetail $userdetail) 
	{
		$this->userdetail = $userdetail;
	}

	public function getAll() 
	{
		$val = $this->userdetail->all();
		return $val;
	}

	public function show($id) 
	{
		return $this->userdetail->findOrFail($id);
	}

	public function create(array $attributes) 
	{
		return $this->userdetail->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$userdetailUpdate = $this->userdetail->find($id);

		$userdetailUpdate->update($attributes);
		return $userdetailUpdate;
	}

	public function delete($id) 
	{
		$userdetailDelete = $this->userdetail->destroy($id);
		return $userdetailDelete;
	}
}