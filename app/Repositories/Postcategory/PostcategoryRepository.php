<?php 

namespace App\Repositories\Postcategory;
use App\Model\Postcategory;

class PostcategoryRepository implements PostcategoryInterface {

	protected $postcategory;

	public function __construct(Postcategory $postcategory) 
	{
		$this->postcategory = $postcategory;
	}

	public function getAll() 
	{
		return $this->postcategory->all();
	}

	public function show($id) 
	{
		return $this->postcategory->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->postcategory->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$postcategory = $this->postcategory->find($id);
		return $postcategory->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->postcategory->destroy($id);
	}

}