<?php

namespace App\Repositories\Consultancy;
use App\Model\Consultancy;
use \App\Mail\ConsultancyMail;
use App\Repositories\Consultancy\ConsultancyInterface;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class ConsultancyRepository implements ConsultancyInterface
{
	protected $consultancy;

	public function __construct(Consultancy $consultancy)
	{
		$this->consultancy = $consultancy;
	}

	public function getAll()
	{
		return $this->consultancy->all();
	}

	public function show($id)
	{
		return $this->consultancy->find($id);
	}

	public function create(array $attributes)
	{
		return $this->consultancy->create($attributes);
	}

	public function update(array $attributes, $id)
	{
		$consultancy = $this->consultancy->find($id);
		return $consultancy->update($attributes);
	}

	public function delete($id)
	{
		return  $this->consultancy->destroy($id);
	}

	public function sendmail($details, $to)
	{
		 Mail::to($to)->send(new ConsultancyMail($details));
	}

}
