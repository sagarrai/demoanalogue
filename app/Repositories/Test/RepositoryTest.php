<?php 

namespace App\Repositories\Test;
use App\Model\Test;

class RepositoryTest implements TestInterface {

	protected $test;

	public function __construct(Test $test) 
	{
		$this->test = $test;
	}

	public function getAll() 
	{
		$val = $this->test->all();
		return $val;
	}

	public function show($id) 
	{
		return $this->test->findOrFail($id);
	}

	public function create(array $attributes) 
	{
		return $this->test->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$testUpdate = $this->test->find($id);

		$testUpdate->update($attributes);
		return $testUpdate;
	}

	public function delete($id) 
	{
		$testDelete = $this->test->destroy($id);
		return $testDelete;
	}

}