<?php 

namespace App\Repositories\Office;
use App\Model\Office;

class EloquentOffice implements OfficeRepository {

	protected $office;

	public function __construct(Office $office) 
	{
		$this->office = $office;
	}

	public function getAll() 
	{
		$val = $this->office->all();
		return $val;
	}

	public function show($id) 
	{
		return $this->office->findOrFail($id);
	}

	public function create(array $attributes) 
	{
		return $this->office->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$officeUpdate = $this->office->find($id);

		$officeUpdate->update($attributes);
		return $officeUpdate;
	}

	public function delete($id) 
	{
		$officeDelete = $this->office->destroy($id);
		return $officeDelete;
	}
}