<?php 

namespace App\Repositories\UserPackage;
use App\Model\UserPackage;
use App\Repositories\Userpackage\UserPackageInterface;

class UserPackageRepository implements UserPackageInterface {

	protected $userpackage;

	public function __construct(UserPackage $userpackage) 
	{
		$this->userpackage = $userpackage;
	}

	public function getAll() 
	{
		return $this->userpackage->all();
	}

	public function show($id) 
	{
		return $this->userpackage->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->userpackage->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$userpackage = $this->userpackage->find($id);
		return $userpackage->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->userpackage->destroy($id);
	}

} 