<?php 

namespace App\Repositories\OfficeInfo;
use App\Model\OfficeInfo;
use App\Repositories\OfficeInfo\OfficeInfoInterface;


class OfficeInfoRepository implements OfficeInfoInterface {

	protected $officeinfo;

	public function __construct(OfficeInfo $officeinfo) 
	{
		$this->officeinfo = $officeinfo;
	}

	public function getAll() 
	{
		return $this->officeinfo->all();
	}

	public function show($id) 
	{
		return $this->officeinfo->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->officeinfo->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$officeinfo = $this->officeinfo->find($id);
		return $officeinfo->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->officeinfo->destroy($id);
	}

} 