<?php 

namespace App\Repositories\Contact;
use \App\Mail\SendMail;

interface ContactInterface
{
	public function getAll();

	public function create(array $attributes);

	public function show($id);

	public function update(array $attributes, $id);

	public function delete($id);

	public function sendmail($details, $to);
	
}