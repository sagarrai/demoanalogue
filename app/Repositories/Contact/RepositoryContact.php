<?php 

namespace App\Repositories\Contact;
use App\Model\Contact;
use \App\Mail\SendMail;
use App\Repositories\Contact\ContactInterface;

class RepositoryContact implements ContactInterface 
{
	protected $contact;

	public function __construct(Contact $contact) 
	{
		$this->contact = $contact;
	}

	public function getAll() 
	{
		return $this->contact->all();
	}

	public function show($id) 
	{
		return $this->contact->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->contact->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$contact = $this->contact->find($id);
		return $contact->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->contact->destroy($id);
	}

	public function sendmail($details, $to)
	{
		\Mail::to($to)->send(new SendMail($details));
	}

}