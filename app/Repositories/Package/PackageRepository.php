<?php 

namespace App\Repositories\Package;
use App\Model\Package;
use App\Repositories\Package\PackageInterface;

class PackageRepository implements PackageInterface {

	protected $package;

	public function __construct(Package $package) 
	{
		$this->package = $package;
	}

	public function getAll() 
	{
		return $this->package->all();
	}

	public function show($id) 
	{
		return $this->package->find($id);
	}

	public function create(array $attributes) 
	{
		return $this->package->create($attributes);
	}

	public function update(array $attributes, $id) 
	{
		$package = $this->package->find($id);
		return $package->update($attributes);
	}

	public function delete($id) 
	{
		return  $this->package->destroy($id);
	}

} 