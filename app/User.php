<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use App\Model\Userdetail;
use App\Model\Post;
use App\Model\Office;
use App\Model\Imageupload;

class User extends Authenticatable
{
    use Notifiable , HasApiTokens, HasRoles;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function userdetail()
    {
        return $this->hasOne('App\Model\Userdetail');
    }

    public function officedetail()
    {
        return $this->hasMany('App\Model\Office');
    }

    public function post()
    {
        return $this->hasMany('Post::class');
    }

    public function imageupload()
    {
        return $this->hasMany('Imageupload::class');
    }

}
