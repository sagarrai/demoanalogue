<div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="/admin-resources/dist/img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="{{ route('admin.dashboard') }}" class="d-block">Analogue Admin</a>
        </div>
    </div>
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            {{-- All Module Starts --}}
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-plus-square"></i>
                    <p>
                        Modules
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.module.index') }}" class="nav-link">
                            <i class="fas fa-share nav-icon"></i>
                            <p>All Module</p>
                        </a>
                    </li>
                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.module.store') }}" class="nav-link">
                            <i class="fas fa-plus nav-icon"></i>
                            <p>Add Module</p>
                        </a>
                    </li>
                </ul>
            </li>
            {{-- All Module Ends --}}


            {{-- All Package Starts --}}
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-archive"></i>
                    <p>
                        Package
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.package.index') }}" class="nav-link">
                            <i class="fas fa-share nav-icon"></i>
                            <p>All Packages</p>
                        </a>
                    </li>
                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.package.store') }}" class="nav-link">
                            <i class="fas fa-plus nav-icon"></i>
                            <p>Add Package</p>
                        </a>
                    </li>
                </ul>
            </li>
            {{-- All Package Ends --}}


            {{-- All User Package Starts --}}
            <li class="nav-item">
                <a href="{{ route('admin.userpackage.index') }}" class="nav-link">
                    <i class="fas fa-user-check nav-icon"></i>
                    <p>User Package Lists</p>
                </a>
            </li>
            {{-- All User Package Starts --}}

    
            {{-- All Consultancy Starts --}}
            <li class="nav-item">
                <a href="{{ route('admin.consultancy.index') }}" class="nav-link">
                    <i class="fas fa-people-carry nav-icon"></i>
                    <p>Consultancy Lists</p>
                </a>
            </li>
            {{-- All Consultancy Starts --}}


            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-business-time"></i>
                    <p>
                        Press Analogue
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                {{-- Business Stationary Menu Starts--}}
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.businessstationary.index') }}" class="nav-link">
                            <i class="fas fa-share nav-icon"></i>
                            <p>All Business Stationary</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.businessstationary.store') }}" class="nav-link">
                            <i class="fas fa-plus nav-icon"></i>
                            <p>Add Business Brand</p>
                        </a>
                    </li>
                </ul>
                {{-- Business Stationary Menu Ends--}}

                {{-- Merchandise Brands Starts --}}
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.merchandisestationary.index') }}" class="nav-link">
                            <i class="fas fa-share nav-icon"></i>
                            <p>All Merchandise</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.merchandisestationary.store') }}" class="nav-link">
                            <i class="fas fa-plus nav-icon"></i>
                            <p>Add Merchandise Brand</p>
                        </a>
                    </li>
                </ul>
                {{-- Merchandise Brands Starts --}}

            </li>
            

            {{-- Analogue Website Starts --}}
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-desktop"></i>
                    <p>
                        Website
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Users
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.user.index') }}" class="nav-link">
                                    <i class="fas fa-share nav-icon"></i>
                                    <p>All user</p>
                                </a>
                            </li>
                            <!-- <li class="nav-item">
                                <a href="{{ route('admin.user.store') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Add user</p>
                                </a>
                            </li> -->
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-clipboard"></i>
                            <p>
                                Post
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.post.index') }}" class="nav-link">
                                    <i class="fas fa-share nav-icon"></i>
                                    <p>All post</p>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.post.store') }}" class="nav-link">
                                    <i class="fas fa-plus nav-icon"></i>
                                    <p>Add post</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-list-ol"></i>
                            <p>
                                Post Category
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.postcategory.index') }}" class="nav-link">
                                    <i class="fas fa-share nav-icon"></i>
                                    <p>All post category</p>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.postcategory.create') }}" class="nav-link">
                                    <i class="fas fa-plus nav-icon"></i>
                                    <p>Add post category</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-building"></i>
                            <p>
                                Office
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.office.index') }}" class="nav-link">
                                    <i class="fas fa-share nav-icon"></i>
                                    <p>All office</p>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.office.store') }}" class="nav-link">
                                    <i class="fas fa-plus nav-icon"></i>
                                    <p>Add Office</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-address-book"></i>
                            <p>
                                Contact
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.contact.index') }}" class="nav-link">
                                    <i class="fas fa-share nav-icon"></i>
                                    <p>All contact</p>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.contact.store') }}" class="nav-link">
                                    <i class="fas fa-plus nav-icon"></i>
                                    <p>Add contact</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.career.index') }}" class="nav-link">
                            <i class="fas fa-share nav-icon"></i>
                            <p>Career</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-folder-open"></i>
                            <p>
                                Office Info
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.officeinfo.index') }}" class="nav-link">
                                    <i class="fas fa-share nav-icon"></i>
                                    <p>All Office info</p>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('admin.officeinfo.store') }}" class="nav-link">
                                    <i class="fas fa-plus nav-icon"></i>
                                    <p>Add Office information</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        {{-- Analogue Website Ends --}}


    </nav>
</div>