<!DOCTYPE html>
<html lang="en">
<head>
    <title>Analogue Inc - Homepage</title>

    <script src="/frontend/js/createjs.min.js"></script>
    <script src="/frontend/js/logopreloader.js"></script>
    <script>
        var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
        function init() {
            canvas = document.getElementById("canvas");
            anim_container = document.getElementById("animation_container");
            dom_overlay_container = document.getElementById("dom_overlay_container");
            var comp=AdobeAn.getComposition("20D5A530B99A1E4EBB539E03C6BAAC68");
            var lib=comp.getLibrary();
            handleComplete({},comp);
        }
        function handleComplete(evt,comp) {
            var lib=comp.getLibrary();
            var ss=comp.getSpriteSheet();
            exportRoot = new lib.logopreloader();
            stage = new lib.Stage(canvas);  

            fnStartAnimation = function() {
                stage.addChild(exportRoot);
                createjs.Ticker.framerate = lib.properties.fps;
                createjs.Ticker.addEventListener("tick", stage);
            }
            AdobeAn.makeResponsive(false,'both',false,1,[canvas,anim_container,dom_overlay_container]);
            AdobeAn.compositionLoaded(lib.properties.id);
            fnStartAnimation();
        }
    </script>
    <style>
        #ai-preloader-container {
            position: absolute;
            z-index: 1000000;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background: #fff;
            animation: aipreloader 7s forwards;
        }
        .preloader-hide {
            display: none;
        }
        @keyframes aipreloader {
            0% { opacity: 1; }
            97% { opacity: 1; }
            100% { opacity: 0; visibility: none; pointer-events: none; }
        }
    </style>
    <style>
        .alert.alert-light.alert-dismissible.fade.show {
            width: 100%;
            max-width: 720px;
            position: absolute;
            left: 50%;
            top: 64px;
            z-index: 1050;
            transform: translateX(-50%);
        }
        .alert.alert-light.alert-dismissible.fade.show:after {
            content: '';
            position: absolute;
            top: -9999px;
            left: -9999px;
            right: -9999px;
            bottom: -9999px;
            background: rgba(255,255,255,0.9);
            z-index: -1;
        }
    </style>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/frontend/css/main.css">
    <link href="/frontend/images/favicon.png" rel="icon">

    <!-- Begin Sendinblue Form -->
    <!-- START - We recommend to place the below code in head tag of your website html  -->
    <style>
        @font-face {
            font-display: block;
            font-family: Roboto;
            src: url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/7529907e9eaf8ebb5220c5f9850e3811.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/25c678feafdc175a70922a116c9be3e7.woff) format("woff")
        }

        @font-face {
            font-display: fallback;
            font-family: Roboto;
            font-weight: 600;
            src: url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/6e9caeeafb1f3491be3e32744bc30440.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/71501f0d8d5aa95960f6475d5487d4c2.woff) format("woff")
        }

        @font-face {
            font-display: fallback;
            font-family: Roboto;
            font-weight: 700;
            src: url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/3ef7cf158f310cf752d5ad08cd0e7e60.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/ece3a1d82f18b60bcce0211725c476aa.woff) format("woff")
        }

        #sib-container input:-ms-input-placeholder {
            text-align: left;
            font-family: "Helvetica", sans-serif;
            color: #c0ccda;
            border-width: px;
        }

        #sib-container input::placeholder {
            text-align: left;
            font-family: "Helvetica", sans-serif;
            color: #c0ccda;
            border-width: px;
        }

        #sib-container textarea::placeholder {
            text-align: left;
            font-family: "Helvetica", sans-serif;
            color: #c0ccda;
            border-width: px;
        }
    </style>
    <link rel="stylesheet" href="https://sibforms.com/forms/end-form/build/sib-styles.css">
    <!--  END - We recommend to place the above code in head tag of your website html -->
</head>
<body onload="init();">

    <div id="ai-preloader-container">
        <div id="animation_container">
            <canvas id="canvas" width="280" height="280"></canvas>
            <div id="dom_overlay_container"></div>
        </div>
    </div>

    <div id="wrapper">

        <div id="covidalert" class="alert alert-light alert-dismissible fade show p-2" role="alert">
            <div class="img-holder"><img src="frontend/images/covid.jpg" alt=""></div>
            <button type="button" class="close py-0" data-dismiss="alert" aria-label="Close" style="margin-top: -20px;">
            <span aria-hidden="true" style="font-size: 4rem; font-weight: 400;">&times;</span>
            </button>
        </div>

       <b class="screen-overlay"></b>
<div id="sticky-header-holder" class="fixed-top main-header">
    <div class="container-xxl">
        <div class="mobile-logo"><a href="/"><img src="/frontend/images/logo.png" alt=""></a></div>

        <button class="btn btn-outline-secondary btn-sm ml-auto d-xl-none" type="button" data-toggle="collapse" data-trigger="#sticky-header">Menu</button>

        <nav id="sticky-header" class="mobile-offcanvas navbar navbar-expand-xl navbar-light">
            <a class="navbar-brand ai-logo" href="/"><img src="/frontend/images/logo.png" alt=""></a>
            <div class="offcanvas-header">
                <button class="btn btn-link btn-close float-left mr-2"><svg class="btn-icon ml-0" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></button>
                <h6>Navigate</h6>
            </div>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="/" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="/about" class="nav-link">About</a></li>

                <li class="nav-item dropdown has-megamenu">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">UncleWebs</a>
                    <div class="dropdown-menu megamenu">
                        <div class="row">
                            <div class="col-xl-4 col-12 d-xl-block d-none">
                                <div class="img-holder mb-6"><img src="/frontend/images/uw-logo.png" alt="" class="w-75"></div>
                                <p class="mb-3">In this world full of chaos, let UncleWebs be your peace. With widest selection on web, UncleWebs brings you variety of services that will fulfill your need for Web Solution.</p>
                                <p>Don't get lost in choosing different service provider when we bring you all the service in palm of your hand.</p>
                            </div>
                            <div class="col-xl-8 col-12">
                                <div class="row row-cols-xl-3 row-cols-lg-1 row-cols-1">
                                    <div class="col megamenu-cols">
                                        <h6 class="mm-sec-title">Domain Hosting</h6>
                                        <p class="mm-sec-subtitle text-primary">Be Unique</p>
                                        <p class="mm-sec-content small">Host your business domain name. 24/7 Live Support with 99.99% Uptime guaranteed!</p>
                                        <p><a href="https://www.unclewebs.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <h6 class="mm-sec-title">Email Hosting</h6>
                                        <p class="mm-sec-subtitle text-primary">Lets Be Professional</p>
                                        <p class="mm-sec-content small">Get your own business email; organize and collaborate with your team professionally.</p>
                                        <p><a href="https://www.unclewebs.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <h6 class="mm-sec-title">DIY Website</h6>
                                        <p class="mm-sec-subtitle text-primary">Websites made easier</p>
                                        <p class="mm-sec-content small">Create a website for your business by yourself at your own pace with just a few clicks.</p>
                                        <p><a href="https://www.unclewebs.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <h6 class="mm-sec-title">Web Hosting</h6>
                                        <p class="mm-sec-subtitle text-primary">Get Your Website Online</p>
                                        <p class="mm-sec-content small">Hosting your website is now super-easy. Host your website with UncleWeb's Hosting.</p>
                                        <p><a href="https://www.unclewebs.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <h6 class="mm-sec-title">eCommerce</h6>
                                        <p class="mm-sec-subtitle text-primary">From Somewhere to Everywhere</p>
                                        <p class="mm-sec-content small">Manage your online store as per your preferences & just start selling. No muss, no fuss.</p>
                                        <p><a href="https://www.unclewebs.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols no-hover mt-xxl-6 mt-xl-5 mt-3 no-borders no-paddings">
                                        <a href="https://www.unclewebs.com/" target="_blank" class="mm-fh-btn stretched-link"><span>Visit Website &rarr;</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="nav-item dropdown has-megamenu">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Scholarkeys</a>
                    <div class="dropdown-menu megamenu">
                        <div class="row">
                            <div class="col-xl-4 col-12 d-xl-block d-none">
                                <div class="img-holder mb-6"><img src="/frontend/images/sk-logo.png" alt="" class="w-75"></div>
                                <p>Scholarkeys is here to bring interactive education to your doorsteps. With our innovative solution, you can stay at the comfort of your home and learn and teach with ease. Thanks to our amazing software integration, we are able to bring your students to you, and the teachers to them, which makes learning more fun and interesting.</p>
                            </div>
                            <div class="col-xl-8 col-12">
                                <div class="row row-cols-xl-3 row-cols-lg-1 row-cols-1">
                                    <div class="col megamenu-cols">
                                        <h6 class="mm-sec-title">Grading</h6>
                                        <p class="mm-sec-content small">Comprehensive Grading system allows the teachers to breakdown grades, categorize, add new grades, etc.</p>
                                        <p><a href="https://www.scholarkeys.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <h6 class="mm-sec-title">Tracking</h6>
                                        <p class="mm-sec-content small">Keep track of your students' history on their scores, attendance, overall performance, etc.</p>
                                        <p><a href="https://www.scholarkeys.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <h6 class="mm-sec-title">eLearning</h6>
                                        <p class="mm-sec-content small">Innovative and modern feature that allows the students to view class works and home works via their devices.</p>
                                        <p><a href="https://www.scholarkeys.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <h6 class="mm-sec-title">Assignments</h6>
                                        <p class="mm-sec-content small">Assignments got an upgrade and now lets your students complete assignments much easier and faster.</p>
                                        <p><a href="https://www.scholarkeys.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <h6 class="mm-sec-title">Teachers & Staffs</h6>
                                        <p class="mm-sec-content small">List out your teachers and school staffs, categorize them and get detailed information personnel-wise.</p>
                                        <p><a href="https://www.scholarkeys.com/" target="_blank" class="mm-sec-link stretched-link"><span>Get Started &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols no-hover mt-xxl-6 mt-xl-5 mt-3 no-borders no-paddings">
                                        <a href="https://www.scholarkeys.com/" target="_blank" class="mm-fh-btn stretched-link"><span>Visit Website &rarr;</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="nav-item"><a href="/services" class="nav-link">Services</a></li>
                <li class="nav-item"><a href="/careers" class="nav-link">Career</a></li>

                <li class="nav-item dropdown has-megamenu">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">University</a>
                    <div class="dropdown-menu megamenu">
                        <div class="row">
                            <div class="col-xl-4 col-12 d-xl-block d-none">
                                <h4>University</h4>
                                <div class="img-holder w-75"><img src="/frontend/images/university.png" alt=""></div>
                                <p class="mb-3">Welcome to our University, where you'll find useful information about our services, products, resources and much more. You'll also find blogs and articles our team have curated for you to learn from.</p>
                            </div>
                            <div class="col-xl-8 col-12">
                                <div class="row row-cols-xl-3 row-cols-lg-1 row-cols-1">
                                    <div class="col megamenu-cols">
                                        <div class="img-holder mb-3"><img src="/frontend/images/logo-bm.png" alt=""></div>
                                        <p><a href="https://bm.analogueinc.com.np/" target="_blank" class="mm-sec-link stretched-link"><span>View More &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <div class="img-holder mb-3"><img src="/frontend/images/logo-it.png" alt=""></div>
                                        <p><a href="https://it.analogueinc.com.np/" target="_blank" class="mm-sec-link stretched-link"><span>View More &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <div class="img-holder mb-3"><img src="/frontend/images/logo-ec.png" alt=""></div>
                                        <p><a href="https://ecommerce.analogueinc.com.np/" target="_blank" class="mm-sec-link stretched-link"><span>View More &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3 has-btm-crnr">
                                        <div class="img-holder mb-3"><img src="/frontend/images/logo-cm.png" alt=""></div>
                                        <p><a href="https://computers.analogueinc.com.np/" target="_blank" class="mm-sec-link stretched-link"><span>View More &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3 has-btm-crnr">
                                        <div class="img-holder mb-3"><img src="/frontend/images/logo-su.png" alt=""></div>
                                        <p><a href="https://surveillance.analogueinc.com.np/" target="_blank" class="mm-sec-link stretched-link"><span>View More &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <div class="img-holder mb-3"><img src="/frontend/images/logo-pr.png" alt=""></div>
                                        <p><a href="https://press.analogueinc.com.np/" target="_blank" class="mm-sec-link stretched-link"><span>View More &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <h6 class="mm-sec-title">Brand Guidelines</h6>
                                        <p class="mm-sec-subtitle text-primary">Colors, Fonts, & More</p>
                                        <p><a href="https://www.unclewebs.com/" target="_blank" class="mm-sec-link stretched-link"><span>Learn More &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3 has-top-crnr">
                                        <h6 class="mm-sec-title">Downloads</h6>
                                        <p class="mm-sec-subtitle text-primary">Various Resources</p>
                                        <p><a href="https://www.unclewebs.com/" target="_blank" class="mm-sec-link stretched-link"><span>Explore &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols no-hover mt-xxl-6 mt-xl-5 mt-3 has-top-crnr no-borders no-paddings">
                                        <a href="https://www.unclewebs.com/" target="_blank" class="mm-fh-btn stretched-link"><span>Visit Website &rarr;</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="nav-item"><a href="/contact" class="nav-link">Contact</a></li>

                <li class="nav-item dropdown has-megamenu d-xl-flex d-block align-items-xl-center ml-xl-4 ml-0">
                    <a class="btn btn-primary dropdown-toggle" href="#" data-toggle="dropdown">Analogue Mall</a>
                    <div class="dropdown-menu megamenu">
                        <div class="row">
                            <div class="col-xl-4 col-12 d-xl-block d-none">
                                <div class="img-holder mb-6"><img src="/frontend/images/am-logo.png" alt="" class="w-75"></div>
                                <p>Technology has been taking over and you can only wish for a Vendor that delivers your favorite gadget to your doorstep. Until now, because Analogue Mall brings you a platform where you can shop your electronic needs and delivers it where you want. Amazing right? Check out our collection.</p>
                            </div>
                            <div class="col-xl-8 col-12">
                                <div class="row row-cols-xl-3 row-cols-lg-1 row-cols-1">
                                    <div class="col megamenu-cols">
                                        <h6 class="mm-sec-title">Desktops</h6>
                                        <p class="mm-sec-content small">Desktop PCs range from Dell, HP, Asus, Apple, Maingear, Origin, Lenovo, Acer, etc</p>
                                        <p><a href="https://www.analoguemall.com/" target="_blank" class="mm-sec-link stretched-link"><span>Shop Now &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <h6 class="mm-sec-title">Laptops</h6>
                                        <p class="mm-sec-content small">Laptops range from Apple, Acer, Asus, Dell, Maingear, HP, Lenovo, Origin, Alienware, etc</p>
                                        <p><a href="https://www.analoguemall.com/" target="_blank" class="mm-sec-link stretched-link"><span>Shop Now &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xl-0 mt-3">
                                        <h6 class="mm-sec-title">Printers & Scanners</h6>
                                        <p class="mm-sec-content small">Products range from Brother, Canon & HP, that have spent decades creating amazing products.</p>
                                        <p><a href="https://www.analoguemall.com/" target="_blank" class="mm-sec-link stretched-link"><span>Shop Now &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <h6 class="mm-sec-title">Surveillance</h6>
                                        <p class="mm-sec-content small">Products include CCTV Camera, Network Video Recorder, IP Surveillance, Biometrics, etc</p>
                                        <p><a href="https://www.analoguemall.com/" target="_blank" class="mm-sec-link stretched-link"><span>Shop Now &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols mt-xxl-6 mt-xl-5 mt-3">
                                        <h6 class="mm-sec-title">Accessories</h6>
                                        <p class="mm-sec-content small">Products like Headphones, Speakers, Pendrives, USB Cables, Power Cables, SATA Cables, etc</p>
                                        <p><a href="https://www.analoguemall.com/" target="_blank" class="mm-sec-link stretched-link"><span>Shop Now &rarr;</span></a></p>
                                    </div>
                                    <div class="col megamenu-cols no-hover mt-xxl-6 mt-xl-5 mt-3 no-borders no-paddings">
                                        <a href="https://www.analoguemall.com/" target="_blank" class="mm-fh-btn stretched-link"><span>Visit Website &rarr;</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </nav>
    </div>
</div>

@yield('content')

<footer id="footer">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-12 footer-legal">
                <div class="footer-logo"><img class="w-50" src="/frontend/images/logo.png" alt=""></div>
                <p class="mb-2">&copy; Analogue Inc, a PGNSONS Company. All Rights Reserved.</p>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                <h6 class="footer-links-title">Navigate</h6>
                <ul class="footer-links">
                    <li><a href="/">Home</a></li>
                    <li><a href="/services">Services</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/career">Career</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                <h6 class="footer-links-title">Services</h6>
                <ul class="footer-links">
                    <li><a target="_blank" href="https://www.analoguemall.com/">Analogue Mall</a></li>
                    <li><a target="_blank" href="https://bm.analogueinc.com.np/">Branding & Marketing</a></li>
                    <li><a target="_blank" href="https://it.analogueinc.com.np/">IT Solution</a></li>
                    <li><a target="_blank" href="https://computers.analogueinc.com.np/">Computers & Hardware</a></li>
                    <li><a target="_blank" href="https://surveillance.analogueinc.com.np/">Surveillance</a></li>
                    <li><a target="_blank" href="https://press.analogueinc.com.np/">Press</a></li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                <h6 class="footer-links-title">Products</h6>
                <ul class="footer-links">
                    <li><a target="_blank" href="https://www.unclewebs.com/">UncleWebs</a></li>
                    <li><a target="_blank" href="https://www.scholarkeys.com/">Scholarkeys</a></li>
                    <li><a target="_blank" href="https://www.hopperspass.com/">Hopper's Pass</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="scroll-to-top"><svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></div>

<script src="/frontend/js/aos.js"></script>
<script>
    AOS.init({
        mirror: true,
    });
</script>
<script src="/frontend/js/jquery-3.4.1.min.js"></script>
<script src="/frontend/js/bootstrap.bundle.min.js"></script>
<script>
    $(document).ready(function(){
        // BS4.4 sticky scrollspy nav trigger
        $('body').scrollspy({
            target: '#sticky-header',
            offset: 75
        });
        $('.scrollto').click(function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 70 }, 750);
            event.preventDefault();
        });

        // BS4.4 off-canvas nav trigger
        $("[data-trigger]").on("click", function(e){
            e.preventDefault();
            e.stopPropagation();
            var offcanvas_id = $(this).attr('data-trigger');
            $(offcanvas_id).toggleClass("show");
            $('body').toggleClass("offcanvas-active");
            $(".screen-overlay").toggleClass("show");
        });
        $(".btn-close, .screen-overlay, .scrollto").click(function(e){
            $(".screen-overlay").removeClass("show");
            $(".mobile-offcanvas").removeClass("show");
            $("body").removeClass("offcanvas-active");
        });
    });
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
</script>
<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 720) {
                $('.scroll-to-top').addClass("visible");
            } else {
                $('.scroll-to-top').removeClass("visible");
            }
        });
        $('.scroll-to-top').click(function () {
            $('body, html').animate({
                scrollTop: 0
            }, 750);
            return false;
        });
    });
</script>

<script>
    $(document).on('load', function(){
        $("body").css({"overflow":"hidden"});
    });
    $(document).ready(function(){
        $("body").css({"overflow":"hidden"});

        $('#ai-preloader-container').addClass('preloader-hide');

        $('#covidalert').on('closed.bs.alert', function () {
            $("body").css({"overflow":"visible"});
        });
    });
</script>


<!-- Start of Async Drift Code -->
<script>
    window.addEventListener('load', function () {
        "use strict";
        !function() {
            var t = window.driftt = window.drift = window.driftt || [];
            if (!t.init) {
                if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
                t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
                t.factory = function(e) {
                    return function() {
                        var n = Array.prototype.slice.call(arguments);
                        return n.unshift(e), t.push(n), t;
                    };
                }, t.methods.forEach(function(e) {
                    t[e] = t.factory(e);
                }), t.load = function(t) {
                    var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
                    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
                    var i = document.getElementsByTagName("script")[0];
                    i.parentNode.insertBefore(o, i);
                };
            }
        }();
        drift.SNIPPET_VERSION = '0.3.1';
        drift.load('szk4eythda8s');
    });
</script>
<!-- End of Async Drift Code -->

        <script src="/frontend/js/glide.min.js"></script>
<script src="/frontend/js/phonecode.js"></script>
<script>
    var input = document.querySelector("#phonenumber");
    window.intlTelInput(input, {
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io?token=de0c1d7341370d', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        preferredCountries: ['np', 'in', 'cn', 'us', 'uk', 'jp'],
        utilsScript: "/frontend/js/utils.js",
    });
</script>

<!-- Start of Async Drift Code -->
<script>
    window.addEventListener('load', function () {
        "use strict";
        !function() {
            var t = window.driftt = window.drift = window.driftt || [];
            if (!t.init) {
                if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
                t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
                t.factory = function(e) {
                    return function() {
                        var n = Array.prototype.slice.call(arguments);
                        return n.unshift(e), t.push(n), t;
                    };
                }, t.methods.forEach(function(e) {
                    t[e] = t.factory(e);
                }), t.load = function(t) {
                    var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
                    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
                    var i = document.getElementsByTagName("script")[0];
                    i.parentNode.insertBefore(o, i);
                };
            }
        }();
        drift.SNIPPET_VERSION = '0.3.1';
        drift.load('szk4eythda8s');
    });
</script>
<!-- End of Async Drift Code -->
    </div>

    <!-- START - We recommend to place the below code in footer or bottom of your website html  -->
    <script>
        window.REQUIRED_CODE_ERROR_MESSAGE = 'Please choose a country code';

        window.EMAIL_INVALID_MESSAGE = window.SMS_INVALID_MESSAGE = "The information provided is invalid. Please try again.";

        window.REQUIRED_ERROR_MESSAGE = "This field cannot be left blank. ";

        window.GENERIC_INVALID_MESSAGE = "The information provided is invalid. Please try again.";




        window.translation = {
            common: {
                selectedList: '{quantity} list selected',
                selectedLists: '{quantity} lists selected'
            }
        };

        var AUTOHIDE = Boolean(0);
    </script>
    <script src="https://sibforms.com/forms/end-form/build/main.js">
    </script>
    <script src="https://www.google.com/recaptcha/api.js?hl=en"></script>
    <!-- END - We recommend to place the above code in footer or bottom of your website html  -->
    <!-- End Sendinblue Form -->
</body>
</html>