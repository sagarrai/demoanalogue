<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Analogue | @yield('title') </title>
        <link rel="stylesheet" href="/admin-resources/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="/admin-resources/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <link rel="stylesheet" href="/admin-resources/dist/css/adminlte.min.css">
        {{-- <link rel="stylesheet" href="/admin-resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"> --}}
        <link rel="stylesheet" href="/css/app.css"/>
        <script src="/admin-resources/plugins/jquery/jquery.min.js"></script> 
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="/admin-resources/plugins/toastr/toastr.min.css">
        <link rel="stylesheet" href="/admin-resources/dist/css/adminlte.min.css">
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="<?php echo URL::to('/') ?>/api/documentation" class="nav-link">API Swagger</a>
                    </li>
                </ul>
                <form class="form-inline ml-3">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-comments"></i>
                            <span class="badge badge-danger navbar-badge">1</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <a href="#" class="dropdown-item">
                                <div class="media">
                                    <img src="/admin-resources/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                                    <div class="media-body">
                                        <h3 class="dropdown-item-title">
                                        Sagar Rai
                                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                        </h3>
                                        <p class="text-sm">Call me whenever you can...</p>
                                        <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                    </div>
                                </div>
                            </a>
                            
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-bell"></i>
                            <span class="badge badge-warning navbar-badge">15</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header">15 Notifications</span>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-envelope mr-2"></i> 4 new messages
                                <span class="float-right text-muted text-sm">3 mins</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-users mr-2"></i> 8 friend requests
                                <span class="float-right text-muted text-sm">12 hours</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-file mr-2"></i> 3 new reports
                                <span class="float-right text-muted text-sm">2 days</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                        </div>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                        class="fas fa-th-large"></i></a>
                    </li> -->
                    <button onClick="logOut()" class="btn-link"><i
                    class="fas fa-sign-out-alt"></i></button>
                </li>
            </nav>
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="{{ route('admin.dashboard') }}" class="brand-link">
                    <img src="/admin-resources/dist/img/dashboard_logo.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                    <span class="brand-text font-weight-light">Analogue Inc</span>
                </a>
                @include('layouts.admin-sidebar')
            </aside>
            <div class="content-wrapper">
                @yield('content')
            </div>
            <aside class="control-sidebar control-sidebar-dark">
            </aside>
            <footer class="main-footer">
                <strong>Copyright &copy; 2020 <a href="#">Analogue Admin</a>.</strong>
                All rights reserved.
                <div class="float-right d-none d-sm-inline-block">
                    <b>Version</b> 1.0.0
                </div>
            </footer>
        </div>
        <script src="/admin-resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/admin-resources/plugins/fastclick/fastclick.js"></script>
        <script src="/admin-resources/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <script src="/admin-resources/dist/js/adminlte.js"></script>
        <script src="/admin-resources/dist/js/demo.js"></script>
        <script src="/admin-resources/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
        <script src="/admin-resources/plugins/raphael/raphael.min.js"></script>
        <script src="/admin-resources/plugins/jquery-mapael/jquery.mapael.min.js"></script>
        <script src="/admin-resources/plugins/jquery-mapael/maps/usa_states.min.js"></script>
        <script src="/admin-resources/plugins/chart.js/Chart.min.js"></script>
        <script src="/admin-resources/dist/js/global.js"></script>
        <script src="/admin-resources/plugins/jquery/jquery.min.js"></script>
        <script src="/admin-resources/plugins/sweetalert2/sweetalert2.min.js"></script>
        <script src="/admin-resources/plugins/toastr/toastr.min.js"></script>
        <script src="/admin-resources/dist/js/function.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>

        <script>
        function get_baseurl() {
            var base_url = "<?php echo URL::to('/') ?>";
            return base_url;
        }

        var checkAuth = getCookie("token");

        if(checkAuth == null || checkAuth == "") {
            window.location.href = get_baseurl()+"/admin/login";
            toastr.error('Unauthorized ! Please login to continue...');
        }

        function logOut() {
            eraseCookie("token");
            window.location.href = get_baseurl()+"/admin/login";
            toastr.info('Please login to continue...');
        }
        </script>
    </body>
</html>