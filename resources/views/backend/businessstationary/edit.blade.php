@extends('layouts.admin') 
@section('title','Business Stationaries') 

@section('content')

    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Brand</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
                        <li class="breadcrumb-item active">Edit Brand </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="brand_form" method="post" enctype="multipart/form-data" action="{{Url('api/businessstationary')}}/{{Request::route('id')}}" >
                @csrf
                @method("PUT")
                <div class="form-row">
                    <div class="col-6">
                        <div style="display:none;" id="alert-msg" role="alert" class="alert alert-success">
                            <strong>Brand Updated Successfully !  <a href="{{Url('admin/businessstationary/allbusinessstationary')}}">See in list </a></strong>
                        </div>
                    </div>
                    <div class="col-6">
                        <div style="display:none;" id="error-msg" role="alert" class="alert alert-danger">
                            <strong>Something went wrong while posting the brand ! </strong>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label for="full_name">Brand Name</label>
                            <input type="text" id="brand_name" name="brand_name" placeholder="Enter your full name" class="form-control">
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="form-group">
                            <label for="inputDescription">Brand Description</label>
                            <textarea name="brand_description" id="brand_description" class="form-control" rows="1"></textarea>
                        </div>
                    </div>  

                    <div class="col-5">
                        <div class="form-group">
                            <label for="feature_image">Previous Feature Image</label>
                                <img id="image" height="100"/>
                                <br>
                            <label for="feature_image">Choose new Brand Image</label>
                            <input type="file" id="brand_image" name="brand_image" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                            <div class="spinner-border text-success" id="spinner_add" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <input type="hidden" id="businessstationaryUrl" value="{{Url('api/businessstationary')}}/{{Request::route('id')}}"/>
    </section>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
    $("#spinner_add").hide();
    
        //For Storing new Data
        $('#brand_form').on('submit',function(e){
        e.preventDefault();
        var token = getCookie("token");

        $.ajax({
        headers: {
            'Authorization': `Bearer ${token}`,
        },
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:new FormData(this),
            processData:false,
            contentType:false,
            cache:false,
            async:true,

            beforeSend: function(){
                $("#spinner_add").show();
            },
            complete: function(){
                $("#spinner_add").hide();
            },
            success: function(response)
            {
                console.log('Post from response', response);
                if(response.code == 200 && response.status== true) 
                {
                    $("#alert-msg").show();
                }
                else 
                {
                    $("#error-msg").show();
                }
            }
            });
        });

        //For Retrieveing previous data

        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get", 
            url: $("#businessstationaryUrl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,
                
            beforeSend: function(){ 
                $("#spinner_show").show();
            },

            complete: function(){ 
                $("#spinner_show").hide();
            },

            success: function(response){ 
                if(response.code == 200 && response.status==true){
                    var result = response.data;
                    $("#brand_name").val(result.brand_name);
                    $("#brand_description").val(result.brand_description);
                    $("#image").attr('src',"{{ URL::to('/') }}/imageupload/businessstationary/"+ result.brand_image);
                }else{
                    window.location.href = $("#backLink").attr("href");
                }
            }
        });

    });
</script>

@endsection
