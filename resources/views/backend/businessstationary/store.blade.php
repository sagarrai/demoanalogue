@extends('layouts.admin') 
@section('title','Business Stationaries') 

@section('content')

    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Brand</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add Brand </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="brand_form" method="post" enctype="multipart/form-data" action="<?php echo URL::to('/') ?>/api/businessstationary" >
                <div class="form-row">
                    @csrf
                    <div class="col-5">
                        <div class="form-group">
                            <label for="full_name">Brand Name</label>
                            <input type="text" name="brand_name" placeholder="Enter the name of the brand" class="form-control">
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="form-group">
                            <label for="inputDescription">Brand Description</label>
                            <textarea name="brand_description" class="form-control" rows="1"></textarea>
                        </div>
                    </div>  
                    <div class="col-5">
                        <div class="form-group">
                            <label for="feature_image">Brand Image</label>
                            <input type="file" name="brand_image" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                            <div class="spinner-border text-success" id="spinner_add" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </section>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
    $("#spinner_add").hide();

        $('#brand_form').on('submit',function(e){
        e.preventDefault();
        var base_url = get_baseurl();
        var token = getCookie("token");

            $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:new FormData(this),
            processData:false,
            contentType:false,
            cache:false,
            async:true,

            beforeSend: function(){
                $("#spinner_add").show();
            },
            complete: function(){
                $("#spinner_add").hide();
            },
            success: function(response)
            {
                if(response.code == 200 && response.status== true) 
                {
                    toastr.success('Added Successfully');
                    window.setTimeout(function () {
                        window.location.href = base_url + '/admin/businessstationary/allbusinessstationary';
                    }, 1000);
                }
                else 
                {
                    toastr.error('Something went wrong while submitting the package');
                    window.setTimeout(function () {
                        window.location.href = base_url + '/admin/businessstationary/allbusinessstationary';
                    }, 1000);
                }
            }
            });
        });

    });
</script>

@endsection
