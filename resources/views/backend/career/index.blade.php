@extends('layouts.admin')
@section('title', 'Career')
@section('content')
<section class="content">
    <br>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Career</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body p-2">
            <table class="table table-striped projects" id="ContactListTableId">
                <thead>
                    <tr>
                        <th>
                            Full Name
                        </th>
                        <th>
                            Phone
                        </th>
                        <th>
                            Apply For
                        </th>
                        <th>
                            Expected Salary
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="render-here">Are you sure to delete this contact?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="contact_delete" value="" class="btn btn-danger">Delete</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="/admin-resources/dist/js/global.js"></script>
<script src="/admin-resources/plugins/jquery/jquery.min.js"></script>
<script src="/admin-resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/admin-resources/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="/admin-resources/plugins/toastr/toastr.min.js"></script>
<script src="/admin-resources/dist/js/adminlte.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">

$(function () {
    LoadTable();
        $(document).keypress(function (e) {
        if (e.which == 13) {
        LoadTable();
        }
    });
})

function LoadTable() {
    var base_url = get_baseurl();
    $('#ContactListTableId').dataTable().fnDestroy();
    $('#ContactListTableId').dataTable({
        "pageLength":50,
        "processing": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },

        "ajax": {
            "type": "GET",
            "url": base_url + "/api/career",
            "contentType": "application/json; charset=utf-8",
            "headers": {
            'Authorization': `Bearer ${getCookie("token")}`,
        },
        "data": function (data) {
        data.SearchContent = $('#SearchContent').val();
        return JSON.stringify(data);
            }
        },
        "columns": 
        [
            {"data": "full_name" },
            {"data": "phone_number" },
            {"data": "apply_for" },
            {"data": "expected_salary" },
            {
                "data": "full_name", "apply_for": "Action", "render": enable,
                "searchable": false,
                "orderable": false,
                "visible": true
            }
        ],
    })
}

    var enable = function (data, type, full, meta) {
    $.fn.dataTable.render.text();
    var actionBtn = "";
    actionBtn ='&nbsp;&nbsp;<a href="/admin/career/' + full.id +' "  class="btn btn-primary btn-sm btn-update">Detail</a>'
    + '&nbsp;&nbsp';
    return actionBtn;
    }

    $("#contact_delete").click(function() {
        var base_url = get_baseurl();
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });
        
        var id = $("#delete_btn").val();
        var url = base_url + '/api/career/'+ id;
        
        $.ajax({
            type: 'DELETE',
            url: url,
            success: function (response) {
                if (response.code == "200") {
                    window.setTimeout(function () {
                    window.location.href = base_url +'/admin/contact/allcontact';
                    }, 2000 );
                }
            }
        });
    toastr.success('Delete Successfully !!');
    });
</script>
@endsection