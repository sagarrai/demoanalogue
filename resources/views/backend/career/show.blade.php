@extends('layouts.admin') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h2>Career Detail &nbsp; &nbsp;  <div class="spinner-border" id="spinner_add" role="status" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div></h2>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a id="backLink" href="{{url('admin/career/')}}">Career</a></li>
                        <li class="breadcrumb-item active">Career Detail</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">

                       
                        <div class="card-header">
                            <h3 class="card-title" id="topName">...</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                            </div>
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Full Name</label>
                                        <p id="fullName">...</p>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Phone Number</label>
                                        <p id="PhoneNumber">...</p>
                                    </div>



                                    <div class="col-md-3">
                                        <label>Email Address</label>
                                        <p id="emailid">...</p>
                                    </div>



                                    <div class="col-md-3">
                                        <label>Apply For</label>
                                        <p id="applyFor">...</p>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Salary</label>
                                        <p id="salary">...</p>
                                    </div>

                                    <div class="col-md-12">
                                        <hr>
                                    </div>

                                    <div class="col-md-12">
                                        <label>Job Descripton Id </label>
                                        <p id="artieldId">...</p>
                                    </div>

                                    <div class="col-md-12">
                                        <hr>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Apply Date</label>
                                        <p id="applyDate">...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">File</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>
                                </div>
                                </div>
                            <div class="card-body">
                                <a id="file" href="" class="btn btn-primary">Download CV</a>
                            </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card collapsed-card">
                    <div class="card-header">
                            <h3 class="card-title" id="topName">Delete</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                            </div>
                        <div class="card-body" style="display: none;">
                            <button id="delete"  class="btn btn-danger">Delete this item </button> 
                            <div class="spinner-border" id="spinner_dlt" role="status" style="display: none;">
                                <span class="sr-only">Loading...</span>
                            </div>

                            <br>
                            <p>Once you can delete it cannot be undone.</p>
                        </div>
                    </div>
                </div>
            </div>
               
            </div>
        </div>
    </section>
    
    <input type="hidden" id="posturl" value="{{Url('api/career')}}/{{Request::route('id')}}"/>

</div>

<script type="text/javascript">

    $(document).ready(function () {
            
        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get", 
            url: $("#posturl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,
                
            beforeSend: function() { 
                $("#spinner_add").show();
            },

            complete: function(){ 
                $("#spinner_add").hide();
            },

            success: function(response){ 
                if(response.code == 200 && response.status==true) {
                    var result = response.data;
                    $("#topName").html(result.full_name);
                    $("#fullName").html(result.full_name);
                    $("#PhoneNumber").html(result.phone_number);
                    $("#emailid").html(result.email_address);
                    $("#applyFor").html(result.apply_for);
                    $("#salary").html(result.expected_salary);
                    $("#artieldId").html(result.article_id);
                    $("#applyDate").html(result.apply_date);
                    $("#file").attr('href',"{{ URL::to('/') }}/cvupload/"+ result.cv_file);
                }
                else {
                    window.location.href = $("#backLink").attr("href");
                }
            }
        });

        // delete
        $("#delete").click(function(e) {
            var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "delete", 
            url: $("#posturl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,
                
            beforeSend: function() { 
                $("#spinner_dlt").show();
            },

            complete: function(){ 
                $("#spinner_dlt").hide();
            },

            success: function(response) { 
                if(response.code == 200 && response.status==true){
                    location.href = $("#backLink").attr("href");
               }
            }
         });
        });
    });

</script>

@endsection
