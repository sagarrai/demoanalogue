@extends('layouts.admin') 
@section('title','POST CATEGORY') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Post Category</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Project Category Add</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="postcategory_form">
                <div class="form-row">
                    @csrf
                    <div class="col-4">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" id="title" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="inputDescription">Description</label>
                            <textarea id="description" class="form-control" rows="1"></textarea>
                        </div>
                    </div>  
                    <div class="col-4">
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input type="text" id="slug" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="active">Active</label>
                            <input type="number" id="active" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="weight">Weight</label>
                            <input type="number" id="weight" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="User_ID">User ID</label>
                            <input type="number" id="user_id" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="office_id">Office ID</label>
                            <input type="number" id="office_id" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
                    </form>
            </div>
        </div>
    </section>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $('#postcategory_form').on('submit',function(e){
    e.preventDefault();

        var title = $("#title").val();
        var description = $("#description").val();
        var slug = $("#slug").val();
        var active = $("#active").val();
        var weight = $("#weight").val();
        var user_id = $("#user_id").val();
        var office_id = $("#office_id").val();
        var base_url = get_baseurl();
        var url = base_url + "/api/post-category";
        var token = getCookie("token");
        
        $.ajax({
            type: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            url: url,
            data: {
                "title": title,
                "description": description,
                "slug": slug,
                "active": active,
                "weight": weight,
                "user_id": user_id,
                "office_id": office_id,
            },
            success: function (response) {
                if (response.code == "200") {
                toastr.success('Added Successfully');
                    window.setTimeout(function () {
                        window.location.href = base_url + '/admin/postcategory/allpostcategory';
                    }, 2000);
                }
            },

            error: function (response) {
                if (response.code != 200) {
                    toastr.error('Something went wrong while submitting the package');
                    window.setTimeout(function () {
                        window.location.href = base_url + '/admin/postcategory/allpostcategory';
                    }, 2000);
                }
            }
        });
    });
});
</script>

@endsection
