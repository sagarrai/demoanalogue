@extends('layouts.admin') 
@section('title','POST CATEGORY') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Post Category  
                        <div class="spinner-border" id="spinner_getbyis" role="status" style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>
                        </h1>   
                            
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a id="backurl" href="{{Url('/admin/postcategory/allpostcategory')}}">Post Category</a></li>
                        <li class="breadcrumb-item active">Update </li>
                        </ol>
                    </div>


                    <div class="col-md-3 messagebox">
                        <div style="display:none;"  id="alert-msg" role="alert" class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Success</strong> 
                            <p>Post Category Updated Successfully ! <a href="{{Url('admin/postcategory/allpostcategory')}}">See in list</a> </p>
                           
                        </div>
                        <div style="display:none;" id="error-msg" role="alert" class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Something went wrong while posting the post ! </strong> 
                           
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="content">
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Update Post Category</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
                </div>
            <div class="card-body">
            
                       

                <form id="postcategory_form" method="POST" action="{{Url('api/post-category')}}/{{Request::route('id')}}">
                    <div class="form-row">
                        @csrf
                        @method('PUT')
                       
                        <div class="col-4">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="inputDescription">Description</label>
                                <textarea id="description" name="description" class="form-control" rows="1"></textarea>
                            </div>
                        </div>  
                        <div class="col-4">
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input type="text" name="slug" id="slug" class="form-control">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="active">Active</label>
                                <input type="text" name="active" id="active" class="form-control">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="weight">Weight</label>
                                <input type="text" name="width" id="weight" class="form-control">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="User_ID">User ID</label>
                                <input type="number" name='user_id' id="user_id" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="office_id">Office ID</label>
                                <input type="number" name="office_id" id="office_id" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                                <a href="#" class="btn btn-secondary">Cancel</a>

                                <div class="spinner-border" id="spinner_show" role="status" style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </section>
    <input type="hidden" id="postUrl" value="{{Url('api/post-category')}}/{{Request::route('id')}}"/>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    
    $('#postcategory_form').on('submit',function(e){
    e.preventDefault();
        var token = getCookie("token");

        $.ajax({
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:true,

        beforeSend: function(){
            $("#spinner_show").show();
        },
        complete: function(){
            $("#spinner_show").hide();
        },
        success: function(response)
        {
            console.log('Post from response', response);
            if(response.code == 200 && response.status== true) 
            {
                $("#alert-msg").show();

                setTimeout(() => {
                    $("#alert-msg").hide();
                }, '5000');
            }
            else 
            {
                $("#error-msg").show();
            }
        }
    });
    });


    // get by id 

    var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get", 
            url: $("#postUrl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,
                
            beforeSend: function(){ 
                $("#spinner_getbyis").show();
            },

            complete: function(){ 
                $("#spinner_getbyis").hide();
            },

            success: function(response){ 
                console.log('Response From Api of post', response);
                if(response.code == 200 && response.status==true){
                    var result = response.data;
                    $("#title").val(result.title);
                    $("#slug").val(result.slug);
                    $("#weight").val(result.weight);
                    $("#active").val(result.active);
                    $("#description").val(result.description);
                    $("#user_id").val(result.user_id);
                    $("#office_id").val(result.office_id);
                }else{
                    window.location.href = $("#backurl").attr("href");
                }
            }
        });

});
</script>

@endsection
