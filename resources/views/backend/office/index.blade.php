@extends('layouts.admin') @section('title', 'Office') @section('content')

    <section class="content">
<br>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Office</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-2">
          <table class="table table-striped projects" id="officeListTableId">
              <thead>
                  <tr>
                      <th>
                          ID
                      </th>
                      <th>
                          User ID
                      </th>
                      <th>
                          Office Code
                      </th>
                      <th>
                          Office Name
                      </th>
                      <th>
                        PO box
                    </th>
                     <th>
                          Address Line 1
                      </th>
                      <th>
                          Street
                      </th>
                        <th>
                          Phone
                      </th>
                      <th>
                          Email
                      </th> 
                      <th>
                          Action
                      </th>
                  </tr>
              </thead>
          </table>
        </div>
      </div>

    </section>

<script src="/admin-resources/dist/js/global.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>

<script type="text/javascript">
  
     $(function () {
            LoadTable();
            $(document).keypress(function (e) {
                if (e.which == 13) {
                    LoadTable();
                }
            });
        })

 function LoadTable() {
    var base_url = get_baseurl();
    $('#officeListTableId').dataTable().fnDestroy();
    $('#officeListTableId').dataTable({
        "pageLength":50,
        "processing": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },
        "ajax": {
            "type": "GET",
            "url": base_url + "/api/office",
            "contentType": "application/json; charset=utf-8",
            "headers": {
                'Authorization': `Bearer ${getCookie("token")}`,
            },
            "data": function (data) {
                data.SearchContent = $('#SearchContent').val();
                return JSON.stringify(data);
            }
        },
        "columns": [
            {"data": "id" },
            {"data": "user_id" },
            {"data": "office_code" },
            {"data": "office_name" },
            {"data": "po_box"},
            {"data": "address_line_1"},
            {"data": "street"},
            {"data": "phone"},
            {"data": "email"},
            {
                "data": "id", "title": "Action", "render": enable,
                "searchable": false,
                "orderable": false,
                "visible": true
            }
        ],
    })
}
    var enable = function (data, type, full, meta) {
    $.fn.dataTable.render.text();
    var actionBtn = "";
        actionBtn ='&nbsp;&nbsp;<a href= base_url +"/api/office/' + full.id + '" class="btn btn-primary btn-sm btn-update">Edit</a>'
            + '&nbsp;&nbsp; <a href= base_url +"/api/office/' + full.id + '" class="btn btn-danger btn-sm btn-update">Delete</a>';
    return actionBtn;
    }

</script>

@endsection
