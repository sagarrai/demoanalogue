@extends('layouts.admin') 
@section('title','Office') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Office</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Office Add</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="office_form">
                <div class="form-row">
                    @csrf
                    <div class="col-4">
                        <div class="form-group">
                            <label for="user_id">User ID</label>
                            <input type="number" id="user_id" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="office_code">Office Code</label>
                            <input type="text" id="office_code" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="office_name">Office Name</label>
                            <input type="text" id="office_name" class="form-control">
                        </div>
                    </div>
                   
                    <div class="col-4">
                        <div class="form-group">
                            <label for="po_box">P.O. Box</label>
                            <input type="number" id="po_box" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="address_line_1">Address Line 1</label>
                            <input type="text" id="address_line_1" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="street">Street</label>
                            <input type="text" id="street" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="number" id="phone" class="form-control" >
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
                    </form>
            </div>
        </div>
    </section>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $('#office_form').on('submit',function(e){
    e.preventDefault();

        var user_id = $("#user_id").val();
        var office_code = $("#office_code").val();
        var office_name = $("#office_name").val();
        var po_box = $("#po_box").val();
        var address_line_1 = $("#address_line_1").val();
        var street = $("#street").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var base_url = get_baseurl();
        var url = base_url +"/api/office";
        var token = getCookie("token");
        
        $.ajax({
            type: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            url: url,
            data: {
                "user_id": user_id,
                "office_code": office_code,
                "office_name": office_name,
                "po_box": po_box,
                "address_line_1": address_line_1,
                "street": street,
                "phone": phone,
                "email": email,
            },
            success: function (response) {
                if (response.code == "200") {
                    toastr.success('Added Successfully');
                    window.setTimeout(function () {
                        window.location.href = base_url +'/admin/office';
                    }, 1500);
                }
            },

            error: function (response) {
                if (response.code != 200) {
                    toastr.error('Something went wrong while submitting the package');
                    window.setTimeout(function () {
                        window.location.href = base_url +'/admin/office';
                    }, 1500);
                }
            }
        });
    });
});
</script>

@endsection
