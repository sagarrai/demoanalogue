@extends('layouts.admin') @section('title', 'Consultancy') @section('content')
<section class="content">
    <br>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Consultancy Lists</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <table class="table table-striped projects" id="consultancyList">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Full Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Phone
                        </th>
                        <th>
                            Objective
                        </th>
                        <th>
                            Budget
                        </th>
                        <th>
                            Deadline
                        </th>
                        <th>
                            Message
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="render-here">Are you sure to delete this user package?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="consultancy_delete" value="" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>
<script src="/admin-resources/dist/js/global.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
    LoadTable();
    $(document).keypress(function (e) {
    if (e.which == 13) 
    {
        LoadTable();
    }
        });
    })

function LoadTable() {
    var base_url = get_baseurl();
    
    $('#consultancyList').dataTable().fnDestroy();
    $('#consultancyList').dataTable({
        "pageLength":50,
        "processing": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },
        "ajax": {
            "type": "GET",
            "url":  base_url + "/api/consultancy",
            "contentType": "application/json; charset=utf-8",
            "headers": {
                'Authorization': `Bearer ${getCookie("token")}`,
            },
        "data": function (data) {
            data.SearchContent = $('#SearchContent').val();
                return JSON.stringify(data);
            }
        },
        "columns": [
            {"data": "id" },
            {"data": "full_name" },
            {"data": "email" },
            {"data": "phone"},
            {"data": "objective"},
            {"data": "budget"},
            {"data": "deadline"},
            {"data": "message"},
            {
                "data": "id", "title": "Action", "render": enable,
                "searchable": false,
                "orderable": false,
                "visible": true
            }
        ],
    })
}

var enable = function (data, type, full, meta) {
    $.fn.dataTable.render.text();
var actionBtn = "";
actionBtn ="&nbsp;&nbsp; <button type='button' onclick='bdelete("+full.id+")' data-target='#modal-default' data-toggle='modal' class='btn btn-primary btn-sm btn-danger'>Delete</button>";
return actionBtn;
}

function bdelete(id){
    $('#consultancy_delete').val(id);
}

$("#consultancy_delete").click(function() {
    var base_url = get_baseurl();
    
    var id = $(this).val();
    var token = getCookie("token");
    var url = base_url + '/api/consultancy/'+ id;
    $.ajax({
        type: 'DELETE',
        url: url,
        headers: {
          'Authorization': `Bearer ${getCookie("token")}`,
        },
        success: function (response) 
        {
            if (response.code == "200") 
            {
                window.setTimeout(function () 
                {
                    window.location.href = base_url +'/admin/consultancy/allconsultancy';
                }, 1000 );
            }
        }
    });
    alert('Delete Successfully !!');
});
</script>
@endsection