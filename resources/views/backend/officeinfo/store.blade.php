@extends('layouts.admin')
@section('title','Office Info')
@section('content')
<div class="content">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Office Info</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Office Info Add</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <form id="officeinfo_form" method="POST" enctype="multipart/form-data" action="<?php echo URL::to('/') ?>/api/officeinfo" >
            <div class="form-row">
                @csrf
                <div class="col-4">
                    <div class="form-group">
                        <label for="website_logo">Website Logo</label>
                        <input type="file" name="website_logo" class="form-control" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="footer_title">Footer Title</label>
                        <input type="text" name="footer_title" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="footer_description">Footer Description</label>
                        <textarea name="footer_description" name="footer_description" class="form-control" rows="2"></textarea>
                    </div>
                </div>
                
                <div class="col-4">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="phone">Phone number</label>
                        <input type="tel" name="phone" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="website">Website</label>
                        <input type="text" name="website" class="form-control" >
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="facebook_link">Facebook Link</label>
                        <input type="text" name="facebook_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="twitter_link">Twitter Link</label>
                        <input type="text" name="twitter_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="linkedin_link">LinkedIn Link</label>
                        <input type="text" name="linkedin_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="instagram_link">Instagram Link</label>
                        <input type="text" name="instagram_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="copyright_description">Copyright Description</label>
                        <textarea name="copyright_description" name="copyright_description" class="form-control" rows="2"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                        <a href="#" class="btn btn-secondary">Cancel</a>
                        <div class="spinner-border text-success" id="spinner_add" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

<script src="/admin-resources/dist/js/global.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
    $("#spinner_add").hide();
    $('#officeinfo_form').on('submit',function(e){
        e.preventDefault();
        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:new FormData(this),
            processData:false,
            contentType:false,
            cache:false,
            async:true,

            beforeSend: function(){
                $("#spinner_add").show();
            },
            complete: function(){
                $("#spinner_add").hide();
            },
            success: function(response)
            {
                console.log('Post from response', response);
                if(response.code == 200 && response.status== true) 
                {
                    $("#alert-msg").show();
                }
                else 
                {
                    $("#error-msg").show();
                }
            }
        });
    });
});

</script>

@endsection