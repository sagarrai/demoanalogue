@extends('layouts.admin')
@section('title', 'Office Info')
@section('content')
<section class="content">
    <br>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Office Info</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body p-2">
            <table class="table table-striped projects" id="OfficeinfoList">
                <thead>
                    <tr>
                        <th width="5%">
                            ID
                        </th>
                        <th width="10%">
                            Website logo
                        </th>
                        <th width="15%">
                            Footer Title
                        </th>
                        {{-- <th width="20%">
                            Footer Description
                        </th> --}}
                        <th width="20%">
                            Address
                        </th>
                        <th width="20%">
                            Phone Number
                        </th> 
                        <th width="20%">
                            Email Address
                        </th> 
                        {{-- <th width="25%">
                            Copyright Description
                        </th>  --}}
                        <th width="25%">
                            Action
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="render-here">Are you sure to delete this contact?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="officeinfo_delete" value="" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </div>
</div>

<script src="/admin-resources/dist/js/global.js"></script>
<script src="/admin-resources/plugins/jquery/jquery.min.js"></script>
<script src="/admin-resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/admin-resources/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="/admin-resources/plugins/toastr/toastr.min.js"></script>
<script src="/admin-resources/dist/js/adminlte.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
  
    $(function () {
    LoadTable();
    $(document).keypress(function (e) {
        if (e.which == 13) {
            LoadTable();
        }
    });
})

function LoadTable() {
    var base_url = get_baseurl();
    $('#OfficeinfoList').dataTable().fnDestroy();
    $('#OfficeinfoList').dataTable({
        "pageLength":50,
        "processing": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },
    "ajax": {
        "type": "GET",
        "url": base_url + "/api/officeinfo",
        "contentType": "application/json; charset=utf-8",
        "headers": {
        'Authorization': `Bearer ${getCookie("token")}`,
        },
        "data": function (data) {
            data.SearchContent = $('#SearchContent').val();
        return JSON.stringify(data);
}
      },
        "columns": [
            {"data": "id" },
            {
                data: 'website_logo',
                name: 'website_logo',
                render: function(data, type, full, meta){
                return "<img src={{ URL::to('/') }}/officeinfo/" + data + " width='90' class='img-thumbnail' />";
            },
            orderable: false
            },
            {"data": "footer_title" },
            // {"data": "footer_description" },
            {"data": "address" },
            {"data": "phone"},
            {"data": "email"},
            // {"data": "copyright_description"},
            {
            "data": "id", "title": "Action", "render": enable,
            "searchable": false,
            "orderable": false,
            "visible": true
            }
        ],
    })
}

var enable = function (data, type, full, meta) {
$.fn.dataTable.render.text();
    var actionBtn = "";
    actionBtn ='&nbsp;&nbsp;<a href= "/admin/officeinfo/edit/' + full.id + '" class="btn btn-primary btn-sm btn-update">Edit</a>'
          + '&nbsp;&nbsp; <button type="button" value="'+full.id+'" data-target="#modal-default" data-toggle="modal" id="delete_btn" class="btn btn-danger btn-sm btn-update">Delete</a>';
    return actionBtn;
}

$("#officeinfo_delete").click(function() {
    var base_url = get_baseurl();
    
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    var id = $("#delete_btn").val();
    var token = getCookie("token");
    var url = base_url + '/api/officeinfo/'+ id;
    $.ajax({
        type: 'DELETE',
        url: url,
        headers: {
          'Authorization': `Bearer ${getCookie("token")}`,
        },
        success: function (response) 
        {
            if (response.code == "200") 
            {
                window.setTimeout(function () 
                {
                    window.location.href = base_url +'/admin/officeinfo/allofficeinfo';
                }, 2000 );
            }
        }
    });
    toastr.success('Delete Successfully !!');
});

</script>
        
@endsection