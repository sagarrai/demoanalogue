@extends('layouts.admin')
@section('title','Office Info')
@section('content')
<div class="content">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Office Info</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Update Office Info Add</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <form id="officeinfo_form" method="POST" enctype="multipart/form-data" action="{{Url('api/officeinfo')}}/{{Request::route('id')}}" >
            <div class="form-row">
                @csrf
                @method("PUT")
                <div class="col-6">
                    <div style="display:none;" id="alert-msg" role="alert" class="alert alert-success">
                        <strong>Office Updated Successfully ! <a href="{{Url('admin/officeinfo/allofficeinfo')}}">See in list </a></strong>
                    </div>
                </div>
                <div class="col-6">
                    <div style="display:none;" id="error-msg" role="alert" class="alert alert-danger">
                        <strong>Something went wrong while updating the office info ! </strong>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="website_logo">Website Logo</label>
                        <img id="website_logo" name="website_logo" height="100"/>
                        <input type="file" id="website_logo" name="website_logo" class="form-control" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="footer_title">Footer Title</label>
                        <input type="text" id="footer_title" name="footer_title" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="footer_description">Footer Description</label>
                        <textarea id="footer_description" name="footer_description" class="form-control" rows="2"></textarea>
                    </div>
                </div>
                
                <div class="col-4">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" id="address" name="address" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="phone">Phone number</label>
                        <input type="tel" id="phone" name="phone" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email" class="form-control" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="website">Website</label>
                        <input type="text" id="website" name="website" class="form-control" >
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="facebook_link">Facebook Link</label>
                        <input type="text" name="facebook_link" id="facebook_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="twitter_link">Twitter Link</label>
                        <input type="text" name="twitter_link" id="twitter_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="linkedin_link">LinkedIn Link</label>
                        <input type="text" name="linkedin_link" id="linkedin_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="instagram_link">Instagram Link</label>
                        <input type="text" name="instagram_link" id="instagram_link" class="form-control">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="copyright_description">Copyright Description</label>
                        <textarea id="copyright_description" name="copyright_description" class="form-control" rows="2"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                        <a href="#" class="btn btn-secondary">Cancel</a>
                        <div class="spinner-border text-success" id="spinner_add" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <input type="hidden" id="officeinfoUrl" value="{{Url('api/officeinfo')}}/{{Request::route('id')}}"/>
    </div>

<script type="text/javascript">
    $(document).ready(function () {
    $("#spinner_add").hide();
    
        $('#officeinfo_form').on('submit',function(e){
        e.preventDefault();
        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data:new FormData(this),
            processData:false,
            contentType:false,
            cache:false,
            async:true,

            beforeSend: function(){
                $("#spinner_add").show();
            },
            complete: function(){
                $("#spinner_add").hide();
            },
            success: function(response)
            {
                if(response.code == 200 && response.status== true) 
                {
                    $("#alert-msg").show();
                }
                else 
                {
                    $("#error-msg").show();
                }
            }
        });
    });


    var token = getCookie("token");

    $.ajax({
        headers: {
            'Authorization': `Bearer ${token}`,
        },

        type: "get", 
        url: $("#officeinfoUrl").val(),
        processData:false,
        contentType:false,
        cache:false,
        async:true,
            
        beforeSend: function(){ 
            $("#spinner_show").show();
        },

        complete: function(){ 
            $("#spinner_show").hide();
        },

        success: function(response){ 
            if(response.code == 200 && response.status==true){
                var result = response.data;
                $("#website_logo").attr('src',"{{ URL::to('/') }}/officeinfo/"+ result.website_logo);
                $("#footer_title").val(result.footer_title);
                $("#footer_description").val(result.footer_description);
                $("#address").val(result.address);
                $("#phone").val(result.phone);
                $("#email").val(result.email);
                $("#website").val(result.website);
                $("#facebook_link").val(result.facebook_link);
                $("#twitter_link").val(result.twitter_link);
                $("#linkedin_link").val(result.linkedin_link);
                $("#instagram_link").val(result.instagram_link);
                $("#copyright_description").val(result.copyright_description);
            }else{
                window.location.href = $("#backLink").attr("href");
            }
        }
    });

    });
</script>

@endsection