@extends('layouts.admin')
@section('title','POST')
@section('content')
<div class="content">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Post</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"> <a href="{{Url('admin/post/allposts')}}">Post</a></li>
                        <li class="breadcrumb-item active">Add Post</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <form id="post_form" method="POST" enctype="multipart/form-data" action="<?php echo URL::to('/') ?>/api/post" >
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="topName">Add New Post</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                @csrf
                                <div class="col-8">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>
                                </div>
                                <!-- <div class="col-6">
                                    <div class="form-group">
                                        <label for="slug">Slug</label>
                                        <input type="text" name="slug" id="slug" class="form-control">
                                    </div>
                                </div> -->
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="short_text">Short Text</label>
                                        <input type="text" name="short_text" id="short_text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="inputDescription">Description</label>
                                        <textarea id="description" name="description" class="form-control" rows="2"></textarea>
                                    </div>
                                </div>
                                
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="metaDescription">Meta Description</label>
                                        <textarea id="meta_description" name="meta_description" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                                <!-- <div class="col-12">
                                    <div class="form-group">
                                        <label for="Keywords">Keywords</label>
                                        <input type="text" id="keywords" name="keywords" class="form-control">
                                    </div>
                                </div> -->
                                <div class="col-12" id="jobs">
                                    <div class="col-12" id="deadline">
                                        <div class="form-group">
                                            <label for="deadline">Deadline</label>
                                            <input type="date" name="deadline" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-12" id="other_description">
                                        <div class="form-group">
                                            <label for="other_description">Other Description</label>
                                            <textarea id="other_description" name="other_description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12" id="no_of_opening">
                                        <div class="form-group">
                                            <label for="no_of_opening">No of Opening</label>
                                            <input type="number" name="no_of_opening" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <button type="submit" name="add_post" id="add_post" class="btn btn-success">Submit</button>
                                        <a href="#" class="btn btn-secondary">Cancel</a>
                                        <div class="spinner-border" id="spinner_add" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="topName">Post</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select name="category" id="category" class="form-control category">                                   
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="feature_image">Feature Image</label>
                                <input type="file" name="feature_image" id="feature_image" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="office_id">Office ID</label>
                                <input type="number" name="office_id"  id="office_id" value="1" class="form-control" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="User_ID">User ID</label>
                                <input type="number" name="user_id" id="user_id" value="1" class="form-control" required>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <input type="hidden" id="postUrl" value="{{Url('/api/post-category')}}"/>
    </section>
</div>
<script src="/admin-resources/dist/js/global.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
    $('#description').summernote();
    $("#spinner_add").hide();
    $("#jobs").hide();
        $('#category').change(function(){
            if($(this).val() == 2 || 3) {
                $('#jobs').show();
            } else if($(this).val() == 1){
                $('#jobs').hide();
            } else if($(this).val()){
                $('#jobs').hide();
            }
        });

    
        $('#post_form').on('submit',function(e){
        e.preventDefault();
        var token = getCookie("token");
        var base_url = get_baseurl();

        $.ajax({
        headers: {  
            'Authorization': `Bearer ${token}`,
        },
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:true,

        beforeSend: function(){
            $("#spinner_add").show();
        },
        complete: function(){
            $("#spinner_add").hide();
        },
        success: function(response)
        {
            console.log('Post from response', response);
            if(response.code == 200 && response.status== true) 
            {
                toastr.success('Added Successfully');
                window.setTimeout(function () {
                    window.location.href = base_url + '/admin/post/allposts';
                }, 1500);
            }
            else 
            {
                toastr.error('Something went wrong while submitting the package');
                window.setTimeout(function () {
                    window.location.href = base_url + '/admin/post/allposts';
                }, 1500);
            }
        }
        });
        });

        // GET POST CATEGORY 

        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get", 
            url: $("#postUrl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,
                
            beforeSend: function(){ 
                $("#spinner_add").show();
            },

            complete: function(){ 
                $("#spinner_add").hide();
            },

            success: function(response){ 
                if(response.code == 200 && response.status==true){
                    var result = response.data;

                    $.each(result, function () {
                        ///console.log("ID: " + this.id);
                        $("#category").append("<option value='"+this.id+"'>"+this.title+"</option>");
                    });

                }else{
                    window.location.href = $("#backLink").attr("href");
                }
            }
 
        });

          
    });
</script>


@endsection