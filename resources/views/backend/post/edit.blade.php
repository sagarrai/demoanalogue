@extends('layouts.admin')
@section('title','POST')
@section('content')
<div class="content">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Update Post
                    <div class="spinner-border" id="spinner_show" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"> <a href="{{Url('admin/post/allposts')}}">Post</a></li>
                        <li class="breadcrumb-item active">Update Post</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <form id="post_form" method="POST" enctype="multipart/form-data" action="{{Url('api/post')}}/{{Request::route('id')}}">
            <div class="row">
                <div class="col-6">
                    <div style="display:none;" id="alert-msg" role="alert" class="alert alert-success">
                        <strong>Post Updated Successfully !  <a href="{{Url('admin/post/allposts')}}">See in list </a></strong>
                    </div>
                </div>
                <div class="col-6">
                    <div style="display:none;" id="error-msg" role="alert" class="alert alert-danger">
                        <strong>Something went wrong while posting the post ! </strong>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="topName">Update Post</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="form-row">
                                @csrf
                                @method("PUT")
                                <div class="col-8">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" id="title" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="short_text">Short Text</label>
                                        <input type="text" name="short_text" id="short_text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="inputDescription">Description</label>
                                        <textarea id="description" name="description" class="form-control" rows="2">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="metaDescription">Meta Description</label>
                                        <textarea id="meta_description" name="meta_description" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>

<<<<<<< HEAD
                                <div class="col-12" id="jobs">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="deadline">Deadline</label>
                                            <input type="date" id="deadline" name="deadline" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-12" >
                                        <div class="form-group">
                                            <label for="other_description">Other Description</label>
                                            <textarea id="other_description" name="other_description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12" >
                                        <div class="form-group">
                                            <label for="no_of_opening">No of Opening</label>
                                            <input type="number" id="no_of_opening" name="no_of_opening" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                
=======
>>>>>>> 27a0d0ec685ce732c9ceb55a37dfed505e9b5434
                                <div class="col-12">
                                    <div class="form-group">
                                        <button type="submit" name="add_post"  id="add_post" class="btn btn-success">Submit</button>
                                        <a href="#" class="btn btn-secondary">Cancel</a>
                                        <div class="spinner-border" id="spinner_add" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="topName">Post Type</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select name="category" id="category" class="form-control category">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="feature_image">Previous Feature Image</label>
                                <img id="image" height="100"/>
                                <label id="no_image" style="display:none">The post has no any images</label>
                                <br>
                                <input type="file" name="feature_image" id="feature_image" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="office_id">Office ID</label>
                                <input type="number" name="office_id"  id="office_id" value="1" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="User_ID">User ID</label>
                                <input type="number" name="user_id" id="user_id" value="1" class="form-control" required>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </form>
        <input type="hidden" id="postUrl" value="{{Url('api/post')}}/{{Request::route('id')}}"/>
        <input type="hidden" id="postUrlGetCat" value="{{Url('api/post-category')}}"/>
    </section>
</div>
<script src="/admin-resources/dist/js/global.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
    $('#description').summernote();
    $("#spinner_add").hide();

        $('#post_form').on('submit',function(e){
        e.preventDefault();
        var token = getCookie("token");

        $.ajax({
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:true,

        beforeSend: function(){
            $("#spinner_add").show();
        },
        complete: function(){
            $("#spinner_add").hide();
        },
        success: function(response)
        {
            console.log('Post from response', response);
            if(response.code == 200 && response.status== true)
            {
                $("#alert-msg").show();
            }
            else
            {
                $("#error-msg").show();
            }
        }
        });
        });


        // GET POST CATEGORY
        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get",
            url: $("#postUrlGetCat").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,

            beforeSend: function(){
                $("#spinner_show").show();
            },

            complete: function(){
                $("#spinner_show").hide();
            },

            success: function(response){
                console.log('Response From Api of post category', response);
                if(response.code == 200 && response.status==true){
                    var result = response.data;

                    $.each(result, function () {
                        $("#category").append("<option value='"+this.id+"'>"+this.title+"</option>");
                    });

                }else{
                    window.location.href = $("#backLink").attr("href");
                }
            }
        });


        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get",
            url: $("#postUrl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,

            beforeSend: function(){
                $("#spinner_show").show();
            },

            complete: function(){
                $("#spinner_show").hide();
            },

            success: function(response){
                ///console.log('Response From Api of post', response);
                if(response.code == 200 && response.status==true){
                    var result = response.data;
                    $("#title").val(result.title);
                    $("#short_text").val(result.short_text);
                    document.getElementById('description').innerHTML=result.description;
                    $("#description").val(result.description);
                    $("#meta_description").val(result.meta_description);
                    $('#category').val(result.category).prop("selected",true);
                    $("#other_description").val(result.other_description);
                    $("#deadline").val(result.deadline);
                    $("#office_id").val(result.office_id);
                    $("#no_of_opening").val(result.no_of_opening);
                    $("#user_id").val(result.user_id);
                    
                    if(result.feature_image){
                        $("#image").attr('src',"{{ URL::to('/') }}/imageupload/blog/"+ result.feature_image);
                    }
                    else{
                        $("#no_image").show();
                    }
                }else{
                    window.location.href = $("#backLink").attr("href");
                }
            }
        });



    });
</script>



@endsection
