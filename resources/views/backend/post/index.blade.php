@extends('layouts.admin') @section('title', 'Posts') @section('content')
<!-- Main content -->
<section class="content ">
    <!-- Default box -->
    <br>
    <div class="card mt-2">
        <div class="card-header">
            <h3 class="card-title">Posts</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body p-2">
            <table class="table table-striped projects" id="postListTableId">
                <thead>
                    <tr>
                        <th width="10%">
                            POST ID
                        </th>
                        <th width="20%">
                            Post Title
                        </th>
                        <th width="10%">
                            Category
                        </th>
                        <th width="25%">
                            Feature Image
                        </th>
                        
                        {{-- <th>
                            Active
                        </th>  --}}
                        <th width="25%">
                            Action
                        </th>
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<!-- <div class="modal fade" id="confirmModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to remove this data?&hellip;</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" name="ok_button" class="btn btn-danger" id="ok_button">OK</button>
            </div>
        </div>
    </div>
</div> -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="render-here">Are you sure to delete this contact?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="post_delete" value=""  class="btn btn-danger post_delete">Delete</button>
            </div>
        </div>
    </div>
</div>
<script src="/admin-resources/dist/js/global.js"></script>
<script src="/admin-resources/plugins/jquery/jquery.min.js"></script>
<script src="/admin-resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/admin-resources/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="/admin-resources/plugins/toastr/toastr.min.js"></script>
<script src="/admin-resources/dist/js/adminlte.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
    LoadTable();
    $(document).keypress(function (e) {
    if (e.which == 13) 
    {
        LoadTable();
    }
        });
    })

    function LoadTable() {
    var base_url = get_baseurl();
    $('#postListTableId').dataTable().fnDestroy();
    
    $('#postListTableId').dataTable({
        "pageLength":50,
        "processing": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },
        "ajax": 
        {
            "type": "GET",
            "url": base_url + "/api/post",
            "contentType": "application/json; charset=utf-8",
            "headers": 
            {
                'Authorization': `Bearer ${getCookie("token")}`,
            },
            "data": function (data) {
            data.SearchContent = $('#SearchContent').val();
            return JSON.stringify(data);
            }
        },
        "columns": [
            {"data": "id" },
            {"data": "title" },
            {"data": "category"},
            {
                data: 'feature_image',
                name: 'feature_image',
                render: function(data, type, full, meta){
                return "<img src={{ URL::to('/') }}/imageupload/blog/" + data + " width='90' class='img-thumbnail' alt='N/A'/>";
            },
            orderable: false
            },
            {
                "data": "id", "title": "Action", "render": enable,
                "searchable": false,
                "orderable": false,
                "visible": true
            }
        ],
        })
    }

        var enable = function (data, type, full, meta) {
        $.fn.dataTable.render.text();
        var actionBtn = "";
        actionBtn ='&nbsp;&nbsp;<a href="/admin/post/edit/' + full.id + '" class="btn btn-primary btn-sm btn-update">Edit</a>'
        + '&nbsp;&nbsp; <button type="button" onClick="bdelete('+full.id+')" data-target="#modal-default" data-toggle="modal" class="btn btn-danger btn-sm btn-update">Delete</a>';
        return actionBtn;
        }

        function bdelete(id){
            $("#post_delete").val(id);
        }
        
        $(".post_delete").click(function() {
        var base_url = get_baseurl();
        
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        var id = $(this).val();
        var token = getCookie("token");
        var url = base_url + '/api/post/'+ id;
        $.ajax({
            type: 'DELETE',
            url: url,
            headers: {
              'Authorization': `Bearer ${getCookie("token")}`,
            },
            success: function (response) 
            {
                if (response.code == "200") 
                {
                    window.setTimeout(function () 
                    {
                        window.location.href = base_url +'/admin/post/allposts';
                    }, 2000 );
                }
            }
        });
        toastr.success('Deleted Successfully !!');
        });
</script>
@endsection