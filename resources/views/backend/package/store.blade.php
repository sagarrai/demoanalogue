@extends('layouts.admin') 
@section('title','Package') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Package</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Package Add</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="package_form" method="POST" enctype="multipart/form-data" action="<?php echo URL::to('/') ?>/api/package" >
                <div class="form-row">
                    @csrf
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Name</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Code</label>
                            <input type="number" name="code" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="inputDescription">Description</label>
                            <textarea name="description" class="form-control" rows="1"></textarea>
                        </div>
                    </div>  
                    <div class="col-6">
                        <div class="form-group">
                            <label for="module">Module</label>
                            <select name="module" id="module" class="form-control">                                   
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="active">Type</label>
                            <select name="type" class="form-control">       
                                <option value="basic">Basic</option>
                                <option value="standard">Standard</option>
                                <option value="premium">Premium</option>
                                <option value="professional">Professional</option>
                                <option value="tailor">Tailor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                            <div class="spinner-border" id="spinner_add" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <input type="hidden" id="packageUrl" value="{{Url('/api/module')}}"/>
            </div>
        </div>
    </section>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
    $("#spinner_add").hide();
        $('#package_form').on('submit',function(e){
        e.preventDefault();
        var token = getCookie("token");
        var base_url = get_baseurl();

        $.ajax({
        headers: {
            'Authorization': `Bearer ${token}`,
        },
        type: $(this).attr("method"),
        url: $(this).attr("action"),
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:true,

        beforeSend: function(){
            $("#spinner_add").show();
        },
        complete: function(){
            $("#spinner_add").hide();
        },
        success: function(response)
        {
            if(response.code == 200 && response.status== true) 
            {
                toastr.success('Added Successfully');
                window.setTimeout(function () {
                    window.location.href = base_url + '/admin/module/allmodule';
                }, 1500);
            }
            else 
            {
                toastr.error('Something went wrong while submitting the package');
                window.setTimeout(function () {
                    window.location.href = base_url + '/admin/module/allmodule';
                }, 1500);
            }
        }
        });
        });

        // Get Module type 

        var token = getCookie("token");

        $.ajax({
            headers: {
                'Authorization': `Bearer ${token}`,
            },

            type: "get", 
            url: $("#packageUrl").val(),
            processData:false,
            contentType:false,
            cache:false,
            async:true,
                
            beforeSend: function(){ 
                $("#spinner_add").show();
            },

            complete: function(){ 
                $("#spinner_add").hide();
            },

            success: function(response){ 
                if(response.code == 200 && response.status==true){
                    var result = response.data;
                    $.each(result, function () {
                        $("#module").append("<option value='"+this.id+"'>"+this.name+"</option>");
                    });

                }else{
                    window.location.href = $("#backLink").attr("href");
                }
            }
 
        });
    });
</script>

@endsection
