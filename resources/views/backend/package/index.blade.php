@extends('layouts.admin') @section('title', 'Package') @section('content')
<section class="content">
    <br>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Module Lists</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <table class="table table-striped projects" id="moduleListTableId">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Code
                        </th>
                        <th>
                            Description
                        </th>
                        <th>
                            Module
                        </th>
                        <th>
                            Type
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<script src="/admin-resources/dist/js/global.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
    LoadTable();
        
    $(document).keypress(function (e) {
        if (e.which == 13) {
            LoadTable();
        }
    });
})

function LoadTable() {
    var base_url = get_baseurl();
    
    $('#moduleListTableId').dataTable().fnDestroy();
    $('#moduleListTableId').dataTable({
        "pageLength":50,
        "processing": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },
        "ajax": {
            "type": "GET",
            "url":  base_url + "/api/package",
            "contentType": "application/json; charset=utf-8",
            "headers": {
                'Authorization': `Bearer ${getCookie("token")}`,
            },
        "data": function (data) {
            data.SearchContent = $('#SearchContent').val();
                return JSON.stringify(data);
            }
        },
        "columns": [
            {"data": "id" },
            {"data": "name" },
            {"data": "code" },
            {"data": "description"},
            {"data": "module"},
            {"data": "type"},
            {
                "data": "id", "title": "Action", "render": enable,
                "searchable": false,
                "orderable": false,
                "visible": true
            }
        ],
    })
}

var enable = function (data, type, full, meta) {
    $.fn.dataTable.render.text();
var actionBtn = "";
actionBtn ="&nbsp;&nbsp; <a href='/admin/package/edit/"+full.id+"' class='btn btn-primary btn-sm btn-update'>Edit</a>";
return actionBtn;
}
</script>
@endsection