@extends('layouts.admin') 
@section('title','Contact') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Contact</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add Contact </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="contact_form">
                <div class="form-row">
                    @csrf
                    <div class="col-6">
                        <div style="display:none;" id="alert-msg" role="alert" class="alert alert-success">
                            <strong>Contact Updated Successfully ! </strong> 
                        </div>
                    </div>
                    <div class="col-6">
                        <div style="display:none;" id="error-msg" role="alert" class="alert alert-danger">
                            <strong>Something went wrong while updating the Contact ! </strong> 
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="full_name">Full Name</label>
                            <input type="text" id="full_name" value="{{ old('full_name',$contact->full_name) }}" placeholder="Enter your full name" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" id="phone" value="{{ old('phone',$contact->phone) }}" placeholder="Enter your phone number" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" value="{{ old('email',$contact->email) }}" placeholder="Enter your email" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="inputMessage">Message</label>
                            <textarea id="message" class="form-control" rows="3">{{ $contact->message }}</textarea>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" id="subject" value="{{ old('subject',$contact->subject) }}" placeholder="Enter your subject" class="form-control" >
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                    <input type="hidden" value="{{ $contact->id }}" id="contact_id">
                </div>
                </form>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
$(document).ready(function () {
    $('#contact_form').on('submit',function(e){
    e.preventDefault();

        var full_name = $("#full_name").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var subject = $("#subject").val();
        var message = $("#message").val();
        var contact_id = $("#contact_id").val();
        var base_url = get_baseurl();
        var url = base_url +"/api/contact/";
        
        $.ajax({
            type: 'PUT',
            url: url + contact_id,
            data: {
                "full_name": full_name,
                "phone": phone,
                "email": email,
                "subject": subject,
                "message": message,
            },
            success: function (response) {
                if (response.code == "200") {
                    $('#alert-msg').removeAttr('style');
                    window.setTimeout(function () {
                        window.location.href = base_url + "/admin/contact/allcontact";
                    }, 2000);
                }
            },

            error: function (response) {
                if (response.code != 200) {
                    $('#error-msg').removeAttr('style');
                        setTimeout(location.reload.bind(location), 2000);
                }
            }
        });
    });
});
</script>

@endsection
