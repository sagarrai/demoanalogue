@extends('layouts.admin') 
@section('title','Contact') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Contact</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Add Contact </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="contact_form">
                <div class="form-row">
                    @csrf
                    <div class="col-4">
                        <div class="form-group">
                            <label for="full_name">Full Name</label>
                            <input type="text" id="full_name" placeholder="Enter your full name" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" id="phone" placeholder="Enter your phone number" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" placeholder="Enter your email" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="inputMessage">Message</label>
                            <textarea id="message" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" id="subject" placeholder="Enter your subject" class="form-control" >
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </section>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $('#contact_form').on('submit',function(e){
    e.preventDefault();

        var full_name = $("#full_name").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var subject = $("#subject").val();
        var message = $("#message").val();
        var base_url = get_baseurl();
        var url = base_url + "/api/contact";
        var token = getCookie("token");
        
        $.ajax({
            type: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            url: url,
            data: {
                "full_name": full_name,
                "phone": phone,
                "email": email,
                "subject": subject,
                "message": message,
            },
            success: function (response) {
                if (response.code == "200") {
                    $('#alert-msg').removeAttr('style');
                    window.setTimeout(function () {
                        window.location.href = base_url + '/admin/contact';
                    }, 2000);
                }
            },

            error: function (response) {
                if (response.code != 200) {
                    $('#error-msg').removeAttr('style');
                        setTimeout(location.reload.bind(location), 2000);
                }
            }
        });
    });
});
</script>

@endsection
