@extends('layouts.admin') 
@section('title','User') 

@section('content')
    <div class="content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add User</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User Add</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <form id="post_form">
                <div class="form-row">
                    @csrf
                    <div class="col-4">
                        <div class="form-group">
                            <label for="title">Username</label>
                            <input type="text" id="username" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="slug">Email</label>
                            <input type="email" id="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="slug">Password</label>
                            <input type="text" id="password" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="submit" id="add_post" class="btn btn-success">Submit</button>
                            <a href="#" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
                    </form>
            </div>
        </div>
    </section>
</div>

<script src="/admin-resources/dist/js/global.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $('#post_form').on('submit',function(e){
    e.preventDefault();

        var username = $("#username").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var base_url = get_baseurl();
        var url = base_url + "/api/post";
        var token = getCookie("token");
        
        $.ajax({
            type: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            url: url,
            data: {
                "username": username,
                "email": email,
                "password": password,
            },
            success: function (response) {
                if (response.code == "200") {
                    $('#alert-msg').removeAttr('style');
                    window.setTimeout(function () {
                        window.location.href = base_url + '/admin/user';
                    }, 2000);
                }
            },

            error: function (response) {
                if (response.code != 200) {
                    $('#error-msg').removeAttr('style');
                    setTimeout(location.reload.bind(location), 2000);
                }
            }
        });
    });
});
</script>

@endsection
