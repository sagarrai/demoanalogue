@extends('layouts.admin') @section('title', 'Users') @section('content')

<!-- Main content -->
    <section class="content">
    <br>
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">User</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-2">
          <table class="table table-striped projects" id="userListTableId">
              <thead>
                  <tr>
                      <th>
                          S.No
                      </th>
                      <th>
                          Username
                      </th>
                      <th>
                          Email
                      </th>
                      <th>
                          Status
                      </th>
                      <th>
                          Action
                      </th>
                  </tr>
              </thead>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

<script src="/admin-resources/dist/js/global.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(function () {
    LoadTable();
    $(document).keypress(function (e) {
        if (e.which == 13) {
            LoadTable();
        }
    });
})

function LoadTable() {
    var base_url = get_baseurl();

    $('#userListTableId').dataTable().fnDestroy();
    $('#userListTableId').dataTable({
        "pageLength":50,
        "processing": true,
        // "serverSide": true,
        "filter": false,
        "drawCallback": function () {
            $("#dataTable_wrapper").children().eq(1).css("overflow", "auto");
        },
        "ajax": {
            "type": "GET",
            "url": base_url + "/api/allusers",
            "contentType": "application/json; charset=utf-8",
            "headers": {
                'Authorization': `Bearer ${getCookie("token")}`,
            },
            "data": function (data) {
                data.SearchContent = $('#SearchContent').val();
                return JSON.stringify(data);
            }
        },
        "columns": [
            {"data": "id" },
            {"data": "name" },
            {"data": "email"},
            {
                "data": "status", "title": "Active", "render": check,
                "visible": true
            },
            {
                "data": "id", "title": "Action", "render": enable,
                "searchable": false,
                "orderable": false,
                "visible": true
            }
        ],
    })
}
var check = function (data, type, full, meta) {
    $.fn.dataTable.render.text();
    if(full.status == 1 ){
        status ='<input type="checkbox" checked data-toggle="toggle">';
    } else {
        status ='<input type="checkbox" unchecked data-toggle="toggle">';
    }
    return status;
}

var enable = function (data, type, full, meta) {
    $.fn.dataTable.render.text();
    var actionBtn = "";
        actionBtn ='&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm btn-update">Edit</a>';
            // + '&nbsp;&nbsp; <a href="#" class="btn btn-danger btn-sm btn-update">Delete</a>';
    return actionBtn;
}

</script>

@endsection
