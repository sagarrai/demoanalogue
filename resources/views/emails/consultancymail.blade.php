<!DOCTYPE html>
<html>
<head>
    <title>Analogue Inc</title>
</head>
<body>
    <p>Name: {{ $details['full_name'] }}</p>
    <p>Email: {{ $details['email'] }}</p>
    <p>Phone: {{ $details['phone'] }}</p>
    <p> <strong>Subject: </strong> {{ $details['title'] }} </p>
    <p>Message: {{ $details['body'] }}</p>
    <p>Budget: {{ $details['budget'] }}</p>
    <p>Deadline: {{ $details['deadline'] }}</p>
</body>
</html>