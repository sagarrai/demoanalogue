<style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        border: none;
    }

    html {
        font-family: sans-serif;
    }

    body {
        background: #ccc;
    }

    p {
        font-size: 12px;
    }

    h5 {
        font-size: .83em;
    }

    h4 {
        font-size: 1em;
    }

    h3 {
        font-size: 1.17em;
    }

    h2 {
        font-size: 1.5em;
    }

    h1 {
        font-size: 2em;
    }

    page {
        background: white;
        display: block;
        margin: 0 auto;
        box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
    }

    page[size="A4"] {
        width: 595px;
        height: 770px;
    }

    table {
        position: absolute;
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        top:18rem;
    }

    td, th {
        border: 1px solid #000000;
        text-align: left;
        padding: 5px;
        font-size: 12px;
    }

    tr td ul li{
        margin-left: 15px;
        font-size: 12px;
    }
    /*.container {*/
    /*    position: absolute;*/
    /*    width: 595px;*/
    /*    height: 770px;*/
    /*    background-color: rgba(255,255,255,1);*/
    /*    overflow: hidden;*/
    /*    --web-view-name: custom – 1;*/
    /*    --web-view-id: container;*/
    /*    --web-scale-on-resize: true;*/
    /*    --web-enable-deep-linking: true;*/
    /*}*/
    .header-img {
        position: absolute;
        padding: 20px;
        width: 600px;
    }
    .footer-img {
        position: absolute;
        width: 595px;
        height: 59px;
        top: 711px;
    }
    .background-img {
        opacity: 0.1;
        position: absolute;
        width: 284px;
        height: 516px;
        left: 156px;
        top: 150px;
        overflow: visible;
    }
    .quotation-area {
        position: absolute;
        left: 440px;
        top: 128px;
    }
    .quotation-id {
        white-space: nowrap;
        text-align: left;
        font-family: Times New Roman;
    }

    .title {
        position: absolute;
        left: 36px;
        top: 143px;
        font-family: Times New Roman;
        font-weight: bold;
        color: rgba(0,0,0,1);
    }

    .subject-title{
        position: absolute;
        left: 36px;
        top: 143px;
        font-family: Times New Roman;
        font-weight: bold;
        color: rgba(0,0,0,1);  
        font-size: 2em; 
    }

    .personal-info {
        position: absolute;
        left: 36px;
        top: 175px;
        font-family: Times New Roman;
        font-size: 12px;
        color: rgba(0,0,0,1);
        list-style: none;
    }
    .subject {
        position: absolute;
        left: 235px;
        top: 255px;
        text-align: center;
        font-family: Times New Roman;
        font-weight: bold;
    }
    .red-col {
        color: red;
    }
</style>
<div id="invoice" style="display: none">
    <page id="content" class="form" size="A4">
        <div class="container" style="  position: absolute;
            width: 595px;
            height: 770px;
            background-color: rgba(255,255,255,1);
            overflow: hidden;
            --web-view-name: custom – 1;
            --web-view-id: container;
            --web-scale-on-resize: true;
            --web-enable-deep-linking: true;">
            <img class="header-img" src="/quotation/header.jpeg">
            <img class="footer-img" src="/quotation/footer.jpeg">
            <img class="background-img" src="/quotation/background.jpg">
            <div class="quotation-area">
                <div class="quotation-id">
                    <p class="red-col">Quotation no: &nbsp;<?= rand(); ?>A</p>
                </div>
            </div>
            <div class="title">
                <h3> Gadgets Trade World</h3>
                <h4 id="quotationName"> </h4><br>
            </div>
            <ul class="personal-info">
                <p></p>
                <p id="quotationEmail"></p>
                <p id="quotationPhone"></p>
            </ul>
            {{-- <div class="subject-title">
                <h6> <u><strong>Branding & Marketing</strong></u></h6>
            </div><br><br><br> --}}
            <div class="subject">
                <p id="subject"><strong><i><u> Subject: Price Quotation</u></i></strong></p>
            </div>
            <br><br>
            <table class="table" style="margin: -20px">
                <tr>
                    <th>Particular</th>
                    <th>Total</th>
                </tr>

                <tr>
                    <td>
                        <p class="font-15" id="package-name">
                            Packages
                        </p>
                        <p class="font-15">
                            Includes: Full library of Graphic elements
                        </p>
                        <ul>
                          <li class="font-15" id="package-detail"></li>
                        </ul>
                    </td>
                    <td>
                        <p class="font-15" id="package-total">50,000</p>
                        <p class="font-15">On time charge</p>
                    </td>
                </tr>
            </table>
        </div>
    </page>
</div>
