@extends('layouts.frontend')

@section('content')

<style type="text/css">
    pre span{
        font-size: 1.5rem;
        font-family: "TT Commons", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        /* list-style: none; */
        text-transform: none;
        text-align: center;
        line-height: 160%;
        box-sizing: border-box;
        background:transparent;
    }
</style>

	@foreach($jobs as $job)
        <section class="section background-light has-bg-img is-page-title">
            <div class="container">
                <div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                    <h1 class="section-heading">{{ $job->title }}</h1>
                    <p class="sub-title">Number of openings - {{ $job->no_of_opening }} <span class="badge badge-light ml-3">Apply before {{ date('d-M-Y', strtotime($job->deadline)) }}</span></p>
                </div>
            </div>
            <div class="section-backgrounds">
                <img src="http://www.analogueinc.com.np/frontend/images/arrow-lg.png" alt="" class="big-arrow">
                <img src="http://www.analogueinc.com.np/frontend/images/arrow-sm.png" alt="" class="small-arrow">
            </div>
        </section>

		<main role="main">
			<div class="container">
				<div class="row">
					<div class="col-xxl-8 col-xl-9 col-lg-10 col-12 mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
						<p class="lead text-dark">Analogue Inc is an IT firm which is a company under PG and Sons, designed for future excellence where we provide paramount services pertaining to the field. Analogue Inc is the only company that will be fully based on pure IT and computer applications.</p>

						<h5>Job description</h5>
	                    @if($job->description)
							<div class="mb-5">
	                        	{!! $job->description !!}
							</div>
                   		@endif

						<h5>Other Specification</h5>
						<div class="mb-5">
                        	{!! $job->other_description !!}
						</div>

                    	@if( $job->meta_description!=null)
							<h5>Project Manager top skills & proficiencies</h5>
							<div class="mb-5">
                                <pre><span>{!! $job->meta_description !!}</span></pre>
							</div>
                    	@endif

						<p class="lead text-dark">Interested candidates should apply with complete CV giving details of their age, educational qualifications, experience and current & expected salary, clearly mentioning their contact address and telephone/mobile number to hr@analogueinc.com.np</p>

						<p><a href="/career_apply/{{ $job->title }}" class="btn btn-primary">Upload Your CV<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
					</div>
				</div>

			</div>
		</main>

		<script src="frontend/js/particles.min.js"></script>
		<script src="frontend/js/particles.settings.js"></script>

	@endforeach
@endsection
