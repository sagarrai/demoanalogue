@extends('layouts.frontend')

@section('content')

<section class="section background-light has-bg-img is-page-title">
	<div class="container">
		<div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false">
			<h1 class="section-heading">Contact Us</h1>
			<p class="sub-title">Get in touch with us</p>
		</div>
	</div>
	<div class="section-backgrounds">
		<img src="/frontend/images/arrow-lg.png" alt="" class="big-arrow">
		<img src="/frontend/images/arrow-sm.png" alt="" class="small-arrow">
	</div>
</section>

<main role="main">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-lg-6 col-12 mb-md-7 mb-6" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart" data-aos-mirror="false" data-aos-delay="200">
				<p class="mb-6">We strive to provide you with the best from us. Thank you for going through our website. Feel free to drop in a message and give us some feedback. Our mailbox is open!</p>
				<div class="img-holder"><img src="frontend/images/img13.jpg" alt=""></div>
			</div>
			<div class="col-xl-6 col-lg-6 col-12" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart" data-aos-mirror="false" data-aos-delay="400">
				<form class="contact-form" method="POST" id="contact_form">
					<fieldset class="form-group contact-full-name">
						<div class="input-group">
							<div class="input-group-prepend">
								<select class="custom-select" id="salutation">
									<option>Mr.</option>
									<option>Ms.</option>
									<option>Mrs.</option>
									<option>None</option>
								</select>
							</div>
							<input type="text" class="form-control" id="full_name" placeholder="Your Full Name">
						</div>
					</fieldset>
					<div class="form-row">
						<div class="col-xl-6 col-lg-12 col-md-6 col-sm-6 col-12">
							<fieldset class="form-group">
								<input type="email" class="form-control" id="email" placeholder="Valid Email Address">
							</fieldset>
						</div>
						<div class="col-xl-6 col-lg-12 col-md-6 col-sm-6 col-12">
							<fieldset class="form-group">
								<input type="text" class="form-control" id="phone" placeholder="Your Phone Number">
							</fieldset>
						</div>
					</div>
					<fieldset class="form-group">
						<input type="text" class="form-control" id="subject" placeholder="Subject (Tell us what you'd like to talk about)">
					</fieldset>
					<fieldset class="form-group mb-xl-6 mb-lg-6 mb-md-5 mb-sm-4 mb-4">
						<textarea class="form-control" id="message" rows="15" placeholder="What's on your mind?"></textarea>
					</fieldset>
					<p>
						<button type="submit" class="btn btn-primary">Send Your Message<svg class="btn-icon" viewBox="0 0 24 24"><span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true"></span><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>
						<button type="reset" class="btn btn-outline-primary">Start Over<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
					</p>
				</form>
			</div>
		</div>
	</div>
</main>

<iframe id="contact-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.4327236184195!2d85.3465611154154!3d27.703922732248564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198820904e8b%3A0xf0ee5bfcde89041b!2sAnalogue%20Inc!5e0!3m2!1sen!2snp!4v1586708668950!5m2!1sen!2snp" width="100%" height="480" frameborder="0" style="border:0;vertical-align:top;" allowfullscreen="" aria-hidden="false" tabindex="0" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart" data-aos-mirror="true"></iframe>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<script>
    $(document).ready(function () {
        function get_baseurl() {
            var base_url = "<?php echo URL::to('/') ?>";
            return base_url;
        }

        $('#spinner').hide();

        $('#contact_form').on('submit',function(e){
        e.preventDefault();

            var full_name = $("#full_name").val();
            var phone = $("#phone").val();
            var email = $("#email").val();
            var subject = $("#subject").val();
            var message = $("#message").val();
            var base_url = get_baseurl();
            toastr.options.timeOut = 1000;

            $.ajax({
                url: base_url + '/api/contact/create',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    "_token" : "{{ csrf_token() }}",
                    "full_name": full_name,
                    "phone": phone,
                    "email": email,
                    "subject": subject,
                    "message": message,
                },

                beforeSend: function(){ 
                	$("#spinner").show();
	            },

	            complete: function(){ 
	                $("#spinner").hide();
	            },
                success:function(data){
                    toastr.success('Sent Successfully');
                    window.setTimeout(function () { 
                        window.location.href = base_url +'/contact';
                    }, 2000 );
                } 

            });
        });
    });

</script>

@endsection