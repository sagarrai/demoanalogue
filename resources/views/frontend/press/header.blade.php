<b class="screen-overlay"></b>
<div id="sticky-header-holder" class="fixed-top">
    <div class="services-top">
        <div class="links">
            <a href="https://bm.analogueinc.com.np/">Branding & Marketing</a>
            <a href="https://it.analogueinc.com.np/">IT Solutions</a>
            <a href="https://ecommerce.analogueinc.com.np/">E-Commerce</a>
            <a href="https://computers.analogueinc.com.np/">Computers</a>
            <a href="https://surveillance.analogueinc.com.np/">Security & Surveillance</a>
            <a class="active" href="https://press.analogueinc.com.np/">Analogue Press</a>
            <a href="https://demo.analogueinc.com.np/services.php">View all Services</a>
        </div>
        <div class="dropdown">
            <button class="btn btn-light btn-sm dropdown-toggle m-1" type="button" id="servicedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="btn-icon ml-0 mr-2" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg>Navigate to Services</button>
            <div class="dropdown-menu" aria-labelledby="servicedropdown">
                <a class="dropdown-item" href="https://bm.analogueinc.com.np/">Branding & Marketing</a>
                <a class="dropdown-item" href="https://it.analogueinc.com.np/">IT Solutions</a>
                <a class="dropdown-item" href="https://ecommerce.analogueinc.com.np/">E-Commerce</a>
                <a class="dropdown-item" href="https://computers.analogueinc.com.np/">Computers</a>
                <a class="dropdown-item" href="https://surveillance.analogueinc.com.np/">Security & Surveillance</a>
                <a class="dropdown-item" href="https://press.analogueinc.com.np/">Analogue Press</a>
                <a class="dropdown-item" href="https://demo.analogueinc.com.np/services.php">View all Services</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="logo"><a href="index.php"><img src="frontend/press/images/logo.png" alt=""></a></div>

        <button class="btn btn-outline-secondary btn-sm ml-auto d-xl-none" type="button" data-toggle="collapse" data-trigger="#sticky-header">Menu</button>

        <nav id="sticky-header" class="mobile-offcanvas navbar-expand-xl navbar-light bg-white">
            <div class="offcanvas-header">
                <button class="btn btn-link btn-close float-left mr-2"><svg class="btn-icon ml-0" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></button>
                <h6>Navigate</h6>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item"><a href="index.php" class="scrollto nav-link">Home</a></li>
                <li class="nav-item"><a href="#printing" class="scrollto nav-link">Printing</a></li>
                <li class="nav-item"><a href="#merchandise" class="scrollto nav-link">Merchandise</a></li>
                <li class="nav-item"><a href="{{ route('press_tailor_made') }}" class="nav-link">Tailor Made</a></li>
                <li class="nav-item"><a href="#contact" class="scrollto nav-link">Contact</a></li>
            </ul>
        </nav>
    </div>
</div>
