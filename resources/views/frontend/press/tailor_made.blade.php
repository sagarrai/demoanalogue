<!DOCTYPE html>
<html lang="en">
<head>
    <title>Analogue Inc - Tailor Made</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="frontend/images/favicon.png" rel="icon">
    <link rel="stylesheet" href="frontend/css/main.css">
    <link rel="stylesheet" href="frontend/css/main-inner.css">
    <link rel="stylesheet" href="frontend/css/lightbox.min.css">
    <!-- Begin Sendinblue Form -->
    <!-- START - We recommend to place the below code in head tag of your website html  -->
    <style>
        @font-face {
            font-display: block;
            font-family: Roboto;
            src: url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/7529907e9eaf8ebb5220c5f9850e3811.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/25c678feafdc175a70922a116c9be3e7.woff) format("woff")
        }

        @font-face {
            font-display: fallback;
            font-family: Roboto;
            font-weight: 600;
            src: url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/6e9caeeafb1f3491be3e32744bc30440.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/71501f0d8d5aa95960f6475d5487d4c2.woff) format("woff")
        }

        @font-face {
            font-display: fallback;
            font-family: Roboto;
            font-weight: 700;
            src: url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/3ef7cf158f310cf752d5ad08cd0e7e60.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/ece3a1d82f18b60bcce0211725c476aa.woff) format("woff")
        }

        #sib-container input:-ms-input-placeholder {
            text-align: left;
            font-family: "Helvetica", sans-serif;
            color: #c0ccda;
            border-width: px;
        }

        #sib-container input::placeholder {
            text-align: left;
            font-family: "Helvetica", sans-serif;
            color: #c0ccda;
            border-width: px;
        }

        #sib-container textarea::placeholder {
            text-align: left;
            font-family: "Helvetica", sans-serif;
            color: #c0ccda;
            border-width: px;
        }
    </style>
    <link rel="stylesheet" href="https://sibforms.com/forms/end-form/build/sib-styles.css">
    <!--  END - We recommend to place the above code in head tag of your website html -->

</head>
<body>
    <div id="wrapper">
        <b class="screen-overlay"></b>
        <div id="sticky-header-holder" class="fixed-top">
            <div class="services-top">
                <div class="links">
                    <a href="https://bm.analogueinc.com.np/">Branding & Marketing</a>
                    <a href="https://it.analogueinc.com.np/">IT Solutions</a>
                    <a href="https://ecommerce.analogueinc.com.np/">E-Commerce</a>
                    <a href="https://computers.analogueinc.com.np/">Computers</a>
                    <a href="https://surveillance.analogueinc.com.np/">Security & Surveillance</a>
                    <a class="active" href="index.php">Analogue Press</a>
                    <a href="https://demo.analogueinc.com.np/services.php">View all Services</a>
                </div>
                <div class="dropdown">
                    <button class="btn btn-light btn-sm dropdown-toggle m-1" type="button" id="servicedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="btn-icon ml-0 mr-2" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg>Navigate to Services</button>
                    <div class="dropdown-menu" aria-labelledby="servicedropdown">
                        <a class="dropdown-item" href="https://bm.analogueinc.com.np/">Branding & Marketing</a>
                        <a class="dropdown-item" href="https://it.analogueinc.com.np/">IT Solutions</a>
                        <a class="dropdown-item" href="https://ecommerce.analogueinc.com.np/">E-Commerce</a>
                        <a class="dropdown-item" href="https://computers.analogueinc.com.np/">Computers</a>
                        <a class="dropdown-item" href="https://surveillance.analogueinc.com.np/">Security & Surveillance</a>
                        <a class="dropdown-item" href="index.php">Analogue Press</a>
                        <a class="dropdown-item" href="https://demo.analogueinc.com.np/services.php">View all Services</a>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="logo"><a href="index.php"><img src="frontend/images/logo.png" alt=""></a></div>

                <button class="btn btn-outline-secondary btn-sm ml-auto d-xl-none" type="button" data-toggle="collapse" data-trigger="#sticky-header">Menu</button>

                <nav id="sticky-header" class="mobile-offcanvas navbar-expand-xl navbar-light bg-white">
                    <div class="offcanvas-header">  
                        <button class="btn btn-link btn-close float-left mr-2"><svg class="btn-icon ml-0" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></button>
                        <h6>Navigate</h6>
                    </div>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="index.php" class="scrollto nav-link">Home</a></li>
                        <li class="nav-item"><a href="#printing" class="scrollto nav-link">Printing</a></li>
                        <li class="nav-item"><a href="#merchandise" class="scrollto nav-link">Merchandise</a></li>
                        <li class="nav-item"><a href="tailor-made.php" class="nav-link">Tailor Made</a></li>
                        <li class="nav-item"><a href="#contact" class="scrollto nav-link">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <section class="section ai-s-section">
            <div class="container pt-lg-8 pt-md-7 pt-8">
                <div class="section-header text-center pb-half">
                    <p class="seo-title">Our Services</p>
                    <h3 class="section-heading">Business Stationaries</h3>
                    <p class="sub-title">Also known as your brand kit.</p>
                </div>
                <div class="row justify-content-center aos-init" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart">
                    <div class="col-xl-8 col-lg-9 col-md-10 col-12">
                        <h5 class="subsection-ftr-title text-center">Make your own package!</h5>
                        <div class="row justify-content-center text-center">
                            <div class="col-12">
                                <p class="subsection-ftr-lead">You can choose what you need and what you don't need from what we offer, and create your own package based on your choices. The final price will be calculated accordingly.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-lg">
                <form>
                    <div class="row justify-content-center my-6 ai-s-ckbx-blk">
                        <div class="col-xxl-9 col-md-7 col-sm-12">
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Flyers</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" status="0" id="press_flyers">
                                                    <label class="form-check-label" for="inlineRadio1">Flyers</label>
                                                </div>
                                                <!-- <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="press_leaflet">
                                                    <label class="form-check-label" for="inlineRadio2">Leaflet</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="press_flyers_leaflet" >
                                                    <label class="form-check-label" for="inlineRadio3">Both</label>
                                                </div> -->
                                            </div>
                                        </div>
                                        <input type="number" class="form-control" id="flyers_quantity" min="0" vaule="0" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Leaflet</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="press_leaflet">
                                                    <label class="form-check-label" for="inlineRadio2">Leaflet</label>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="number" class="form-control" id="leaflet_quantity" min="0" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Brochures</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_brochure" value="0">
                                                <label class="form-check-label" for="checkbox_brochure">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <select id="brochure_pages" class="form-control">
                                            <option value="">Select pages</option>
                                            <option value="Two Fold">Two fold</option>
                                            <option value="Three Fold">Three fold</option>
                                            <option value="Four Fold">Four fold</option>
                                        </select>
                                        <input type="number" id="brochures_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Business profiles</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_business">
                                                <label class="form-check-label" for="checkbox_bizprofile">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <select id="business_pages" class="form-control">
                                            <option value="">Select pages</option>
                                            <option value="One Fold">One</option>
                                            <option value="Two Fold">Two</option>
                                            <option value="Three Fold">Three</option>
                                        </select>
                                        <input type="number" min="0" class="form-control" id="business_quantity" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Banners</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_banner" value="Banners">
                                                <label class="form-check-label" for="checkbox_banner">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" min="0" id="banner_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Business Cards</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_business_card" value="Business Cards">
                                                <label class="form-check-label" for="checkbox_bizcard">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <select id="business_card_sides" class="form-control">
                                            <option value="">Select sides</option>
                                            <option value="Single Side">Single side</option>
                                            <option value="Double Sides">Double side</option>
                                        </select>
                                        <input type="number" class="form-control" id="business_card_quantity" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Stickers</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_sticker" value="Stickers">
                                                <label class="form-check-label" for="checkbox_sticker">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="sticker_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Cover letter</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_cover_letter">
                                                <label class="form-check-label" for="checkbox_coverletter">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="cover_letter_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Letterhead</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_letterhead">
                                                <label class="form-check-label" for="checkbox_letterhead">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="letterhead_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Envelope</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_envelope">
                                                <label class="form-check-label" for="checkbox_envelope">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="envelope_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">ID Cards</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_idcard" value="ID Card">
                                                <label class="form-check-label" for="checkbox_idcard">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="idcard_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Notebook</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_notebook" value="Notebook">
                                                <label class="form-check-label" for="checkbox_notebook">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="notebook_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Calendars</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3 flex-lg-row flex-column">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_calendar">
                                                <label class="form-check-label" for="checkbox_calendar">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="calendar_quantity" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="web-textarea" class="text-dark">Did we miss anything? Are you looking for something more? <small>(optional)</small></label>
                                <textarea class="form-control" id="other_message" rows="8" placeholder="What more would you like?"></textarea>
                            </div>
                            <p class="mt-4">
                                <button type="button" class="btn btn-primary" id="press_tailor_create" data-toggle="modal" data-target="#pkg-modal-web4">Create My Package</button>
                                <button type="reset" class="btn btn-outline-primary ml-3">Start Over</button>
                            </p>
                        </div>

                        <div class="col-xxl-3 col-md-5 col-sm-12 mt-sm-0 mt-6">
                            <section class="ai-price">
                                <header class="ai-price-h">
                                    <h4 class="ai-price-h-title">Tailor Made</h4>
                                    <p class="ai-price-h-subtitle">One Time Payment</p>
                                    <!-- <p class="ai-price-h-prc">
                                        <span class="ai-price-h-prc-pfx">Rs.</span>
                                        <span class="ai-price-h-prc-amt">25K</span>
                                        <span class="ai-price-h-prc-sfx">per item</span>
                                    </p> -->
                                </header>
                                <div class="ai-price-c">
                                    <p class="ai-price-c-lead">What you've selected -</p>
                                    <ul class="ai-price-c-list" id="select_package_lists">
                                        <li style="display:none" id="tailor_press_flyers">Flyers <span id="qty1">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_leaflet">Leaflet <span id="qty2">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_brochures">Brochures <span id="page1">0 </span>  <span id="qty3">0 </span> pcs </li>
                                        <li style="display:none" id="tailor_press_business">Business profiles <span id="page2">0 </span> <span id="qty4">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_banner">Banners <span id="qty5">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_businesscard">Business Cards <span id="page3">0 </span> <span id="qty6">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_sticker">Sticker <span id="qty7">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_cover_letter">Cover letter <span id="qty8">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_letterhead">Letterhead <span id="qty9">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_envelope">Envelope <span id="qty10">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_idcard">ID cards <span id="qty11">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_notebook">Notebook <span id="qty12">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_calendar">Calendars <span id="qty13">0 </span> pcs</li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <section class="section ai-s-section mt-5 mb-5">
            <div class="container">
                <div class="section-header text-center pb-half">
                    <p class="seo-title">Our Services</p>
                    <h3 class="section-heading">Merchandising Pack</h3>
                    <p class="sub-title">Best way for branding.</p>
                </div>
                <div class="row justify-content-center aos-init" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart">
                    <div class="col-xl-8 col-lg-9 col-md-10 col-12">
                        <h5 class="subsection-ftr-title text-center">Make your own package!</h5>
                        <div class="row justify-content-center text-center">
                            <div class="col-12">
                                <p class="subsection-ftr-lead">You can choose what you need and what you don't need from what we offer, and create your own package based on your choices. The final price will be calculated accordingly.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-lg">
                <form>
                    <div class="row justify-content-center my-5 ai-s-ckbx-blk">
                        <div class="col-xxl-9 col-md-8 col-sm-12">
                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">T-Shirts</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_tshirt">
                                                <label class="form-check-label" for="checkbox_tshirt">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="tshirt_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Bags</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_bag">
                                                <label class="form-check-label" for="checkbox_bag">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="bag_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Cups</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_cup">
                                                <label class="form-check-label" for="checkbox_cups">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="cup_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Pens & Pencils</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_penpencil">
                                                <label class="form-check-label" for="checkbox_pens">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="penpencil_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">To-Go Cups</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_togocup">
                                                <label class="form-check-label" for="checkbox_togocups">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="togocup_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Packing Bags</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_packingbag">
                                                <label class="form-check-label" for="checkbox_packingbag">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="packingbag_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Cardboard Boxes</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_cardboard">
                                                <label class="form-check-label" for="checkbox_cardboardbox">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="cardboard_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Coasters</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_coaster">
                                                <label class="form-check-label" for="checkbox_coaster">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="coaster_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Badges</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_badge">
                                                <label class="form-check-label" for="checkbox_badges">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="badge_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mt-5">
                                <label for="web-pages" class="col-lg-2 col-md-12 col-sm-12 col-form-label text-dark">Shopping Bags</label>
                                <div class="col-lg-10 col-md-12 col-sm-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="press_shoppingbag">
                                                <label class="form-check-label" for="checkbox_shoppingbag">Yes</label>
                                              </div>
                                            </div>
                                        </div>
                                        <input type="number" id="shoppingbag_quantity" min="0" class="form-control" placeholder="How many in quantity?">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="web-textarea" class="text-dark">Did we miss anything? Are you looking for something more? <small>(optional)</small></label>
                                <textarea class="form-control" id="other_message2" rows="8" placeholder="What more would you like?"></textarea>
                            </div>
                            <p class="mt-4">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pkg-modal-web">Create My Package</button>
                                <button type="reset" class="btn btn-outline-primary ml-3">Start Over</button>
                            </p>
                        </div>

                        <div class="col-xxl-3 col-md-4 col-sm-12 mt-sm-0 mt-6">
                            <section class="ai-price">
                                <header class="ai-price-h">
                                    <h4 class="ai-price-h-title">Tailor Made</h4>
                                    <p class="ai-price-h-subtitle">One Time Payment</p>
                                    <!-- <p class="ai-price-h-prc">
                                        <span class="ai-price-h-prc-pfx">Rs.</span>
                                        <span class="ai-price-h-prc-amt">25K</span>
                                        <span class="ai-price-h-prc-sfx">per item</span>
                                    </p> -->
                                </header>
                                <div class="ai-price-c">
                                    <p class="ai-price-c-lead">What you've selected -</p>
                                    <ul class="ai-price-c-list" id="select_package_lists2">
                                        <li style="display:none" id="tailor_press_tshirt">T-shirts <span id="qty14">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_bag">Bags  <span id="qty15">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_cup">Cups <span id="qty16">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_penpencil">Pens & Pencils <span id="qty17">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_togocup">To-Go Cups <span id="qty18">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_packingbag">Packing Bags <span id="qty19">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_cardboard">Cardboard Boxes <span id="qty20">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_coaster">Coasters <span id="qty21">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_badge">Badges <span id="qty22">0 </span> pcs</li>
                                        <li style="display:none" id="tailor_press_shoppingbag">Shopping Bags <span id="qty23">0 </span> pcs</li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <section class="section ai-s-section" id="printing">
            <div class="container">
                <div class="section-header text-center pb-half">
                    <h3 class="section-heading">Business Stationaries aka your brand kit.</h3>
                </div>
                
                    <div class="row justify-content-center res-col">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala1">
                                <span>Flyers</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala2">
                                <span>Leaflets</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala3">
                                <span>Brochures</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala4">
                                <span>Business Profiles</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala5">
                                <span>Banners</span>
                            </a>
                        </div>
                        <div class="w-100 d-xl-block d-none"></div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala6">
                                <span>Business Cards</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala7">
                                <span>Stickers</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala8">
                                <span>Cover Letters</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala9">
                                <span>Letterhead</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala10">
                                <span>Envelope</span>
                            </a>
                        </div>
                        <div class="w-100 d-xl-block d-none"></div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala11">
                                <span>ID Cards</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala12">
                                <span>Notebook</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala13">
                                <span>Calendars</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala14">
                                <span>Magazines</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala15">
                                <span>Menu Cards</span>
                            </a>
                        </div>
                        <div class="w-100 d-xl-block d-none"></div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala16">
                                <span>Book Covers</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala17">
                                <span>Invoices</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala18">
                                <span>Receipts</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modala19">
                                <span>Signage</span>
                            </a>
                        </div>
                    </div>
            </div>
        </section>

        <section class="section ai-s-section" id="merchandise">
            <div class="container">
                <div class="section-header text-center pb-half">
                    <h3 class="section-heading">Merchandise your everyday items.</h3>
                </div>
                
                    <div class="row justify-content-center res-col">
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb1">
                                <span>T-Shirts</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb2">
                                <span>Bags</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb3">
                                <span>Cups</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb4">
                                <span>Pens & Pencils</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb5">
                                <span>To-Go Cups</span>
                            </a>
                        </div>
                        <div class="w-100 d-xl-block d-none"></div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb6">
                                <span>Packing Bags</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb7">
                                <span>Cardboard Boxes</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb8">
                                <span>Coasters</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb9">
                                <span>Badges</span>
                            </a>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-9 mt-4">
                            <a href="" class="ai-card ai-card-sm text-center" data-toggle="modal" data-target="#modalb10">
                                <span>Shopping Bags</span>
                            </a>
                        </div>
                    </div>
            </div>
        </section>

        <section id="contact" class="section ai-s-section background-light">
            <div class="container">
            <div class="section-header text-center pb-half">
            <p class="seo-title">Contact Us</p>
            <h3 class="section-heading">Got An Idea?</h3>
            </div>
            <!-- START - We recommend to place the below code where you want the form in your website html  -->
            <div class="sib-form" style="text-align: center; background-color: transparent;">
              <div id="sib-form-container" class="sib-form-container">
                <div id="error-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;max-width:540px; border-width:px;">
                  <div class="sib-form-message-panel__text sib-form-message-panel__text--center">
                    <svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
                      <path d="M256 40c118.621 0 216 96.075 216 216 0 119.291-96.61 216-216 216-119.244 0-216-96.562-216-216 0-119.203 96.602-216 216-216m0-32C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm-11.49 120h22.979c6.823 0 12.274 5.682 11.99 12.5l-7 168c-.268 6.428-5.556 11.5-11.99 11.5h-8.979c-6.433 0-11.722-5.073-11.99-11.5l-7-168c-.283-6.818 5.167-12.5 11.99-12.5zM256 340c-15.464 0-28 12.536-28 28s12.536 28 28 28 28-12.536 28-28-12.536-28-28-28z"
                      />
                    </svg>
                    <span class="sib-form-message-panel__inner-text">
                                      Your message could not be sent. Please try again.
                                  </span>
                  </div>
                </div>
                <div></div>
                <div id="success-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#085229; background-color:#e7faf0; border-radius:3px; border-width:px; border-color:#13ce66;max-width:540px; border-width:px;">
                  <div class="sib-form-message-panel__text sib-form-message-panel__text--center">
                    <svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
                      <path d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 464c-118.664 0-216-96.055-216-216 0-118.663 96.055-216 216-216 118.664 0 216 96.055 216 216 0 118.663-96.055 216-216 216zm141.63-274.961L217.15 376.071c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.97.068l68.976 69.533 163.441-162.13c4.705-4.667 12.303-4.637 16.97.068l8.451 8.52c4.668 4.705 4.637 12.303-.068 16.97z"
                      />
                    </svg>
                    <span class="sib-form-message-panel__inner-text">
                                      Thank you for your message. We&#039;ll get back to you shortly.
                                  </span>
                  </div>
                </div>
                <div></div>
                <div id="sib-container" class="sib-container--large sib-container--vertical" style="text-align:center; background-color:transparent; max-width:540px; border-width:0px; border-color:#C0CCD9; border-style:solid;">
                  <form id="sib-form" method="POST" action="https://486ae310.sibforms.com/serve/MUIEAAXgkS5AdDaUZLdNqKAx4h2mpjWolwMmKTUTM_py4GcPRdAtHAiseUQPkRqENkJSS8oGpR7bYbhxUA7rA7JyVsD72qjSq5RZKr4p-d1BCmRyLpne_SZCOrJNtFyDv4X7IgGJixkT6Pawvs313bcb8HWWOPCe9Ziz8d3ttsuhCT6AyoOCkdk78u0GIXmis6nb-zaOH6G_OIJ3"
                    data-type="subscription">
                    <div style="padding: 8px 0;">
                      <div class="sib-input sib-form-block">
                        <div class="form__entry entry_block">
                          <div class="form__label-row ">
                            <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" data-required="*">
                              Your Full Name
                            </label>

                            <div class="entry__field">
                              <input class="input" maxlength="200" type="text" autocomplete="off" data-required="true" required />
                            </div>
                          </div>

                          <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div style="padding: 8px 0;">
                      <div class="sib-input sib-form-block">
                        <div class="form__entry entry_block">
                          <div class="form__label-row ">
                            <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="EMAIL" data-required="*">
                              Your Email Address
                            </label>

                            <div class="entry__field">
                              <input class="input" type="text" autocomplete="off" data-required="true" required />
                            </div>
                          </div>

                          <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div style="padding: 8px 0;">
                      <div class="sib-sms-field sib-form-block">
                        <div class="form__entry entry_block">
                          <div class="form__label-row ">
                            <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="SMS" data-required="*">
                              Your Phone Number
                            </label>

                            <div class="sib-sms-input-wrapper">
                              <div class="sib-sms-input" data-placeholder="Phone Number" data-required="1" data-country-code="NP" data-value="">
                                <div class="entry__field">
                                  <select class="input" name="SMS__COUNTRY_CODE" data-required="true">
                                    <option value="+93">
                                      +93 AF
                                    </option>
                                    <option value="+358">
                                      +358 AX
                                    </option>
                                    <option value="+355">
                                      +355 AL
                                    </option>
                                    <option value="+213">
                                      +213 DZ
                                    </option>
                                    <option value="+1684">
                                      +1684 AS
                                    </option>
                                    <option value="+376">
                                      +376 AD
                                    </option>
                                    <option value="+244">
                                      +244 AO
                                    </option>
                                    <option value="+1264">
                                      +1264 AI
                                    </option>
                                    <option value="+672">
                                      +672 AQ
                                    </option>
                                    <option value="+1268">
                                      +1268 AG
                                    </option>
                                    <option value="+54">
                                      +54 AR
                                    </option>
                                    <option value="+374">
                                      +374 AM
                                    </option>
                                    <option value="+297">
                                      +297 AW
                                    </option>
                                    <option value="+61">
                                      +61 AU
                                    </option>
                                    <option value="+43">
                                      +43 AT
                                    </option>
                                    <option value="+994">
                                      +994 AZ
                                    </option>
                                    <option value="+1242">
                                      +1242 BS
                                    </option>
                                    <option value="+973">
                                      +973 BH
                                    </option>
                                    <option value="+880">
                                      +880 BD
                                    </option>
                                    <option value="+1246">
                                      +1246 BB
                                    </option>
                                    <option value="+375">
                                      +375 BY
                                    </option>
                                    <option value="+32">
                                      +32 BE
                                    </option>
                                    <option value="+501">
                                      +501 BZ
                                    </option>
                                    <option value="+229">
                                      +229 BJ
                                    </option>
                                    <option value="+1441">
                                      +1441 BM
                                    </option>
                                    <option value="+975">
                                      +975 BT
                                    </option>
                                    <option value="+591">
                                      +591 BO
                                    </option>
                                    <option value="+599">
                                      +599 BQ
                                    </option>
                                    <option value="+387">
                                      +387 BA
                                    </option>
                                    <option value="+267">
                                      +267 BW
                                    </option>
                                    <option value="+47">
                                      +47 BV
                                    </option>
                                    <option value="+55">
                                      +55 BR
                                    </option>
                                    <option value="+246">
                                      +246 IO
                                    </option>
                                    <option value="+673">
                                      +673 BN
                                    </option>
                                    <option value="+359">
                                      +359 BG
                                    </option>
                                    <option value="+226">
                                      +226 BF
                                    </option>
                                    <option value="+257">
                                      +257 BI
                                    </option>
                                    <option value="+855">
                                      +855 KH
                                    </option>
                                    <option value="+237">
                                      +237 CM
                                    </option>
                                    <option value="+1">
                                      +1 CA
                                    </option>
                                    <option value="+238">
                                      +238 CV
                                    </option>
                                    <option value="+1345">
                                      +1345 KY
                                    </option>
                                    <option value="+236">
                                      +236 CF
                                    </option>
                                    <option value="+235">
                                      +235 TD
                                    </option>
                                    <option value="+56">
                                      +56 CL
                                    </option>
                                    <option value="+86">
                                      +86 CN
                                    </option>
                                    <option value="+61">
                                      +61 CX
                                    </option>
                                    <option value="+61">
                                      +61 CC
                                    </option>
                                    <option value="+57">
                                      +57 CO
                                    </option>
                                    <option value="+269">
                                      +269 KM
                                    </option>
                                    <option value="+242">
                                      +242 CG
                                    </option>
                                    <option value="+243">
                                      +243 CD
                                    </option>
                                    <option value="+682">
                                      +682 CK
                                    </option>
                                    <option value="+506">
                                      +506 CR
                                    </option>
                                    <option value="+225">
                                      +225 CI
                                    </option>
                                    <option value="+385">
                                      +385 HR
                                    </option>
                                    <option value="+53">
                                      +53 CU
                                    </option>
                                    <option value="+599">
                                      +599 CW
                                    </option>
                                    <option value="+357">
                                      +357 CY
                                    </option>
                                    <option value="+420">
                                      +420 CZ
                                    </option>
                                    <option value="+45">
                                      +45 DK
                                    </option>
                                    <option value="+253">
                                      +253 DJ
                                    </option>
                                    <option value="+1767">
                                      +1767 DM
                                    </option>
                                    <option value="+1809">
                                      +1809 DO
                                    </option>
                                    <option value="+593">
                                      +593 EC
                                    </option>
                                    <option value="+20">
                                      +20 EG
                                    </option>
                                    <option value="+503">
                                      +503 SV
                                    </option>
                                    <option value="+240">
                                      +240 GQ
                                    </option>
                                    <option value="+291">
                                      +291 ER
                                    </option>
                                    <option value="+372">
                                      +372 EE
                                    </option>
                                    <option value="+251">
                                      +251 ET
                                    </option>
                                    <option value="+500">
                                      +500 FK
                                    </option>
                                    <option value="+298">
                                      +298 FO
                                    </option>
                                    <option value="+679">
                                      +679 FJ
                                    </option>
                                    <option value="+358">
                                      +358 FI
                                    </option>
                                    <option value="+33">
                                      +33 FR
                                    </option>
                                    <option value="+594">
                                      +594 GF
                                    </option>
                                    <option value="+689">
                                      +689 PF
                                    </option>
                                    <option value="+262">
                                      +262 TF
                                    </option>
                                    <option value="+241">
                                      +241 GA
                                    </option>
                                    <option value="+220">
                                      +220 GM
                                    </option>
                                    <option value="+995">
                                      +995 GE
                                    </option>
                                    <option value="+49">
                                      +49 DE
                                    </option>
                                    <option value="+233">
                                      +233 GH
                                    </option>
                                    <option value="+350">
                                      +350 GI
                                    </option>
                                    <option value="+30">
                                      +30 GR
                                    </option>
                                    <option value="+299">
                                      +299 GL
                                    </option>
                                    <option value="+1473">
                                      +1473 GD
                                    </option>
                                    <option value="+590">
                                      +590 GP
                                    </option>
                                    <option value="+1671">
                                      +1671 GU
                                    </option>
                                    <option value="+502">
                                      +502 GT
                                    </option>
                                    <option value="+44">
                                      +44 GG
                                    </option>
                                    <option value="+224">
                                      +224 GN
                                    </option>
                                    <option value="+245">
                                      +245 GW
                                    </option>
                                    <option value="+592">
                                      +592 GY
                                    </option>
                                    <option value="+509">
                                      +509 HT
                                    </option>
                                    <option value="+672">
                                      +672 HM
                                    </option>
                                    <option value="+379">
                                      +379 VA
                                    </option>
                                    <option value="+504">
                                      +504 HN
                                    </option>
                                    <option value="+852">
                                      +852 HK
                                    </option>
                                    <option value="+36">
                                      +36 HU
                                    </option>
                                    <option value="+354">
                                      +354 IS
                                    </option>
                                    <option value="+91">
                                      +91 IN
                                    </option>
                                    <option value="+62">
                                      +62 ID
                                    </option>
                                    <option value="+98">
                                      +98 IR
                                    </option>
                                    <option value="+964">
                                      +964 IQ
                                    </option>
                                    <option value="+353">
                                      +353 IE
                                    </option>
                                    <option value="+44">
                                      +44 IM
                                    </option>
                                    <option value="+972">
                                      +972 IL
                                    </option>
                                    <option value="+39">
                                      +39 IT
                                    </option>
                                    <option value="+1876">
                                      +1876 JM
                                    </option>
                                    <option value="+81">
                                      +81 JP
                                    </option>
                                    <option value="+44">
                                      +44 JE
                                    </option>
                                    <option value="+962">
                                      +962 JO
                                    </option>
                                    <option value="+7">
                                      +7 KZ
                                    </option>
                                    <option value="+254">
                                      +254 KE
                                    </option>
                                    <option value="+686">
                                      +686 KI
                                    </option>
                                    <option value="+850">
                                      +850 KP
                                    </option>
                                    <option value="+82">
                                      +82 KR
                                    </option>
                                    <option value="+965">
                                      +965 KW
                                    </option>
                                    <option value="+996">
                                      +996 KG
                                    </option>
                                    <option value="+856">
                                      +856 LA
                                    </option>
                                    <option value="+371">
                                      +371 LV
                                    </option>
                                    <option value="+961">
                                      +961 LB
                                    </option>
                                    <option value="+266">
                                      +266 LS
                                    </option>
                                    <option value="+231">
                                      +231 LR
                                    </option>
                                    <option value="+218">
                                      +218 LY
                                    </option>
                                    <option value="+423">
                                      +423 LI
                                    </option>
                                    <option value="+370">
                                      +370 LT
                                    </option>
                                    <option value="+352">
                                      +352 LU
                                    </option>
                                    <option value="+853">
                                      +853 MO
                                    </option>
                                    <option value="+389">
                                      +389 MK
                                    </option>
                                    <option value="+261">
                                      +261 MG
                                    </option>
                                    <option value="+265">
                                      +265 MW
                                    </option>
                                    <option value="+60">
                                      +60 MY
                                    </option>
                                    <option value="+960">
                                      +960 MV
                                    </option>
                                    <option value="+223">
                                      +223 ML
                                    </option>
                                    <option value="+356">
                                      +356 MT
                                    </option>
                                    <option value="+692">
                                      +692 MH
                                    </option>
                                    <option value="+596">
                                      +596 MQ
                                    </option>
                                    <option value="+222">
                                      +222 MR
                                    </option>
                                    <option value="+230">
                                      +230 MU
                                    </option>
                                    <option value="+262">
                                      +262 YT
                                    </option>
                                    <option value="+52">
                                      +52 MX
                                    </option>
                                    <option value="+691">
                                      +691 FM
                                    </option>
                                    <option value="+373">
                                      +373 MD
                                    </option>
                                    <option value="+377">
                                      +377 MC
                                    </option>
                                    <option value="+976">
                                      +976 MN
                                    </option>
                                    <option value="+382">
                                      +382 ME
                                    </option>
                                    <option value="+1664">
                                      +1664 MS
                                    </option>
                                    <option value="+212">
                                      +212 MA
                                    </option>
                                    <option value="+258">
                                      +258 MZ
                                    </option>
                                    <option value="+95">
                                      +95 MM
                                    </option>
                                    <option value="+264">
                                      +264 NA
                                    </option>
                                    <option value="+674">
                                      +674 NR
                                    </option>
                                    <option value="+977">
                                      +977 NP
                                    </option>
                                    <option value="+31">
                                      +31 NL
                                    </option>
                                    <option value="+687">
                                      +687 NC
                                    </option>
                                    <option value="+64">
                                      +64 NZ
                                    </option>
                                    <option value="+505">
                                      +505 NI
                                    </option>
                                    <option value="+227">
                                      +227 NE
                                    </option>
                                    <option value="+234">
                                      +234 NG
                                    </option>
                                    <option value="+683">
                                      +683 NU
                                    </option>
                                    <option value="+672">
                                      +672 NF
                                    </option>
                                    <option value="+1670">
                                      +1670 MP
                                    </option>
                                    <option value="+47">
                                      +47 NO
                                    </option>
                                    <option value="+968">
                                      +968 OM
                                    </option>
                                    <option value="+92">
                                      +92 PK
                                    </option>
                                    <option value="+680">
                                      +680 PW
                                    </option>
                                    <option value="+970">
                                      +970 PS
                                    </option>
                                    <option value="+507">
                                      +507 PA
                                    </option>
                                    <option value="+675">
                                      +675 PG
                                    </option>
                                    <option value="+595">
                                      +595 PY
                                    </option>
                                    <option value="+51">
                                      +51 PE
                                    </option>
                                    <option value="+63">
                                      +63 PH
                                    </option>
                                    <option value="+64">
                                      +64 PN
                                    </option>
                                    <option value="+48">
                                      +48 PL
                                    </option>
                                    <option value="+351">
                                      +351 PT
                                    </option>
                                    <option value="+1787">
                                      +1787 PR
                                    </option>
                                    <option value="+974">
                                      +974 QA
                                    </option>
                                    <option value="+383">
                                      +383 XK
                                    </option>
                                    <option value="+262">
                                      +262 RE
                                    </option>
                                    <option value="+40">
                                      +40 RO
                                    </option>
                                    <option value="+7">
                                      +7 RU
                                    </option>
                                    <option value="+250">
                                      +250 RW
                                    </option>
                                    <option value="+590">
                                      +590 BL
                                    </option>
                                    <option value="+290">
                                      +290 SH
                                    </option>
                                    <option value="+1869">
                                      +1869 KN
                                    </option>
                                    <option value="+1758">
                                      +1758 LC
                                    </option>
                                    <option value="+599">
                                      +599 MF
                                    </option>
                                    <option value="+508">
                                      +508 PM
                                    </option>
                                    <option value="+1784">
                                      +1784 VC
                                    </option>
                                    <option value="+685">
                                      +685 WS
                                    </option>
                                    <option value="+378">
                                      +378 SM
                                    </option>
                                    <option value="+239">
                                      +239 ST
                                    </option>
                                    <option value="+966">
                                      +966 SA
                                    </option>
                                    <option value="+221">
                                      +221 SN
                                    </option>
                                    <option value="+381">
                                      +381 RS
                                    </option>
                                    <option value="+248">
                                      +248 SC
                                    </option>
                                    <option value="+232">
                                      +232 SL
                                    </option>
                                    <option value="+65">
                                      +65 SG
                                    </option>
                                    <option value="+599">
                                      +599 SX
                                    </option>
                                    <option value="+421">
                                      +421 SK
                                    </option>
                                    <option value="+386">
                                      +386 SI
                                    </option>
                                    <option value="+677">
                                      +677 SB
                                    </option>
                                    <option value="+252">
                                      +252 SO
                                    </option>
                                    <option value="+27">
                                      +27 ZA
                                    </option>
                                    <option value="+500">
                                      +500 GS
                                    </option>
                                    <option value="+211">
                                      +211 SS
                                    </option>
                                    <option value="+34">
                                      +34 ES
                                    </option>
                                    <option value="+94">
                                      +94 LK
                                    </option>
                                    <option value="+249">
                                      +249 SD
                                    </option>
                                    <option value="+597">
                                      +597 SR
                                    </option>
                                    <option value="+47">
                                      +47 SJ
                                    </option>
                                    <option value="+268">
                                      +268 SZ
                                    </option>
                                    <option value="+46">
                                      +46 SE
                                    </option>
                                    <option value="+41">
                                      +41 CH
                                    </option>
                                    <option value="+963">
                                      +963 SY
                                    </option>
                                    <option value="+886">
                                      +886 TW
                                    </option>
                                    <option value="+992">
                                      +992 TJ
                                    </option>
                                    <option value="+255">
                                      +255 TZ
                                    </option>
                                    <option value="+66">
                                      +66 TH
                                    </option>
                                    <option value="+670">
                                      +670 TL
                                    </option>
                                    <option value="+228">
                                      +228 TG
                                    </option>
                                    <option value="+690">
                                      +690 TK
                                    </option>
                                    <option value="+676">
                                      +676 TO
                                    </option>
                                    <option value="+1868">
                                      +1868 TT
                                    </option>
                                    <option value="+216">
                                      +216 TN
                                    </option>
                                    <option value="+90">
                                      +90 TR
                                    </option>
                                    <option value="+993">
                                      +993 TM
                                    </option>
                                    <option value="+1649">
                                      +1649 TC
                                    </option>
                                    <option value="+688">
                                      +688 TV
                                    </option>
                                    <option value="+256">
                                      +256 UG
                                    </option>
                                    <option value="+380">
                                      +380 UA
                                    </option>
                                    <option value="+971">
                                      +971 AE
                                    </option>
                                    <option value="+44">
                                      +44 GB
                                    </option>
                                    <option value="+1">
                                      +1 US
                                    </option>
                                    <option value="+246">
                                      +246 UM
                                    </option>
                                    <option value="+598">
                                      +598 UY
                                    </option>
                                    <option value="+998">
                                      +998 UZ
                                    </option>
                                    <option value="+678">
                                      +678 VU
                                    </option>
                                    <option value="+58">
                                      +58 VE
                                    </option>
                                    <option value="+84">
                                      +84 VN
                                    </option>
                                    <option value="+1284">
                                      +1284 VG
                                    </option>
                                    <option value="+1340">
                                      +1340 VI
                                    </option>
                                    <option value="+681">
                                      +681 WF
                                    </option>
                                    <option value="+212">
                                      +212 EH
                                    </option>
                                    <option value="+967">
                                      +967 YE
                                    </option>
                                    <option value="+260">
                                      +260 ZM
                                    </option>
                                    <option value="+263">
                                      +263 ZW
                                    </option>
                                  </select>
                                </div>
                                <div class="entry__field" style="width: 100%">
                                  <input type="tel" type="text" class="input" id="SMS" name="SMS" autocomplete="off" placeholder="Phone Number" data-required="true" required />
                                </div>
                              </div>
                              <div class="sib-sms-tooltip">
                                <div class="sib-sms-tooltip__box">
                                  The SMS field must contain between 6 and 19 digits and include the country code without using +/0 (e.g. 1xxxxxxxxxx for the United States)
                                </div>
                                <span class="sib-sms-tooltip__icon">?</span>
                              </div>
                            </div>
                          </div>

                          <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                          </label>
                          <label class="entry__error entry__error--secondary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div style="padding: 8px 0;">
                      <div class="sib-select sib-form-block" data-required="true">
                        <div class="form__entry entry_block">
                          <div class="form__label-row ">
                            <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="PRESS_LIST" data-required="*">
                              What would you like to work on with us?
                            </label>
                            <div class="entry__field">
                              <select class="input" id="PRESS_LIST" name="PRESS_LIST" data-required="true">
                                <option value="" disabled selected hidden>Select one</option>

                                <option class="sib-menu__item" value="1">
                                  Business Stationeries
                                </option>
                                <option class="sib-menu__item" value="2">
                                  Merchandising
                                </option>
                              </select>
                            </div>
                          </div>
                          <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div style="padding: 8px 0;">
                      <div class="sib-input sib-form-block">
                        <div class="form__entry entry_block">
                          <div class="form__label-row ">
                            <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" data-required="*">
                              Your Message
                            </label>

                            <div class="entry__field">
                              <textarea rows="2" class="input" maxlength="500" autocomplete="off" placeholder="Tell us about your needs..." data-required="true" required></textarea>
                            </div>
                          </div>

                          <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div style="padding: 8px 0;">
                      <div class="sib-captcha sib-form-block">
                        <div class="form__entry entry_block">
                          <div class="form__label-row ">
                            <script>
                              function handleCaptchaResponse() {
                                var event = new Event('captchaChange');
                                document.getElementById('sib-captcha').dispatchEvent(event);
                              }
                            </script>
                            <div class="g-recaptcha sib-visible-recaptcha" id="sib-captcha" data-sitekey="6LctNfgUAAAAALtRKva7dr_mHEfLJoxtwOjgikf7" data-callback="handleCaptchaResponse"></div>
                          </div>
                          <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div style="padding: 8px 0;">
                      <div class="sib-form-block" style="text-align: left">
                        <button class="sib-form-block__button sib-form-block__button-with-loader" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#FFFFFF; background-color:#02aeef; border-width:0px;" form="sib-form"
                          type="submit">
                          <svg class="icon clickable__icon progress-indicator__icon sib-hide-loader-icon" viewBox="0 0 512 512">
                            <path d="M460.116 373.846l-20.823-12.022c-5.541-3.199-7.54-10.159-4.663-15.874 30.137-59.886 28.343-131.652-5.386-189.946-33.641-58.394-94.896-95.833-161.827-99.676C261.028 55.961 256 50.751 256 44.352V20.309c0-6.904 5.808-12.337 12.703-11.982 83.556 4.306 160.163 50.864 202.11 123.677 42.063 72.696 44.079 162.316 6.031 236.832-3.14 6.148-10.75 8.461-16.728 5.01z"
                            />
                          </svg>
                          Subscribe
                        </button>
                      </div>
                    </div>

                    <input type="text" name="email_address_check" value="" class="input--hidden">
                    <input type="hidden" name="locale" value="en">
                  </form>
                </div>
              </div>
            </div>
            <!-- END - We recommend to place the below code where you want the form in your website html  -->
            </div>
        </section>

        {{-- Business Stationaries Modal Starts--}}
        
        <div class="modal fade" id="pkg-modal-web4" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h5 class="modal-title lh-1">Business Stationaries</h5>
                            <p class="text-primary lh-1">Best for Smaller Businesses</p>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="box1">
                            <form class="contact-form row">
                                <fieldset class="col-12 form-group contact-full-name">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <select class="custom-select" id="">
                                                <option>Mr.</option>
                                                <option>Ms.</option>
                                                <option>Mrs.</option>
                                                <option>Other</option>
                                            </select>
                                        </div>
                                        <input type="text" class="form-control" id="full_name" placeholder="Your Full Name" required="required">
                                    </div>
                                </fieldset>
                                
                                <fieldset class="col-6 form-group">
                                    <input type="email" class="form-control" id="email" placeholder="Your Email Address" required="required">
                                </fieldset>
                                <fieldset class="col-6 form-group">
                                    <input type="text" class="form-control" id="phone" placeholder="Your Phone Number" required="required">
                                </fieldset>
                                <fieldset class="col-6 form-group">
                                    <input type="text" class="form-control" id="company_name" placeholder="Your Company name" >
                                </fieldset>
                                <fieldset class="col-6 form-group">
                                    <input type="text" class="form-control" id="company_address" placeholder="Your Company Address" >
                                </fieldset>
                                <div class="col-12">
                                    <p>
                                        {{-- <button type="submit" data-toggle="modal" data-target="#code-generate" class="btn btn-primary">Get Code<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button> --}}
                                        <button type="button" id="business_submit" class="btn btn-primary">Get Code<span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true"></span><svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>

                                        <button type="reset" id="business_reset" class="btn btn-outline-primary">Start Over<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
                                    </p>
                                </div>
                            </form>
                            <button id="download-pdf1" class="btn btn-primary" hidden>Download Quotation</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Business Stationaries Modal Ends--}}

        @include('frontend.quotation')

        <!-- Merchandise Pack Modal Starts-->
        <div class="modal fade" id="pkg-modal-web" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h5 class="modal-title lh-1"> Merchandise pack</h5>
                            <p class="text-primary lh-1">Best for Smaller Businesses</p>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="box2">
                            <form class="contact-form row">
                                <fieldset class="col-12 form-group contact-full-name">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <select class="custom-select" id="">
                                                <option>Mr.</option>
                                                <option>Ms.</option>
                                                <option>Mrs.</option>
                                                <option>Other</option>
                                            </select>
                                        </div>
                                        <input type="text" class="form-control" id="full_name2" placeholder="Your Full Name" required="required">
                                    </div>
                                </fieldset>
                                
                                <fieldset class="col-6 form-group">
                                    <input type="email" class="form-control" id="email2" placeholder="Your Email Address" required="required">
                                </fieldset>
                                <fieldset class="col-6 form-group">
                                    <input type="text" class="form-control" id="phone2" placeholder="Your Phone Number" required="required">
                                </fieldset>
                                <fieldset class="col-6 form-group">
                                    <input type="text" class="form-control" id="company_name2" placeholder="Your Company name" >
                                </fieldset>
                                <fieldset class="col-6 form-group">
                                    <input type="text" class="form-control" id="company_address2" placeholder="Your Company Address" >
                                </fieldset>
                                <div class="col-12">
                                    <p>
                                        {{-- <button type="submit" data-toggle="modal" data-target="#code-generate" class="btn btn-primary">Get Code<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button> --}}
                                        <button type="button" id="merchandising_submit" class="btn btn-primary">Get Code<span class="spinner-border spinner-border-sm" role="status" id="spinner2" aria-hidden="true"></span><svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>

                                        <button type="reset" id="merchandising_reset" class="btn btn-outline-primary">Start Over<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
                                    </p>
                                </div>
                            </form>
                            <button id="download-pdf2" class="btn btn-primary" hidden>Download Quotation</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Merchandise Pack Modal Ends-->

        @include('frontend.quotation')

        <!-- Code generated -->
        <div class="modal fade" id="code-generate" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <h5 class="modal-title lh-1">Great!</h5>
                            <p class="text-primary lh-1">Here is a generated code</p>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <h4>35BX46B4</h4>
                            </div>
                            <div class="col-12">
                                <ul style="list-style: none; margin-left: -2.5rem;">
                                    <!-- <ul class="package-code-preview"> -->
                                    <li>Mr. John Doe</li>
                                    <li>XYZ Company</li>
                                    <li>Address: Kathmandu, Nepal</li>
                                    <li>Contact no: 012345678</li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <p class="text-success">If given information is correct, please click down below to get an email notification. Thank you.</p>
                            </div>
                            <div class="col-12 mt-3">
                                <p>
                                    <button type="submit" class="btn btn-primary">Submit<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>
                                    <button type="reset" class="btn btn-outline-primary ml-4">Re-enter information<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section background-dark has-bg-img social-section">
            <div class="container">
                <div class="section-header" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                    <p class="seo-title">Get Connected</p>
                    <h3 class="section-heading">How You Can Reach Us</h3>
                    <p class="sub-title">You can get in touch with us directly in various ways</p>
                </div>
                <p class="social-btns mb-xl-7 mb-lg-7 mb-md-6 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                    <a href="https://www.facebook.com/Analogue-Inc-111451123769739/" class="btn btn-primary btn-lg facebook" target="_blank">Like Us on Facebook<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                    <a href="https://twitter.com/analogueinc" class="btn btn-primary btn-lg twitter" target="_blank">Follow Us on Twitter<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                    <a href="https://www.instagram.com/analogueincnepal/" class="btn btn-primary btn-lg instagram" target="_blank">Follow Us on Instagram<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                    <a href="https://www.linkedin.com/company/analogueinc/" class="btn btn-primary btn-lg linkedin" target="_blank">Connect on LinkedIn<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                </p>
                <div class="row pt-2">
                    <div class="col-xl-6 col-lg-6 col-md-8 col-sm-7 col-12 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <h5 class="social-subtitle">We're Here</h5>
                        <address class="social-address">
                            <p>
                                <span class="prefix">Location</span>
                                <span class="suffix">Pinglasthan, Gausala, Kathmandu</span>
                            </p>
                            <p>
                                <span class="prefix">Phone</span>
                                <span class="suffix">+977 9802320803</span>
                            </p>
                            <p>
                                <span class="prefix">Email</span>
                                <span class="suffix">info@analogueinc.com.np</span>
                            </p>
                        </address>
                        <a href="contact.php#contact-map" class="btn btn-outline-primary">Get Directions<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-4 col-sm-5 col-12" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <h5 class="social-subtitle">DM Us On</h5>
                        <p class="social-direct">
                            <a target="_blank" href="https://wa.me/9779802320803" class="btn btn-primary whatsapp">WhatsApp<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                            <a target="_blank" class="btn btn-primary viber">Viber<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                            <a target="_blank" href="https://m.me/111451123769739" class="btn btn-primary messenger">Messenger<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="section-backgrounds">
                <img src="frontend/images/arrow-lg.png" alt="" class="big-arrow">
                <img src="frontend/images/arrow-sm.png" alt="" class="small-arrow">
            </div>
        </section>

        <footer id="footer">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-12 footer-legal">
                        <div class="footer-logo"><img class="w-50" src="frontend/images/logo.png" alt=""></div>
                        <p class="mb-2">&copy; Analogue Inc, a PGNSONS Company. All Rights Reserved.</p>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                        <h6 class="footer-links-title">Navigate</h6>
                        <ul class="footer-links">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="services.php">Services</a></li>
                            <li><a href="about.php">About</a></li>
                            <li><a href="career/">Career</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                        <h6 class="footer-links-title">Services</h6>
                        <ul class="footer-links">
                            <li><a target="_blank" href="https://bm.analogueinc.com.np/">Branding & Marketing</a></li>
                            <li><a target="_blank" href="https://it.analogueinc.com.np/">IT Solution</a></li>
                            <li><a target="_blank" href="https://computers.analogueinc.com.np/">Computers & Hardware</a></li>
                            <li><a target="_blank" href="https://surveillance.analogueinc.com.np/">Surveillance</a></li>
                            <li><a target="_blank" href="https://press.analogueinc.com.np/">Press</a></li>
                        </ul>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                        <h6 class="footer-links-title">Products</h6>
                        <ul class="footer-links">
                            <li><a target="_blank" href="https://www.unclewebs.com/">UncleWebs</a></li>
                            <li><a target="_blank" href="https://www.scholarkeys.com/">Scholarkeys</a></li>
                            <li><a target="_blank" href="https://www.hopperspass.com/">Hopper's Pass</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <script src="frontend/js/aos.js"></script>
        <script>
            AOS.init({
                mirror: true,
            });
        </script>
        <script src="frontend/js/jquery-3.4.1.min.js"></script>
        <script src="frontend/js/lightbox.min.js"></script>
        <script src="frontend/js/bootstrap.bundle.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        <script>
            $(document).ready(function(){
                // BS4.4 sticky scrollspy nav trigger
                $('body').scrollspy({
                    target: '#sticky-header',
                    offset: 75
                });
                $('.scrollto').click(function(event) {
                    var $anchor = $(this);
                    $('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 70 }, 750);
                    event.preventDefault();
                });

                // BS4.4 off-canvas nav trigger
                $("[data-trigger]").on("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    var offcanvas_id = $(this).attr('data-trigger');
                    $(offcanvas_id).toggleClass("show");
                    $('body').toggleClass("offcanvas-active");
                    $(".screen-overlay").toggleClass("show");
                });
                $(".btn-close, .screen-overlay, .scrollto").click(function(e){
                    $(".screen-overlay").removeClass("show");
                    $(".mobile-offcanvas").removeClass("show");
                    $("body").removeClass("offcanvas-active");
                });
            });
            $(document).on('click', '.dropdown-menu', function (e) {
                e.stopPropagation();
            });
        </script>
        <!-- START - We recommend to place the below code in footer or bottom of your website html  -->
        <script>
            window.REQUIRED_CODE_ERROR_MESSAGE = 'Please choose a country code';

            window.EMAIL_INVALID_MESSAGE = window.SMS_INVALID_MESSAGE = "The information provided is invalid. Please review the field format and try again.";

            window.REQUIRED_ERROR_MESSAGE = "This field cannot be left blank. ";

            window.GENERIC_INVALID_MESSAGE = "The information provided is invalid. Please review the field format and try again.";

            window.translation = {
            common: {
              selectedList: '{quantity} list selected',
              selectedLists: '{quantity} lists selected'
            }
            };

        var AUTOHIDE = Boolean(0);
        </script>
        <script src="https://sibforms.com/forms/end-form/build/main.js">
        </script>
        <script src="https://www.google.com/recaptcha/api.js?hl=en"></script>

        <!-- END - We recommend to place the above code in footer or bottom of your website html  -->
        <!-- End Sendinblue Form -->

        </div>

        <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
        $(document).ready(function(){
            $('#press_flyers').click(function(){
                if($('#press_flyers').is(':checked')){
                    $("#flyers_quantity").val('0');
                    $("#flyers_quantity").change( function(){
                        $('#tailor_press_flyers').removeAttr('style');
                        $("#qty1").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_flyers').toggle();
                    $("#flyers_quantity").val('0');
                }
            });


            $('#press_leaflet').click(function(){
                if($('#press_leaflet').is(':checked')){
                    $("#leaflet_quantity").val('0');
                    $("#leaflet_quantity").change( function(){
                        $('#tailor_press_leaflet').removeAttr('style');
                        $("#qty2").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_leaflet').toggle();
                    $("#leaflet_quantity").val('0');
                }
            });


            $('#press_brochure').change(function(){
                if($('#press_brochure').is(':checked')){
                    $("#brochures_quantity").val('0');
                    $("#brochures_quantity").change(function(){
                        $('#tailor_press_brochures').removeAttr('style');
                        $("#qty3").text($(this).val());
                        var page = $('#brochure_pages').val();
                        $("#page1").text(page);
                    });
                } 
                else {
                    $("#brochures_quantity").val('0');
                    $('#tailor_press_brochures').toggle();
                }
            });

            $('#press_business').change(function(){
                if($('#press_business').is(':checked')){
                    $("#business_quantity").val('0');
                    $("#business_quantity").change(function(){
                        $('#tailor_press_business').removeAttr('style');
                        $("#qty4").text($(this).val());
                        var page = $('#business_pages').val();
                        $("#page2").text(page);
                    });
                } 
                else {
                    $("#business_quantity").val('0');
                    $('#tailor_press_businesss').toggle();
                }
            });

            $('#press_banner').click(function(){
                if($('#press_banner').is(':checked')){
                    $("#banner_quantity").val('0');
                    $("#banner_quantity").change( function(){
                        $('#tailor_press_banner').removeAttr('style');
                        $("#qty5").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_banner').toggle();
                    $("#banner_quantity").val('0');
                }
            });

            $('#press_business_card').change(function(){
                if($('#press_business_card').is(':checked')){
                    $("#business_card_quantity").val('0');
                    $("#business_card_quantity").change(function(){
                        $('#tailor_press_businesscard').removeAttr('style');
                        $("#qty6").text($(this).val());
                        var page = $('#business_card_sides').val();
                        $("#page3").text(page);
                    });
                } 
                else {
                    $("#business_card_quantity").val('0');
                    $('#tailor_press_businesss').toggle();
                }
            });


            $('#press_sticker').click(function(){
                if($('#press_sticker').is(':checked')){
                    $("#sticker_quantity").val('0');
                    $("#sticker_quantity").change( function(){
                        $('#tailor_press_sticker').removeAttr('style');
                        $("#qty7").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_sticker').toggle();
                    $("#sticker_quantity").val('0');
                }
            });

            $('#press_cover_letter').click(function(){
                if($('#press_cover_letter').is(':checked')){
                    $("#cover_letter_quantity").val('0');
                    $("#cover_letter_quantity").change( function(){
                        $('#tailor_press_cover_letter').removeAttr('style');
                        $("#qty8").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_cover_letter').toggle();
                    $("#cover_letter_quantity").val('0');
                }
            });

            $('#press_letterhead').click(function(){
                if($('#press_letterhead').is(':checked')){
                    $("#letterhead_quantity").val('0');
                    $("#letterhead_quantity").change( function(){
                        $('#tailor_press_letterhead').removeAttr('style');
                        $("#qty9").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_letterhead').toggle();
                    $("#letterhead_quantity").val('0');
                }
            });

            $('#press_envelope').click(function(){
                if($('#press_envelope').is(':checked')){
                    $("#envelope_quantity").val('0');
                    $("#envelope_quantity").change( function(){
                        $('#tailor_press_envelope').removeAttr('style');
                        $("#qty10").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_envelope').toggle();
                    $("#envelope_quantity").val('0');
                }
            });

            $('#press_idcard').click(function(){
                if($('#press_idcard').is(':checked')){
                    $("#idcard_quantity").val('0');
                    $("#idcard_quantity").change( function(){
                        $('#tailor_press_idcard').removeAttr('style');
                        $("#qty11").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_idcard').toggle();
                    $("#idcard_quantity").val('0');
                }
            });

            $('#press_notebook').click(function(){
                if($('#press_notebook').is(':checked')){
                    $("#notebook_quantity").val('0');
                    $("#notebook_quantity").change( function(){
                        $('#tailor_press_notebook').removeAttr('style');
                        $("#qty12").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_notebook').toggle();
                    $("#notebook_quantity").val('0');
                }
            });

            $('#press_calendar').click(function(){
                if($('#press_calendar').is(':checked')){
                    $("#calendar_quantity").val('0');
                    $("#calendar_quantity").change( function(){
                        $('#tailor_press_calendar').removeAttr('style');
                        $("#qty13").text($(this).val());
                    });
                }
                else{
                    $('#tailor_press_calendar').toggle();
                    $("#calendar_quantity").val('0');
                }
            });


        });
        </script>
        

        <script>
            function get_baseurl() {
                var base_url = "<?php echo URL::to('/') ?>";
                return base_url;
            }

            $('#spinner').hide();

            $('#business_submit').on('click',function(){
            var full_name = $('body .modal #full_name').val();
            var email = $('body .modal #email').val();
            var phone = $('body .modal #phone').val();
            var company_name = $('body .modal #company_name').val();
            var company_address = $('body .modal #company_address').val();
            var package_details = $('#select_package_lists').text()
            var other_message = $('#other_message').val();
            var package_name = "Business Stationaries";

            var base_url = get_baseurl();

            $.ajax({
                url: base_url + '/api/userpackage/create',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    "_token" : "{{ csrf_token() }}",
                    "full_name": full_name,
                    "email": email,
                    "phone": phone,
                    "company_name": company_name,
                    "company_address": company_address,
                    "package_name": package_name,
                    "package_type": "Tailor Type",
                    "package_details": package_details,
                    "other_message": other_message,
                },

                beforeSend: function(){ 
                    $("#spinner").show();
                },

                complete: function(){ 
                    $("#spinner").hide();
                },

                success:function(data){
                    var parent = document.getElementById('box1');
                    var span = document.createElement('span');
                    span.style.fontSize = 'large';
                    span.style.borderRadius= 30;
                    span.style.color = '#000';
                    span.style.backgroundColor = '#27ae60'
                    span.innerHTML = 'Sent! Please check your email for more details.';
                    setTimeout(function () {
                        span.setAttribute('hidden', true);
                    }, 5000)
                    parent.parentNode.insertBefore(span,parent);
                    document.getElementById('business_submit').setAttribute('hidden', true);
                    document.getElementById('business_reset').setAttribute('hidden', true);
                    var downloadPdfBtn = document.getElementById('download-pdf1');
                    downloadPdfBtn.removeAttribute('hidden');
                    downloadPdfBtn.addEventListener('click', function() {
                        var element = document.getElementById('invoice');
                        $('#invoice').css({'display': 'block'});
                        document.getElementById('quotationName').innerHTML = full_name;
                        document.getElementById('quotationEmail').innerHTML = email;
                        document.getElementById('quotationPhone').innerHTML = '+977 (' + phone +')';
                        // document.getElementById('subject').innerHTML = 'Subject: Price Quotation of Tailor Made Package';
                        document.getElementById('package-name').innerHTML = package_name;
                        document.getElementById('package-detail').innerHTML = package_details;
                        // document.getElementById('package-total').innerHTML = package_total_price;

                        var element = document.getElementById('invoice');
                        html2pdf().from(element).save();
                        toastr.success('Sent Successfully');
                    });
                    // window.setTimeout(function () {
                    //     window.location.href = base_url +'/bm';
                    // }, 9000 );
                },
                error: function(jqXHR, exception){
                    var parent = document.getElementById('box');
                    var span = document.createElement('span');
                    span.style.fontSize = 'large';
                    span.style.borderRadius= 30;
                    span.style.color = '#000';
                    span.style.backgroundColor = '#d63031'
                    span.innerHTML = jqXHR.responseJSON.message;
                    setTimeout(function () {
                        span.setAttribute('hidden', true);
                    }, 5000)
                    parent.parentNode.insertBefore(span,parent);
                }
            });
        });

        </script>

        <script>
            $('#press_tshirt').click(function(){
                if($('#press_tshirt').is(':checked'))
                {
                    $("#tshirt_quantity").val('0');
                    $("#tshirt_quantity").change( function(){
                        $('#tailor_press_tshirt').removeAttr('style');
                        $("#qty14").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_tshirt').toggle();
                    $("#tshirt_quantity").val('0');
                }
            });

            $('#press_bag').click(function(){
                if($('#press_bag').is(':checked'))
                {
                    $("#bag_quantity").val('0');
                    $("#bag_quantity").change( function(){
                        $('#tailor_press_bag').removeAttr('style');
                        $("#qty15").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_bag').toggle();
                    $("#bag_quantity").val('0');
                }
            });

            $('#press_cup').click(function(){
                if($('#press_cup').is(':checked'))
                {
                    $("#cup_quantity").val('0');
                    $("#cup_quantity").change( function(){
                        $('#tailor_press_cup').removeAttr('style');
                        $("#qty16").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_cup').toggle();
                    $("#cup_quantity").val('0');
                }
            });

            $('#press_penpencil').click(function(){
                if($('#press_penpencil').is(':checked'))
                {
                    $("#penpencil_quantity").val('0');
                    $("#penpencil_quantity").change( function(){
                        $('#tailor_press_penpencil').removeAttr('style');
                        $("#qty17").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_penpencil').toggle();
                    $("#penpencil_quantity").val('0');
                }
            });

            $('#press_togocup').click(function(){
                if($('#press_togocup').is(':checked'))
                {
                    $("#togocup_quantity").val('0');
                    $("#togocup_quantity").change( function(){
                        $('#tailor_press_togocup').removeAttr('style');
                        $("#qty18").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_togocup').toggle();
                    $("#togocup_quantity").val('0');
                }
            });

            $('#press_packingbag').click(function(){
                if($('#press_packingbag').is(':checked'))
                {
                    $("#packingbag_quantity").val('0');
                    $("#packingbag_quantity").change( function(){
                        $('#tailor_press_packingbag').removeAttr('style');
                        $("#qty19").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_packingbag').toggle();
                    $("#packingbag_quantity").val('0');
                }
            });

            $('#press_cardboard').click(function(){
                if($('#press_cardboard').is(':checked'))
                {
                    $("#cardboard_quantity").val('0');
                    $("#cardboard_quantity").change( function(){
                        $('#tailor_press_cardboard').removeAttr('style');
                        $("#qty20").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_cardboard').toggle();
                    $("#cardboard_quantity").val('0');
                }
            });

            $('#press_coaster').click(function(){
                if($('#press_coaster').is(':checked'))
                {
                    $("#coaster_quantity").val('0');
                    $("#coaster_quantity").change( function(){
                        $('#tailor_press_coaster').removeAttr('style');
                        $("#qty21").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_coaster').toggle();
                    $("#coaster_quantity").val('0');
                }
            });

            $('#press_badge').click(function(){
                if($('#press_badge').is(':checked'))
                {
                    $("#badge_quantity").val('0');
                    $("#badge_quantity").change( function(){
                        $('#tailor_press_badge').removeAttr('style');
                        $("#qty22").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_badge').toggle();
                    $("#badge_quantity").val('0');
                }
            });

            $('#press_shoppingbag').click(function(){
                if($('#press_shoppingbag').is(':checked'))
                {
                    $("#shoppingbag_quantity").val('0');
                    $("#shoppingbag_quantity").change( function(){
                        $('#tailor_press_shoppingbag').removeAttr('style');
                        $("#qty23").text($(this).val());
                    });
                }
                else
                {
                    $('#tailor_press_shoppingbag').toggle();
                    $("#shoppingbag_quantity").val('0');
                }
            });

        </script>

        <script>
            function get_baseurl() {
                var base_url = "<?php echo URL::to('/') ?>";
                return base_url;
            }

            $('#spinner2').hide();

            $('#merchandising_submit').on('click',function(){
            var full_name = $('body .modal #full_name2').val();
            var email = $('body .modal #email2').val();
            var phone = $('body .modal #phone2').val();
            var company_name = $('body .modal #company_name2').val();
            var company_address = $('body .modal #company_address2').val();
            var package_name = "Merchandising Pack";
            var package_details = $('#select_package_lists2').text();
            var other_message = $('#other_message2').val();

            var base_url = get_baseurl();

            $.ajax({
                url: base_url + '/api/userpackage/create',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    "_token" : "{{ csrf_token() }}",
                    "full_name": full_name,
                    "email": email,
                    "phone": phone,
                    "company_name": company_name,
                    "company_address": company_address,
                    "package_name": package_name,
                    "package_type": "Tailor Type",
                    "package_details": package_details,
                    "other_message": other_message,
                },

                beforeSend: function(){ 
                    $("#spinner2").show();
                },

                complete: function(){ 
                    $("#spinner2").hide();
                },

                success:function(data){
                    var parent = document.getElementById('box2');
                    var span = document.createElement('span');
                    span.style.fontSize = 'large';
                    span.style.borderRadius= 30;
                    span.style.color = '#000';
                    span.style.backgroundColor = '#27ae60'
                    span.innerHTML = 'Sent! Please check your email for more details.';
                    setTimeout(function () {
                        span.setAttribute('hidden', true);
                    }, 5000)
                    parent.parentNode.insertBefore(span,parent);
                    document.getElementById('merchandising_submit').setAttribute('hidden', true);
                    document.getElementById('merchandising_reset').setAttribute('hidden', true);
                    var downloadPdfBtn = document.getElementById('download-pdf2');
                    downloadPdfBtn.removeAttribute('hidden');
                    downloadPdfBtn.addEventListener('click', function() {
                        var element = document.getElementById('invoice');
                        $('#invoice').css({'display': 'block'});
                        document.getElementById('quotationName').innerHTML = full_name;
                        document.getElementById('quotationEmail').innerHTML = email;
                        document.getElementById('quotationPhone').innerHTML = '+977 (' + phone +')';
                        // document.getElementById('subject').innerHTML = 'Subject: Price Quotation of Tailor Made Package';
                        document.getElementById('package-name').innerHTML = package_name;
                        document.getElementById('package-detail').innerHTML = package_details;
                        // document.getElementById('package-total').innerHTML = package_total_price;

                        var element = document.getElementById('invoice');
                        html2pdf().from(element).save();
                        toastr.success('Sent Successfully');
                    });
                    // window.setTimeout(function () {
                    //     window.location.href = base_url +'/bm';
                    // }, 9000 );
                },
                error: function(jqXHR, exception){
                    var parent = document.getElementById('box');
                    var span = document.createElement('span');
                    span.style.fontSize = 'large';
                    span.style.borderRadius= 30;
                    span.style.color = '#000';
                    span.style.backgroundColor = '#d63031'
                    span.innerHTML = jqXHR.responseJSON.message;
                    setTimeout(function () {
                        span.setAttribute('hidden', true);
                    }, 5000)
                    parent.parentNode.insertBefore(span,parent);
                }
            });
        });

        </script>
    </body>
</html>