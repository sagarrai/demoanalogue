<section class="section background-dark has-bg-img social-section">
    <div class="container">
        <div class="section-header" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
            <p class="seo-title">Get Connected</p>
            <h3 class="section-heading">How You Can Reach Us</h3>
            <p class="sub-title">You can get in touch with us directly in various ways</p>
        </div>
        <p class="social-btns mb-xl-7 mb-lg-7 mb-md-6 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
            <a href="https://www.facebook.com/Analogue-Inc-111451123769739/" class="btn btn-primary btn-lg facebook" target="_blank">Like Us on Facebook<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
            <a href="https://twitter.com/analogueinc" class="btn btn-primary btn-lg twitter" target="_blank">Follow Us on Twitter<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
            <a href="https://www.instagram.com/analogueincnepal/" class="btn btn-primary btn-lg instagram" target="_blank">Follow Us on Instagram<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
            <a href="https://www.linkedin.com/company/analogueinc/" class="btn btn-primary btn-lg linkedin" target="_blank">Connect on LinkedIn<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
        </p>
        <div class="row pt-2">
            <div class="col-xl-6 col-lg-6 col-md-8 col-sm-7 col-12 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <h5 class="social-subtitle">We're Here</h5>
                <address class="social-address">
                    <p>
                        <span class="prefix">Location</span>
                        <span class="suffix">Pinglasthan, Gausala, Kathmandu</span>
                    </p>
                    <p>
                        <span class="prefix">Phone</span>
                        <span class="suffix">+977 9802320803</span>
                    </p>
                    <p>
                        <span class="prefix">Email</span>
                        <span class="suffix">info@analogueinc.com.np</span>
                    </p>
                </address>
                <a href="contact.php#contact-map" class="btn btn-outline-primary">Get Directions<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-4 col-sm-5 col-12" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <h5 class="social-subtitle">DM Us On</h5>
                <p class="social-direct">
                    <a target="_blank" href="https://wa.me/9779802320803" class="btn btn-primary whatsapp">WhatsApp<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                    <a target="_blank" class="btn btn-primary viber">Viber<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                    <a target="_blank" href="https://m.me/111451123769739" class="btn btn-primary messenger">Messenger<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                </p>
            </div>
        </div>
    </div>

    <div class="section-backgrounds">
        <img src="frontend/press/images/arrow-lg.png" alt="" class="big-arrow">
        <img src="frontend/press/images/arrow-sm.png" alt="" class="small-arrow">
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-12 footer-legal">
                <div class="footer-logo"><img class="w-50" src="frontend/press/images/logo.png" alt=""></div>
                <p class="mb-2">&copy; Analogue Inc, a PGNSONS Company. All Rights Reserved.</p>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                <h6 class="footer-links-title">Navigate</h6>
                <ul class="footer-links">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="services.php">Services</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="career/">Career</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                <h6 class="footer-links-title">Services</h6>
                <ul class="footer-links">
                    <li><a target="_blank" href="https://bm.analogueinc.com.np/">Branding & Marketing</a></li>
                    <li><a target="_blank" href="https://it.analogueinc.com.np/">IT Solution</a></li>
                    <li><a target="_blank" href="https://computers.analogueinc.com.np/">Computers & Hardware</a></li>
                    <li><a target="_blank" href="https://surveillance.analogueinc.com.np/">Surveillance</a></li>
                    <li><a target="_blank" href="https://press.analogueinc.com.np/">Press</a></li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
                <h6 class="footer-links-title">Products</h6>
                <ul class="footer-links">
                    <li><a target="_blank" href="https://www.unclewebs.com/">UncleWebs</a></li>
                    <li><a target="_blank" href="https://www.scholarkeys.com/">Scholarkeys</a></li>
                    <li><a target="_blank" href="https://www.hopperspass.com/">Hopper's Pass</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<script src="frontend/press/js/aos.js"></script>
<script>
    AOS.init({
        mirror: true,
    });
</script>
<script src="frontend/press/js/jquery-3.4.1.min.js"></script>
<script src="frontend/press/js/bootstrap.bundle.min.js"></script>
<script>
    $(document).ready(function(){
        // BS4.4 sticky scrollspy nav trigger
        $('body').scrollspy({
            target: '#sticky-header',
            offset: 75
        });
        $('.scrollto').click(function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 70 }, 750);
            event.preventDefault();
        });

        // BS4.4 off-canvas nav trigger
        $("[data-trigger]").on("click", function(e){
            e.preventDefault();
            e.stopPropagation();
            var offcanvas_id = $(this).attr('data-trigger');
            $(offcanvas_id).toggleClass("show");
            $('body').toggleClass("offcanvas-active");
            $(".screen-overlay").toggleClass("show");
        });
        $(".btn-close, .screen-overlay, .scrollto").click(function(e){
            $(".screen-overlay").removeClass("show");
            $(".mobile-offcanvas").removeClass("show");
            $("body").removeClass("offcanvas-active");
        });
    });
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
</script>
<script src="frontend/press/js/lightbox.min.js"></script>
