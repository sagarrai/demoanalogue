@extends('layouts.frontend')

@section('content')

	@foreach($blogs as $blog)

		<section class="section background-light has-bg-img is-page-title">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-8">
						<div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false">
							<h1 class="section-heading">{{ $blog->title }}</h1>
							<p class="sub-title">Boost up your online presence with Analogue Branding and Marketing Team</p>
						</div>
					</div>
				</div>
			</div>
			<div class="section-backgrounds">
				<img src="frontend/images/arrow-lg.png" alt="" class="big-arrow">
				<img src="frontend/images/arrow-sm.png" alt="" class="small-arrow">
			</div>
		</section>
		<main role="main">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-8">
						<div class="mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
							<p class="lead">{!! $blog->description !!}</p>
						</div>
					</div>
				</div>
			</div>
		</main>

	@endforeach

@endsection
