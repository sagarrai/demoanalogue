@extends('layouts.frontend')
@section('content')
    <div class="splash">
            <div class="container-lg">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-7 col-sm-7 col-12">
                        <div class="splash-caption" data-aos="fade-right" data-aos-offset="100" data-aos-delay="200" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                            <h1>Take your business to the next level.</h1>
                            <div class="caption-content">
                                <p>Your business needs an IT partner who not only has the right expertise but also got the zeal to roll up their sleeves and solve your IT infrastructure related issues.</p>
                                <p>Analogue Inc. will be the one-stop destination upgrading your business to the <strong>next level</strong>.</p>
                            </div>
                            <p class="splash-btns">
                                <a href="/services" class="btn btn-primary btn-lg">Begin your success<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                                <a href="#consultation-form" class="btn btn-outline-primary btn-lg scroll-activator">Free Consultation<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-5 col-sm-5 col-12">
                        <div class="splash-image" data-aos="fade-left" data-aos-offset="100" data-aos-delay="500" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                            <div class="big-a"><img src="/frontend/images/big-a-right.svg" alt=""></div>
                            <div class="people"><img src="/frontend/images/people1.png" alt=""></div>
                            <div class="balloon shake-constant"><img src="/frontend/images/balloon.png" alt=""></div>
                            <div class="splash-cloud cloud1 shake-constant"><img src="/frontend/images/cloud1.png"></div>
                            <div class="splash-cloud cloud2 shake-constant"><img src="/frontend/images/cloud2.png"></div>
                            <div class="splash-cloud cloud3 shake-constant"><img src="/frontend/images/cloud3.png"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section background-light pb-2x">
            <div class="container-lg">
                <div class="section-header text-center pb-half">
                    <h3 class="section-heading">About Us</h3>
                    <p class="sub-title">Who we are and how we came to be</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-8 col-12" data-aos="fade-right" data-aos-offset="100" data-aos-delay="200" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <p class="text-dark">People often wonder about our name.. What does Analogue mean? Why Analogue? Why did you go for this name for your company? Does it have any meaning?</p>
                        <p class="text-primary">Well actually Yes.. Yes, we do...</p>
                        <p>Analogue comes from the ancient greek word Analogos which means using signals or information represented by a continuously variable physical quantity. The word can refer to something that is analogous (as in an analog organ), but it is most often used to distinguish analog electronics from digital electronics like an analog computer or an analog clock.</p>
                        <p class="text-dark">Like our name, our ambition is to visualize your business with an interior analogue to an exterior world.</p>
                        <p><a href="/about" class="btn btn-primary">Learn More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-8 col-12" data-aos="fade-left" data-aos-offset="100" data-aos-delay="500" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="img-holder mt-md-0 mt-5"><img src="/frontend/images/about.jpg" alt=""></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-center align-items-center">
                    <div class="col-xxl-5 col-xl-5 col-12">
                        <div class="row align-items-xl-baseline align-items-md-center justify-content-md-between justify-content-center">
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="section-header pb-half" data-aos="fade-up" data-aos-offset="100" data-aos-delay="150" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <div class="img-holder w-75 mb-4"><img src="/frontend/images/uw-logo2.png" alt=""></div>
                                    <p class="seo-title">Start your business Journey</p>
                                </div>
                                <p class="mb-3" data-aos="fade-up" data-aos-offset="100" data-aos-delay="150" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">In this world full of chaos, let UncleWebs be your peace. With widest selection on web, UncleWebs brings you variety of services that will fulfill your need for Web Solution. <br><br>
                                Don't get lost in choosing different service provider when we bring you all the service in palm of your hand.</p>
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12 mt-xl-5 mt-lg-0 mt-5" data-aos="fade-up" data-aos-offset="100" data-aos-delay="150" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                <div class="img-holder"><img src="/frontend/images/uncle_webs_preview.jpg" alt=""></div>
                                <p class="mt-lg-6 mt-5"><a href="http://www.unclewebs.com/" target="_blank" class="btn btn-primary">Start Your Free Trial Today</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-7 col-12">
                        <div class="row align-items-center justify-content-md-center">
                            <div class="col-xl-6 col-lg-12 col-md-7 col-12 mt-xl-0 mt-lg-6 d-xl-block d-lg-flex justify-content-lg-between">
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="50" data-aos-duration="400" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Domain Hosting</h6>
                                    <p class="sm-card-subtitle text-primary">Be Unique</p>
                                    <p class="sm-card-content">We specialize on hosting your business domain name. Your domain is your asset and we know how to take care of your asset. With our all in one solution, you can get all your web based solution in one place.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.unclewebs.com/" target="_blank">Get Your Free Trial Today &rarr;</a></p>
                                </div>
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="150" data-aos-duration="400" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">DIY Website</h6>
                                    <p class="sm-card-subtitle text-primary">Websites made easier</p>
                                    <p class="sm-card-content">Every business owner wants the freedom to create their own website. Now with DIY website from UncleWebs, you can design and build your own high quality website with just a click.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.unclewebs.com/" target="_blank">Get Your Free Trial Today &rarr;</a></p>
                                </div>
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="250" data-aos-duration="400" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">eCommerce</h6>
                                    <p class="sm-card-subtitle text-primary">From Somewhere to Everywhere</p>
                                    <p class="sm-card-content">You are one step away from building your own online store. Now with UncleWebs, you have the power to manage your business with your preference and start selling. Enhance the power of eCommerce with UncleWebs.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.unclewebs.com/" target="_blank">Get Your Free Trial Today &rarr;</a></p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-8 col-md-7 col-12 mt-xl-0 mt-lg-5 d-xl-block d-lg-flex justify-content-lg-between">
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="100" data-aos-duration="400" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Email Hosting</h6>
                                    <p class="sm-card-subtitle text-primary">Lets Be Professional</p>
                                    <p class="sm-card-content">Get your professional Business email to organize your mail and collaborate with your team. With our email hosting service, we reduce the risk of email being marked as spam giving clear link to your website.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.unclewebs.com/" target="_blank">Get Your Free Trial Today &rarr;</a></p>
                                </div>
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="200" data-aos-duration="400" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Web Hosting</h6>
                                    <p class="sm-card-subtitle text-primary">Get Your Website Online</p>
                                    <p class="sm-card-content">Your hosting is one click away after designing your website. Just building a website isn’t enough; you need a trusted hosting service to host your website. Get your website online with UncleWebs and start your business.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.unclewebs.com/" target="_blank">Get Your Free Trial Today &rarr;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section background-dark">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-center align-items-center">
                    <div class="col-xxl-6 col-xl-7 col-12 order-xl-1 order-2">
                        <div class="row align-items-center justify-content-md-center">
                            <div class="col-xl-6 col-lg-12 col-md-7 col-12 mt-xl-0 mt-lg-6 d-xl-block d-lg-flex justify-content-lg-between">
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="50" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Desktops</h6>
                                    <p class="sm-card-content">Let us take care of your computer setup because we can help you with what you need. From home setup to your office, Analogue Mall has you covered.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.analoguemall.com/" target="_blank">Shop Now &rarr;</a></p>
                                </div>
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="150" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Laptops</h6>
                                    <p class="sm-card-content">Whether you run a business or are a student, you need to work on the go. With that in mind, Analogue Mall brings you a series of laptops that suit your needs.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.analoguemall.com/" target="_blank">Shop Now &rarr;</a></p>
                                </div>
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="250" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Printers & Scanners</h6>
                                    <p class="sm-card-content">Let Analogue Mall ink your thoughts onto paper and documents with our range of high quality printers & scanners from globally recognized brands.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.analoguemall.com/" target="_blank">Shop Now &rarr;</a></p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-8 col-md-7 col-12 mt-xl-0 mt-lg-5 d-xl-block d-lg-flex justify-content-lg-between">
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="100" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Surveillance</h6>
                                    <p class="sm-card-content">Providing you with the state of the art solutions in network security and connectivity. You want to sleep safe & sound, & Analogue Mall is here to deliver that.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.analoguemall.com/" target="_blank">Shop Now &rarr;</a></p>
                                </div>
                                <div class="sm-card" data-aos="fade-up" data-aos-offset="100" data-aos-delay="200" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <h6 class="sm-card-title">Accessories</h6>
                                    <p class="sm-card-content">Complete your home computer setup, office desktop or your studio PC with a wide range of accessories that bring up the quality of life on your daily work.</p>
                                    <p class="sm-card-footer"><a class="stretched-link" href="http://www.analoguemall.com/" target="_blank">Shop Now &rarr;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-5 col-xl-5 col-12 order-xl-2 order-1">
                        <div class="row align-items-xl-baseline align-items-md-center justify-content-md-between justify-content-center">
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12">
                                <div class="section-header pb-half" data-aos="fade-up" data-aos-offset="100" data-aos-delay="100" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                    <p class="seo-title">Comprehensive Online Store</p>
                                    <h3 class="section-heading">Analogue Mall</h3>
                                    <p class="sub-title">Innovate your world</p>
                                </div>
                                <p data-aos="fade-up" data-aos-offset="100" data-aos-delay="100" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">Technology has been taking over and you can only wish for a Vendor that delivers your favorite gadget to your doorstep. Until now, because Analogue Mall brings you a platform where you can shop your electronic needs and delivers it where you want. Amazing right? Check out our collection.</p>
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-6 col-12 mt-xl-5 mt-lg-0 mt-5" data-aos="fade-up" data-aos-offset="100" data-aos-delay="100" data-aos-duration="600" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                                <div class="img-holder"><img src="/frontend/images/analogue-mall-preview.jpg" alt=""></div>
                                <p class="mt-lg-6 mt-5"><a href="http://www.analoguemall.com/" target="_blank" class="btn btn-primary">Shop Now with Analogue Mall</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section pb-2x has-bg-img">
            <div class="container">
                <div class="section-header text-center pb-half">
                    <p class="seo-title">Our Services</p>
                    <h3 class="section-heading">Your Path to Success</h3>
                    <p class="sub-title">is filled with hurdles that we'll help you overcome</p>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10 col-12 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="service-block">
                            <div class="img-holder mb-5"><img src="/frontend/images/logo-bm.png" alt=""></div>
                            <!-- <div class="service-icon"><img src="/frontend/images/logo-bm.png" alt=""></div> -->
                            <!-- <h5 class="service-title">Branding & Marketing</h5> -->
                            <p class="service-content">Your brand is a visual embodiment of your business and is unique to you. We believe your brand should be as unique as your business.</p>
                            <h6 class="service-links"><a target="_blank" href="https://bm.analogueinc.com.np/#why-bm">Social Media Marketing<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://bm.analogueinc.com.np/#why-bm">Brand Guidelines<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://bm.analogueinc.com.np/#why-bm">Email Marketing<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://bm.analogueinc.com.np/#why-bm">Search Engine Optimization<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://bm.analogueinc.com.np/#why-bm">Merchandising<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <p class="service-btn"><a target="_blank" href="https://bm.analogueinc.com.np/" class="btn btn-primary">Tell Me More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10 col-12 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="service-block">
                            <div class="img-holder mb-5"><img src="/frontend/images/logo-it.png" alt=""></div>
                            <!-- <div class="service-icon"><img src="/frontend/images/logo-it.png" alt=""></div> -->
                            <!-- <h5 class="service-title">IT Solution</h5> -->
                            <p class="service-content">Make yourself future-proof. Maximum value to customers in terms of speedy development, excellent quality and on time delivery.</p>
                            <h6 class="service-links"><a target="_blank" href="https://it.analogueinc.com.np/#services">Web Design<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://it.analogueinc.com.np/#services">App Design<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://it.analogueinc.com.np/#services">Desktop Applications<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://it.analogueinc.com.np/#services">Development<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://it.analogueinc.com.np/#services">UI/UX<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <p class="service-btn"><a target="_blank" href="https://it.analogueinc.com.np/" class="btn btn-primary">Tell Me More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10 col-12 mt-xl-0 mt-lg-5 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="service-block">
                            <div class="img-holder mb-5"><img src="/frontend/images/logo-cm.png" alt=""></div>
                            <!-- <div class="service-icon"><img src="/frontend/images/logo-cm.png" alt=""></div> -->
                            <!-- <h5 class="service-title">Computers & Hardware</h5> -->
                            <p class="service-content">Challenging to imagine life looking at the rate in which newer technology is being introduced in the market every other day right?</p>
                            <h6 class="service-links"><a target="_blank" href="https://computers.analogueinc.com.np/#desktops">Desktop PCs<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://computers.analogueinc.com.np/#laptops">Laptops<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://computers.analogueinc.com.np/#printers">Printers<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://computers.analogueinc.com.np/#accessories">Accessories<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://computers.analogueinc.com.np/#discover-scroll">Our Products<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <p class="service-btn"><a target="_blank" href="https://computers.analogueinc.com.np/" class="btn btn-primary">Tell Me More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10 col-12 mt-xl-6 mt-lg-5 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="300" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="service-block">
                            <div class="img-holder mb-5"><img src="/frontend/images/logo-su.png" alt=""></div>
                            <!-- <div class="service-icon"><img src="/frontend/images/logo-su.png" alt=""></div> -->
                            <!-- <h5 class="service-title">Security & Surveillance</h5> -->
                            <p class="service-content">Analogue | Surveillance is committed to providing state of the art solutions in network security and connectivity, cloud based Intelligent surveillance systems and much more.</p>
                            <h6 class="service-links"><a target="_blank" href="https://surveillance.analogueinc.com.np/#cctv">CCTV Cameras<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://surveillance.analogueinc.com.np/#ip">IP Surveillance<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://surveillance.analogueinc.com.np/#biometrics">Biometrics<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://surveillance.analogueinc.com.np/#nvr">NVR<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://surveillance.analogueinc.com.np/#rfid">RFID Access<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <p class="service-btn"><a target="_blank" href="https://surveillance.analogueinc.com.np/" class="btn btn-primary">Tell Me More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10 col-12 mt-xl-6 mt-lg-5 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="400" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="service-block">
                            <div class="img-holder mb-5"><img src="/frontend/images/logo-ec.png" alt=""></div>
                            <!-- <div class="service-icon"><img src="/frontend/images/logo-ec.png" alt=""></div> -->
                            <!-- <h5 class="service-title">Analogue E-Commerce</h5> -->
                            <p class="service-content">Our all new service allows up and coming ecommerce websites to start their online business hassle-free. The process is simplified, from setup up to launch, without any programming knowledge!</p>
                            <h6 class="service-links"><a target="_blank" href="https://ecommerce.analogueinc.com.np/">DIY Builder<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://ecommerce.analogueinc.com.np/">Consultation<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://ecommerce.analogueinc.com.np/">eCommerce Platform<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://ecommerce.analogueinc.com.np/">Domain & Hosting<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://ecommerce.analogueinc.com.np/">Chat & Phone Support<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <p class="service-btn"><a target="_blank" href="https://ecommerce.analogueinc.com.np/" class="btn btn-primary">Tell Me More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10 col-12 mt-xl-6 mt-lg-5 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="500" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="service-block">
                            <div class="img-holder mb-5"><img src="/frontend/images/logo-pr.png" alt=""></div>
                            <!-- <div class="service-icon"><img src="/frontend/images/logo-pr.png" alt=""></div> -->
                            <!-- <h5 class="service-title">Analogue Press</h5> -->
                            <p class="service-content">We offer a professional printing for any application you desire. We print all kinds of post card, wedding card, posters, visiting cards, brochure, leaflet, flyer and banners of different sizes.</p>
                            <h6 class="service-links"><a target="_blank" href="https://press.analogueinc.com.np/#printing">Brochures<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://press.analogueinc.com.np/#printing">Banners & Flexes<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://press.analogueinc.com.np/#printing">Magazines<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://press.analogueinc.com.np/#merchandise">T-Shirts<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <h6 class="service-links"><a target="_blank" href="https://press.analogueinc.com.np/#merchandise">Merchandise<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6>
                            <!-- <h6 class="service-links"><a target="_blank" href="">Chat & Phone Support<svg class="icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></h6> -->
                            <p class="service-btn"><a target="_blank" href="https://press.analogueinc.com.np/" class="btn btn-primary">Tell Me More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section background-light">
            <div class="container">
                <div class="section-header text-center">
                    <p class="seo-title">Our Process</p>
                    <h3 class="section-heading">How We Work</h3>
                    <p class="sub-title">Bring in your ideas, and we'll make them a reality</p>
                </div>
                <div class="row justify-content-center">
                    <div class="text-lg-left text-center col-xl-4 col-lg-4 col-md-8 col-sm-10 col-12 mt-xl-9 mt-lg-9" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="/frontend/images/img15.png" alt="" class="img-fluid rounded"></div>
                        <h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Research & Strategy</h5>
                        <p class="mb-xl-4 mb-lg-4 mb-md-5 mb-sm-2 mb-2">Before we do anything else, it is of the utmost importance for our field experts to conduct a thorough research to formulate a strategy that we can follow together. This process will allow us to see a clear path to our goals, how we can achieve them & how we will deal with hurdles that could come up in the future.</p>
                        <!-- <a href="" class="btn btn-link">How does this Work?<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a> -->
                    </div>
                    <div class="text-lg-left text-center col-xl-4 col-lg-4 col-md-8 col-sm-10 col-12 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="/frontend/images/img16.png" alt="" class="img-fluid rounded"></div>
                        <h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Production & Final Pass</h5>
                        <p class="mb-xl-4 mb-lg-4 mb-md-5 mb-sm-2 mb-2">This is where we put in all our heart and soul into fulfilling our clients' expectations, requests and everything else that we finalize during initial research. The production team work swiftly and in sprints, reaching each goal one step at a time. Once the production is at the final stages, it goes through multiple passes along with QA to make sure that the product is ready for the market.</p>
                        <!-- <a href="" class="btn btn-link">Our plan of action<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a> -->
                    </div>
                    <div class="text-lg-left text-center col-xl-4 col-lg-4 col-md-8 col-sm-10 col-12 mt-xl-9 mt-lg-9 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="/frontend/images/img17.png" alt="" class="img-fluid rounded"></div>
                        <h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Implementation & Follow-up</h5>
                        <p class="mb-xl-4 mb-lg-4 mb-md-5 mb-sm-2 mb-2">Your product is complete! But that doesn't mean we wrap things up and move on. We strategically market your product to make sure it reaches the maximum number of eyeballs. Then we follow up for months, making sure that you're hitting your targeted goals, and how we can enhance it further. The possibilities are endless, and we'll be prepared.</p>
                        <!-- <a href="" class="btn btn-link">Read our success stories<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a> -->
                    </div>
                </div>
            </div>
        </section>

        <section class="section background-dark pos-r">
            <div class="container">
                <div class="section-header text-center">
                    <p class="seo-title">Our Products & Services</p>
                    <h3 class="section-heading">What We're Proud Of</h3>
                    <p class="sub-title">These products are the perfect answer to your hurdles</p>
                </div>
            </div>
            <div class="container-xl">
                <div class="row no-gutters justify-content-center">
                    <!-- <div class="product-hover-bg" style="background-image: url(/frontend/images/img1.jpg);"></div> -->
                    <div class="col-xxl-4 col-xl-4 col-lg-8 col-md-8 col-sm-10 col-12 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4 product-hoverfx" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <a target="_blank" href="http://www.scholarkeys.com/" class="product-block">
                            <div class="product-caption">
                                <h6 class="product-title">Scholarkeys</h6>
                                <svg class="arrow" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg>
                            </div>
                            <div class="product-img"><img class="img-fluid" src="/frontend/images/product_scholar_keys.png" alt=""></div>
                        </a>
                    </div>
                    <!-- <div class="product-hover-bg" style="background-image: url(/frontend/images/img1.jpg);"></div> -->
                    <div class="col-xxl-4 col-xl-4 col-lg-8 col-md-8 col-sm-10 col-12 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-4 mt-4 product-hoverfx" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <a target="_blank" href="https://hopperspass.com/" class="product-block">
                            <div class="product-caption">
                                <h6 class="product-title">Hopper's Pass</h6>
                                <svg class="arrow" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg>
                            </div>
                            <div class="product-img"><img class="img-fluid" src="/frontend/images/product_hoppers_pass.png" alt=""></div>
                        </a>
                    </div>
                    <div class="col-xxl-4 col-xl-4 col-lg-8 col-md-8 col-sm-10 col-12 product-hoverfx" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                        <a target="_blank" href="http://www.unclewebs.com/" class="product-block">
                            <div class="product-caption">
                                <h6 class="product-title">UncleWebs</h6>
                                <svg class="arrow" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg>
                            </div>
                            <div class="product-img"><img class="img-fluid" src="/frontend/images/product_uncle_webs.png" alt=""></div>
                        </a>
                    </div>
                    <!-- <div class="product-hover-bg" style="background-image: url(/frontend/images/img2.jpg);"></div> -->
                </div>
            </div>
        </section>

        <section class="section background-light has-bg-img">
            <div class="container">
                <div class="section-header text-center">
                    <p class="seo-title">Testimonials</p>
                    <h3 class="section-heading">Success Stories</h3>
                    <p class="sub-title">Success is a two way street for us - we both deserve a win</p>
                </div>

                <div class="glide" id="review">
                    <div data-glide-el="track" class="glide__track">
                        <ul class="glide__slides reviews-block">
                            <li class="glide__slide review-panel">
                                <div class="img-holder"><img src="/frontend/images/client1.jpg" alt=""></div>
                                <h5 class="reviewer-name">Noblemale</h5>
                                <p class="review-content">As we are newly established luxury brand, with a vision to expand our services all over Nepal. From the very start, Analogue Inc is working hard to take us to the Nepalese luxury market.</p>
                            </li>
                            <li class="glide__slide review-panel">
                                <div class="img-holder"><img src="/frontend/images/client2.jpg" alt=""></div>
                                <h5 class="reviewer-name">Regal Inc</h5>
                                <p class="review-content">I have been working with Analogue Inc from the past 3 months. With their effective marketing strategy, our business has increased significantly.</p>
                            </li>
                            <li class="glide__slide review-panel">
                                <div class="img-holder"><img src="/frontend/images/client3.jpg" alt=""></div>
                                <h5 class="reviewer-name">Natraj Boutique</h5>
                                <p class="review-content">Amazing branding strategy of Analogue Inc have transformed the way we do business. Which led to a considerable impact on our targeted market.</p>
                            </li>
                        </ul>
                    </div>

                    <div class="glide__arrows" data-glide-el="controls">
                        <button data-glide-dir="<"><svg class="review-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
                        <button data-glide-dir=">"><svg class="review-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></button>
                    </div>
                </div>
            </div>
            <div class="section-backgrounds">
                <img src="/frontend/images/arrow-lg.png" alt="" class="big-arrow">
                <img src="/frontend/images/arrow-sm.png" alt="" class="small-arrow">
            </div>
        </section>

        <section class="section pb-2x" id="consultation-form">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-xl-6 col-lg-6 col-12">
                        <div class="big-a-reflection">
                            <img src="/frontend/images/big-a-left.png" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="section-header pb-half">
                            <p class="seo-title">Consultation Form</p>
                            <h3 class="section-heading">Free Consultation</h3>
                            <p class="sub-title">Get an idea on how things work</p>
                        </div>
                        <p>We're glad you've decided to work with us. This is a start of something amazing & and we're as excited as you are! Tell us about you and your plans, your project, and what you wish to achieve at the end of the day, and we'll get back to you as soon as we can.</p>

                        <!-- START - We recommend to place the below code where you want the form in your website html  -->
                        <div class="sib-form" style="text-align: center; background-color: transparent;">
                            <div id="sib-form-container" class="sib-form-container">
                                <div id="error-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;max-width:540px; border-width:px;">
                                    <div class="sib-form-message-panel__text sib-form-message-panel__text--center">
                                        <svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
                                            <path d="M256 40c118.621 0 216 96.075 216 216 0 119.291-96.61 216-216 216-119.244 0-216-96.562-216-216 0-119.203 96.602-216 216-216m0-32C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm-11.49 120h22.979c6.823 0 12.274 5.682 11.99 12.5l-7 168c-.268 6.428-5.556 11.5-11.99 11.5h-8.979c-6.433 0-11.722-5.073-11.99-11.5l-7-168c-.283-6.818 5.167-12.5 11.99-12.5zM256 340c-15.464 0-28 12.536-28 28s12.536 28 28 28 28-12.536 28-28-12.536-28-28-28z"
                                            />
                                        </svg>
                                        <span class="sib-form-message-panel__inner-text">We&#039;re sorry, your message couldn&#039;t be sent. Please try again.</span>
                                    </div>
                                </div>
                                <div></div>
                                <div id="success-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#085229; background-color:#e7faf0; border-radius:3px; border-width:px; border-color:#13ce66;max-width:540px; border-width:px;">
                                    <div class="sib-form-message-panel__text sib-form-message-panel__text--center">
                                        <svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
                                            <path d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 464c-118.664 0-216-96.055-216-216 0-118.663 96.055-216 216-216 118.664 0 216 96.055 216 216 0 118.663-96.055 216-216 216zm141.63-274.961L217.15 376.071c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.97.068l68.976 69.533 163.441-162.13c4.705-4.667 12.303-4.637 16.97.068l8.451 8.52c4.668 4.705 4.637 12.303-.068 16.97z"
                                            />
                                        </svg>
                                        <span class="sib-form-message-panel__inner-text">Thank you for contacting us. We will get back to you as soon as possible.</span>
                                    </div>
                                </div>
                                <div></div>
                                <div id="sib-container" class="sib-container--large sib-container--vertical" style="text-align:center; background-color:transparent; max-width:540px; border-width:0px; border-color:#C0CCD9; border-style:solid;">
                                    <form id="sib-form" method="POST" action="https://486ae310.sibforms.com/serve/MUIEACaAmh2DxfAYdmXy9Nvxwgi6BZ0rjQTexEZT5W5ooSd5dqY2W9_1C5TFENk6XPlqHvH7A0l8IdbrtTjqaKGK3ibsMpVmB-B7cXPHxcr0moLTrRq6Bs29g8Is7PeUx11C11avbIDpHci89Gwm6vZYjwqLsL-TuGdgGllgHT0KEwZIotR1h06erra4FYlsj4g9tPsKpxHToI4D"
                                        data-type="subscription">
                                        <div style="padding: 8px 0;">
                                            <div class="sib-input sib-form-block">
                                                <div class="form__entry entry_block">
                                                    <div class="form__label-row ">
                                                        <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="FULL_NAME" data-required="*">
                                                            Your Full Name
                                                        </label>

                                                        <div class="entry__field">
                                                            <input class="input" maxlength="200" type="text" id="FULL_NAME" name="FULL_NAME" autocomplete="off" data-required="true" required />
                                                        </div>
                                                    </div>

                                                    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="padding: 8px 0;">
                                            <div class="sib-input sib-form-block">
                                                <div class="form__entry entry_block">
                                                    <div class="form__label-row ">
                                                        <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="EMAIL" data-required="*">
                                                            Your Email Address
                                                        </label>

                                                        <div class="entry__field">
                                                            <input class="input" type="text" id="EMAIL" name="EMAIL" autocomplete="off" data-required="true" required />
                                                        </div>
                                                    </div>

                                                    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="padding: 8px 0;">
                                            <div class="sib-sms-field sib-form-block">
                                                <div class="form__entry entry_block">
                                                    <div class="form__label-row ">
                                                        <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="SMS" data-required="*">
                                                            Your Phone Number
                                                        </label>

                                                        <div class="sib-sms-input-wrapper">
                                                            <div class="sib-sms-input" data-placeholder="Phone Number" data-required="1" data-country-code="NP" data-value="">
                                                                <div class="entry__field">
                                                                    <select class="input" name="SMS__COUNTRY_CODE" data-required="true">
                                                                        <option value="+93">
                                                                            +93 AF
                                                                        </option>
                                                                        <option value="+358">
                                                                            +358 AX
                                                                        </option>
                                                                        <option value="+355">
                                                                            +355 AL
                                                                        </option>
                                                                        <option value="+213">
                                                                            +213 DZ
                                                                        </option>
                                                                        <option value="+1684">
                                                                            +1684 AS
                                                                        </option>
                                                                        <option value="+376">
                                                                            +376 AD
                                                                        </option>
                                                                        <option value="+244">
                                                                            +244 AO
                                                                        </option>
                                                                        <option value="+1264">
                                                                            +1264 AI
                                                                        </option>
                                                                        <option value="+672">
                                                                            +672 AQ
                                                                        </option>
                                                                        <option value="+1268">
                                                                            +1268 AG
                                                                        </option>
                                                                        <option value="+54">
                                                                            +54 AR
                                                                        </option>
                                                                        <option value="+374">
                                                                            +374 AM
                                                                        </option>
                                                                        <option value="+297">
                                                                            +297 AW
                                                                        </option>
                                                                        <option value="+61">
                                                                            +61 AU
                                                                        </option>
                                                                        <option value="+43">
                                                                            +43 AT
                                                                        </option>
                                                                        <option value="+994">
                                                                            +994 AZ
                                                                        </option>
                                                                        <option value="+1242">
                                                                            +1242 BS
                                                                        </option>
                                                                        <option value="+973">
                                                                            +973 BH
                                                                        </option>
                                                                        <option value="+880">
                                                                            +880 BD
                                                                        </option>
                                                                        <option value="+1246">
                                                                            +1246 BB
                                                                        </option>
                                                                        <option value="+375">
                                                                            +375 BY
                                                                        </option>
                                                                        <option value="+32">
                                                                            +32 BE
                                                                        </option>
                                                                        <option value="+501">
                                                                            +501 BZ
                                                                        </option>
                                                                        <option value="+229">
                                                                            +229 BJ
                                                                        </option>
                                                                        <option value="+1441">
                                                                            +1441 BM
                                                                        </option>
                                                                        <option value="+975">
                                                                            +975 BT
                                                                        </option>
                                                                        <option value="+591">
                                                                            +591 BO
                                                                        </option>
                                                                        <option value="+599">
                                                                            +599 BQ
                                                                        </option>
                                                                        <option value="+387">
                                                                            +387 BA
                                                                        </option>
                                                                        <option value="+267">
                                                                            +267 BW
                                                                        </option>
                                                                        <option value="+47">
                                                                            +47 BV
                                                                        </option>
                                                                        <option value="+55">
                                                                            +55 BR
                                                                        </option>
                                                                        <option value="+246">
                                                                            +246 IO
                                                                        </option>
                                                                        <option value="+673">
                                                                            +673 BN
                                                                        </option>
                                                                        <option value="+359">
                                                                            +359 BG
                                                                        </option>
                                                                        <option value="+226">
                                                                            +226 BF
                                                                        </option>
                                                                        <option value="+257">
                                                                            +257 BI
                                                                        </option>
                                                                        <option value="+855">
                                                                            +855 KH
                                                                        </option>
                                                                        <option value="+237">
                                                                            +237 CM
                                                                        </option>
                                                                        <option value="+1">
                                                                            +1 CA
                                                                        </option>
                                                                        <option value="+238">
                                                                            +238 CV
                                                                        </option>
                                                                        <option value="+1345">
                                                                            +1345 KY
                                                                        </option>
                                                                        <option value="+236">
                                                                            +236 CF
                                                                        </option>
                                                                        <option value="+235">
                                                                            +235 TD
                                                                        </option>
                                                                        <option value="+56">
                                                                            +56 CL
                                                                        </option>
                                                                        <option value="+86">
                                                                            +86 CN
                                                                        </option>
                                                                        <option value="+61">
                                                                            +61 CX
                                                                        </option>
                                                                        <option value="+61">
                                                                            +61 CC
                                                                        </option>
                                                                        <option value="+57">
                                                                            +57 CO
                                                                        </option>
                                                                        <option value="+269">
                                                                            +269 KM
                                                                        </option>
                                                                        <option value="+242">
                                                                            +242 CG
                                                                        </option>
                                                                        <option value="+243">
                                                                            +243 CD
                                                                        </option>
                                                                        <option value="+682">
                                                                            +682 CK
                                                                        </option>
                                                                        <option value="+506">
                                                                            +506 CR
                                                                        </option>
                                                                        <option value="+225">
                                                                            +225 CI
                                                                        </option>
                                                                        <option value="+385">
                                                                            +385 HR
                                                                        </option>
                                                                        <option value="+53">
                                                                            +53 CU
                                                                        </option>
                                                                        <option value="+599">
                                                                            +599 CW
                                                                        </option>
                                                                        <option value="+357">
                                                                            +357 CY
                                                                        </option>
                                                                        <option value="+420">
                                                                            +420 CZ
                                                                        </option>
                                                                        <option value="+45">
                                                                            +45 DK
                                                                        </option>
                                                                        <option value="+253">
                                                                            +253 DJ
                                                                        </option>
                                                                        <option value="+1767">
                                                                            +1767 DM
                                                                        </option>
                                                                        <option value="+1809">
                                                                            +1809 DO
                                                                        </option>
                                                                        <option value="+593">
                                                                            +593 EC
                                                                        </option>
                                                                        <option value="+20">
                                                                            +20 EG
                                                                        </option>
                                                                        <option value="+503">
                                                                            +503 SV
                                                                        </option>
                                                                        <option value="+240">
                                                                            +240 GQ
                                                                        </option>
                                                                        <option value="+291">
                                                                            +291 ER
                                                                        </option>
                                                                        <option value="+372">
                                                                            +372 EE
                                                                        </option>
                                                                        <option value="+251">
                                                                            +251 ET
                                                                        </option>
                                                                        <option value="+500">
                                                                            +500 FK
                                                                        </option>
                                                                        <option value="+298">
                                                                            +298 FO
                                                                        </option>
                                                                        <option value="+679">
                                                                            +679 FJ
                                                                        </option>
                                                                        <option value="+358">
                                                                            +358 FI
                                                                        </option>
                                                                        <option value="+33">
                                                                            +33 FR
                                                                        </option>
                                                                        <option value="+594">
                                                                            +594 GF
                                                                        </option>
                                                                        <option value="+689">
                                                                            +689 PF
                                                                        </option>
                                                                        <option value="+262">
                                                                            +262 TF
                                                                        </option>
                                                                        <option value="+241">
                                                                            +241 GA
                                                                        </option>
                                                                        <option value="+220">
                                                                            +220 GM
                                                                        </option>
                                                                        <option value="+995">
                                                                            +995 GE
                                                                        </option>
                                                                        <option value="+49">
                                                                            +49 DE
                                                                        </option>
                                                                        <option value="+233">
                                                                            +233 GH
                                                                        </option>
                                                                        <option value="+350">
                                                                            +350 GI
                                                                        </option>
                                                                        <option value="+30">
                                                                            +30 GR
                                                                        </option>
                                                                        <option value="+299">
                                                                            +299 GL
                                                                        </option>
                                                                        <option value="+1473">
                                                                            +1473 GD
                                                                        </option>
                                                                        <option value="+590">
                                                                            +590 GP
                                                                        </option>
                                                                        <option value="+1671">
                                                                            +1671 GU
                                                                        </option>
                                                                        <option value="+502">
                                                                            +502 GT
                                                                        </option>
                                                                        <option value="+44">
                                                                            +44 GG
                                                                        </option>
                                                                        <option value="+224">
                                                                            +224 GN
                                                                        </option>
                                                                        <option value="+245">
                                                                            +245 GW
                                                                        </option>
                                                                        <option value="+592">
                                                                            +592 GY
                                                                        </option>
                                                                        <option value="+509">
                                                                            +509 HT
                                                                        </option>
                                                                        <option value="+672">
                                                                            +672 HM
                                                                        </option>
                                                                        <option value="+379">
                                                                            +379 VA
                                                                        </option>
                                                                        <option value="+504">
                                                                            +504 HN
                                                                        </option>
                                                                        <option value="+852">
                                                                            +852 HK
                                                                        </option>
                                                                        <option value="+36">
                                                                            +36 HU
                                                                        </option>
                                                                        <option value="+354">
                                                                            +354 IS
                                                                        </option>
                                                                        <option value="+91">
                                                                            +91 IN
                                                                        </option>
                                                                        <option value="+62">
                                                                            +62 ID
                                                                        </option>
                                                                        <option value="+98">
                                                                            +98 IR
                                                                        </option>
                                                                        <option value="+964">
                                                                            +964 IQ
                                                                        </option>
                                                                        <option value="+353">
                                                                            +353 IE
                                                                        </option>
                                                                        <option value="+44">
                                                                            +44 IM
                                                                        </option>
                                                                        <option value="+972">
                                                                            +972 IL
                                                                        </option>
                                                                        <option value="+39">
                                                                            +39 IT
                                                                        </option>
                                                                        <option value="+1876">
                                                                            +1876 JM
                                                                        </option>
                                                                        <option value="+81">
                                                                            +81 JP
                                                                        </option>
                                                                        <option value="+44">
                                                                            +44 JE
                                                                        </option>
                                                                        <option value="+962">
                                                                            +962 JO
                                                                        </option>
                                                                        <option value="+7">
                                                                            +7 KZ
                                                                        </option>
                                                                        <option value="+254">
                                                                            +254 KE
                                                                        </option>
                                                                        <option value="+686">
                                                                            +686 KI
                                                                        </option>
                                                                        <option value="+850">
                                                                            +850 KP
                                                                        </option>
                                                                        <option value="+82">
                                                                            +82 KR
                                                                        </option>
                                                                        <option value="+965">
                                                                            +965 KW
                                                                        </option>
                                                                        <option value="+996">
                                                                            +996 KG
                                                                        </option>
                                                                        <option value="+856">
                                                                            +856 LA
                                                                        </option>
                                                                        <option value="+371">
                                                                            +371 LV
                                                                        </option>
                                                                        <option value="+961">
                                                                            +961 LB
                                                                        </option>
                                                                        <option value="+266">
                                                                            +266 LS
                                                                        </option>
                                                                        <option value="+231">
                                                                            +231 LR
                                                                        </option>
                                                                        <option value="+218">
                                                                            +218 LY
                                                                        </option>
                                                                        <option value="+423">
                                                                            +423 LI
                                                                        </option>
                                                                        <option value="+370">
                                                                            +370 LT
                                                                        </option>
                                                                        <option value="+352">
                                                                            +352 LU
                                                                        </option>
                                                                        <option value="+853">
                                                                            +853 MO
                                                                        </option>
                                                                        <option value="+389">
                                                                            +389 MK
                                                                        </option>
                                                                        <option value="+261">
                                                                            +261 MG
                                                                        </option>
                                                                        <option value="+265">
                                                                            +265 MW
                                                                        </option>
                                                                        <option value="+60">
                                                                            +60 MY
                                                                        </option>
                                                                        <option value="+960">
                                                                            +960 MV
                                                                        </option>
                                                                        <option value="+223">
                                                                            +223 ML
                                                                        </option>
                                                                        <option value="+356">
                                                                            +356 MT
                                                                        </option>
                                                                        <option value="+692">
                                                                            +692 MH
                                                                        </option>
                                                                        <option value="+596">
                                                                            +596 MQ
                                                                        </option>
                                                                        <option value="+222">
                                                                            +222 MR
                                                                        </option>
                                                                        <option value="+230">
                                                                            +230 MU
                                                                        </option>
                                                                        <option value="+262">
                                                                            +262 YT
                                                                        </option>
                                                                        <option value="+52">
                                                                            +52 MX
                                                                        </option>
                                                                        <option value="+691">
                                                                            +691 FM
                                                                        </option>
                                                                        <option value="+373">
                                                                            +373 MD
                                                                        </option>
                                                                        <option value="+377">
                                                                            +377 MC
                                                                        </option>
                                                                        <option value="+976">
                                                                            +976 MN
                                                                        </option>
                                                                        <option value="+382">
                                                                            +382 ME
                                                                        </option>
                                                                        <option value="+1664">
                                                                            +1664 MS
                                                                        </option>
                                                                        <option value="+212">
                                                                            +212 MA
                                                                        </option>
                                                                        <option value="+258">
                                                                            +258 MZ
                                                                        </option>
                                                                        <option value="+95">
                                                                            +95 MM
                                                                        </option>
                                                                        <option value="+264">
                                                                            +264 NA
                                                                        </option>
                                                                        <option value="+674">
                                                                            +674 NR
                                                                        </option>
                                                                        <option value="+977">
                                                                            +977 NP
                                                                        </option>
                                                                        <option value="+31">
                                                                            +31 NL
                                                                        </option>
                                                                        <option value="+687">
                                                                            +687 NC
                                                                        </option>
                                                                        <option value="+64">
                                                                            +64 NZ
                                                                        </option>
                                                                        <option value="+505">
                                                                            +505 NI
                                                                        </option>
                                                                        <option value="+227">
                                                                            +227 NE
                                                                        </option>
                                                                        <option value="+234">
                                                                            +234 NG
                                                                        </option>
                                                                        <option value="+683">
                                                                            +683 NU
                                                                        </option>
                                                                        <option value="+672">
                                                                            +672 NF
                                                                        </option>
                                                                        <option value="+1670">
                                                                            +1670 MP
                                                                        </option>
                                                                        <option value="+47">
                                                                            +47 NO
                                                                        </option>
                                                                        <option value="+968">
                                                                            +968 OM
                                                                        </option>
                                                                        <option value="+92">
                                                                            +92 PK
                                                                        </option>
                                                                        <option value="+680">
                                                                            +680 PW
                                                                        </option>
                                                                        <option value="+970">
                                                                            +970 PS
                                                                        </option>
                                                                        <option value="+507">
                                                                            +507 PA
                                                                        </option>
                                                                        <option value="+675">
                                                                            +675 PG
                                                                        </option>
                                                                        <option value="+595">
                                                                            +595 PY
                                                                        </option>
                                                                        <option value="+51">
                                                                            +51 PE
                                                                        </option>
                                                                        <option value="+63">
                                                                            +63 PH
                                                                        </option>
                                                                        <option value="+64">
                                                                            +64 PN
                                                                        </option>
                                                                        <option value="+48">
                                                                            +48 PL
                                                                        </option>
                                                                        <option value="+351">
                                                                            +351 PT
                                                                        </option>
                                                                        <option value="+1787">
                                                                            +1787 PR
                                                                        </option>
                                                                        <option value="+974">
                                                                            +974 QA
                                                                        </option>
                                                                        <option value="+383">
                                                                            +383 XK
                                                                        </option>
                                                                        <option value="+262">
                                                                            +262 RE
                                                                        </option>
                                                                        <option value="+40">
                                                                            +40 RO
                                                                        </option>
                                                                        <option value="+7">
                                                                            +7 RU
                                                                        </option>
                                                                        <option value="+250">
                                                                            +250 RW
                                                                        </option>
                                                                        <option value="+590">
                                                                            +590 BL
                                                                        </option>
                                                                        <option value="+290">
                                                                            +290 SH
                                                                        </option>
                                                                        <option value="+1869">
                                                                            +1869 KN
                                                                        </option>
                                                                        <option value="+1758">
                                                                            +1758 LC
                                                                        </option>
                                                                        <option value="+599">
                                                                            +599 MF
                                                                        </option>
                                                                        <option value="+508">
                                                                            +508 PM
                                                                        </option>
                                                                        <option value="+1784">
                                                                            +1784 VC
                                                                        </option>
                                                                        <option value="+685">
                                                                            +685 WS
                                                                        </option>
                                                                        <option value="+378">
                                                                            +378 SM
                                                                        </option>
                                                                        <option value="+239">
                                                                            +239 ST
                                                                        </option>
                                                                        <option value="+966">
                                                                            +966 SA
                                                                        </option>
                                                                        <option value="+221">
                                                                            +221 SN
                                                                        </option>
                                                                        <option value="+381">
                                                                            +381 RS
                                                                        </option>
                                                                        <option value="+248">
                                                                            +248 SC
                                                                        </option>
                                                                        <option value="+232">
                                                                            +232 SL
                                                                        </option>
                                                                        <option value="+65">
                                                                            +65 SG
                                                                        </option>
                                                                        <option value="+599">
                                                                            +599 SX
                                                                        </option>
                                                                        <option value="+421">
                                                                            +421 SK
                                                                        </option>
                                                                        <option value="+386">
                                                                            +386 SI
                                                                        </option>
                                                                        <option value="+677">
                                                                            +677 SB
                                                                        </option>
                                                                        <option value="+252">
                                                                            +252 SO
                                                                        </option>
                                                                        <option value="+27">
                                                                            +27 ZA
                                                                        </option>
                                                                        <option value="+500">
                                                                            +500 GS
                                                                        </option>
                                                                        <option value="+211">
                                                                            +211 SS
                                                                        </option>
                                                                        <option value="+34">
                                                                            +34 ES
                                                                        </option>
                                                                        <option value="+94">
                                                                            +94 LK
                                                                        </option>
                                                                        <option value="+249">
                                                                            +249 SD
                                                                        </option>
                                                                        <option value="+597">
                                                                            +597 SR
                                                                        </option>
                                                                        <option value="+47">
                                                                            +47 SJ
                                                                        </option>
                                                                        <option value="+268">
                                                                            +268 SZ
                                                                        </option>
                                                                        <option value="+46">
                                                                            +46 SE
                                                                        </option>
                                                                        <option value="+41">
                                                                            +41 CH
                                                                        </option>
                                                                        <option value="+963">
                                                                            +963 SY
                                                                        </option>
                                                                        <option value="+886">
                                                                            +886 TW
                                                                        </option>
                                                                        <option value="+992">
                                                                            +992 TJ
                                                                        </option>
                                                                        <option value="+255">
                                                                            +255 TZ
                                                                        </option>
                                                                        <option value="+66">
                                                                            +66 TH
                                                                        </option>
                                                                        <option value="+670">
                                                                            +670 TL
                                                                        </option>
                                                                        <option value="+228">
                                                                            +228 TG
                                                                        </option>
                                                                        <option value="+690">
                                                                            +690 TK
                                                                        </option>
                                                                        <option value="+676">
                                                                            +676 TO
                                                                        </option>
                                                                        <option value="+1868">
                                                                            +1868 TT
                                                                        </option>
                                                                        <option value="+216">
                                                                            +216 TN
                                                                        </option>
                                                                        <option value="+90">
                                                                            +90 TR
                                                                        </option>
                                                                        <option value="+993">
                                                                            +993 TM
                                                                        </option>
                                                                        <option value="+1649">
                                                                            +1649 TC
                                                                        </option>
                                                                        <option value="+688">
                                                                            +688 TV
                                                                        </option>
                                                                        <option value="+256">
                                                                            +256 UG
                                                                        </option>
                                                                        <option value="+380">
                                                                            +380 UA
                                                                        </option>
                                                                        <option value="+971">
                                                                            +971 AE
                                                                        </option>
                                                                        <option value="+44">
                                                                            +44 GB
                                                                        </option>
                                                                        <option value="+1">
                                                                            +1 US
                                                                        </option>
                                                                        <option value="+246">
                                                                            +246 UM
                                                                        </option>
                                                                        <option value="+598">
                                                                            +598 UY
                                                                        </option>
                                                                        <option value="+998">
                                                                            +998 UZ
                                                                        </option>
                                                                        <option value="+678">
                                                                            +678 VU
                                                                        </option>
                                                                        <option value="+58">
                                                                            +58 VE
                                                                        </option>
                                                                        <option value="+84">
                                                                            +84 VN
                                                                        </option>
                                                                        <option value="+1284">
                                                                            +1284 VG
                                                                        </option>
                                                                        <option value="+1340">
                                                                            +1340 VI
                                                                        </option>
                                                                        <option value="+681">
                                                                            +681 WF
                                                                        </option>
                                                                        <option value="+212">
                                                                            +212 EH
                                                                        </option>
                                                                        <option value="+967">
                                                                            +967 YE
                                                                        </option>
                                                                        <option value="+260">
                                                                            +260 ZM
                                                                        </option>
                                                                        <option value="+263">
                                                                            +263 ZW
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="entry__field" style="width: 100%">
                                                                    <input type="tel" type="text" class="input" id="SMS" name="SMS" autocomplete="off" placeholder="Phone Number" data-required="true" required />
                                                                </div>
                                                            </div>
                                                            <div class="sib-sms-tooltip">
                                                                <div class="sib-sms-tooltip__box">
                                                                    The SMS field must contain between 6 and 19 digits and include the country code without using +/0 (e.g. 1xxxxxxxxxx for the United States)
                                                                </div>
                                                                <span class="sib-sms-tooltip__icon">?</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                                                    </label>
                                                    <label class="entry__error entry__error--secondary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="padding: 8px 0;">
                                            <div class="sib-select sib-form-block" data-required="true">
                                                <div class="form__entry entry_block">
                                                    <div class="form__label-row ">
                                                        <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="SERVICES_LIST" data-required="*">
                                                            What would you like to work on with us?
                                                        </label>
                                                        <div class="entry__field">
                                                            <select class="input" id="SERVICES_LIST" name="SERVICES_LIST" data-required="true">
                                                                <option value="" disabled selected hidden>Select one</option>

                                                                <option class="sib-menu__item" value="1">
                                                                    Branding &amp; Marketing
                                                                </option>
                                                                <option class="sib-menu__item" value="2">
                                                                    IT Solutions
                                                                </option>
                                                                <option class="sib-menu__item" value="3">
                                                                    E-Commerce
                                                                </option>
                                                                <option class="sib-menu__item" value="4">
                                                                    Computers
                                                                </option>
                                                                <option class="sib-menu__item" value="5">
                                                                    Press (Printing &amp; Merchandising)
                                                                </option>
                                                                <option class="sib-menu__item" value="6">
                                                                    Surveillance
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="padding: 8px 0;">
                                            <div class="sib-input sib-form-block">
                                                <div class="form__entry entry_block">
                                                    <div class="form__label-row ">
                                                        <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="COMPANY_NAME">
                                                            What&#039;s in your mind?
                                                        </label>

                                                        <div class="entry__field">
                                                            <textarea rows="2" class="input" maxlength="500" id="COMPANY_NAME" name="COMPANY_NAME" autocomplete="off" placeholder="Tell us about your project..."></textarea>
                                                        </div>
                                                    </div>

                                                    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="padding: 8px 0;">
                                            <div class="sib-captcha sib-form-block">
                                                <div class="form__entry entry_block">
                                                    <div class="form__label-row ">
                                                        <script>
                                                            function handleCaptchaResponse() {
                                                                var event = new Event('captchaChange');
                                                                document.getElementById('sib-captcha').dispatchEvent(event);
                                                            }
                                                        </script>
                                                        <div class="g-recaptcha sib-visible-recaptcha" id="sib-captcha" data-sitekey="6LctNfgUAAAAALtRKva7dr_mHEfLJoxtwOjgikf7" data-callback="handleCaptchaResponse"></div>
                                                    </div>
                                                    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="padding: 8px 0;">
                                            <div class="sib-form-block" style="text-align: left">
                                                <button class="sib-form-block__button sib-form-block__button-with-loader" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#FFFFFF; background-color:#02aeef; border-width:0px;" form="sib-form"
                                                    type="submit">
                                                    <svg class="icon clickable__icon progress-indicator__icon sib-hide-loader-icon" viewBox="0 0 512 512">
                                                        <path d="M460.116 373.846l-20.823-12.022c-5.541-3.199-7.54-10.159-4.663-15.874 30.137-59.886 28.343-131.652-5.386-189.946-33.641-58.394-94.896-95.833-161.827-99.676C261.028 55.961 256 50.751 256 44.352V20.309c0-6.904 5.808-12.337 12.703-11.982 83.556 4.306 160.163 50.864 202.11 123.677 42.063 72.696 44.079 162.316 6.031 236.832-3.14 6.148-10.75 8.461-16.728 5.01z"
                                                        />
                                                    </svg>
                                                    Send Message
                                                </button>
                                            </div>
                                        </div>

                                        <input type="text" name="email_address_check" value="" class="input--hidden">
                                        <input type="hidden" name="locale" value="en">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END - We recommend to place the below code where you want the form in your website html  -->
                    </div>
                </div>
            </div>
        </section>

        <section class="section background-dark has-bg-img social-section">
        <div class="container">
            <div class="section-header" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <p class="seo-title">Get Connected</p>
                <h3 class="section-heading">How You Can Reach Us</h3>
                <p class="sub-title">You can get in touch with us directly in various ways</p>
            </div>
            <p class="social-btns mb-xl-7 mb-lg-7 mb-md-6 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <a href="https://www.facebook.com/Analogue-Inc-111451123769739/" class="btn btn-primary btn-lg facebook" target="_blank">Like Us on Facebook<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                <a href="https://twitter.com/analogueinc" class="btn btn-primary btn-lg twitter" target="_blank">Follow Us on Twitter<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                <a href="https://www.instagram.com/analogueincnepal/" class="btn btn-primary btn-lg instagram" target="_blank">Follow Us on Instagram<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                <a href="https://www.linkedin.com/in/analogue-inc-1675941a2/" class="btn btn-primary btn-lg linkedin" target="_blank">Connect on LinkedIn<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
            </p>
            <div class="row pt-2">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-7 col-12 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                    <h5 class="social-subtitle">We're Here</h5>
                    <address class="social-address">
                        <p>
                            <span class="prefix">Location</span>
                            <span class="suffix">Pinglasthan, Gausala, Kathmandu</span>
                        </p>
                        <p>
                            <span class="prefix">Phone</span>
                            <span class="suffix">+977 9802320803</span>
                        </p>
                        <p>
                            <span class="prefix">Email</span>
                            <span class="suffix">info@analogueinc.com.np</span>
                        </p>
                    </address>
                    <a href="/contact#contact-map" class="btn btn-outline-primary">Get Directions<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-4 col-sm-5 col-12" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                    <h5 class="social-subtitle">DM Us On</h5>
                    <p class="social-direct">
                        <a target="_blank" href="https://wa.me/9779802320803" class="btn btn-primary whatsapp">WhatsApp<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        <a target="_blank" class="btn btn-primary viber">Viber<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        <a target="_blank" href="https://m.me/111451123769739" class="btn btn-primary messenger">Messenger<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                    </p>
                </div>
            </div>
        </div>

        <div class="section-backgrounds">
            <img src="/frontend/images/arrow-lg.png" alt="" class="big-arrow">
            <img src="/frontend/images/arrow-sm.png" alt="" class="small-arrow">
        </div>
    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
    $(document).ready(function () {
        function get_baseurl() {
            var base_url = "<?php echo URL::to('/') ?>";
            return base_url;
        }

        $('#spinner').hide();

        $('#index-form').on('submit',function(e){
            e.preventDefault();
            var full_name = $("#name").val();
            var phone = $("#phonenumber").val();
            var email = $("#email").val();
            var message = $("#comment").val();
            var base_url = get_baseurl();
            var data_from = 'index_page_form';
            toastr.options.timeOut = 1000;
            $.ajax({
                url: base_url + '/api/consultancy/create',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    "_token" : "{{ csrf_token() }}",
                    "full_name": full_name + data_from,
                    "phone": phone ,
                    "email": email,
                    "message": message,

                },

                beforeSend: function(){
                    $("#spinner").show();
                },

                complete: function(){
                    $("#spinner").hide();
                },
                success:function(data){
                    // alert('Sent Successfully');
                    if(data.code=='200')
                    {
                        var parent = document.getElementById('box');
                        var span = document.createElement('span');
                        span.setAttribute('class', 'alert alert-success');
                        span.innerHTML = 'Sent Successfully';
                        parent.parentNode.insertBefore(span,parent);
                        toastr.success('Sent Successfully');
                        // window.setTimeout(function () {
                        //     window.location.href = base_url +'/';
                        // }, 2000 );
                    }
                    else
                    {
                        toastr.error('There was some problem while proceeding. Please try again');
                    }
                }

            });
        });

    });

</script>

@endsection
