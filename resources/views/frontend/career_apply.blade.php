@extends('layouts.frontend')

@section('content')

<section class="section background-light has-bg-img is-page-title">
	<div class="container">
		<div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false">
			<h1 class="section-heading">Upload Your CV</h1>
			<p class="sub-title">Begin your path to bigger things</p>
		</div>
	</div>
	<div class="section-backgrounds">
		<img src="/frontend/images/arrow-lg.png" alt="" class="big-arrow">
		<img src="/frontend/images/arrow-sm.png" alt="" class="small-arrow">
	</div>
	<div id="bokeh-bg"></div>
	<div id="particle-bg"></div>
</section>
<main role="main">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12 mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
				<p class="mb-6">We're glad to learn that you're interested in working with us. Analogue Inc team is always looking to grow its family and we'd be more than happy to have you among us. To apply for the position you're interested in, please complete the form below.</p>
                <div id="box" class="mb-5"></div>
            	<form class="needs-validation pb-3 container" id="apply_form" method="POST">
					<div class="form-row">
						<div class="form-group col-md-6 col-12">
							<label for="name">Your Name</label>
							<input type="text" class="form-control" name="full_name">
						</div>
						<div class="form-group col-md-6 col-12">
							<label for="email">Email Address</label>
							<input type="email" class="form-control" name="email_address">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6 col-12">
							<label for="phone">Phone Number</label>
							<input type="text" class="form-control" name="phone_number">
						</div>
						<div class="form-group col-md-6 col-12">
							<label for="address">Where do you live? <small class="text-secondary">(optional)</small></label>
							<input type="text" class="form-control" name="address_line1">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6 col-12">
							<label for="expected_salary">Expected Salary</label>
							<input type="text" class="form-control" name="expected_salary">
						</div>
						<div class="form-group col-md-6 col-12">
							<label for="expected_salary">What position are you applying for?</label>
							{{-- <select class="custom-select">
								<option selected>Choose here..</option>
								<option value="">Project Manager</option>
								<option value="">Digital Marketing Specialist</option>
								<option value="">Marketing and Sales Executive</option>
								<option value="">Shopify Expert</option>
								<option value="">UI/UX Engineer / Angular Developer</option>
								<option value="">Internship - Digital Marketing Intern</option>
								<option value="">Internship - Graphic Design Intern</option>
								<option value="">Internship - Web Developer Intern</option>
							</select> --}}
							<input type="text" class="form-control" value="{{ $jobs->title }}" name="apply_for" placeholder="Apply For">
						</div>
					</div>
					<div class="form-row my-6">
						<div class="input-group col-md-6 col-12">
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="cv_file" aria-describedby="uploadcv">
								<label class="custom-file-label" for="uploadcv">Upload your CV</label>
							</div>
							<div class="input-group-append">
								<button class="btn btn-secondary" type="button" id="uploadfile1btn">Upload</button>
							</div>
						</div>
						<div class="input-group col-md-6 col-12">
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="cover_letter" aria-describedby="coverletter">
								<label class="custom-file-label" for="coverletter">Upload your cover letter</label>
							</div>
							<div class="input-group-append">
								<button class="btn btn-secondary" type="button" id="coverletter">Upload</button>
							</div>
						</div>
					</div>
					<div class="form-group">
                        <input type="hidden" value="<?php echo date("Y-m-d H:m");?>" class="form-control" name="apply_date">
                    </div>
					<p class="text-center">
						<button type="submit" id="submit-btn" class="btn btn-primary">Apply Now!<svg class="btn-icon" viewBox="0 0 24 24"><span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true"></span><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>
						<button type="reset" class="btn btn-outline-primary ml-4">Start Over<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
					</p>
				</form>
			</div>
		</div>
	</div>
</main>

<script src="/admin-resources/dist/js/global.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#spinner").hide();
    function get_baseurl() {
    var base_url = "<?php echo URL::to('/') ?>";
        return base_url;
    }

    $('#apply_form').on('submit', function(event){
        event.preventDefault();
        toastr.options.timeOut = 10000;

        var base_url = get_baseurl();

            $.ajax({
            url: base_url + '/api/career/create',
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,

            beforeSend: function(){
                $("#submit-btn").attr("disabled","disabled");
                $("#spinner").show();
            },

            complete: function(){
                $("#submit-btn").removeAttr("disabled","disabled");
                $("#spinner").hide();
            },

                success:function(data)
                {
                    if(data.code == 200 && data.status == true){
    					$("#spinner").hide();
                        var parent = document.getElementById('box');
                        var span = document.createElement('span');
                        span.setAttribute('class', 'alert alert-success');
                        span.innerHTML = 'Sent Successfully';
                        parent.parentNode.insertBefore(span,parent);
                        toastr.success('Sent Successfully');
                        window.setTimeout(function () {
                            window.location.href = base_url +'/careers';
                        }, 2000 );

                    }else{
                        toastr.error('Error');
                    }


                }
            })
        });
    });
</script>
@endsection
