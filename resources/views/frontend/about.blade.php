@extends('layouts.frontend')

@section('content')

<section class="section background-light has-bg-img is-page-title">
	<div class="container">
		<div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false">
			<h1 class="section-heading">About Us</h1>
			<p class="sub-title">Who we are and how we came to be</p>
		</div>
	</div>
	<div class="section-backgrounds">
		<img src="frontend/images/arrow-lg.png" alt="" class="big-arrow">
		<img src="frontend/images/arrow-sm.png" alt="" class="small-arrow">
	</div>

	<div id="bokeh-bg"></div>
	<div id="particle-bg"></div>
</section>

<main role="main">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xxl-8 col-xl-9 col-lg-10 col-12 mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
				<p class="lead text-dark">People often wonder about our name.. What does Analogue mean? Why Analogue? Why did you go for this name for your company? Does it have any meaning?</p>
				<p class="lead text-dark"><strong>Well actually Yes.. Yes, we do...</strong></p>
			</div>
			<div class="col-xxl-8 col-xl-9 col-lg-10 col-12 mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
				<p>Analogue comes from the ancient greek word Analogos which means using signals or information represented by a continuously variable physical quantity. The word can refer to something that is analogous (as in an analog organ), but it is most often used to distinguish analog electronics from digital electronics like an analog computer or an analog clock.</p>
				<p>It all started when a young boy decided he wants to achieve great things in life. Make a huge contribution to the IT world that can reshape the business for those who think and dream. While aiming for the star, he knew his time would come soon. He looked at his Analog watch and thought.. Maybe I can give my customer with an <strong>U</strong>ltimate <strong>E</strong>xperience naming his dream company Analog<strong>ue</strong>.</p>
				<p>Like our name, our ambition is to visualize your business with an interior analogue to an exterior world.</p>
				<p>With this vision, Analogue Incorporate plans to bring you astonishing services in different business sectors. From Men's Luxury fashion to Branding your company, software development to eCommerce service, we Analogue have a variety of projects under our wings providing our nation one stop solution with ambition to shape the future for the coming generation.</p>
				<p>Our Projects -</p>
				<ul>
					<li>NobleMale - We do Men’s right</li>
					<li>Analogue Mall - Innovate your world</li>
					<li>UncleWebs</li>
					<li>ScholarKeys</li>
					<li>EdenEx</li>
					<li>Hoppers Pass - Exploring made easy</li>
				</ul>
				<p>Do you have a Idea? Collaborate with us and watch your business grow like a Bamboo.</p>
			</div>
		</div>

		<div class="row no-gutters" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-1 order-sm-2 order-2 d-flex align-items-center">
				<div class="px-xl-8 px-lg-6 px-md-4 px-sm-6 px-4 py-md-0 py-sm-5 py-5">
					<h3 class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-3 mb-3">Who We Are</h3>
					<p>A group of experienced, young and professional people in the world of web and mobile technology, Analogue Inc. is an energetic startup software company engaged in application development software, web development, SAP related solutions, digital marketing, computers and overall branding and technology solutions.</p>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-2 order-sm-1 order-1">
				<div class="img-holder"><img src="frontend/images/img18.jpg" alt=""></div>
			</div>
		</div>

		<div class="row no-gutters mt-md-0 mt-sm-5 mt-5" data-aos="fade-right" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-2 d-flex align-items-center">
				<div class="px-xl-8 px-lg-6 px-md-4 px-sm-6 px-4 py-md-0 py-sm-5 py-5">
					<h3 class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-3 mb-3">Why choose Analogue Inc.</h3>
					<p>We have a right blend of award-winning designers, expert web developers and certified digital marketers which make us a unique one-stop solution for hundreds of our clients, spread across the country.</p>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-1">
				<div class="img-holder"><img src="frontend/images/img19.jpg" alt=""></div>
			</div>
		</div>

		<div class="row no-gutters mt-md-0 mt-sm-5 mt-5" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-1 order-sm-2 order-2 d-flex align-items-center">
				<div class="px-xl-8 px-lg-6 px-md-4 px-sm-6 px-4 py-md-0 py-sm-5 py-5">
					<h3 class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-3 mb-3">Our Mission</h3>
					<p>With a sole purpose to improvise efficiency by setting up right systems in place using latest tools and technology to make things easy &amp; benefit the end user, Analogue Inc. is excelling to empower business through technologies to help you stay ahead of times.</p>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-2 order-sm-1 order-1">
				<div class="img-holder"><img src="frontend/images/img20.jpg" alt=""></div>
			</div>
		</div>

	</div>
</main>

@endsection