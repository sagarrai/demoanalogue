<!DOCTYPE html>
<html lang="en">
<head>
	<title>Analogue Inc - Branding & Marketing</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="frontend/bm/images/favicon.png" rel="icon">
	<link rel="stylesheet" href="frontend/css/main.css">
	<link rel="stylesheet" href="frontend/css/main-inner.css">
	<link rel="stylesheet" href="frontend/bm/css/swiper.css">
	<script src="frontend/bm/js/jquery-3.4.1.min.js"></script>
	<script src="frontend/bm/js/bootstrap.bundle.min.js"></script>

	<script>
		$(document).ready(function(){
			// BS4.4 sticky scrollspy nav trigger
			$('body').scrollspy({
				target: '#sticky-header',
				offset: 75
			});
			$('.scrollto').click(function(event) {
				var $anchor = $(this);
				$('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 70 }, 750);
				event.preventDefault();
			});

			// BS4.4 off-canvas nav trigger
			$("[data-trigger]").on("click", function(e){
				e.preventDefault();
				e.stopPropagation();
				var offcanvas_id = $(this).attr('data-trigger');
				$(offcanvas_id).toggleClass("show");
				$('body').toggleClass("offcanvas-active");
				$(".screen-overlay").toggleClass("show");
			});
			$(".btn-close, .screen-overlay, .scrollto").click(function(e){
				$(".screen-overlay").removeClass("show");
				$(".mobile-offcanvas").removeClass("show");
				$("body").removeClass("offcanvas-active");
			});
		});
	</script>

</head>
<body>
	<div id="wrapper">
		<b class="screen-overlay"></b>
		<div id="sticky-header-holder" class="fixed-top">
			<div class="services-top">
				<div class="links">
					<a href="https://demo.analogueinc.com.np/" target="_blank">Home</a>
					<a class="active" href="index.php">Branding & Marketing</a>
					<a href="https://it.analogueinc.com.np/" target="_blank">IT Solutions</a>
					<a href="https://ecommerce.analogueinc.com.np/" target="_blank">E-Commerce</a>
					<a href="https://computers.analogueinc.com.np/" target="_blank">Computers</a>
					<a href="https://surveillance.analogueinc.com.np/" target="_blank">Security & Surveillance</a>
					<a href="https://press.analogueinc.com.np/" target="_blank">Analogue Press</a>
					<a href="https://demo.analogueinc.com.np/services" target="_blank">View all Services</a>
				</div>
				<div class="dropdown">
					<button class="btn btn-light btn-sm dropdown-toggle m-1" type="button" id="servicedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="btn-icon ml-0 mr-2" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg>Navigate to Services</button>
					<div class="dropdown-menu" aria-labelledby="servicedropdown">
						<a class="dropdown-item" href="https://www.analogueinc.com.np/" target="_blank">Home</a>
						<a class="dropdown-item" href="https://bm.analogueinc.com.np/">Branding & Marketing</a>
						<a class="dropdown-item" href="https://it.analogueinc.com.np/" target="_blank">IT Solutions</a>
						<a class="dropdown-item" href="https://ecommerce.analogueinc.com.np/" target="_blank">E-Commerce</a>
						<a class="dropdown-item" href="https://computers.analogueinc.com.np/" target="_blank">Computers</a>
						<a class="dropdown-item" href="https://surveillance.analogueinc.com.np/" target="_blank">Security & Surveillance</a>
						<a class="dropdown-item" href="https://press.analogueinc.com.np/" target="_blank">Analogue Press</a>
						<a class="dropdown-item" href="https://analogueinc.com.np/services" target="_blank">View all Services</a>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="logo"><a href="index.php"><img src="images/logo-bm.png" alt=""></a></div>

				<button class="btn btn-outline-secondary btn-sm ml-auto d-xl-none" type="button" data-toggle="collapse" data-trigger="#sticky-header">Menu</button>

				<nav id="sticky-header" class="mobile-offcanvas navbar-expand-xl navbar-light bg-white">
					<div class="offcanvas-header">  
						<button class="btn btn-link btn-close float-left mr-2"><svg class="btn-icon ml-0" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></button>
						<h6>Navigate</h6>
					</div>
					<ul class="navbar-nav">
						<li class="nav-item"><a href="#home-bm" class="scrollto nav-link">Home</a></li>
						<li class="nav-item"><a href="#scroll-to" class="scrollto nav-link">About</a></li>
						<li class="nav-item"><a href="#why-bm" class="scrollto nav-link">Services</a></li>
						<li class="nav-item"><a href="#how-bm" class="scrollto nav-link">Process</a></li>
						<li class="nav-item"><a href="#pricing" class="scrollto nav-link">Pricing</a></li>
						<!-- <li class="nav-item"><a href="http://test.analogueinc.com.np/blogs" class="nav-link" target="_blank">University</a></li> -->
						<li class="nav-item"><a href="#contact" class="scrollto nav-link">Contact</a></li>
					</ul>
				</nav>
			</div>
		</div>

		<div id="home-bm" class="swiper-container ai-s-slider">
			<div class="ai-s-slider-parallax" style="background-image:url(frontend/images/ai-s-slider-img1.png);" data-swiper-parallax="-40%"></div>
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="ai-s-slider--caption-mega-stroke" data-swiper-parallax="300">branding &<br>marketing</div>
					<div class="ai-s-slider--caption-bottom">
						<div class="ai-s-slider--caption-regular" data-swiper-parallax="-1000">From developing a corporate or e-commerce site to creating enterprise and mobile application, Analogue | Branding & Marketing Solution can provide customized solutions that are as unique as your business.</div>
						<div class="ai-s-slider--img1" data-swiper-parallax="-250">
							<img src="frontend/images/ai-s-slider-img2.png" alt="">
						</div>
					</div>
				</div>
				<div class="swiper-slide ai-s-slider-slide2">
					<div class="ai-s-slider-images">
						<div class="ai-s-slider--img2" data-swiper-parallax="-500">
							<img src="frontend/images/ai-s-slider-img3.png" alt="">
						</div>
						<div class="ai-s-slider--img1" data-swiper-parallax="-800">
							<img src="frontend/images/ai-s-slider-img2.png" alt="">
						</div>
					</div>
					<div class="ai-s-slider--caption-large" data-swiper-parallax="-1000">
						<p>Prepare to</p>
						<p>stand out</p>
						<p>from the rest</p>
					</div>
				</div>
				<div class="swiper-slide ai-s-slider-slide3">
					<div class="ai-s-slider--caption-mega-stroke" data-swiper-parallax="-1000">Begin your<br>journey</div>
					<a href="#scroll-to" class="scrollto"><svg class="ai-s-slider-down-icon" viewBox="0 0 24 24"><path d="M12 24l8-8 -1.4-1.4 -5.6 5.6V0h-2v20.2l-5.6-5.6L4 16 12 24z"/></svg></a>
				</div>
			</div>
			<!-- Add Navigation -->
			<div class="swiper-button-prev swiper-button-black"></div>
			<div class="swiper-button-next swiper-button-black"></div>
		</div>

		<section id="scroll-to" class="section ai-s-section">
			<div class="container">
				<div class="section-header text-center pb-half">
					<p class="seo-title">About Branding & Marketing</p>
					<h3 class="section-heading">Why Is Branding Important?</h3>
				</div>
				<div class="row justify-content-center">
					<div class="col-xl-8 col-lg-9 col-md-10 col-sm-12 col-12" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<p class="text-center">Branding is not just about your logo or graphic elements. It goes way beyond that. Your brand is the way your customer perceives you. When you think about your brand, you need to think about your whole customer's experience. From Website to Social media experience, Logo to the way you answer phone and to the way your customer experiences your team.</p>
						<p class="text-center">It is very critical to your business and the overall impact it makes to your company.</p>
						<p class="text-center">
							<a href="#why-bm" class="btn btn-primary scrollto">How do I get started?<svg class="btn-icon" viewBox="0 0 24 24"><path d="M12 24l8-8 -1.4-1.4 -5.6 5.6V0h-2v20.2l-5.6-5.6L4 16 12 24z"/></svg></a>
						</p>
					</div>
				</div>
			</div>
		</section>

		<section id="why-bm" class="section ai-s-section background-light">
			<div class="container">
				<div class="section-header text-center pb-half">
					<p class="seo-title">Our Services</p>
					<h3 class="section-heading">What do we offer</h3>
					<p class="sub-title">Branding and marketing isn't something you should avoid</p>
				</div>
				<div class="row justify-content-center">
					<div id="qlbm1" class="text-md-left text-center col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="frontend/bm/images/img22.jpg" alt="" class="img-fluid rounded"></div>
						<h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Brand Guidelines</h5>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2">It's not just a kick-ass logo and then job done, we can develop you a brand document, giving your business the exact colors, fonts and details...</p>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2"><a href="" data-toggle="modal" data-target="#offer1" class="btn btn-primary btn-sm">Learn More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
					</div>
					<div id="qlbm2" class="text-md-left text-center col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="frontend/bm/images/img23.jpg" alt="" class="img-fluid rounded"></div>
						<h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Social Media Marketing</h5>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2">Brand awareness and brand loyalty do not happen magically. Your business needs engaging text and high-impact visuals draw customers in...</p>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2"><a href="" data-toggle="modal" data-target="#offer2" class="btn btn-primary btn-sm">Learn More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
					</div>
					<div id="qlbm3" class="text-md-left text-center col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12 mt-xl-0 mt-lg-0 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="frontend/bm/images/img24.jpg" alt="" class="img-fluid rounded"></div>
						<h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Email Marketing</h5>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2">Let email marketing be your friend in need with its commercial message distribution with the goal of driving your sales and increasing...</p>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2"><a href="" data-toggle="modal" data-target="#offer3" class="btn btn-primary btn-sm">Learn More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
					</div>
					<div class="text-md-left text-center col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12 mt-xl-6 mt-lg-5 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="frontend/bm/images/img25.jpg" alt="" class="img-fluid rounded"></div>
						<h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Search Engine Optimization</h5>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2">Do you want to stay ahead of your competition? Good - Because, Search Engine Optimization or "SEO" is the process of making your website...</p>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2"><a href="" data-toggle="modal" data-target="#offer4" class="btn btn-primary btn-sm">Learn More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
					</div>
					<div class="text-md-left text-center col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12 mt-xl-6 mt-lg-5 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="frontend/bm/images/img26.jpg" alt="" class="img-fluid rounded"></div>
						<h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Digital Marketing Solution</h5>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2">Regardless of your business type, you need a digital presence. Oftentimes, business owners are overwhelmed at the idea of managing their...</p>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2"><a href="" data-toggle="modal" data-target="#offer5" class="btn btn-primary btn-sm">Learn More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
					</div>
					<div class="text-md-left text-center col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12 mt-xl-6 mt-lg-5 mt-md-5 mt-sm-5 mt-5" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="img-holder mb-xl-5 mb-lg-5 mb-md-4 mb-sm-3 mb-3"><img src="frontend/bm/images/img27.jpg" alt="" class="img-fluid rounded"></div>
						<h5 class="mb-xl-3 mb-lg-3 mb-md-2 mb-sm-2 mb-2">Merchandising</h5>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2">It is important to promote your product with visual attractiveness. With merchandising service, you can not only brand your company...</p>
						<p class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-2 mb-2"><a href="" data-toggle="modal" data-target="#offer6" class="btn btn-primary btn-sm">Learn More<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a></p>
					</div>
				</div>
				<div class="row justify-content-center" data-aos="fade-up" data-aos-delay="0" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
					<div class="col-xl-8 col-lg-9 col-md-10 col-12">
						<p class="text-center mt-6">Branding is not just about your logo or graphic elements. It goes way beyond that. Your brand is the way your customer perceives you. When you think about your brand, you need to think about your whole customer's experience. From Website to Social media experience, Logo to the way you answer phone and to the way your customer experiences your team.</p>
						<p class="text-center">
							<a href="#how-bm" class="btn btn-primary scrollto">So how does it work?<svg class="btn-icon" viewBox="0 0 24 24"><path d="M12 24l8-8 -1.4-1.4 -5.6 5.6V0h-2v20.2l-5.6-5.6L4 16 12 24z"/></svg></a>
						</p>
					</div>
				</div>
			</div>
		</section>

		<section id="how-bm" class="section ai-s-section background-dark">
			<div class="container">
				<div class="section-header text-center pb-half">
					<p class="seo-title">Our Branding & Marketing Process</p>
					<h3 class="section-heading">How do we do it?</h3>
					<p class="sub-title">This is where the fun begins for all of us</p>
				</div>
				<div class="row justify-content-center">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12" data-aos="fade-right" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="ai-card ai-card-dark has-hover h-100">
							<h5 class="ai-card-title">Research & Analysis</h5>
							<p>Before we get started, we explore on your requirements to help you achieve a competitive advantage for your business. We believe, a good research will always set your business to a right path and that is what we do best.</p>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12 mt-md-0 mt-5" data-aos="fade-right" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="ai-card ai-card-dark has-hover h-100">
							<h5 class="ai-card-title">Strategies & Creation</h5>
							<p>Discovering a right path is not the key to success, the key is to plan the execution and develop a roadmap to success. With our team of expert, we will develop a strategy and plan our execution that sets your business to a goal you dream to achieve.</p>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-10 col-12 mt-lg-0 mt-5" data-aos="fade-right" data-aos-delay="400" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
						<div class="ai-card ai-card-dark has-hover h-100">
							<h5 class="ai-card-title">Deployment</h5>
							<p>Lastly, we bring all of these resources into effective action.</p>
							<h6 class="ai-card-cnt-title">We create brands that people love, <br>Build a brand that lasts longer.</h6>
						</div>
				</div>
			</div>
		</section>

		<section id="pricing" class="section ai-s-section pb-0">
			<div class="container">
				<div class="section-header text-center pb-half">
					<p class="seo-title">Our Packages</p>
					<h3 class="section-heading">How Much Does it cost?</h3>
					<p class="sub-title">Prices that are too good to pass</p>
				</div>
				<p class="text-center">Are you looking to give your business a fresh, new look? Do you plan to redesign your branding and marketing materials but don't know where to start? Maybe you're looking to increase your audience engagement, or to simply stand out in the crowd. What if you could get all of that for an amazing price and zero hassle? We provide you with various forms of branding and marketing tools and materials, and all you have to do is sit back and relax. Check out our prices for various packages.</p>
			</div>

			<div class="container-xxl">
				<div class="subsection-hdr divider-top text-center">
					<h5 class="subsection-hdr-title">Branding Packages</h5>
					<div class="row justify-content-center">
						<div class="col-xl-7 col-lg-9 col-12">
							<p class="subsection-hdr-lead">We have four different Branding Packages based on your needs and preferences. We provide the fundamentals like Logos, Social Media Assets, Email Signatures, and Business Cards, as well as Style Guidelines, Cover Letters, Merchandising, etc among other branding materials.</p>
						</div>
					</div>
				</div>
				<div class="row justify-content-center res-col">
					<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-5 col-sm-6 col-12">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Basic</h4>
								<p class="ai-price-h-subtitle">One Time Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">10,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Style Guidelines</li>
									<li>Logo</li>
									<li>Typography</li>
									<li>Social Media Assets</li>
									<li>Email Signature</li>
									<li>Business Card</li>
									<li>Letterhead</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Basic Type" data-action="basic-branding" data-id="Branding and Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Basic</button></p>
							</footer>
						</section>
					</div>

					<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-5 col-sm-6 col-12 mt-sm-0 mt-3">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Standard</h4>
								<p class="ai-price-h-subtitle">One Time Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">20,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Everything included in the Basic Package, plus</li>
									<li>Branding Templates</li>
									<li>Cover Letter</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Standard Type" data-action="standard-branding" data-id="Branding and Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Standard</button></p>
							</footer>
						</section>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-5 col-sm-6 col-12 mt-lg-0 mt-3">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Premium</h4>
								<p class="ai-price-h-subtitle">One Time Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">50,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Everything included in the Standard Package, plus</li>
									<li>Signage</li>
									<li>Merchandising <a class="ai-price-c-list-icon" data-toggle="modal" data-target="#merchandising"><svg viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a></li>
									<li>Leaflets</li>
									<li>Envelope</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Premium Type" data-action="premium-branding" data-toggle="modal" data-id="Branding and Marketing" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Premium</button></p>
							</footer>
						</section>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-5 col-sm-6 col-12 mt-lg-0 mt-3">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Professional</h4>
								<p class="ai-price-h-subtitle">One Time Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">70,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Everything included in the Premium Package, plus</li>
									<li>Icon Pack</li>
									<li>Brochure</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Professional Type" data-action="professional-branding" data-id="Branding and Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Professional</button></p>
							</footer>
						</section>
					</div>
				</div>
			</div>
            <p class="text-center my-4">
                <a href="" class="btn btn-outline-primary" data-toggle="modal" data-target="#pkg-compare-brand">Compare Packages</a>
            </p>
			<div class="subsection-ftr grad-divider">
				<div class="container-xl">
					<h6 class="text-center text-primary">Don't like what you see?</h6>
					<h5 class="subsection-ftr-title text-center">Make your own package!</h5>
					<div class="row justify-content-center text-center">
						<div class="col-xl-7 col-lg-9 col-12">
							<p class="subsection-ftr-lead">You can choose what you need and what you don't need from what we offer, and create your own package based on your choices. The final price will be calculated accordingly.</p>

							<p><a href="/branding_package" class="btn btn-primary">Make My Package</a></p>
						</div>
					</div>
				</div>
			</div>

			<div class="container-xxl">
				<div class="subsection-hdr divider-top text-center">
					<h5 class="subsection-hdr-title">Social Media Marketing Packages</h5>
					<div class="row justify-content-center">
						<div class="col-xl-7 col-lg-9 col-12">
							<p class="subsection-hdr-lead">We have three different Social Media Marketing Packages based on your needs and preferences. We provide the fundamentals like Caption Updates, a number of posters a week, Strategic Campaign Running, Post Distribution & Management, Analysis & Re-targeting, etc among other branding materials.</p>
						</div>
					</div>
				</div>
				<div class="row justify-content-center res-col">
					<div class="col-xxl-3 col-xl-3 col-lg-4 col-md-4 col-sm-7 col-12">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Basic</h4>
								<p class="ai-price-h-subtitle">Monthly Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">5,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Marketing Guidelines</li>
									<li>1 post per week</li>
									<li>Caption Update</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Basic Type"data-action="basic-branding"  data-id="Social Media Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Basic</button></p>
							</footer>
						</section>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-4 col-md-4 col-sm-7 col-12 mt-md-0 mt-3">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Standard</h4>
								<p class="ai-price-h-subtitle">Monthly Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">10,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Everything from Basic Package, plus</li>
									<li>2 posts per week</li>
									<li>Strategic Campaign Running</li>
									<li>Post Distribution & Management</li>
									<li>Analysis & Re-Targeting</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Standard Type"data-action="standard-branding" data-id="Social Media Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Standard</button></p>
							</footer>
						</section>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-4 col-md-4 col-sm-7 col-12 mt-md-0 mt-3">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Professional</h4>
								<p class="ai-price-h-subtitle">Monthly Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">20,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Everything from Standard Package, plus</li>
									<li>3 posts per week</li>
									<li>Audience Identification & Distribution</li>
									<li>Online Reputation Management</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Professional Type"data-action="professional-branding"  data-id="Social Media Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Professional</button></p>
							</footer>
						</section>
					</div>
				</div>
			</div>
			<div class="subsection-ftr grad-divider">
				<div class="container-xl">
					<h6 class="text-center text-primary">Don't like what you see?</h6>
					<h5 class="subsection-ftr-title text-center">Make your own package!</h5>
					<div class="row justify-content-center text-center">
						<div class="col-xl-7 col-lg-9 col-12">
							<p class="subsection-ftr-lead">You can choose what you need and what you don't need from what we offer, and create your own package based on your choices. The final price will be calculated accordingly.</p>

							<p><a href="/social_package" class="btn btn-primary">Make My Package</a></p>
						</div>
					</div>
				</div>
			</div>

			<div class="container-xxl">
				<div class="subsection-hdr divider-top text-center">
					<h5 class="subsection-hdr-title">Email Marketing Packages</h5>
					<div class="row justify-content-center">
						<div class="col-xl-7 col-lg-9 col-12">
							<p class="subsection-hdr-lead">We have three different Email Marketing Packages based on your needs and preferences. We provide the fundamentals like Email Marketing Guidelines, Email Templates, Newsletter, Custom Branding, Marketing CRM, Re-targeting Audience, etc among other branding materials.</p>
						</div>
					</div>
				</div>
				<div class="row justify-content-center res-col">
					<div class="col-xxl-3 col-xl-3 col-lg-4 col-md-4 col-sm-7 col-12">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Basic</h4>
								<p class="ai-price-h-subtitle">Monthly Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">8,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Email Marketing Guidelines</li>
									<li>2000 Contacts</li>
									<li>5 Email Templates</li>
									<li>1 Newsletter per week</li>
									<li>Custom Branding</li>
									<li>Marketing CRM</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Basic Type"data-action="basic-branding"  data-id="Email Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Basic</button></p>
							</footer>
						</section>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-4 col-md-4 col-sm-7 col-12 mt-md-0 mt-3">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Standard</h4>
								<p class="ai-price-h-subtitle">Monthly Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">10,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Email Marketing Guidelines</li>
									<li>3000 Contacts</li>
									<li>10 Email Templates</li>
									<li>2 Newsletters per week</li>
									<li>Custom Branding</li>
									<li>Marketing CRM</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Standard Type" data-action="standard-branding"  data-id="Email Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Standard</button></p>
							</footer>
						</section>
					</div>
					<div class="col-xxl-3 col-xl-3 col-lg-4 col-md-4 col-sm-7 col-12 mt-md-0 mt-3">
						<section class="ai-price h-100">
							<header class="ai-price-h">
								<div class="ai-price-h-icons">
									<p class="ai-price-h-fix">
										<span class="ai-price-h-fix-ico">
											<svg viewBox="0 0 20 20" class="ai-price-h-fix-ico-i"><path d="M17,9.5v6c0,0.83-0.67,1.5-1.5,1.5h-11C3.67,17,3,16.33,3,15.5v-6C3,8.67,3.67,8,4.5,8h0.75V5.75C5.25,3.13,7.38,1,10,1s4.75,2.13,4.75,4.75V8h0.75C16.33,8,17,8.67,17,9.5z M12.25,5.75c0-1.24-1.01-2.25-2.25-2.25S7.75,4.51,7.75,5.75V8h4.5V5.75z"/></svg>
										</span>
										<span class="ai-price-h-fix-txt">Fixed Package</span>
									</p>
								</div>
								<h4 class="ai-price-h-title">Professional</h4>
								<p class="ai-price-h-subtitle">Monthly Payment</p>
								<p class="ai-price-h-prc">
									<span class="ai-price-h-prc-pfx">Rs.</span>
									<span class="ai-price-h-prc-amt">20,000</span>
									<span class="ai-price-h-prc-sfx">per brand</span>
								</p>
							</header>
							<div class="ai-price-c">
								<p class="ai-price-c-lead">What you'll get -</p>
								<ul class="ai-price-c-list">
									<li>Email Marketing Guidelines</li>
									<li>Unlimited Contacts</li>
									<li>Unlimited Templates</li>
									<li>3 Newsletter per week</li>
									<li>Custom Branding</li>
									<li>Marketing CRM</li>
									<li>Product or Service Update</li>
									<li>Re-targeting Audience</li>
									<li>Automation Series</li>
								</ul>
							</div>
							<footer class="ai-price-f">
								<p class="ai-price-f-btn"><button data-type="Professional Type"data-action="professional-branding"  data-id="Email Marketing" data-toggle="modal" data-target="#pkg-modal-brand" class="btn btn-primary btn-action">Choose Professinal</button></p>
							</footer>
						</section>
					</div>
				</div>
			</div>
			<div class="subsection-ftr grad-divider">
				<div class="container-xl">
					<h6 class="text-center text-primary">Don't like what you see?</h6>
					<h5 class="subsection-ftr-title text-center">Make your own package!</h5>
					<div class="row justify-content-center text-center">
						<div class="col-xl-7 col-lg-9 col-12">
							<p class="subsection-ftr-lead">You can choose what you need and what you don't need from what we offer, and create your own package based on your choices. The final price will be calculated accordingly.</p>

							<p><a href="/email_package" class="btn btn-primary">Make My Package</a></p>
						</div>
					</div>
				</div>
			</div>
		</section>


    </div>


	<div class="modal fade" id="merchandising" tabindex="-1" role="dialog" aria-labelledby="socialmediaassetstitle" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="socialmediaassetstitle">What is merchandising?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>It is important to promote your product with visual attractiveness. With merchandising service, you can not only brand your company, you can also brand your products. Merchandising seems simple on the surface but it's more of a science than art. Merchandising can make very good impact on your business to increase sales by making your business more appealing to the eyes of the beholder.</p>
					<div class="img-holder"><img src="frontend/bm/images/img9.jpg" alt=""></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="socialmediaassets" tabindex="-1" role="dialog" aria-labelledby="socialmediaassetstitle" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="socialmediaassetstitle">What are Social Media Assets?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Social Media Assets cover things like profile photos, cover photos, post templates, etc for your social media pages.</p>
					<p>Below is an image which will act like a preview for Social Media Assets -</p>
					<div class="row">
						<div class="col-md-6 col-12"><div class="img-holder"><img src="frontend/bm/images/img7.png" alt=""></div></div>
						<div class="col-md-6 col-12 mt-md-0 mt-4"><div class="img-holder"><img src="frontend/bm/images/img8.png" alt=""></div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="onlinereputation" tabindex="-1" role="dialog" aria-labelledby="onlinereputationtitle" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="onlinereputationtitle">What is Online Reputation Management?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>The good and bad thing about the internet is an open forum that allows people to say whatever they want. There is no control over what your customer is going to say in the future.</p>
					<p>Online reputation management is all about managing your online review portal in such way that your company comes out looking good despite negative review. It is all about the way we respond to the bad press. Online Reputation Management never seeks to hide a negative review from your review portal because everyone knows the fact that no business will be able to keep all their customer happy all the time.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="offer1" tabindex="-1" role="dialog" aria-labelledby="offer1title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1" id="offer1title">Brand Guidelines</h5>
						<p class="text-primary lh-1">Get your style guidelines</p>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>It's not just a kick-ass logo and then job done, we can develop you a brand document, giving your business the exact colors, fonts and details to be coherent and consistent right across your business and marketing communications. Analogue| Branding & Marketing Solution can provide you a solution that is as unique as your business.</p>
					<div class="row">
						<div class="col-md-6 col-12"><div class="img-holder"><img src="frontend/images/img1.jpg" alt=""></div></div>
						<div class="col-md-6 col-12 mt-md-0 mt-4"><div class="img-holder"><img src="frontend/images/img2.jpg" alt=""></div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="offer2" tabindex="-1" role="dialog" aria-labelledby="offer2title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1" id="offer2title">Social Media Marketing</h5>
						<p class="text-primary lh-1">All you add is "Social Media Marketing"</p>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Brand awareness and brand loyalty do not happen magically. Your business needs engaging text and high-impact visuals draw customers in and speak to their needs or create them. We will develop and run your social media platform to launch your business to the better future.</p>
					<div class="row">
						<div class="col-12"><div class="img-holder"><img src="frontend/images/img3.jpg" alt=""></div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="offer3" tabindex="-1" role="dialog" aria-labelledby="offer3title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1" id="offer3title">Email Marketing</h5>
						<p class="text-primary lh-1">"Email Marketing" is your friend</p>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Let email marketing be your friend in need with its commercial message distribution with the goal of driving your sales and increasing customer’s loyalty. We believe communicating important information to your subscribers plays a crucial role to increase your revenue by collaborating and sharing notification and updates through Email Marketing. </p>
					<div class="row">
						<div class="col-md-6 col-12"><div class="img-holder"><img src="frontend/images/img4.jpg" alt=""></div></div>
						<div class="col-md-6 col-12 mt-md-0 mt-4"><div class="img-holder"><img src="frontend/images/img5.jpg" alt=""></div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="offer4" tabindex="-1" role="dialog" aria-labelledby="offer4title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1" id="offer4title">Search Engine Optimization</h5>
						<p class="text-primary lh-1">Where is your SEO?</p>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Do you want to stay ahead of your competition? Good - Because, Search Engine Optimization or "SEO" is the process of making your website more appealing to the search engines. Many factors, from the keywords used in the title and text of your webpages, to the time users spend on your website, and even the number of links to and from your website. SEO professionals use a variety of tactics to improve rankings and stay ahead with your website listing.</p>
					<p>Ultimately, SEO is just like every business, aiming to please their customers.</p>
					<p>Let us help you find your SEO for your business.</p>
					<div class="row">
						<div class="col-12"><div class="img-holder"><img src="frontend/images/img6.jpg" alt=""></div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="offer5" tabindex="-1" role="dialog" aria-labelledby="offer5title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1" id="offer5title">Digital Marketing Solution</h5>
						<p class="text-primary lh-1">Digital Marketing Solution - your game</p>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Regardless of your business type, you need a digital presence. Often times, business owners are overwhelmed at the idea of managing their own digital marketing strategy, falling victim to the idea that they must be everywhere. When you consider the sheer number of platforms available - it's no wonder so many strategies fail.</p>
					<p>With a sole purpose of increasing your revenue by setting up right systems & strategies in place using latest tools and technology to make your business shine better than ever. Analogue Inc. is excelling to empower business through technologies to help you stay ahead of times.</p>
					<div class="row">
						<div class="col-md-6 col-12"><div class="img-holder"><img src="frontend/images/img7.jpg" alt=""></div></div>
						<div class="col-md-6 col-12 mt-md-0 mt-4"><div class="img-holder"><img src="frontend/images/img8.jpg" alt=""></div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="offer6" tabindex="-1" role="dialog" aria-labelledby="offer6title" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1" id="offer6title">Merchandising</h5>
						<p class="text-primary lh-1">Hey, have you tried "Merchandising"?</p>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>It is important to promote your product with visual attractiveness. With merchandising service, you can not only brand your company, you can also brand your products. Merchandising seems simple on the surface but it's more of a science than art. Merchandising can make very good impact on your business to increase sales by making your business more appealing to the eyes of the beholder.</p>
					<div class="row">
						<div class="col-md-6 col-12"><div class="img-holder"><img src="frontend/images/img9.jpg" alt=""></div></div>
						<div class="col-md-6 col-12 mt-md-0 mt-4"><div class="img-holder"><img src="frontend/images/img10.jpg" alt=""></div></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	{{-- Dyanmic Modal Starts--}}
	<div class="modal fade" id="pkg-modal-brand" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1" id="dynamic_package_title">Package name here</h5>
						<p class="text-primary lh-1" id="dynamic_module_name">Module name here</p>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row" id="box">
						<div class="col-lg-7 col-12">
							<form class="contact-form" id="package-form">
								<input type="hidden" class="form-control" id="package_name">
								<input type="hidden" class="form-control" id="package_type">
								<fieldset class="form-group contact-full-name">
									<div class="input-group">
										<div class="input-group-prepend">
											<select class="custom-select">
												<option>Mr.</option>
												<option>Ms.</option>
												<option>Mrs.</option>
												<option>Other</option>
											</select>
										</div>
										<input type="text" class="form-control" id="full_name" placeholder="Your Full Name" required="required">
									</div>
								</fieldset>
								<fieldset class="form-group">
									<input type="email" class="form-control" id="email" placeholder="Your Email Address" required >
								</fieldset>
								<fieldset class="form-group">
									<input type="number" class="form-control" id="phone" placeholder="Your Phone Number" required >
								</fieldset>
								<p>
									<button type="button" id="submit-btn-ok" class="btn btn-primary btn-action">Get Started<span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true"></span><svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>
									<button type="reset" id="tailor_reset" class="btn btn-outline-primary">Start Over<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
								</p>
							</form>
                            <button id="download-pdf" class="btn btn-outline-primary" hidden>Download Quotation</button>
						</div>
						<div class="col-lg-5 col-12 mt-lg-0 mt-5">
							<section class="ai-price sm-for-modal">
								<header class="ai-price-h">
									<h4 class="ai-price-h-title" id="dynamic_package_name">Title Name Here</h4>
									<p class="ai-price-h-subtitle">One Time Payment</p>
									<p class="ai-price-h-prc">
										<span class="ai-price-h-prc-pfx">Rs.</span>
										<span class="ai-price-h-prc-amt" id=dynamic_package_amount>Rupees Here</span>
										<span class="ai-price-h-prc-sfx">per brand</span>
									</p>
								</header>
								<div class="ai-price-c">
									<p class="ai-price-c-lead">What you'll get -</p>
									<ul class="ai-price-c-list" id="dynamic_package_features">
										<li id="all_package_details">All list</li>
									</ul>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- Dyanmic Modal Ends--}}
	<div class="modal fade" id="pkg-compare-brand" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<h5 class="modal-title lh-1">Compare Branding Packages</h5>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th scope="col">What you get -</th>
								<th scope="col">Basic</th>
								<th scope="col">Standard</th>
								<th scope="col">Premium</th>
								<th scope="col">Professional</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>Style Guidelines</th>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Logo</th>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Typography</th>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Social Media Assets</th>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Email Signature</th>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Branding Templates</th>
								<td>&mdash;</td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Signage</th>
								<td>&mdash;</td>
								<td>&mdash;</td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Merchandising</th>
								<td>&mdash;</td>
								<td>&mdash;</td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
							<tr>
								<th>Icon Pack</th>
								<td>&mdash;</td>
								<td>&mdash;</td>
								<td>&mdash;</td>
								<td><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
							</tr>
                            <tr>
                                <th colspan="5" class="bg-dark pt-4"><h6 class="text-white">Stationery Package</h6></th>
                            </tr>
                            <tr>
                                <th>Business Card</th>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                            </tr>
                            <tr>
                                <th>Letterhead</th>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                            </tr>
                            <tr>
                                <th>Cover Letter</th>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                            </tr>
                            <tr>
                                <th>Leaflets</th>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                            </tr>
                            <tr>
                                <th>Envelope</th>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                            </tr>
                            <tr>
                                <th>Brochure</th>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center">&mdash;</td>
                                <td class="text-center"><div class="img-holder"><img style="width: 24px;" src="frontend/images/check.svg" alt=""></div></td>
                            </tr>
                            <tr>
                                <th>Quoting Price in NPR (One Time Payment)</th>
                                <td class="text-center">
                                    <p>NPR 10,000</p>
                                    <p><a href="" class="btn btn-primary btn-sm">Choose</a></p>
                                </td>
                                <td class="text-center">
                                    <p>NPR 20,000</p>
                                    <p><a href="" class="btn btn-primary btn-sm">Choose</a></p>
                                </td>
                                <td class="text-center">
                                    <p>NPR 50,000</p>
                                    <p><a href="" class="btn btn-primary btn-sm">Choose</a></p>
                                </td>
                                <td class="text-center">
                                    <p>NPR 70,000</p>
                                    <p><a href="" class="btn btn-primary btn-sm">Choose</a></p>
                                </td>
                            </tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div id="footer-bm">
	<section class="section background-dark has-bg-img social-section">
		<div class="container">
			<div class="section-header" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
				<p class="seo-title">Get Connected</p>
				<h3 class="section-heading">How You Can Reach Us</h3>
				<p class="sub-title">You can get in touch with us directly in various ways</p>
			</div>
			<p class="social-btns mb-xl-7 mb-lg-7 mb-md-6 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
				<a href="https://www.facebook.com/Analogue-Inc-111451123769739/" class="btn btn-primary btn-lg facebook" target="_blank">Like Us on Facebook<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
				<a href="https://twitter.com/analogueinc" class="btn btn-primary btn-lg twitter" target="_blank">Follow Us on Twitter<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
				<a href="https://www.instagram.com/analogueincnepal/" class="btn btn-primary btn-lg instagram" target="_blank">Follow Us on Instagram<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
				<a href="https://www.linkedin.com/in/analogue-inc-1675941a2/" class="btn btn-primary btn-lg linkedin" target="_blank">Connect on LinkedIn<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
			</p>
			<div class="row pt-2">
				<div class="col-xl-6 col-lg-6 col-md-8 col-sm-7 col-12 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
					<h5 class="social-subtitle">We're Here</h5>
					<address class="social-address">
						<p>
							<span class="prefix">Location</span>
							<span class="suffix">Pinglasthan, Gausala, Kathmandu</span>
						</p>
						<p>
							<span class="prefix">Phone</span>
							<span class="suffix">+977 9802320803</span>
						</p>
						<p>
							<span class="prefix">Email</span>
							<span class="suffix">info@analogueinc.com.np</span>
						</p>
					</address>
					<a href="https://demo.analogueinc.com.np/contact.php#contact-map" class="btn btn-outline-primary">Get Directions<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-4 col-sm-5 col-12" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
					<h5 class="social-subtitle">DM Us On</h5>
					<p class="social-direct">
						<a target="_blank" href="https://wa.me/9779802320803" class="btn btn-primary whatsapp">WhatsApp<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
						<a target="_blank" class="btn btn-primary viber">Viber<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
						<a target="_blank" href="https://m.me/111451123769739" class="btn btn-primary messenger">Messenger<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
					</p>
				</div>
			</div>
		</div>

		<div class="section-backgrounds">
			<img src="frontend/bm/images/arrow-lg.png" alt="" class="big-arrow">
			<img src="frontend/bm/images/arrow-sm.png" alt="" class="small-arrow">
		</div>
	</section>

	<footer id="footer">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-6 col-md-12 footer-legal">
					<div class="footer-logo"><img class="w-50" src="frontend/bm/images/logo-bm.png" alt=""></div>
					<p class="mb-2">&copy; Analogue Inc, a PGNSONS Company. All Rights Reserved.</p>
					<!-- <p><a href="">Privacy Policy</a> - <a href="">Terms & Conditions</a></p> -->
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
					<h6 class="footer-links-title">Navigate</h6>
					<ul class="footer-links">
						<li><a href="https://demo.analogueinc.com.np/index.php">Home</a></li>
						<li><a href="https://demo.analogueinc.com.np/services.php">Services</a></li>
						<li><a href="https://demo.analogueinc.com.np/about.php">About</a></li>
						<li><a href="https://demo.analogueinc.com.np/blog.php">Blog</a></li>
						<li><a href="https://demo.analogueinc.com.np/career.php">Career</a></li>
						<li><a href="https://demo.analogueinc.com.np/contact.php">Contact</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
					<h6 class="footer-links-title">Services</h6>
					<ul class="footer-links">
						<li><a target="_blank" href="https://bm.analogueinc.com.np/">Branding & Marketing</a></li>
						<li><a target="_blank" href="https://it.analogueinc.com.np/">IT Solution</a></li>
						<li><a target="_blank" href="https://ecommerce.analogueinc.com.np/">E-Commerce</a></li>
						<li><a target="_blank" href="https://computers.analogueinc.com.np/">Computers & Hardware</a></li>
						<li><a target="_blank" href="https://surveillance.analogueinc.com.np/">Surveillance</a></li>
						<li><a target="_blank" href="https://press.analogueinc.com.np/">Press</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
					<h6 class="footer-links-title">Products</h6>
					<ul class="footer-links">
						<li><a target="_blank" href="http://edudata360.com/">Edudata360</a></li>
						<li><a target="_blank" href="https://hopperspass.com/">Hopper's Pass</a></li>
						<li><a target="_blank" href="https://ecommerce.analogueinc.com.np/">E-Commerce</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
</div>
    @include('frontend.quotation');

<script src="frontend/bm/js/swiper.min.js"></script>
<script>
	var swiper = new Swiper('.ai-s-slider', {
		speed: 1000,
		autoplay: {
			delay: 4000,
		},
		// loop: true,
		parallax: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
</script>

<script src="frontend/bm/js/aos.js"></script>
<script>
	AOS.init({
		mirror: true,
	});
</script>

<!-- Start of Async Drift Code -->
<script>
	window.addEventListener('load', function () {
		"use strict";
		!function() {
			var t = window.driftt = window.drift = window.driftt || [];
			if (!t.init) {
				if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
				t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
				t.factory = function(e) {
					return function() {
						var n = Array.prototype.slice.call(arguments);
						return n.unshift(e), t.push(n), t;
					};
				}, t.methods.forEach(function(e) {
					t[e] = t.factory(e);
				}), t.load = function(t) {
					var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
					o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
					var i = document.getElementsByTagName("script")[0];
					i.parentNode.insertBefore(o, i);
				};
			}
		}();
		drift.SNIPPET_VERSION = '0.3.1';
		drift.load('szk4eythda8s');
	});
</script>
<!-- End of Async Drift Code -->
    <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

		<script>
		$(document).ready(function () {
		    $('.btn-action').on('click',function() {
		    	var action = $(this).attr('data-action');
		    	var package_type = $(this).attr('data-type');
		    	$('#package_type').attr('value',package_type);
		    	var package_name = $(this).attr('data-id');
		    	$('#package_name').attr('value',package_name);

		    	if(action == 'basic-branding' && package_name == 'Branding and Marketing') {
					$("#dynamic_package_title").html('Basic Package');
					$("#dynamic_module_name").html('For Branding and Marketing');
					$("#dynamic_package_amount").html('10,000');
					$("#dynamic_package_name").html('Basic');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Style Guidelines</li>
							<li>Logo</li>
							<li>Typography</li>
							<li>Social Media Assets</li>
							<li>Email Signature</li>
							<li>Business Card</li>
							<li>Letterhead</li>
						</ul>
					`);
				}

				else if(action == 'standard-branding' && package_name == 'Branding and Marketing' ) {
			        $("#dynamic_package_title").html('Standard Package');
			        $("#dynamic_module_name").html('For Branding and Marketing');
					$("#dynamic_package_amount").html('20,000');
					$("#dynamic_package_name").html('Standard');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Style Guidelines</li>
							<li>Logo</li>
							<li>Typography</li>
							<li>Social Media Assets</li>
							<li>Email Signature</li>
							<li>Business Card</li>
							<li>Letterhead</li>
							<li>Branding Templates</li>
							<li>Cover Letter</li>
						</ul>
					`);
		    	}

		    	else if(action == 'premium-branding' && package_name == 'Branding and Marketing') {
			        $("#dynamic_package_title").html('Premium Package');
			        $("#dynamic_module_name").html('For Branding and Marketing');
					$("#dynamic_package_amount").html('50,000');
					$("#dynamic_package_name").html('Premium');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Style Guidelines</li>
							<li>Logo</li>
							<li>Typography</li>
							<li>Social Media Assets</li>
							<li>Email Signature</li>
							<li>Business Card</li>
							<li>Letterhead</li>
							<li>Branding Templates</li>
							<li>Cover Letter</li>
							<li>Signage</li>
							<li>Merchandising</li>
							<li>Leaflets</li>
							<li>Envelope</li>
						</ul>
					`);
		    	}

		    	else if(action == 'professional-branding' && package_name == 'Branding and Marketing') {
			        $("#dynamic_package_title").html('Professional Package');
			        $("#dynamic_module_name").html('For Branding and Marketing');
					$("#dynamic_package_amount").html('70,000');
					$("#dynamic_package_name").html('Professional');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Style Guidelines</li>
							<li>Logo</li>
							<li>Typography</li>
							<li>Social Media Assets</li>
							<li>Email Signature</li>
							<li>Business Card</li>
							<li>Letterhead</li>
							<li>Branding Templates</li>
							<li>Cover Letter</li>
							<li>Signage</li>
							<li>Merchandising</li>
							<li>Leaflets</li>
							<li>Envelope</li>
							<li>Icon Pack</li>
							<li>Brochure</li>
						</ul>
					`);
		    	}

		    	else if(action == 'basic-branding' && package_name == 'Social Media Marketing') {
			        $("#dynamic_package_title").html('Basic Package');
			        $("#dynamic_module_name").html('For Social Media Marketing');
					$("#dynamic_package_amount").html('5,000');
					$("#dynamic_package_name").html('Basic');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Marketing Guidelines</li>
							<li>1 post per week</li>
							<li>Caption Update</li>
						</ul>
					`);
		    	}

		    	else if(action == 'standard-branding' && package_name == 'Social Media Marketing') {
			        $("#dynamic_package_title").html('Standard Package');
			        $("#dynamic_module_name").html('For Social Media Marketing');
					$("#dynamic_package_amount").html('10,000');
					$("#dynamic_package_name").html('Standard');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Marketing Guidelines
							<li>Caption Update</li>
							<li>2 posts per week</li>
							<li>Strategic Campaign Running</li>
							<li>Post Distribution & Management</li>
							<li>Analysis & Re-Targeting</li>
						</ul>
					`);
		    	}

		    	else if(action == 'professional-branding' && package_name == 'Social Media Marketing') {
			        $("#dynamic_package_title").html('Professional Package');
			        $("#dynamic_module_name").html('For Social Media Marketing');
					$("#dynamic_package_amount").html('20,000');
					$("#dynamic_package_name").html('Professional');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Marketing Guidelines</li>
							<li>3 posts per week</li>
							<li>Caption Update</li>
							<li>Strategic Campaign Running</li>
							<li>Post Distribution & Management</li>
							<li>Analysis & Re-Targeting</li>
							<li>Audience Identification & Distribution</li>
							<li>Online Reputation Management</li>
						</ul>
					`);
		    	}

		    	else if(action == 'basic-branding' && package_name == 'Email Marketing') {
			        $("#dynamic_package_title").html('Basic Package');
			        $("#dynamic_module_name").html('For Email Marketing');
					$("#dynamic_package_amount").html('5,000');
					$("#dynamic_package_name").html('Basic');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Email Marketing Guidelines
							<li>2000 Contacts</li>
							<li>5 Email Templates</li>
							<li>1 Newsletter per week</li>
							<li>Custom Branding</li>
							<li>Marketing CRM</li>
						</ul>
					`);
		    	}

		    	else if(action == 'standard-branding' && package_name == 'Email Marketing') {
			        $("#dynamic_package_title").html('Standard Package');
			        $("#dynamic_module_name").html('For Email Marketing');
					$("#dynamic_package_amount").html('10,000');
					$("#dynamic_package_name").html('Standard');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Email Marketing Guidelines
							<li>3000 Contacts</li>
							<li>10 Email Templates</li>
							<li>2 Newsletters per week</li>
							<li>Custom Branding</li>
							<li>Marketing CRM</li>
						</ul>
					`);
		    	}

		    	else if(action == 'professional-branding' && package_name == 'Email Marketing') {
			        $("#dynamic_package_title").html('Professional Package');
			        $("#dynamic_module_name").html('For Email Marketing');
					$("#dynamic_package_amount").html('20,000');
					$("#dynamic_package_name").html('Professional');
					$("#dynamic_package_features").html(`
						<ul class='ai-price-c-list' id='dynamic_package_features'>
							<li>Email Marketing Guidelines
							<li>Unlimited Contacts</li>
							<li>Unlimited Templates</li>
							<li>3 Newsletter per week</li>
							<li>Custom Branding</li>
							<li>Marketing CRM</li>
							<li>Product or Service Update</li>
							<li>Re-targeting Audience</li>
							<li>Automation Series</li>
						</ul>
					`);
		    	}
		    });

		    function get_baseurl() {
		        var base_url = "<?php echo URL::to('/') ?>";
		        return base_url;
		    }

		    $('#spinner').hide();

		    $('#submit-btn-ok').on('click',function(){
				var full_name = $('body .modal #full_name').val();
				var email = $('body .modal #email').val();
				var phone = $('body .modal #phone').val();
				var package_name = $('body .modal #package_name').attr('value');
				var package_type = $('body .modal #package_type').attr('value');
				var package_price = document.getElementById('dynamic_package_amount').innerText;
				var package_details = document.getElementById('dynamic_package_features').innerText;
				
				// var test = $('#all_package_details').each(function(){
  		// 			var li = $(this).find('li')
  		// 			return li.text();
  		// 		}
				// var package_details = test;
				
		        var base_url = get_baseurl();
		        var data_from = 'BM';
		        // toastr.options.timeOut = 1000;
                if(full_name!==''&& email!=='' && phone!=='' && package_name!== '' && package_type!==''){
                    $.ajax({
                        url: base_url + '/api/userpackage/create',
                        type: 'POST',
                        dataType: 'JSON',
                        data:{
                            "_token" : "{{ csrf_token() }}",
                            "full_name": full_name + data_from,
                            "email": email,
                            "phone": phone,
                            "package_name": package_name,
                            "package_type": package_type,
                        },

                        beforeSend: function(){
                            $("#spinner").show();
                        },

                        complete: function(){
                            $("#spinner").hide();
                        },

                        success:function(data){
                            // alert('Sent Successfully');
                            var parent = document.getElementById('box');
                            var span = document.createElement('span');
                            span.style.fontSize = 'large';
                            span.style.borderRadius= 30;
                            span.style.color = '#000';
                            span.style.backgroundColor = '#27ae60'
                            span.innerHTML = 'Sent! Please check your email for more details.';
                            setTimeout(function () {
                                span.setAttribute('hidden', true);
                            }, 5000)
                            parent.parentNode.insertBefore(span,parent);
                            document.getElementById('submit-btn-ok').setAttribute('hidden', true);
                            document.getElementById('tailor_reset').setAttribute('hidden', true);
                            var downloadPdfBtn = document.getElementById('download-pdf');
                            downloadPdfBtn.removeAttribute('hidden');
                            downloadPdfBtn.addEventListener('click', function() {
                                var element = document.getElementById('invoice');
                                $('#invoice').css({'display': 'block'});
                                document.getElementById('quotationName').innerHTML = full_name;
                                document.getElementById('quotationEmail').innerHTML = email;
                                document.getElementById('quotationPhone').innerHTML = '+977 (' + phone +')';
                                // document.getElementById('subject').innerHTML = 'Subject: Price Quotation for ' + package_name + 'and' + package_type;
                                document.getElementById('package-detail').innerHTML = package_details;
                                document.getElementById('package-total').innerHTML = package_price;

                                var element = document.getElementById('invoice');
                                html2pdf().from(element).save();
                    			toastr.success('Sent Successfully');
                            });
                        }

                    });
                }
		    });
		});

		</script>


</body>
</html>
