@extends('layouts.frontend')

@section('content')

    <section class="section background-light has-bg-img is-page-title">
        <div class="container">
            <div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false" data-aos-mirror="false">
                <h1 class="section-heading">Our Core Services</h1>
                <p class="sub-title">We aim for the stars and reach them</p>
            </div>
        </div>
        <div class="section-backgrounds">
            <img src="http://analogueinc.com.np/frontend/images/arrow-lg.png" alt="" class="big-arrow">
            <img src="http://analogueinc.com.np/frontend/images/arrow-sm.png" alt="" class="small-arrow">
        </div>
        <div id="bokeh-bg"></div>
        <div id="particle-bg"></div>
    </section>

    <main role="main">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xxl-8 col-xl-9 col-lg-10 col-12 mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
                    <p class="lead text-dark">Our primary goal is to provide solutions that execute beautifully and live up to the customers' expectations. We are ready to provide every possible services that you may require to grow your business.</p>
                    <p>From a simple graphics to the marketing campaign which can skyrocket businesses, we are always committed to provide top-notch services for our clients. Quality has always been our top most priority over quantity. That is why our team always work hard to bring best services for you.</p>
                </div>
            </div>

            <div class="row no-gutters service-list" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <div class="img-holder"><img src="http://analogueinc.com.np/frontend/images/services_branding.png" alt=""></div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="service-list-content">
                        <h3 class="service-list-heading">Branding & Marketing</h3>
                        <p class="service-list-text">Whether you're a startup or an established name, you can only create impact with a strong virtual presence. From developing a corporate or e-commerce site to creating enterprise and mobile application, Analogue | Branding &amp; Marketing Solution can provide customized solutions that are as unique as your business. Specializing.</p>
                        <p>
                            <a target="_blank" href="https://bm.analogueinc.com.np/" class="btn btn-primary">Get Started<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row no-gutters service-list" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <div class="img-holder"><img src="http://analogueinc.com.np/frontend/images/services_itsolutions.png" alt=""></div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="service-list-content">
                        <h3 class="service-list-heading">IT Solution</h3>
                        <p class="service-list-text">Make yourself future-proof. Maximum value to our customers in terms of speedy development, excellent quality and on time delivery</p>
                        <p>
                            <a target="_blank" href="https://it.analogueinc.com.np/" class="btn btn-primary">Get Started<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row no-gutters service-list" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <div class="img-holder"><img src="http://analogueinc.com.np/frontend/images/services_ecommerce.png" alt=""></div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="service-list-content">
                        <h3 class="service-list-heading">E-Commerce</h3>
                        <p class="service-list-text">Our all new service allows up and coming ecommerce websites to start their online business hassle-free. The process is simplified, from setup up to launch, without any programming knowledge!</p>
                        <p>
                            <a target="_blank" href="https://ecommerce.analogueinc.com.np/" class="btn btn-primary">Get Started<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row no-gutters service-list" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <div class="img-holder"><img src="http://analogueinc.com.np/frontend/images/services_computers.png" alt=""></div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="service-list-content">
                        <h3 class="service-list-heading">Computers & Hardware</h3>
                        <p class="service-list-text">Challenging to imagine life looking at the rate in which newer technology is being introduced in the market every other day right? Get quality office hardware ranging from desktop, mouse, keyboard, speakers, headsets. network attached storage, firewall, anti-virus and other software licenses and accessories.</p>
                        <p>
                            <a target="_blank" href="https://computers.analogueinc.com.np/" class="btn btn-primary">Get Started<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row no-gutters service-list" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <div class="img-holder"><img src="http://analogueinc.com.np/frontend/images/services_surveillance.png" alt=""></div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="service-list-content">
                        <h3 class="service-list-heading">Security & Surveillance</h3>
                        <p class="service-list-text">Analogue | Surveillance is committed to providing state of the art solutions in network security and connectivity, cloud based Intelligent surveillance systems and much more. We offer wide range of video surveillance system, CCTV Camera, DVR, NVR, IP Surveillance, biometrics and RFID access and make sure nothing interrupts your sound sleep.</p>
                        <p>
                            <a target="_blank" href="https://surveillance.analogueinc.com.np/" class="btn btn-primary">Get Started<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row no-gutters service-list" data-aos="fade-left" data-aos-offset="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <div class="img-holder"><img src="http://analogueinc.com.np/frontend/images/services_press.png" alt=""></div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="service-list-content">
                        <h3 class="service-list-heading">Analogue Press</h3>
                        <p class="service-list-text">We offer a professional printing for any application you desire. We print all kinds of post card, wedding card, posters, visiting cards, brochure, leaflet, flyer and banners of different sizes. We have the best equipment for printing best quality prints. With the use of digitalized feature, we are able to reduce the cost of mass production of prints.</p>
                        <p>
                            <a target="_blank" href="https://demo.analogueinc.com.np/press" class="btn btn-primary">Get Started<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
