<!DOCTYPE html>
<html lang="en">
<head>
	<title>Analogue Inc - Branding Package</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="/frontend/bm/images/favicon.png" rel="icon">
	<link rel="stylesheet" href="/frontend/css/main.css">
	<link rel="stylesheet" href="/frontend/bm/css/swiper.css">
	<script src="/frontend/js/jquery-3.4.1.min.js"></script>
	<script src="/frontend/js/bootstrap.bundle.min.js"></script>
	<script>
		$(document).ready(function(){
			// BS4.4 sticky scrollspy nav trigger
			$('body').scrollspy({
				target: '#sticky-header',
				offset: 75
			});
			$('.scrollto').click(function(event) {
				var $anchor = $(this);
				$('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top - 70 }, 750);
				event.preventDefault();
			});

			// BS4.4 off-canvas nav trigger
			$("[data-trigger]").on("click", function(e){
				e.preventDefault();
				e.stopPropagation();
				var offcanvas_id = $(this).attr('data-trigger');
				$(offcanvas_id).toggleClass("show");
				$('body').toggleClass("offcanvas-active");
				$(".screen-overlay").toggleClass("show");
			});
			$(".btn-close, .screen-overlay, .scrollto").click(function(e){
				$(".screen-overlay").removeClass("show");
				$(".mobile-offcanvas").removeClass("show");
				$("body").removeClass("offcanvas-active");
			});
		});
	</script>

	<link rel="stylesheet" href="/frontend/css/main-inner.css">
</head>
<body>
	<div id="wrapper">
	<b class="screen-overlay"></b>
		<div id="sticky-header-holder" class="fixed-top">
			<div class="services-top">
				<div class="links">
					<a class="active" href="index.php">Branding & Marketing</a>
					<a href="https://it.analogueinc.com.np/">IT Solutions</a>
					<a href="https://ecommerce.analogueinc.com.np/">E-Commerce</a>
					<a href="https://computers.analogueinc.com.np/">Computers</a>
					<a href="https://surveillance.analogueinc.com.np/">Security & Surveillance</a>
					<a href="https://press.analogueinc.com.np/">Analogue Press</a>
					<a href="https://demo.analogueinc.com.np/">View all Services</a>
				</div>
				<div class="dropdown">
					<button class="btn btn-dark btn-sm dropdown-toggle m-1" type="button" id="servicedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="btn-icon ml-0 mr-2" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg>Navigate to Services</button>
					<div class="dropdown-menu" aria-labelledby="servicedropdown">
						<a class="dropdown-item" href="index.php">Branding & Marketing</a>
						<a class="dropdown-item" href="https://it.analogueinc.com.np/">IT Solutions</a>
						<a class="dropdown-item" href="https://ecommerce.analogueinc.com.np/">E-Commerce</a>
						<a class="dropdown-item" href="https://computers.analogueinc.com.np/">Computers</a>
						<a class="dropdown-item" href="https://surveillance.analogueinc.com.np/">Security & Surveillance</a>
						<a class="dropdown-item" href="https://press.analogueinc.com.np/">Analogue Press</a>
						<a class="dropdown-item" href="https://demo.analogueinc.com.np/">View all Services</a>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="logo"><a href="/"><img src="/frontend/bm/images/logo-bm.png" alt=""></a></div>

				<button class="btn btn-outline-secondary btn-sm ml-auto d-lg-none" type="button" data-toggle="collapse" data-trigger="#sticky-header">Menu</button>

				<nav id="sticky-header" class="mobile-offcanvas navbar navbar-expand-lg navbar-light bg-white">
					<div class="offcanvas-header">
						<button class="btn btn-link btn-close float-left mr-2"><svg class="btn-icon ml-0" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></button>
						<h6>Navigate</h6>
					</div>
					<ul class="navbar-nav">
						<li class="nav-item"><a href="/" class="nav-link">Home</a></li>
						<li class="nav-item"><a href="#pricing" class="scrollto nav-link">Pricing</a></li>
						<li class="nav-item"><a href="http://test.analogueinc.com.np/blogs" class="nav-link" target="_blank">Blog</a></li>
						<li class="nav-item"><a href="#contact" class="scrollto nav-link">Contact</a></li>
					</ul>
				</nav>
			</div>
		</div>

		<section id="pricing" class="section ai-s-section pb-0">
			<div class="subsection-ftr grad-divider">
				<div class="container-xl">
					<h5 class="subsection-ftr-title text-center">Make your own Branding package!</h5>
					<div class="row justify-content-center text-center">
						<div class="col-xl-7 col-lg-9 col-12">
							<p class="subsection-ftr-lead">You can choose what you need and what you don't need from what we offer, and create your own package based on your choices. The final price will be calculated accordingly.</p>
						</div>
					</div>

					<form>
						<div class="row justify-content-center my-5 ai-s-ckbx-blk">
							<div class="col-xxl-9 col-md-8 col-sm-6 col-12">
								<div class="ai-s-ckbx-blk-cnt">
									<div class="row">
										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-styleguideline">
													<span class="ai-s-ckbx-title">Style Guideline</span>
													<input type="checkbox" checked="checked" disabled="disabled" class="ai-s-ckbx-input" id="branding-styleguideline">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-selected">This is mandatory</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-typography">
													<span class="ai-s-ckbx-title">Typography</span>
													<input type="checkbox" checked="checked" disabled="disabled" class="ai-s-ckbx-input" id="branding-typography">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-selected">This is mandatory</span>
												</label>
											</div>
										</div>

										<div class="w-100"></div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-logo">
													<span class="ai-s-ckbx-title">Logo</span>
													<input type="checkbox" data-id="1" value="2500"class="ai-s-ckbx-input" status="0" id="branding-logo">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-deselected">I don't need a logo.</span>
													<span class="ai-s-ckbx-cond-msg message-selected">I'd like to upgrade my logo.</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-socialassets">
													<span class="ai-s-ckbx-title">Social Media Assets</span>
													<input type="checkbox" data-id="1" value="1000" class="ai-s-ckbx-input" id="branding-socialassets">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-deselected">What are Social Media Assets? <a class="ai-s-ckbx-cond-msg-link" data-toggle="'mod'al" data-target="#socialmediaassets"><svg viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a></span>
													<span class="ai-s-ckbx-cond-msg message-selected">I need Social Media Assets.</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-brandtemplates">
													<span class="ai-s-ckbx-title">Branding Templates</span>
													<input type="checkbox" data-id="1" value="8000" class="ai-s-ckbx-input" id="branding-brandtemplates">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-deselected">I already have a branding template.</span>
													<span class="ai-s-ckbx-cond-msg message-selected">Yes, I need my own branding template.</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-emailsign">
													<span class="ai-s-ckbx-title">Email Signature</span>
													<input type="checkbox" data-id="1" value="1000" class="ai-s-ckbx-input" id="branding-emailsign">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-deselected">I already have an email signature.</span>
													<span class="ai-s-ckbx-cond-msg message-selected">Sure, let's get this one too.</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-signage">
													<span class="ai-s-ckbx-title">Signage</span>
													<input type="checkbox" data-id="1" value="5000"  class="ai-s-ckbx-input" id="branding-signage">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-deselected">I have my own Signage.</span>
													<span class="ai-s-ckbx-cond-msg message-selected">Yes, I'd like a Signage.</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-merchandising">
													<span class="ai-s-ckbx-title">Merchandising</span>
													<input type="checkbox" data-id="1" value="15000" class="ai-s-ckbx-input" id="branding-merchandising">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-deselected">It's not necessary.</span>
													<span class="ai-s-ckbx-cond-msg message-selected">Help my brand speak for itself.</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-iconpack">
													<span class="ai-s-ckbx-title">Icon Pack</span>
													<input type="checkbox" data-id="1" value="13000" class="ai-s-ckbx-input" id="branding-iconpack">
													<span class="ai-s-ckbx-switch"></span>
													<span class="ai-s-ckbx-cond-msg message-deselected">I don't need this.</span>
													<span class="ai-s-ckbx-cond-msg message-selected">Yes, I need an Icon Pack.</span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-bizcard">
													<span class="ai-s-ckbx-title">Business Card</span>
													<input type="checkbox" data-id="1" value="1000"  class="ai-s-ckbx-input" id="branding-bizcard">
													<span class="ai-s-ckbx-switch"></span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-letterhead">
													<span class="ai-s-ckbx-title">Letterhead</span>
													<input type="checkbox" data-id="1" value="1000" class="ai-s-ckbx-input" id="branding-letterhead">
													<span class="ai-s-ckbx-switch"></span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-coverletter">
													<span class="ai-s-ckbx-title">Cover Letter</span>
													<input type="checkbox" data-id="1" value="2000" class="ai-s-ckbx-input" id="branding-coverletter">
													<span class="ai-s-ckbx-switch"></span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-leaflet">
													<span class="ai-s-ckbx-title">Leaflet</span>
													<input type="checkbox" data-id="1" value="5000"  class="ai-s-ckbx-input" id="branding-leaflet">
													<span class="ai-s-ckbx-switch"></span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-envelope">
													<span class="ai-s-ckbx-title">Envelope</span>
													<input type="checkbox" data-id="1" value="5000" class="ai-s-ckbx-input" id="branding-envelope">
													<span class="ai-s-ckbx-switch"></span>
												</label>
											</div>
										</div>

										<div class="col-xxl-4 col-md-6 col-12">
											<div class="ai-s-ckbx-holder">
												<label class="ai-s-ckbx-label" for="branding-brochure">
													<span class="ai-s-ckbx-title">Brochure</span>
													<input type="checkbox" data-id="1" value="7000" class="ai-s-ckbx-input" id="branding-brochure">
													<span class="ai-s-ckbx-switch"></span>
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group mt-4">
									<label for="branding-textarea" class="text-dark">Did we miss anything? Are you looking for something more? <small>(optional)</small></label>
									<textarea class="form-control" name="" id="branding-textarea" rows="8" placeholder="What more would you like?"></textarea>
								</div>
								<p class="mt-4">
									<button type="button" class="btn btn-primary" id="tailor_create" data-toggle="modal" data-target="#pkg-modal-brand5">Create My Package!</button>
									<button type="reset" class="btn btn-outline-primary ml-3">Start Over</button>
								</p>
							</div>

							<div class="col-xxl-3 col-md-4 col-sm-6 col-12 mt-sm-0 mt-6">
								<section class="ai-price">
									<header class="ai-price-h">
										<h4 class="ai-price-h-title">Tailor Made</h4>
										<p class="ai-price-h-subtitle">One Time Payment</p>
										<p class="ai-price-h-prc">
											<span class="ai-price-h-prc-pfx">Rs.</span>
											<span id="total_amount1" value="3500" class="ai-price-h-prc-amt">3,500</span>
											<span class="ai-price-h-prc-sfx">per brand</span>
										</p>
									</header>
									<div class="ai-price-c">
										<p class="ai-price-c-lead">What you've selected -</p>
										<ul class="ai-price-c-list" id="all_lists">
											<li>Style Guidelines</li>
											<li>Typography</li>
											<li class="test" style="display:none" id="tailor-branding-logo">Logo</li>
											<li class="test" style="display:none" id="tailor-branding-socialassets">Social Media Assets</li>
											<li class="test" style="display:none" id="tailor-branding-brandtemplates">Branding Templates</li>
											<li class="test" style="display:none" id="tailor-branding-emailsign">Email Signature</li>
											<li class="test" style="display:none" id="tailor-branding-signage">Signage</li>
											<li class="test" style="display:none" id="tailor-branding-merchandising">Merchandising</li>
											<li class="test" style="display:none" id="tailor-branding-iconpack">Icon Pack</li>
											<li class="test" style="display:none" id="tailor-branding-bizcard">Business Card</li>
											<li class="test" style="display:none" id="tailor-branding-letterhead">Letterhead</li>
											<li class="test" style="display:none" id="tailor-branding-coverletter">Cover Letter</li>
											<li class="test" style="display:none" id="tailor-branding-leaflet">Leaflet</li>
											<li class="test" style="display:none" id="tailor-branding-envelope">Envelope</li>
											<li class="test" style="display:none" id="tailor-branding-brochure">Brochure</li>
										</ul>
									</div>
								</section>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>

		<section id="contact" class="section ai-s-section background-light">
			<div class="container">
				<div class="section-header text-center pb-half">
					<p class="seo-title">Contact Us</p>
					<h3 class="section-heading">Get In Touch</h3>
					<p class="sub-title">Have any questions with Branding & Marketing?</p>
				</div>
				<form class="contact-form">
					<div class="form-row">
						<div class="col-lg-4 col-12">
							<fieldset class="form-group contact-full-name">
								<div class="input-group">
									<div class="input-group-prepend">
										<select class="custom-select" id="salutation">
											<option>Mr.</option>
											<option>Ms.</option>
											<option>Mrs.</option>
											<option>Other</option>
										</select>
									</div>
									<input type="text" class="form-control" id="name" placeholder="Your Full Name">
								</div>
							</fieldset>
						</div>
						<div class="col-lg-4 col-12">
							<fieldset class="form-group">
								<input type="email" class="form-control" id="email" placeholder="Valid Email Address">
							</fieldset>
						</div>
						<div class="col-lg-4 col-12">
							<fieldset class="form-group">
								<input type="text" class="form-control" id="name" placeholder="Your Phone Number">
							</fieldset>
						</div>
					</div>
					<fieldset class="form-group mb-xl-6 mb-lg-6 mb-md-5 mb-sm-4 mb-4">
						<textarea class="form-control" id="comment" rows="18" placeholder="What's on your mind?"></textarea>
					</fieldset>
					<p>
						<button type="submit" class="btn btn-primary">Send Your Message<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>
						<button type="reset" class="btn btn-outline-primary">Start Over<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
					</p>
				</form>
			</div>
		</section>
	</div>

<section class="section background-dark has-bg-img social-section">
	<div class="container">
		<div class="section-header" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
			<p class="seo-title">Get Connected</p>
			<h3 class="section-heading">How You Can Reach Us</h3>
			<p class="sub-title">You can get in touch with us directly in various ways</p>
		</div>
		<p class="social-btns mb-xl-7 mb-lg-7 mb-md-6 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="0" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
			<a href="https://www.facebook.com/Analogue-Inc-111451123769739/" class="btn btn-primary btn-lg facebook" target="_blank">Like Us on Facebook<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
			<a href="https://twitter.com/analogueinc" class="btn btn-primary btn-lg twitter" target="_blank">Follow Us on Twitter<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
			<a href="https://www.instagram.com/analogueincnepal/" class="btn btn-primary btn-lg instagram" target="_blank">Follow Us on Instagram<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
			<a href="https://www.linkedin.com/in/analogue-inc-1675941a2/" class="btn btn-primary btn-lg linkedin" target="_blank">Connect on LinkedIn<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
		</p>
		<div class="row pt-2">
			<div class="col-xl-6 col-lg-6 col-md-8 col-sm-7 col-12 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
				<h5 class="social-subtitle">We're Here</h5>
				<address class="social-address">
					<p>
						<span class="prefix">Location</span>
						<span class="suffix">Pinglasthan, Gausala, Kathmandu</span>
					</p>
					<p>
						<span class="prefix">Phone</span>
						<span class="suffix">+977 9802320803</span>
					</p>
					<p>
						<span class="prefix">Email</span>
						<span class="suffix">info@analogueinc.com.np</span>
					</p>
				</address>
				<a href="contact.php#contact-map" class="btn btn-outline-primary">Get Directions<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-4 col-sm-5 col-12" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" data-aos-easing="ease-out-quart" data-aos-mirror="false">
				<h5 class="social-subtitle">DM Us On</h5>
				<p class="social-direct">
					<a target="_blank" href="https://wa.me/9779802320803" class="btn btn-primary whatsapp">WhatsApp<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
					<a target="_blank" class="btn btn-primary viber">Viber<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
					<a target="_blank" href="https://m.me/111451123769739" class="btn btn-primary messenger">Messenger<svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></a>
				</p>
			</div>
		</div>
	</div>

	<div class="section-backgrounds">
		<img src="/frontend/bm/images/arrow-lg.png" alt="" class="big-arrow">
		<img src="/frontend/bm/images/arrow-sm.png" alt="" class="small-arrow">
	</div>
</section>

<footer id="footer">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-6 col-md-12 footer-legal">
				<div class="footer-logo"><img class="w-50" src="/frontend/images/logo.png" alt=""></div>
				<p class="mb-2">&copy; Analogue Inc, a PGNSONS Company. All Rights Reserved.</p>
				<!-- <p><a href="">Privacy Policy</a> - <a href="">Terms & Conditions</a></p> -->
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
				<h6 class="footer-links-title">Navigate</h6>
				<ul class="footer-links">
					<li><a href="index.php">Home</a></li>
					<li><a href="services.php">Services</a></li>
					<li><a href="about.php">About</a></li>
					<li><a href="blogs/">Blog</a></li>
					<li><a href="career/">Career</a></li>
					<li><a href="contact.php">Contact</a></li>
				</ul>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
				<h6 class="footer-links-title">Services</h6>
				<ul class="footer-links">
					<li><a target="_blank" href="https://bm.analogueinc.com.np/">Branding & Marketing</a></li>
					<li><a target="_blank" href="https://it.analogueinc.com.np/">IT Solution</a></li>
					<li><a target="_blank" href="https://ecommerce.analogueinc.com.np/">E-Commerce</a></li>
					<li><a target="_blank" href="https://computers.analogueinc.com.np/">Computers & Hardware</a></li>
					<li><a target="_blank" href="https://surveillance.analogueinc.com.np/">Surveillance</a></li>
					<li><a target="_blank" href="https://press.analogueinc.com.np/">Press</a></li>
				</ul>
			</div>
			<div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-5">
				<h6 class="footer-links-title">Products</h6>
				<ul class="footer-links">
					<li><a target="_blank" href="http://edudata360.com/">Edudata360</a></li>
					<li><a target="_blank" href="https://hopperspass.com/">Hopper's Pass</a></li>
					<li><a target="_blank" href="https://ecommerce.analogueinc.com.np/">E-Commerce</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

		<div class="modal fade" id="merchandising" tabindex="-1" role="dialog" aria-labelledby="socialmediaassetstitle" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="socialmediaassetstitle">What is merchandising?</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>This is an example of how we will display additional info button. Since this is an example only, only the skeleton of the design has been worked on.</p>
						<p>Below is an image which will act like a preview for Social Media Assets -</p>
						<div class="img-holder"><img src="https://via.placeholder.com/960x640" alt=""></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="socialmediaassets" tabindex="-1" role="dialog" aria-labelledby="socialmediaassetstitle" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="socialmediaassetstitle">What are Social Media Assets?</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Social Media Assets cover things like profile photos, cover photos, post templates, etc for your social media pages.</p>
						<p>Below is an image which will act like a preview for Social Media Assets -</p>
						<div class="row">
							<div class="col-md-6 col-12"><div class="img-holder"><img src="/frontend/bm/images/img7.png" alt=""></div></div>
							<div class="col-md-6 col-12 mt-md-0 mt-4"><div class="img-holder"><img src="/frontend/bm/images/img8.png" alt=""></div></div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="pkg-modal-brand5" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div>
							<h5 class="modal-title lh-1">Tailor Made Package</h5>
							<p class="text-primary lh-1">For Branding & Marketing</p>
						</div>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row" id="box">
							<div class="col-lg-7 col-12">
								<form id="tailor_form">
									<fieldset class="form-group contact-full-name">
										<div class="input-group">
											<div class="input-group-prepend">
												<select class="custom-select" id="">
													<option>Mr.</option>
													<option>Ms.</option>
													<option>Mrs.</option>
													<option>Other</option>
												</select>
											</div>
											<input type="text" class="form-control" id="full_name" placeholder="Your Full Name" required="required">
										</div>
									</fieldset>
									<fieldset class="form-group">
										<input type="email" class="form-control" id="email" placeholder="Your Email Address" required="required">
									</fieldset>
									<fieldset class="form-group">
										<input type="text" class="form-control" id="phone" placeholder="Your Phone Number" required="required">
									</fieldset>
									<p>
										<button type="button" id="tailor_submit" class="btn btn-primary">Get Started<span class="spinner-border spinner-border-sm" role="status" id="spinner" aria-hidden="true"></span><svg class="btn-icon" viewBox="0 0 24 24"><path d="M21 14.3h-2V6.4L4.4 21 3 19.6 17.6 5H9.7V3H21V14.3z"/></svg></button>
										<button type="reset" id="tailor_reset" class="btn btn-outline-primary">Start Over<svg class="btn-icon" viewBox="0 0 24 24"><path d="M0 12l8 8 1.4-1.4 -5.6-5.6H24v-2H3.8l5.6-5.6L8 4 0 12z"/></svg></button>
									</p>
                                </form>
                                <button id="download-pdf" class="btn btn-primary" hidden>Download Quotation</button>
                            </div>
							<div class="col-xxl-3 col-md-4 col-sm-6 col-12 mt-sm-0 mt-6">
								<section class="ai-price">
									<header class="ai-price-h">
										<h4 class="ai-price-h-title">Tailor Made</h4>
										<p class="ai-price-h-subtitle">One Time Payment</p>
										<p class="ai-price-h-prc">
											<span class="ai-price-h-prc-pfx">Rs.</span>
											<span id="total_amount" class="ai-price-h-prc-amt">3,500</span>
											<span class="ai-price-h-prc-sfx">per brand</span>
										</p>
									</header>
									<div class="ai-price-c">
										<p class="ai-price-c-lead">What you'll get -</p>
										<ul class="ai-price-c-list" id="selected_package_details">
											<li>Style Guidelines</li>
											<li>Typography</li>
											<li style="display:none" id="tailor-branding-logo">Logo</li>
											<li style="display:none" id="tailor-branding-socialassets">Social Media Assets</li>
											<li style="display:none" id="tailor-branding-brandtemplates">Branding Templates</li>
											<li style="display:none" id="tailor-branding-emailsign">Email Signature</li>
											<li style="display:none" id="tailor-branding-signage">Signage</li>
											<li style="display:none" id="tailor-branding-merchandising">Merchandising</li>
											<li style="display:none" id="tailor-branding-iconpack">Icon Pack</li>
											<li style="display:none" id="tailor-branding-bizcard">Business Card</li>
											<li style="display:none" id="tailor-branding-letterhead">Letterhead</li>
											<li style="display:none" id="tailor-branding-coverletter">Cover Letter</li>
											<li style="display:none" id="tailor-branding-leaflet">Leaflet</li>
											<li style="display:none" id="tailor-branding-envelope">Envelope</li>
											<li style="display:none" id="tailor-branding-brochure">Brochure</li>
										</ul>
									</div>
								</section>
							</div>
						</div>
						<div class="mt-4 mb-0 alert alert-primary lh-1">
							<p>
								If you are still not satisfied with the price, please feel free to contact our sales expert. <br>
								<strong>Phone: +977 98011 70771</strong>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
        @include('frontend.quotation')


		<script src="/frontend/bm/js/aos.js"></script>

		<script>
			AOS.init({
				mirror: true,
			});
		</script>
    <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script>
			$(document).ready(function(){
				// For Branding Packages
				$('#branding-logo').click(function(){
					$('#tailor-branding-logo').toggle();
		    		if($('#branding-logo').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-socialassets').click(function(){
					$('#tailor-branding-socialassets').toggle();
					if($('#branding-socialassets').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-brandtemplates').click(function(){
					$('#tailor-branding-brandtemplates').toggle();
					if($('#branding-brandtemplates').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-emailsign').click(function(){
					$('#tailor-branding-emailsign').toggle();
					if($('#branding-emailsign').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-signage').click(function(){
					$('#tailor-branding-signage').toggle();
					if($('#branding-signage').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-merchandising').click(function(){
					$('#tailor-branding-merchandising').toggle();
					if($('#branding-merchandising').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-iconpack').click(function(){
					$('#tailor-branding-iconpack').toggle();
					if($('#branding-iconpack').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-bizcard').click(function(){
					$('#tailor-branding-bizcard').toggle();
					if($('#branding-bizcard').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-letterhead').click(function(){
					$('#tailor-branding-letterhead').toggle();
					if($('#branding-letterhead').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-coverletter').click(function(){
					$('#tailor-branding-coverletter').toggle();
					if($('#branding-coverletter').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-leaflet').click(function(){
					$('#tailor-branding-leaflet').toggle();
					if($('#branding-leaflet').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-envelope').click(function(){
					$('#tailor-branding-envelope').toggle();
					if($('#branding-envelope').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				$('#branding-brochure').click(function(){
					$('#tailor-branding-brochure').toggle();
					if($('#branding-brochure').is(':checked')){
		    			$(this).attr('status','1');
		    		}
		    		else {
		    			$(this).attr('status','0');
		    		}
				});

				// Dynamic total price of checkbox Starts
				$("input[type=checkbox]").change(function(){
					if($(this).attr('data-id')=="1"){
					    var total = 3500;
					    $("input[type=checkbox]:checked").each(function(){
					        if (isNaN($(this).val())) {
					            total = total;
					        }
					        else
					            total += parseFloat($(this).val());
					    });
					    $("#total_amount1").text(total);
					    $("#total_amount1").attr('value',total);
					}
				});
				// Dynamic total price of checkbox Ends

				$('#tailor_create').click(function(){
				var total_amount = $('#total_amount1').attr('value');
				$("#total_amount").html(total_amount);

				$("#selected_package_details").html(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Style Guidelines</li>
						<li>Typography</li>
					</ul>
				`);

				if($('#branding-logo').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Logo</li>
					</ul>
				`);
				}

				if($('#branding-socialassets').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Social Media Assets</li>
					</ul>
				`);
				}

				if($('#branding-brandtemplates').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Brand Templates</li>
					</ul>
				`);
				}

				if($('#branding-emailsign').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Email Sign</li>
					</ul>
				`);
				}

				if($('#branding-signage').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Signage</li>
					</ul>
				`);
				}

				if($('#branding-merchandising').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Merchandising</li>
					</ul>
				`);
				}

				if($('#branding-iconpack').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Iconpack</li>
					</ul>
				`);
				}

				if($('#branding-bizcard').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Bizcard</li>
					</ul>
				`);
				}

				if($('#branding-letterhead').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Letterhead</li>
					</ul>
				`);
				}

				if($('#branding-coverletter').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Cover Letter</li>
					</ul>
				`);
				}

				if($('#branding-leaflet').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Leaflet</li>
					</ul>
				`);
				}

				if($('#branding-envelope').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Envelope</li>
					</ul>
				`);
				}

				if($('#branding-brochure').attr('status')==1){
					$("#selected_package_details").append(`
					<ul class="ai-price-c-list" id="selected_package_details">
						<li>Brochure</li>
					</ul>
				`);
				}

			});

			function get_baseurl() {
		        var base_url = "<?php echo URL::to('/') ?>";
		        return base_url;
		    }
			$('#spinner').hide();
			$('#tailor_submit').on('click',function(){
			var full_name = $('body .modal #full_name').val();
			var email = $('body .modal #email').val();
			var phone = $('body .modal #phone').val();
			var package_name = "Branding and Marketing Package";
			// var package_details =  $('input[type="checkbox"]:checked').parent().text();
			var package_details =  $('#all_lists > li:not([style*="display:none"])').text();
			var package_total_price = $('#total_amount1').attr('value');
			var other_message = $('#branding-textarea').val();

	        var base_url = get_baseurl();
	        console.log(full_name, email, phone,package_details, package_total_price, other_message);

	        $.ajax({
	            url: base_url + '/api/userpackage/create',
	            type: 'POST',
	            dataType: 'JSON',
	            data:{
	                "_token" : "{{ csrf_token() }}",
	                "full_name": full_name,
	                "email": email,
	                "phone": phone,
	                "package_name": package_name,
	                "package_type": "Tailor Type",
	                "package_details": package_details,
	                "package_total_price": package_total_price,
	                "other_message": other_message,
	            },

	            beforeSend: function(){
	            	$("#spinner").show();
	            },

	            complete: function(){
	                $("#spinner").hide();
	            },

	            success:function(data){
                    var parent = document.getElementById('box');
                    var span = document.createElement('span');
                    span.style.fontSize = 'large';
                    span.style.borderRadius= 30;
                    span.style.color = '#000';
                    span.style.backgroundColor = '#27ae60'
                    span.innerHTML = 'Sent! Please check your email for more details.';
                    setTimeout(function () {
                        span.setAttribute('hidden', true);
                    }, 5000)
                    parent.parentNode.insertBefore(span,parent);
                    document.getElementById('tailor_submit').setAttribute('hidden', true);
                    document.getElementById('tailor_reset').setAttribute('hidden', true);
                    var downloadPdfBtn = document.getElementById('download-pdf');
                    downloadPdfBtn.removeAttribute('hidden');
                    downloadPdfBtn.addEventListener('click', function() {
                        var element = document.getElementById('invoice');
                        $('#invoice').css({'display': 'block'});
                        document.getElementById('quotationName').innerHTML = full_name;
                        document.getElementById('quotationEmail').innerHTML = email;
                        document.getElementById('quotationPhone').innerHTML = '+977 (' + phone +')';
                        // document.getElementById('subject').innerHTML = 'Subject: Price Quotation of Tailor Made Package';
                        document.getElementById('package-name').innerHTML = package_name;
                        document.getElementById('package-detail').innerHTML = package_details;
                        document.getElementById('package-total').innerHTML = package_total_price;

                        var element = document.getElementById('invoice');
                        html2pdf().from(element).save();
	                    toastr.success('Sent Successfully');
                    });
                    // window.setTimeout(function () {
                    //     window.location.href = base_url +'/bm';
                    // }, 9000 );
	            },
                error: function(jqXHR, exception){
                    var parent = document.getElementById('box');
                    var span = document.createElement('span');
                    span.style.fontSize = 'large';
                    span.style.borderRadius= 30;
                    span.style.color = '#000';
                    span.style.backgroundColor = '#d63031'
                    span.innerHTML = jqXHR.responseJSON.message;
                    setTimeout(function () {
                        span.setAttribute('hidden', true);
                    }, 5000)
                    parent.parentNode.insertBefore(span,parent);
                }
	        });
		});

		});
	</script>

	</body>
</html>
