@extends('layouts.frontend')

@section('content')
<link rel="stylesheet" href="frontend/css/main-inner.css">

<section class="section background-light has-bg-img is-page-title">
	<div class="container">
		<div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false">
			<h1 class="section-heading">Career Opportunities</h1>
			<p class="sub-title">Luck is what happens when preparation meets opportunity</p>
		</div>
	</div>
	<div class="section-backgrounds">
		<img src="frontend/images/arrow-lg.png" alt="" class="big-arrow">
		<img src="frontend/images/arrow-sm.png" alt="" class="small-arrow">
	</div>

	<div id="bokeh-bg"></div>
	<div id="particle-bg"></div>
</section>

<main role="main">
	<div class="container">
		<div class="row">
			<div class="col-xxl-8 col-xl-9 col-lg-10 col-12 mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
				<p class="lead text-dark">Analogue Inc is an IT firm which is a PGNSONS Company, designed for future excellence where we provide paramount services pertaining to the field. Analogue Inc is the only company that will be fully based on pure IT and computer applications.</p>
				<p>
					<a href="#career-listing" class="btn btn-primary btn-lg scroll-activator">Open Positions<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
					<a href="#intern-listing" class="btn btn-outline-primary btn-lg ml-4 scroll-activator">Become an intern<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
				</p>
			</div>
		</div>

		<section class="section no-padding">
			<div class="section-header text-center pb-half">
				<p class="seo-title">Working With Us</p>
				<h3 class="section-heading">Our Work Culture</h3>
				<p class="sub-title">A few reasons why your should have a career with us</p>
			</div>
		</section>
		<div class="row no-gutters">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-1 order-sm-2 order-2 bg-light d-flex align-items-center">
				<div class="px-xl-8 px-lg-6 px-md-4 px-sm-6 px-4 py-md-0 py-sm-5 py-5">
					<h3 class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-3 mb-3">Skilled & Supportive Team</h3>
					<p>We have a tight-knit team of highly skilled individuals that focuses on providing you with the best results. We look out for each other and support one another to be efficient and creative.</p>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-2 order-sm-1 order-1">
				<div class="img-holder"><img src="frontend/images/img10.jpg" alt=""></div>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-2 bg-primary d-flex align-items-center">
				<div class="px-xl-8 px-lg-6 px-md-4 px-sm-6 px-4 py-md-0 py-sm-5 py-5">
					<h3 class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-3 mb-3 text-white">Modern & Spacious Workplace</h3>
					<p class="text-light">Good work happens in a good office. Great work happens in an amazing workplace. Our office is well-lit with modern tools to help everyone achieve their goals.</p>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-1">
				<div class="img-holder"><img src="frontend/images/img11.jpg" alt=""></div>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-1 order-sm-2 order-2 bg-dark d-flex align-items-center">
				<div class="px-xl-8 px-lg-6 px-md-4 px-sm-6 px-4 py-md-0 py-sm-5 py-5">
					<h3 class="mb-xl-4 mb-lg-4 mb-md-3 mb-sm-3 mb-3 text-white">Occasional Company Trips</h3>
					<p class="text-secondary">All work and no play makes for a burnt out team, and that's not healthy for anybody. We realize that and go out occasionally just to chill and know each other better.</p>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-md-2 order-sm-1 order-1">
				<div class="img-holder"><img src="frontend/images/img12.jpg" alt=""></div>
			</div>
		</div>
	</div>
</main>

<section id="career-listing" class="section background-light">
	<div class="container">
		<div class="section-header pb-half">
			<p class="seo-title">Our Jobs</p>
			<h3 class="section-heading">Available jobs</h3>
			<p class="sub-title">Here is the list of available jobs you may be searching for.</p>
		</div>
		@foreach($jobs as $count => $job)
            @if($job->category == '2')

			<div class="ai-card ai-card-h has-hover" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart">
				<div class="ai-card-title-holder">
					<h5 class="ai-card-title">{{  $job->title }}</h5>
					<p class="ai-card-subtitle-primary">{{ $job->no_of_opening }} Positions <span class="ai-card-subtitle-gray">&mdash; Apply before 20th May 2020</span></p>
				</div>
				<a href="/career_details/{{ $job->title }}" class="btn btn-link">View details<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
			</div>

			@endif
        @endforeach

	</div>
</section>

<section id="intern-listing" class="section pb-2x">
	<div class="container">
		<div class="section-header pb-half">
			<p class="seo-title">Internship</p>
			<h3 class="section-heading">Become an Intern</h3>
			<p class="sub-title">Ready to be a part of something big?</p>
		</div>

        @foreach($jobs as $count => $job)
            @if($job->category == '3')

			<div class="ai-card ai-card-h has-hover" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart" data-aos-once="true">
				<div class="ai-card-title-holder">
					<h5 class="ai-card-title">{{  $job->title }}</h5>
					<p class="ai-card-subtitle-primary">{{ $job->no_of_opening }} Positions <span class="ai-card-subtitle-gray">&mdash; Apply before 20th May 2020</span></p>
				</div>
				<a href="/career_details/{{ $job->title }}" class="btn btn-link">View details<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
			</div>

            @endif
        @endforeach

	</div>
</section>

<script src="frontend/js/particles.min.js"></script>
<script src="frontend/js/particles.settings.js"></script>

@endsection

