@extends('layouts.frontend')

@section('content')

<section class="section background-light has-bg-img is-page-title">
	<div class="container">
		<div class="section-header" data-aos="fade-left" data-aos-offset="240" data-aos-duration="700" data-aos-easing="ease-out-quart" data-aos-mirror="false">
			<h1 class="section-heading">Our Blog</h1>
			<p class="sub-title">Explore and widen your knowledge reading Analogue's Blogs </p>
		</div>
	</div>
	<div class="section-backgrounds">
		<img src="/frontend/images/arrow-lg.png" alt="" class="big-arrow">
		<img src="/frontend/images/arrow-sm.png" alt="" class="small-arrow">
	</div>
</section>

<main role="main">
	<div class="container-lg mb-7">
		<div class="row">
			@foreach($posts as $post)
				@if($post->category == '1' && $post->publish == '1')
					<div class="col-xl-9 col-lg-8 col-md-8 col-sm-8 col-12 order-sm-1 order-2">
						<section class="section background-light rounded" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart" data-aos-mirror="false">
							<div class="container">
								<div class="row align-items-center justify-content-center">
									<div class="col-xl-5 col-lg-12">
										<div class="section-header mb-xl-0 mb-3">
											<h3 class="section-heading">Featured Post</h3>
										</div>
									</div>
									<div class="col-xl-6 col-lg-12">
										<div class="ai-card ai-card-news has-hover">
											<div class="ai-card-content">
												<div class="ai-card-title-holder">
													<h6 class="ai-card-title">{{ $post->title }}</h6>
													<p class="ai-card-subtitle">{!!  substr(strip_tags($post->description), 0, 100) !!}</p>
												</div>
												<div class="img-holder"><img src="{{ URL::to('/') }}/imageupload/blog/{{ $post->feature_image}}" alt=""></div>
											</div>
											<p class="ai-card-footer">
												<a href="/blog_details/{{ $post->title }}" class="btn btn-link btn-sm">View details<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
												<span>posted on {{ $post->created_at->format('jS F Y') }} by <a href="">Admin</a></span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				@endif
			@endforeach
				
			<div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-12 order-sm-2 order-1 mb-sm-0 mb-5">
			<h5>Blog Categories</h5>
				@foreach($postcategories as $category)
					@if($category->id != 2 && $category->id != 3)
						<div class="list-group list-group-flush">
							<button type="button" data-id="{{ $category->id }}" id="category" class="list-group-item btn-action">{{ $category->title }}</a>
							{{-- <a href="/blogs/{{ $category->title }}" class="list-group-item">{{ $category->title }}</a> --}}
						</div>
					@endif
				@endforeach
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row justify-content-center" id="table">
			@foreach($posts as $post)
				@if($post->category == 1 && $post->publish == '0')
					<div class="col-xl-6 col-lg-6 col-md-10 col-sm-12 col-12 mb-5">
						<div class="ai-card ai-card-news has-hover" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart" data-aos-mirror="false">
							<div class="ai-card-content">
								<div class="ai-card-title-holder">
									<h6 class="ai-card-title">{{ $post->title }}</h6>
									<p class="ai-card-subtitle">{!!  substr(strip_tags($post->description), 0, 100) !!}</p>
								</div>
								<div class="img-holder">
									@if($post->feature_image)
									<img src="{{ URL::to('/') }}/imageupload/blog/{{ $post->feature_image}}" alt="">
									@else
										<img src="{{ URL::to('/') }}/frontend/images/no-img.png" class="img-fluid" alt="Responsive image">
									@endif

								</div>
							</div>
							<p class="ai-card-footer">
								<a href="/blog_details/{{ $post->id }}" class="btn btn-link btn-sm">View details<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
								<span>posted on {{ $post->created_at->format('jS F Y') }} by <a href="">Admin</a></span>
							</p>
						</div>
					</div>
				@endif
			@endforeach
		</div>
		
		{{-- @if($post->category != '1' && $post->category != '4' && $post->category != '5' )
			<div class="mb-xl-7 mb-lg-7 mb-md-6 mb-sm-6 mb-6">
				<p class="lead">There is no any blogs posted yet. </p>
			</div>
		@endif --}}

		<!-- Pagination -->
		<nav class="mt-7">
			<ul class="pagination justify-content-center">
				<li class="page-item disabled">
					<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
				</li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item">
					<a class="page-link" href="#">Next</a>
				</li>
			</ul>
		</nav>
	</div>
</main>

<script>
    $('.btn-action').on('click',function(){
		var id = $(this).attr('data-id');
		console.log(id);
		$('#table').html(`
			@foreach($posts as $post)
				@if($post->category == '1' && $post->publish == '0')
					<div class="col-xl-6 col-lg-6 col-md-10 col-sm-12 col-12 mb-5">
						<div class="ai-card ai-card-news has-hover" data-aos="fade-up" data-aos-offset="100" data-aos-duration="750" data-aos-easing="ease-out-quart" data-aos-mirror="false">
							<div class="ai-card-content">
								<div class="ai-card-title-holder">
									<h6 class="ai-card-title">{{ $post->title }}</h6>
									<p class="ai-card-subtitle">{!!  substr(strip_tags($post->description), 0, 100) !!}</p>
								</div>
								<div class="img-holder"><img src="{{ URL::to('/') }}/imageupload/blog/{{ $post->feature_image}}" alt=""></div>
							</div>
							<p class="ai-card-footer">
								<a href="/blog_details/{{ $post->id }}" class="btn btn-link btn-sm">View details<svg class="btn-icon" viewBox="0 0 24 24"><path d="M24 12l-8-8 -1.4 1.4 5.6 5.6H0v2h20.2l-5.6 5.6L16 20 24 12z"/></svg></a>
								<span>posted on {{ $post->created_at->format('jS F Y') }} by <a href="">Admin</a></span>
							</p>
						</div>
					</div>
				@endif
			@endforeach
		`)

	});
</script>

<script src="/frontend/js/particles.min.js"></script>
<script src="/frontend/js/particles.settings.js"></script>

@endsection